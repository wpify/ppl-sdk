<?php
/**
 * This file aims to show you how to use this generated package.
 * In addition, the goal is to show which methods are available and the first needed parameter(s)
 * You have to use an associative array such as:
 * - the key must be a constant beginning with WSDL_ from AbstractSoapClientBase class (each generated ServiceType class extends this class)
 * - the value must be the corresponding key value (each option matches a {@link http://www.php.net/manual/en/soapclient.soapclient.php} option)
 * $options = array(
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://myapi.ppl.cz/MyApi.svc?singleWsdl',
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'you_secret_login',
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => 'you_secret_password',
 * );
 * etc...
 */
require_once __DIR__ . '/vendor/autoload.php';
/**
 * Minimal options
 */
$options = array(
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://myapi.ppl.cz/MyApi.svc?singleWsdl',
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => \PPLSDK\ClassMap::get(),
);
/**
 * Samples for Create ServiceType
 */
$create = new \PPLSDK\ServiceType\Create($options);
/**
 * Sample call for CreatePackages operation/method
 */
if ($create->CreatePackages(new \PPLSDK\StructType\CreatePackages()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for CreatePackageLabel operation/method
 */
if ($create->CreatePackageLabel(new \PPLSDK\StructType\CreatePackageLabel()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for CreateOrders operation/method
 */
if ($create->CreateOrders(new \PPLSDK\StructType\CreateOrders()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for CreatePickupOrders operation/method
 */
if ($create->CreatePickupOrders(new \PPLSDK\StructType\CreatePickupOrders()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Samples for Get ServiceType
 */
$get = new \PPLSDK\ServiceType\Get($options);
/**
 * Sample call for GetCplResult operation/method
 */
if ($get->GetCplResult(new \PPLSDK\StructType\GetCplResult()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetPackages operation/method
 */
if ($get->GetPackages(new \PPLSDK\StructType\GetPackages()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetCitiesRouting operation/method
 */
if ($get->GetCitiesRouting(new \PPLSDK\StructType\GetCitiesRouting()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetParcelShops operation/method
 */
if ($get->GetParcelShops(new \PPLSDK\StructType\GetParcelShops()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetCodCurrency operation/method
 */
if ($get->GetCodCurrency(new \PPLSDK\StructType\GetCodCurrency()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetSprintRoutes operation/method
 */
if ($get->GetSprintRoutes(new \PPLSDK\StructType\GetSprintRoutes()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetProductCountry operation/method
 */
if ($get->GetProductCountry(new \PPLSDK\StructType\GetProductCountry()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetPackProducts operation/method
 */
if ($get->GetPackProducts(new \PPLSDK\StructType\GetPackProducts()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetPackNumberRowTypes operation/method
 */
if ($get->GetPackNumberRowTypes(new \PPLSDK\StructType\GetPackNumberRowTypes()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetNumberRange operation/method
 */
if ($get->GetNumberRange(new \PPLSDK\StructType\GetNumberRange()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetFreeRangeInfo operation/method
 */
if ($get->GetFreeRangeInfo(new \PPLSDK\StructType\GetFreeRangeInfo()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Samples for Login ServiceType
 */
$login = new \PPLSDK\ServiceType\Login($options);
/**
 * Sample call for Login operation/method
 */
if ($login->Login(new \PPLSDK\StructType\Login()) !== false) {
    print_r($login->getResult());
} else {
    print_r($login->getLastError());
}
/**
 * Samples for Version ServiceType
 */
$version = new \PPLSDK\ServiceType\Version($options);
/**
 * Sample call for Version operation/method
 */
if ($version->Version(new \PPLSDK\StructType\Version()) !== false) {
    print_r($version->getResult());
} else {
    print_r($version->getLastError());
}
/**
 * Samples for Partner ServiceType
 */
$partner = new \PPLSDK\ServiceType\Partner($options);
/**
 * Sample call for PartnerSaveStatuses operation/method
 */
if ($partner->PartnerSaveStatuses(new \PPLSDK\StructType\PartnerSaveStatuses()) !== false) {
    print_r($partner->getResult());
} else {
    print_r($partner->getLastError());
}
/**
 * Samples for Save ServiceType
 */
$save = new \PPLSDK\ServiceType\Save($options);
/**
 * Sample call for SavePartners operation/method
 */
if ($save->SavePartners(new \PPLSDK\StructType\SavePartners()) !== false) {
    print_r($save->getResult());
} else {
    print_r($save->getLastError());
}
/**
 * Sample call for SaveProductTransports operation/method
 */
if ($save->SaveProductTransports(new \PPLSDK\StructType\SaveProductTransports()) !== false) {
    print_r($save->getResult());
} else {
    print_r($save->getLastError());
}
/**
 * Samples for Update ServiceType
 */
$update = new \PPLSDK\ServiceType\Update($options);
/**
 * Sample call for UpdatePackage operation/method
 */
if ($update->UpdatePackage(new \PPLSDK\StructType\UpdatePackage()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Samples for Is ServiceType
 */
$is = new \PPLSDK\ServiceType\Is($options);
/**
 * Sample call for IsHealtly operation/method
 */
if ($is->IsHealtly(new \PPLSDK\StructType\IsHealtly()) !== false) {
    print_r($is->getResult());
} else {
    print_r($is->getLastError());
}
