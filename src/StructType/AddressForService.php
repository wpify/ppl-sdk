<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AddressForService StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:AddressForService
 * @subpackage Structs
 */
class AddressForService extends AbstractStructBase
{
    /**
     * The ServiceAddressType
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $ServiceAddressType;
    /**
     * The BackPackNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $BackPackNumber;
    /**
     * The Recipient
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\StructType\ServiceAddressRecipient
     */
    public $Recipient;
    /**
     * The Flags
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiFlag
     */
    public $Flags;
    /**
     * Constructor method for AddressForService
     * @uses AddressForService::setServiceAddressType()
     * @uses AddressForService::setBackPackNumber()
     * @uses AddressForService::setRecipient()
     * @uses AddressForService::setFlags()
     * @param string $serviceAddressType
     * @param string $backPackNumber
     * @param \PPLSDK\StructType\ServiceAddressRecipient $recipient
     * @param \PPLSDK\ArrayType\ArrayOfMyApiFlag $flags
     */
    public function __construct($serviceAddressType = null, $backPackNumber = null, \PPLSDK\StructType\ServiceAddressRecipient $recipient = null, \PPLSDK\ArrayType\ArrayOfMyApiFlag $flags = null)
    {
        $this
            ->setServiceAddressType($serviceAddressType)
            ->setBackPackNumber($backPackNumber)
            ->setRecipient($recipient)
            ->setFlags($flags);
    }
    /**
     * Get ServiceAddressType value
     * @return string|null
     */
    public function getServiceAddressType()
    {
        return $this->ServiceAddressType;
    }
    /**
     * Set ServiceAddressType value
     * @param string $serviceAddressType
     * @return \PPLSDK\StructType\AddressForService
     */
    public function setServiceAddressType($serviceAddressType = null)
    {
        // validation for constraint: string
        if (!is_null($serviceAddressType) && !is_string($serviceAddressType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($serviceAddressType, true), gettype($serviceAddressType)), __LINE__);
        }
        $this->ServiceAddressType = $serviceAddressType;
        return $this;
    }
    /**
     * Get BackPackNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBackPackNumber()
    {
        return isset($this->BackPackNumber) ? $this->BackPackNumber : null;
    }
    /**
     * Set BackPackNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $backPackNumber
     * @return \PPLSDK\StructType\AddressForService
     */
    public function setBackPackNumber($backPackNumber = null)
    {
        // validation for constraint: string
        if (!is_null($backPackNumber) && !is_string($backPackNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($backPackNumber, true), gettype($backPackNumber)), __LINE__);
        }
        if (is_null($backPackNumber) || (is_array($backPackNumber) && empty($backPackNumber))) {
            unset($this->BackPackNumber);
        } else {
            $this->BackPackNumber = $backPackNumber;
        }
        return $this;
    }
    /**
     * Get Recipient value
     * @return \PPLSDK\StructType\ServiceAddressRecipient|null
     */
    public function getRecipient()
    {
        return $this->Recipient;
    }
    /**
     * Set Recipient value
     * @param \PPLSDK\StructType\ServiceAddressRecipient $recipient
     * @return \PPLSDK\StructType\AddressForService
     */
    public function setRecipient(\PPLSDK\StructType\ServiceAddressRecipient $recipient = null)
    {
        $this->Recipient = $recipient;
        return $this;
    }
    /**
     * Get Flags value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\ArrayType\ArrayOfMyApiFlag|null
     */
    public function getFlags()
    {
        return isset($this->Flags) ? $this->Flags : null;
    }
    /**
     * Set Flags value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\ArrayType\ArrayOfMyApiFlag $flags
     * @return \PPLSDK\StructType\AddressForService
     */
    public function setFlags(\PPLSDK\ArrayType\ArrayOfMyApiFlag $flags = null)
    {
        if (is_null($flags) || (is_array($flags) && empty($flags))) {
            unset($this->Flags);
        } else {
            $this->Flags = $flags;
        }
        return $this;
    }
}
