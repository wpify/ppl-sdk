<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiPackageOutService StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiPackageOutService
 * @subpackage Structs
 */
class MyApiPackageOutService extends AbstractStructBase
{
    /**
     * The Price
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $Price;
    /**
     * The PriceCurrency
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PriceCurrency;
    /**
     * The SvcCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $SvcCode;
    /**
     * Constructor method for MyApiPackageOutService
     * @uses MyApiPackageOutService::setPrice()
     * @uses MyApiPackageOutService::setPriceCurrency()
     * @uses MyApiPackageOutService::setSvcCode()
     * @param float $price
     * @param string $priceCurrency
     * @param string $svcCode
     */
    public function __construct($price = null, $priceCurrency = null, $svcCode = null)
    {
        $this
            ->setPrice($price)
            ->setPriceCurrency($priceCurrency)
            ->setSvcCode($svcCode);
    }
    /**
     * Get Price value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getPrice()
    {
        return isset($this->Price) ? $this->Price : null;
    }
    /**
     * Set Price value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $price
     * @return \PPLSDK\StructType\MyApiPackageOutService
     */
    public function setPrice($price = null)
    {
        // validation for constraint: float
        if (!is_null($price) && !(is_float($price) || is_numeric($price))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($price, true), gettype($price)), __LINE__);
        }
        if (is_null($price) || (is_array($price) && empty($price))) {
            unset($this->Price);
        } else {
            $this->Price = $price;
        }
        return $this;
    }
    /**
     * Get PriceCurrency value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPriceCurrency()
    {
        return isset($this->PriceCurrency) ? $this->PriceCurrency : null;
    }
    /**
     * Set PriceCurrency value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $priceCurrency
     * @return \PPLSDK\StructType\MyApiPackageOutService
     */
    public function setPriceCurrency($priceCurrency = null)
    {
        // validation for constraint: string
        if (!is_null($priceCurrency) && !is_string($priceCurrency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($priceCurrency, true), gettype($priceCurrency)), __LINE__);
        }
        if (is_null($priceCurrency) || (is_array($priceCurrency) && empty($priceCurrency))) {
            unset($this->PriceCurrency);
        } else {
            $this->PriceCurrency = $priceCurrency;
        }
        return $this;
    }
    /**
     * Get SvcCode value
     * @return string|null
     */
    public function getSvcCode()
    {
        return $this->SvcCode;
    }
    /**
     * Set SvcCode value
     * @param string $svcCode
     * @return \PPLSDK\StructType\MyApiPackageOutService
     */
    public function setSvcCode($svcCode = null)
    {
        // validation for constraint: string
        if (!is_null($svcCode) && !is_string($svcCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($svcCode, true), gettype($svcCode)), __LINE__);
        }
        $this->SvcCode = $svcCode;
        return $this;
    }
}
