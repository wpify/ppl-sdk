<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiPackageUpdate StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiPackageUpdate
 * @subpackage Structs
 */
class MyApiPackageUpdate extends AbstractStructBase
{
    /**
     * The PackNumber
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $PackNumber;
    /**
     * The STR
     * @var bool
     */
    public $STR;
    /**
     * Constructor method for MyApiPackageUpdate
     * @uses MyApiPackageUpdate::setPackNumber()
     * @uses MyApiPackageUpdate::setSTR()
     * @param string $packNumber
     * @param bool $sTR
     */
    public function __construct($packNumber = null, $sTR = null)
    {
        $this
            ->setPackNumber($packNumber)
            ->setSTR($sTR);
    }
    /**
     * Get PackNumber value
     * @return string|null
     */
    public function getPackNumber()
    {
        return $this->PackNumber;
    }
    /**
     * Set PackNumber value
     * @param string $packNumber
     * @return \PPLSDK\StructType\MyApiPackageUpdate
     */
    public function setPackNumber($packNumber = null)
    {
        // validation for constraint: string
        if (!is_null($packNumber) && !is_string($packNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packNumber, true), gettype($packNumber)), __LINE__);
        }
        $this->PackNumber = $packNumber;
        return $this;
    }
    /**
     * Get STR value
     * @return bool|null
     */
    public function getSTR()
    {
        return $this->STR;
    }
    /**
     * Set STR value
     * @param bool $sTR
     * @return \PPLSDK\StructType\MyApiPackageUpdate
     */
    public function setSTR($sTR = null)
    {
        // validation for constraint: boolean
        if (!is_null($sTR) && !is_bool($sTR)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($sTR, true), gettype($sTR)), __LINE__);
        }
        $this->STR = $sTR;
        return $this;
    }
}
