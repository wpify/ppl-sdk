<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiFault StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiFault
 * @subpackage Structs
 */
class MyApiFault extends AbstractStructBase
{
}
