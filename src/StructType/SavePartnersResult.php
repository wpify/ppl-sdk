<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SavePartnersResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:SavePartnersResult
 * @subpackage Structs
 */
class SavePartnersResult extends PartnerResultOfPartnerDatapewOaM6d
{
}
