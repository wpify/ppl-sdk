<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AuthenticationData StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:AuthenticationData
 * @subpackage Structs
 */
class AuthenticationData extends AbstractStructBase
{
    /**
     * The AuthToken
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $AuthToken;
    /**
     * The CustId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $CustId;
    /**
     * The PartnerId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PartnerId;
    /**
     * The Password
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Password;
    /**
     * The SubjectId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $SubjectId;
    /**
     * The UserName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $UserName;
    /**
     * Constructor method for AuthenticationData
     * @uses AuthenticationData::setAuthToken()
     * @uses AuthenticationData::setCustId()
     * @uses AuthenticationData::setPartnerId()
     * @uses AuthenticationData::setPassword()
     * @uses AuthenticationData::setSubjectId()
     * @uses AuthenticationData::setUserName()
     * @param string $authToken
     * @param int $custId
     * @param string $partnerId
     * @param string $password
     * @param int $subjectId
     * @param string $userName
     */
    public function __construct($authToken = null, $custId = null, $partnerId = null, $password = null, $subjectId = null, $userName = null)
    {
        $this
            ->setAuthToken($authToken)
            ->setCustId($custId)
            ->setPartnerId($partnerId)
            ->setPassword($password)
            ->setSubjectId($subjectId)
            ->setUserName($userName);
    }
    /**
     * Get AuthToken value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAuthToken()
    {
        return isset($this->AuthToken) ? $this->AuthToken : null;
    }
    /**
     * Set AuthToken value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $authToken
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function setAuthToken($authToken = null)
    {
        // validation for constraint: string
        if (!is_null($authToken) && !is_string($authToken)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($authToken, true), gettype($authToken)), __LINE__);
        }
        if (is_null($authToken) || (is_array($authToken) && empty($authToken))) {
            unset($this->AuthToken);
        } else {
            $this->AuthToken = $authToken;
        }
        return $this;
    }
    /**
     * Get CustId value
     * @return int|null
     */
    public function getCustId()
    {
        return $this->CustId;
    }
    /**
     * Set CustId value
     * @param int $custId
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function setCustId($custId = null)
    {
        // validation for constraint: int
        if (!is_null($custId) && !(is_int($custId) || ctype_digit($custId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($custId, true), gettype($custId)), __LINE__);
        }
        $this->CustId = $custId;
        return $this;
    }
    /**
     * Get PartnerId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPartnerId()
    {
        return isset($this->PartnerId) ? $this->PartnerId : null;
    }
    /**
     * Set PartnerId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $partnerId
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function setPartnerId($partnerId = null)
    {
        // validation for constraint: string
        if (!is_null($partnerId) && !is_string($partnerId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($partnerId, true), gettype($partnerId)), __LINE__);
        }
        if (is_null($partnerId) || (is_array($partnerId) && empty($partnerId))) {
            unset($this->PartnerId);
        } else {
            $this->PartnerId = $partnerId;
        }
        return $this;
    }
    /**
     * Get Password value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPassword()
    {
        return isset($this->Password) ? $this->Password : null;
    }
    /**
     * Set Password value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $password
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function setPassword($password = null)
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password, true), gettype($password)), __LINE__);
        }
        if (is_null($password) || (is_array($password) && empty($password))) {
            unset($this->Password);
        } else {
            $this->Password = $password;
        }
        return $this;
    }
    /**
     * Get SubjectId value
     * @return int|null
     */
    public function getSubjectId()
    {
        return $this->SubjectId;
    }
    /**
     * Set SubjectId value
     * @param int $subjectId
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function setSubjectId($subjectId = null)
    {
        // validation for constraint: int
        if (!is_null($subjectId) && !(is_int($subjectId) || ctype_digit($subjectId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($subjectId, true), gettype($subjectId)), __LINE__);
        }
        $this->SubjectId = $subjectId;
        return $this;
    }
    /**
     * Get UserName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUserName()
    {
        return isset($this->UserName) ? $this->UserName : null;
    }
    /**
     * Set UserName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $userName
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function setUserName($userName = null)
    {
        // validation for constraint: string
        if (!is_null($userName) && !is_string($userName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($userName, true), gettype($userName)), __LINE__);
        }
        if (is_null($userName) || (is_array($userName) && empty($userName))) {
            unset($this->UserName);
        } else {
            $this->UserName = $userName;
        }
        return $this;
    }
}
