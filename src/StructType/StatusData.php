<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for StatusData StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:StatusData
 * @subpackage Structs
 */
class StatusData extends AbstractStructBase
{
    /**
     * The CODValue
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $CODValue;
    /**
     * The CustID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $CustID;
    /**
     * The DelivPerson
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DelivPerson;
    /**
     * The GpsE
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $GpsE;
    /**
     * The GpsN
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $GpsN;
    /**
     * The ID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $ID;
    /**
     * The Note
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Note;
    /**
     * The Note2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Note2;
    /**
     * The PackageID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PackageID;
    /**
     * The PackageIDCus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PackageIDCus;
    /**
     * The PartnerID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PartnerID;
    /**
     * The Status
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Status;
    /**
     * The StatusCity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $StatusCity;
    /**
     * The StatusCountry
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $StatusCountry;
    /**
     * The StatusDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $StatusDate;
    /**
     * The StatusStreet
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $StatusStreet;
    /**
     * The StatusZipCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $StatusZipCode;
    /**
     * Constructor method for StatusData
     * @uses StatusData::setCODValue()
     * @uses StatusData::setCustID()
     * @uses StatusData::setDelivPerson()
     * @uses StatusData::setGpsE()
     * @uses StatusData::setGpsN()
     * @uses StatusData::setID()
     * @uses StatusData::setNote()
     * @uses StatusData::setNote2()
     * @uses StatusData::setPackageID()
     * @uses StatusData::setPackageIDCus()
     * @uses StatusData::setPartnerID()
     * @uses StatusData::setStatus()
     * @uses StatusData::setStatusCity()
     * @uses StatusData::setStatusCountry()
     * @uses StatusData::setStatusDate()
     * @uses StatusData::setStatusStreet()
     * @uses StatusData::setStatusZipCode()
     * @param float $cODValue
     * @param int $custID
     * @param string $delivPerson
     * @param float $gpsE
     * @param float $gpsN
     * @param int $iD
     * @param string $note
     * @param string $note2
     * @param string $packageID
     * @param string $packageIDCus
     * @param string $partnerID
     * @param string $status
     * @param string $statusCity
     * @param string $statusCountry
     * @param string $statusDate
     * @param string $statusStreet
     * @param string $statusZipCode
     */
    public function __construct($cODValue = null, $custID = null, $delivPerson = null, $gpsE = null, $gpsN = null, $iD = null, $note = null, $note2 = null, $packageID = null, $packageIDCus = null, $partnerID = null, $status = null, $statusCity = null, $statusCountry = null, $statusDate = null, $statusStreet = null, $statusZipCode = null)
    {
        $this
            ->setCODValue($cODValue)
            ->setCustID($custID)
            ->setDelivPerson($delivPerson)
            ->setGpsE($gpsE)
            ->setGpsN($gpsN)
            ->setID($iD)
            ->setNote($note)
            ->setNote2($note2)
            ->setPackageID($packageID)
            ->setPackageIDCus($packageIDCus)
            ->setPartnerID($partnerID)
            ->setStatus($status)
            ->setStatusCity($statusCity)
            ->setStatusCountry($statusCountry)
            ->setStatusDate($statusDate)
            ->setStatusStreet($statusStreet)
            ->setStatusZipCode($statusZipCode);
    }
    /**
     * Get CODValue value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getCODValue()
    {
        return isset($this->CODValue) ? $this->CODValue : null;
    }
    /**
     * Set CODValue value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $cODValue
     * @return \PPLSDK\StructType\StatusData
     */
    public function setCODValue($cODValue = null)
    {
        // validation for constraint: float
        if (!is_null($cODValue) && !(is_float($cODValue) || is_numeric($cODValue))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($cODValue, true), gettype($cODValue)), __LINE__);
        }
        if (is_null($cODValue) || (is_array($cODValue) && empty($cODValue))) {
            unset($this->CODValue);
        } else {
            $this->CODValue = $cODValue;
        }
        return $this;
    }
    /**
     * Get CustID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getCustID()
    {
        return isset($this->CustID) ? $this->CustID : null;
    }
    /**
     * Set CustID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $custID
     * @return \PPLSDK\StructType\StatusData
     */
    public function setCustID($custID = null)
    {
        // validation for constraint: int
        if (!is_null($custID) && !(is_int($custID) || ctype_digit($custID))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($custID, true), gettype($custID)), __LINE__);
        }
        if (is_null($custID) || (is_array($custID) && empty($custID))) {
            unset($this->CustID);
        } else {
            $this->CustID = $custID;
        }
        return $this;
    }
    /**
     * Get DelivPerson value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDelivPerson()
    {
        return isset($this->DelivPerson) ? $this->DelivPerson : null;
    }
    /**
     * Set DelivPerson value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $delivPerson
     * @return \PPLSDK\StructType\StatusData
     */
    public function setDelivPerson($delivPerson = null)
    {
        // validation for constraint: string
        if (!is_null($delivPerson) && !is_string($delivPerson)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($delivPerson, true), gettype($delivPerson)), __LINE__);
        }
        if (is_null($delivPerson) || (is_array($delivPerson) && empty($delivPerson))) {
            unset($this->DelivPerson);
        } else {
            $this->DelivPerson = $delivPerson;
        }
        return $this;
    }
    /**
     * Get GpsE value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getGpsE()
    {
        return isset($this->GpsE) ? $this->GpsE : null;
    }
    /**
     * Set GpsE value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $gpsE
     * @return \PPLSDK\StructType\StatusData
     */
    public function setGpsE($gpsE = null)
    {
        // validation for constraint: float
        if (!is_null($gpsE) && !(is_float($gpsE) || is_numeric($gpsE))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($gpsE, true), gettype($gpsE)), __LINE__);
        }
        if (is_null($gpsE) || (is_array($gpsE) && empty($gpsE))) {
            unset($this->GpsE);
        } else {
            $this->GpsE = $gpsE;
        }
        return $this;
    }
    /**
     * Get GpsN value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getGpsN()
    {
        return isset($this->GpsN) ? $this->GpsN : null;
    }
    /**
     * Set GpsN value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $gpsN
     * @return \PPLSDK\StructType\StatusData
     */
    public function setGpsN($gpsN = null)
    {
        // validation for constraint: float
        if (!is_null($gpsN) && !(is_float($gpsN) || is_numeric($gpsN))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($gpsN, true), gettype($gpsN)), __LINE__);
        }
        if (is_null($gpsN) || (is_array($gpsN) && empty($gpsN))) {
            unset($this->GpsN);
        } else {
            $this->GpsN = $gpsN;
        }
        return $this;
    }
    /**
     * Get ID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getID()
    {
        return isset($this->ID) ? $this->ID : null;
    }
    /**
     * Set ID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $iD
     * @return \PPLSDK\StructType\StatusData
     */
    public function setID($iD = null)
    {
        // validation for constraint: int
        if (!is_null($iD) && !(is_int($iD) || ctype_digit($iD))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($iD, true), gettype($iD)), __LINE__);
        }
        if (is_null($iD) || (is_array($iD) && empty($iD))) {
            unset($this->ID);
        } else {
            $this->ID = $iD;
        }
        return $this;
    }
    /**
     * Get Note value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNote()
    {
        return isset($this->Note) ? $this->Note : null;
    }
    /**
     * Set Note value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $note
     * @return \PPLSDK\StructType\StatusData
     */
    public function setNote($note = null)
    {
        // validation for constraint: string
        if (!is_null($note) && !is_string($note)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($note, true), gettype($note)), __LINE__);
        }
        if (is_null($note) || (is_array($note) && empty($note))) {
            unset($this->Note);
        } else {
            $this->Note = $note;
        }
        return $this;
    }
    /**
     * Get Note2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNote2()
    {
        return isset($this->Note2) ? $this->Note2 : null;
    }
    /**
     * Set Note2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $note2
     * @return \PPLSDK\StructType\StatusData
     */
    public function setNote2($note2 = null)
    {
        // validation for constraint: string
        if (!is_null($note2) && !is_string($note2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($note2, true), gettype($note2)), __LINE__);
        }
        if (is_null($note2) || (is_array($note2) && empty($note2))) {
            unset($this->Note2);
        } else {
            $this->Note2 = $note2;
        }
        return $this;
    }
    /**
     * Get PackageID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPackageID()
    {
        return isset($this->PackageID) ? $this->PackageID : null;
    }
    /**
     * Set PackageID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $packageID
     * @return \PPLSDK\StructType\StatusData
     */
    public function setPackageID($packageID = null)
    {
        // validation for constraint: string
        if (!is_null($packageID) && !is_string($packageID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packageID, true), gettype($packageID)), __LINE__);
        }
        if (is_null($packageID) || (is_array($packageID) && empty($packageID))) {
            unset($this->PackageID);
        } else {
            $this->PackageID = $packageID;
        }
        return $this;
    }
    /**
     * Get PackageIDCus value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPackageIDCus()
    {
        return isset($this->PackageIDCus) ? $this->PackageIDCus : null;
    }
    /**
     * Set PackageIDCus value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $packageIDCus
     * @return \PPLSDK\StructType\StatusData
     */
    public function setPackageIDCus($packageIDCus = null)
    {
        // validation for constraint: string
        if (!is_null($packageIDCus) && !is_string($packageIDCus)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packageIDCus, true), gettype($packageIDCus)), __LINE__);
        }
        if (is_null($packageIDCus) || (is_array($packageIDCus) && empty($packageIDCus))) {
            unset($this->PackageIDCus);
        } else {
            $this->PackageIDCus = $packageIDCus;
        }
        return $this;
    }
    /**
     * Get PartnerID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPartnerID()
    {
        return isset($this->PartnerID) ? $this->PartnerID : null;
    }
    /**
     * Set PartnerID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $partnerID
     * @return \PPLSDK\StructType\StatusData
     */
    public function setPartnerID($partnerID = null)
    {
        // validation for constraint: string
        if (!is_null($partnerID) && !is_string($partnerID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($partnerID, true), gettype($partnerID)), __LINE__);
        }
        if (is_null($partnerID) || (is_array($partnerID) && empty($partnerID))) {
            unset($this->PartnerID);
        } else {
            $this->PartnerID = $partnerID;
        }
        return $this;
    }
    /**
     * Get Status value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStatus()
    {
        return isset($this->Status) ? $this->Status : null;
    }
    /**
     * Set Status value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $status
     * @return \PPLSDK\StructType\StatusData
     */
    public function setStatus($status = null)
    {
        // validation for constraint: string
        if (!is_null($status) && !is_string($status)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($status, true), gettype($status)), __LINE__);
        }
        if (is_null($status) || (is_array($status) && empty($status))) {
            unset($this->Status);
        } else {
            $this->Status = $status;
        }
        return $this;
    }
    /**
     * Get StatusCity value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStatusCity()
    {
        return isset($this->StatusCity) ? $this->StatusCity : null;
    }
    /**
     * Set StatusCity value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $statusCity
     * @return \PPLSDK\StructType\StatusData
     */
    public function setStatusCity($statusCity = null)
    {
        // validation for constraint: string
        if (!is_null($statusCity) && !is_string($statusCity)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($statusCity, true), gettype($statusCity)), __LINE__);
        }
        if (is_null($statusCity) || (is_array($statusCity) && empty($statusCity))) {
            unset($this->StatusCity);
        } else {
            $this->StatusCity = $statusCity;
        }
        return $this;
    }
    /**
     * Get StatusCountry value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStatusCountry()
    {
        return isset($this->StatusCountry) ? $this->StatusCountry : null;
    }
    /**
     * Set StatusCountry value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $statusCountry
     * @return \PPLSDK\StructType\StatusData
     */
    public function setStatusCountry($statusCountry = null)
    {
        // validation for constraint: string
        if (!is_null($statusCountry) && !is_string($statusCountry)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($statusCountry, true), gettype($statusCountry)), __LINE__);
        }
        if (is_null($statusCountry) || (is_array($statusCountry) && empty($statusCountry))) {
            unset($this->StatusCountry);
        } else {
            $this->StatusCountry = $statusCountry;
        }
        return $this;
    }
    /**
     * Get StatusDate value
     * @return string|null
     */
    public function getStatusDate()
    {
        return $this->StatusDate;
    }
    /**
     * Set StatusDate value
     * @param string $statusDate
     * @return \PPLSDK\StructType\StatusData
     */
    public function setStatusDate($statusDate = null)
    {
        // validation for constraint: string
        if (!is_null($statusDate) && !is_string($statusDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($statusDate, true), gettype($statusDate)), __LINE__);
        }
        $this->StatusDate = $statusDate;
        return $this;
    }
    /**
     * Get StatusStreet value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStatusStreet()
    {
        return isset($this->StatusStreet) ? $this->StatusStreet : null;
    }
    /**
     * Set StatusStreet value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $statusStreet
     * @return \PPLSDK\StructType\StatusData
     */
    public function setStatusStreet($statusStreet = null)
    {
        // validation for constraint: string
        if (!is_null($statusStreet) && !is_string($statusStreet)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($statusStreet, true), gettype($statusStreet)), __LINE__);
        }
        if (is_null($statusStreet) || (is_array($statusStreet) && empty($statusStreet))) {
            unset($this->StatusStreet);
        } else {
            $this->StatusStreet = $statusStreet;
        }
        return $this;
    }
    /**
     * Get StatusZipCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStatusZipCode()
    {
        return isset($this->StatusZipCode) ? $this->StatusZipCode : null;
    }
    /**
     * Set StatusZipCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $statusZipCode
     * @return \PPLSDK\StructType\StatusData
     */
    public function setStatusZipCode($statusZipCode = null)
    {
        // validation for constraint: string
        if (!is_null($statusZipCode) && !is_string($statusZipCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($statusZipCode, true), gettype($statusZipCode)), __LINE__);
        }
        if (is_null($statusZipCode) || (is_array($statusZipCode) && empty($statusZipCode))) {
            unset($this->StatusZipCode);
        } else {
            $this->StatusZipCode = $statusZipCode;
        }
        return $this;
    }
}
