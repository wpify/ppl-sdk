<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetFreeRangeInfoResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:GetFreeRangeInfoResult
 * @subpackage Structs
 */
class GetFreeRangeInfoResult extends MyApiResultBaseOfArrayOfGetFreeNumberRangesDatapewOaM6d
{
}
