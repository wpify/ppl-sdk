<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPackagesResponse StructType
 * @subpackage Structs
 */
class GetPackagesResponse extends AbstractStructBase
{
    /**
     * The GetPackagesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\GetPackagesResult
     */
    public $GetPackagesResult;
    /**
     * Constructor method for GetPackagesResponse
     * @uses GetPackagesResponse::setGetPackagesResult()
     * @param \PPLSDK\StructType\GetPackagesResult $getPackagesResult
     */
    public function __construct(\PPLSDK\StructType\GetPackagesResult $getPackagesResult = null)
    {
        $this
            ->setGetPackagesResult($getPackagesResult);
    }
    /**
     * Get GetPackagesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\GetPackagesResult|null
     */
    public function getGetPackagesResult()
    {
        return isset($this->GetPackagesResult) ? $this->GetPackagesResult : null;
    }
    /**
     * Set GetPackagesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\GetPackagesResult $getPackagesResult
     * @return \PPLSDK\StructType\GetPackagesResponse
     */
    public function setGetPackagesResult(\PPLSDK\StructType\GetPackagesResult $getPackagesResult = null)
    {
        if (is_null($getPackagesResult) || (is_array($getPackagesResult) && empty($getPackagesResult))) {
            unset($this->GetPackagesResult);
        } else {
            $this->GetPackagesResult = $getPackagesResult;
        }
        return $this;
    }
}
