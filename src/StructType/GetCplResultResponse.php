<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCplResultResponse StructType
 * @subpackage Structs
 */
class GetCplResultResponse extends AbstractStructBase
{
    /**
     * The GetCplResultResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\CreatePackageLabelRequestResult
     */
    public $GetCplResultResult;
    /**
     * Constructor method for GetCplResultResponse
     * @uses GetCplResultResponse::setGetCplResultResult()
     * @param \PPLSDK\StructType\CreatePackageLabelRequestResult $getCplResultResult
     */
    public function __construct(\PPLSDK\StructType\CreatePackageLabelRequestResult $getCplResultResult = null)
    {
        $this
            ->setGetCplResultResult($getCplResultResult);
    }
    /**
     * Get GetCplResultResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\CreatePackageLabelRequestResult|null
     */
    public function getGetCplResultResult()
    {
        return isset($this->GetCplResultResult) ? $this->GetCplResultResult : null;
    }
    /**
     * Set GetCplResultResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\CreatePackageLabelRequestResult $getCplResultResult
     * @return \PPLSDK\StructType\GetCplResultResponse
     */
    public function setGetCplResultResult(\PPLSDK\StructType\CreatePackageLabelRequestResult $getCplResultResult = null)
    {
        if (is_null($getCplResultResult) || (is_array($getCplResultResult) && empty($getCplResultResult))) {
            unset($this->GetCplResultResult);
        } else {
            $this->GetCplResultResult = $getCplResultResult;
        }
        return $this;
    }
}
