<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiResultBaseOfArrayOfNumberRangepewOaM6d StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiResultBaseOfArrayOfNumberRangepewOaM6d
 * @subpackage Structs
 */
class MyApiResultBaseOfArrayOfNumberRangepewOaM6d extends MyApiResultBaseVoid
{
    /**
     * The ResultData
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfNumberRange
     */
    public $ResultData;
    /**
     * Constructor method for MyApiResultBaseOfArrayOfNumberRangepewOaM6d
     * @uses MyApiResultBaseOfArrayOfNumberRangepewOaM6d::setResultData()
     * @param \PPLSDK\ArrayType\ArrayOfNumberRange $resultData
     */
    public function __construct(\PPLSDK\ArrayType\ArrayOfNumberRange $resultData = null)
    {
        $this
            ->setResultData($resultData);
    }
    /**
     * Get ResultData value
     * @return \PPLSDK\ArrayType\ArrayOfNumberRange|null
     */
    public function getResultData()
    {
        return $this->ResultData;
    }
    /**
     * Set ResultData value
     * @param \PPLSDK\ArrayType\ArrayOfNumberRange $resultData
     * @return \PPLSDK\StructType\MyApiResultBaseOfArrayOfNumberRangepewOaM6d
     */
    public function setResultData(\PPLSDK\ArrayType\ArrayOfNumberRange $resultData = null)
    {
        $this->ResultData = $resultData;
        return $this;
    }
}
