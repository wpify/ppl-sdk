<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreatePickupOrdersResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:CreatePickupOrdersResult
 * @subpackage Structs
 */
class CreatePickupOrdersResult extends MyApiResultBaseOfArrayOfItemResultpewOaM6d
{
}
