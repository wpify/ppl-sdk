<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UpdatePackageResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:UpdatePackageResult
 * @subpackage Structs
 */
class UpdatePackageResult extends MyApiResultBaseOfUpdatePackageResultDataLQQ4aWVY
{
}
