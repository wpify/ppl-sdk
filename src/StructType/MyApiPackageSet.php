<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiPackageSet StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiPackageSet
 * @subpackage Structs
 */
class MyApiPackageSet extends AbstractStructBase
{
    /**
     * The PackageInSetNr
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $PackageInSetNr;
    /**
     * The PackagesInSet
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $PackagesInSet;
    /**
     * Constructor method for MyApiPackageSet
     * @uses MyApiPackageSet::setPackageInSetNr()
     * @uses MyApiPackageSet::setPackagesInSet()
     * @param int $packageInSetNr
     * @param int $packagesInSet
     */
    public function __construct($packageInSetNr = null, $packagesInSet = null)
    {
        $this
            ->setPackageInSetNr($packageInSetNr)
            ->setPackagesInSet($packagesInSet);
    }
    /**
     * Get PackageInSetNr value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getPackageInSetNr()
    {
        return isset($this->PackageInSetNr) ? $this->PackageInSetNr : null;
    }
    /**
     * Set PackageInSetNr value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $packageInSetNr
     * @return \PPLSDK\StructType\MyApiPackageSet
     */
    public function setPackageInSetNr($packageInSetNr = null)
    {
        // validation for constraint: int
        if (!is_null($packageInSetNr) && !(is_int($packageInSetNr) || ctype_digit($packageInSetNr))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($packageInSetNr, true), gettype($packageInSetNr)), __LINE__);
        }
        if (is_null($packageInSetNr) || (is_array($packageInSetNr) && empty($packageInSetNr))) {
            unset($this->PackageInSetNr);
        } else {
            $this->PackageInSetNr = $packageInSetNr;
        }
        return $this;
    }
    /**
     * Get PackagesInSet value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getPackagesInSet()
    {
        return isset($this->PackagesInSet) ? $this->PackagesInSet : null;
    }
    /**
     * Set PackagesInSet value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $packagesInSet
     * @return \PPLSDK\StructType\MyApiPackageSet
     */
    public function setPackagesInSet($packagesInSet = null)
    {
        // validation for constraint: int
        if (!is_null($packagesInSet) && !(is_int($packagesInSet) || ctype_digit($packagesInSet))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($packagesInSet, true), gettype($packagesInSet)), __LINE__);
        }
        if (is_null($packagesInSet) || (is_array($packagesInSet) && empty($packagesInSet))) {
            unset($this->PackagesInSet);
        } else {
            $this->PackagesInSet = $packagesInSet;
        }
        return $this;
    }
}
