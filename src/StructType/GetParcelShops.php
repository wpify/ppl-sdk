<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetParcelShops StructType
 * @subpackage Structs
 */
class GetParcelShops extends AbstractStructBase
{
    /**
     * The Filter
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\ParcelShopFilter
     */
    public $Filter;
    /**
     * Constructor method for GetParcelShops
     * @uses GetParcelShops::setFilter()
     * @param \PPLSDK\StructType\ParcelShopFilter $filter
     */
    public function __construct(\PPLSDK\StructType\ParcelShopFilter $filter = null)
    {
        $this
            ->setFilter($filter);
    }
    /**
     * Get Filter value
     * @return \PPLSDK\StructType\ParcelShopFilter
     */
    public function getFilter()
    {
        return $this->Filter;
    }
    /**
     * Set Filter value
     * @param \PPLSDK\StructType\ParcelShopFilter $filter
     * @return \PPLSDK\StructType\GetParcelShops
     */
    public function setFilter(\PPLSDK\StructType\ParcelShopFilter $filter = null)
    {
        $this->Filter = $filter;
        return $this;
    }
}
