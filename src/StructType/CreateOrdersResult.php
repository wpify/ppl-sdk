<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateOrdersResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:CreateOrdersResult
 * @subpackage Structs
 */
class CreateOrdersResult extends MyApiResultBaseOfArrayOfItemResultpewOaM6d
{
}
