<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiResultBaseOfArrayOfMyApiCityRoutingpewOaM6d
 * StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiResultBaseOfArrayOfMyApiCityRoutingpewOaM6d
 * @subpackage Structs
 */
class MyApiResultBaseOfArrayOfMyApiCityRoutingpewOaM6d extends MyApiResultBaseVoid
{
    /**
     * The ResultData
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiCityRouting
     */
    public $ResultData;
    /**
     * Constructor method for MyApiResultBaseOfArrayOfMyApiCityRoutingpewOaM6d
     * @uses MyApiResultBaseOfArrayOfMyApiCityRoutingpewOaM6d::setResultData()
     * @param \PPLSDK\ArrayType\ArrayOfMyApiCityRouting $resultData
     */
    public function __construct(\PPLSDK\ArrayType\ArrayOfMyApiCityRouting $resultData = null)
    {
        $this
            ->setResultData($resultData);
    }
    /**
     * Get ResultData value
     * @return \PPLSDK\ArrayType\ArrayOfMyApiCityRouting|null
     */
    public function getResultData()
    {
        return $this->ResultData;
    }
    /**
     * Set ResultData value
     * @param \PPLSDK\ArrayType\ArrayOfMyApiCityRouting $resultData
     * @return \PPLSDK\StructType\MyApiResultBaseOfArrayOfMyApiCityRoutingpewOaM6d
     */
    public function setResultData(\PPLSDK\ArrayType\ArrayOfMyApiCityRouting $resultData = null)
    {
        $this->ResultData = $resultData;
        return $this;
    }
}
