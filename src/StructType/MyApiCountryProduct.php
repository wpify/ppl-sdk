<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiCountryProduct StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiCountryProduct
 * @subpackage Structs
 */
class MyApiCountryProduct extends AbstractStructBase
{
    /**
     * The AllowCOD
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $AllowCOD;
    /**
     * The AllowParcelShop
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $AllowParcelShop;
    /**
     * The DefaultCust
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $DefaultCust;
    /**
     * The DefaultProduct
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $DefaultProduct;
    /**
     * The DefaultStd
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $DefaultStd;
    /**
     * The DefaultSub
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $DefaultSub;
    /**
     * The Export
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $Export;
    /**
     * The Import
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $Import;
    /**
     * The IsSupreme
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $IsSupreme;
    /**
     * The PptId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $PptId;
    /**
     * The ProductCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ProductCode;
    /**
     * Constructor method for MyApiCountryProduct
     * @uses MyApiCountryProduct::setAllowCOD()
     * @uses MyApiCountryProduct::setAllowParcelShop()
     * @uses MyApiCountryProduct::setDefaultCust()
     * @uses MyApiCountryProduct::setDefaultProduct()
     * @uses MyApiCountryProduct::setDefaultStd()
     * @uses MyApiCountryProduct::setDefaultSub()
     * @uses MyApiCountryProduct::setExport()
     * @uses MyApiCountryProduct::setImport()
     * @uses MyApiCountryProduct::setIsSupreme()
     * @uses MyApiCountryProduct::setPptId()
     * @uses MyApiCountryProduct::setProductCode()
     * @param bool $allowCOD
     * @param bool $allowParcelShop
     * @param bool $defaultCust
     * @param bool $defaultProduct
     * @param bool $defaultStd
     * @param bool $defaultSub
     * @param bool $export
     * @param bool $import
     * @param bool $isSupreme
     * @param int $pptId
     * @param string $productCode
     */
    public function __construct($allowCOD = null, $allowParcelShop = null, $defaultCust = null, $defaultProduct = null, $defaultStd = null, $defaultSub = null, $export = null, $import = null, $isSupreme = null, $pptId = null, $productCode = null)
    {
        $this
            ->setAllowCOD($allowCOD)
            ->setAllowParcelShop($allowParcelShop)
            ->setDefaultCust($defaultCust)
            ->setDefaultProduct($defaultProduct)
            ->setDefaultStd($defaultStd)
            ->setDefaultSub($defaultSub)
            ->setExport($export)
            ->setImport($import)
            ->setIsSupreme($isSupreme)
            ->setPptId($pptId)
            ->setProductCode($productCode);
    }
    /**
     * Get AllowCOD value
     * @return bool|null
     */
    public function getAllowCOD()
    {
        return $this->AllowCOD;
    }
    /**
     * Set AllowCOD value
     * @param bool $allowCOD
     * @return \PPLSDK\StructType\MyApiCountryProduct
     */
    public function setAllowCOD($allowCOD = null)
    {
        // validation for constraint: boolean
        if (!is_null($allowCOD) && !is_bool($allowCOD)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($allowCOD, true), gettype($allowCOD)), __LINE__);
        }
        $this->AllowCOD = $allowCOD;
        return $this;
    }
    /**
     * Get AllowParcelShop value
     * @return bool|null
     */
    public function getAllowParcelShop()
    {
        return $this->AllowParcelShop;
    }
    /**
     * Set AllowParcelShop value
     * @param bool $allowParcelShop
     * @return \PPLSDK\StructType\MyApiCountryProduct
     */
    public function setAllowParcelShop($allowParcelShop = null)
    {
        // validation for constraint: boolean
        if (!is_null($allowParcelShop) && !is_bool($allowParcelShop)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($allowParcelShop, true), gettype($allowParcelShop)), __LINE__);
        }
        $this->AllowParcelShop = $allowParcelShop;
        return $this;
    }
    /**
     * Get DefaultCust value
     * @return bool|null
     */
    public function getDefaultCust()
    {
        return $this->DefaultCust;
    }
    /**
     * Set DefaultCust value
     * @param bool $defaultCust
     * @return \PPLSDK\StructType\MyApiCountryProduct
     */
    public function setDefaultCust($defaultCust = null)
    {
        // validation for constraint: boolean
        if (!is_null($defaultCust) && !is_bool($defaultCust)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($defaultCust, true), gettype($defaultCust)), __LINE__);
        }
        $this->DefaultCust = $defaultCust;
        return $this;
    }
    /**
     * Get DefaultProduct value
     * @return bool|null
     */
    public function getDefaultProduct()
    {
        return $this->DefaultProduct;
    }
    /**
     * Set DefaultProduct value
     * @param bool $defaultProduct
     * @return \PPLSDK\StructType\MyApiCountryProduct
     */
    public function setDefaultProduct($defaultProduct = null)
    {
        // validation for constraint: boolean
        if (!is_null($defaultProduct) && !is_bool($defaultProduct)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($defaultProduct, true), gettype($defaultProduct)), __LINE__);
        }
        $this->DefaultProduct = $defaultProduct;
        return $this;
    }
    /**
     * Get DefaultStd value
     * @return bool|null
     */
    public function getDefaultStd()
    {
        return $this->DefaultStd;
    }
    /**
     * Set DefaultStd value
     * @param bool $defaultStd
     * @return \PPLSDK\StructType\MyApiCountryProduct
     */
    public function setDefaultStd($defaultStd = null)
    {
        // validation for constraint: boolean
        if (!is_null($defaultStd) && !is_bool($defaultStd)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($defaultStd, true), gettype($defaultStd)), __LINE__);
        }
        $this->DefaultStd = $defaultStd;
        return $this;
    }
    /**
     * Get DefaultSub value
     * @return bool|null
     */
    public function getDefaultSub()
    {
        return $this->DefaultSub;
    }
    /**
     * Set DefaultSub value
     * @param bool $defaultSub
     * @return \PPLSDK\StructType\MyApiCountryProduct
     */
    public function setDefaultSub($defaultSub = null)
    {
        // validation for constraint: boolean
        if (!is_null($defaultSub) && !is_bool($defaultSub)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($defaultSub, true), gettype($defaultSub)), __LINE__);
        }
        $this->DefaultSub = $defaultSub;
        return $this;
    }
    /**
     * Get Export value
     * @return bool|null
     */
    public function getExport()
    {
        return $this->Export;
    }
    /**
     * Set Export value
     * @param bool $export
     * @return \PPLSDK\StructType\MyApiCountryProduct
     */
    public function setExport($export = null)
    {
        // validation for constraint: boolean
        if (!is_null($export) && !is_bool($export)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($export, true), gettype($export)), __LINE__);
        }
        $this->Export = $export;
        return $this;
    }
    /**
     * Get Import value
     * @return bool|null
     */
    public function getImport()
    {
        return $this->Import;
    }
    /**
     * Set Import value
     * @param bool $import
     * @return \PPLSDK\StructType\MyApiCountryProduct
     */
    public function setImport($import = null)
    {
        // validation for constraint: boolean
        if (!is_null($import) && !is_bool($import)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($import, true), gettype($import)), __LINE__);
        }
        $this->Import = $import;
        return $this;
    }
    /**
     * Get IsSupreme value
     * @return bool|null
     */
    public function getIsSupreme()
    {
        return $this->IsSupreme;
    }
    /**
     * Set IsSupreme value
     * @param bool $isSupreme
     * @return \PPLSDK\StructType\MyApiCountryProduct
     */
    public function setIsSupreme($isSupreme = null)
    {
        // validation for constraint: boolean
        if (!is_null($isSupreme) && !is_bool($isSupreme)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isSupreme, true), gettype($isSupreme)), __LINE__);
        }
        $this->IsSupreme = $isSupreme;
        return $this;
    }
    /**
     * Get PptId value
     * @return int|null
     */
    public function getPptId()
    {
        return $this->PptId;
    }
    /**
     * Set PptId value
     * @param int $pptId
     * @return \PPLSDK\StructType\MyApiCountryProduct
     */
    public function setPptId($pptId = null)
    {
        // validation for constraint: int
        if (!is_null($pptId) && !(is_int($pptId) || ctype_digit($pptId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pptId, true), gettype($pptId)), __LINE__);
        }
        $this->PptId = $pptId;
        return $this;
    }
    /**
     * Get ProductCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProductCode()
    {
        return isset($this->ProductCode) ? $this->ProductCode : null;
    }
    /**
     * Set ProductCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $productCode
     * @return \PPLSDK\StructType\MyApiCountryProduct
     */
    public function setProductCode($productCode = null)
    {
        // validation for constraint: string
        if (!is_null($productCode) && !is_string($productCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($productCode, true), gettype($productCode)), __LINE__);
        }
        if (is_null($productCode) || (is_array($productCode) && empty($productCode))) {
            unset($this->ProductCode);
        } else {
            $this->ProductCode = $productCode;
        }
        return $this;
    }
}
