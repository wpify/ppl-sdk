<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for LoginResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:LoginResult
 * @subpackage Structs
 */
class LoginResult extends MyApiResultBaseVoid
{
}
