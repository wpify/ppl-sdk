<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPackagesResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:GetPackagesResult
 * @subpackage Structs
 */
class GetPackagesResult extends MyApiResultBaseOfArrayOfMyApiPackageOutpewOaM6d
{
}
