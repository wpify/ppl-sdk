<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreatePackages StructType
 * @subpackage Structs
 */
class CreatePackages extends AbstractStructBase
{
    /**
     * The Auth
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\AuthenticationData
     */
    public $Auth;
    /**
     * The CustomerUniqueImportId
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var string
     */
    public $CustomerUniqueImportId;
    /**
     * The IntegrId
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var int
     */
    public $IntegrId;
    /**
     * The Packages
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiPackageIn
     */
    public $Packages;
    /**
     * The ReturnChannel
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\ReturnChannelSettings
     */
    public $ReturnChannel;
    /**
     * Constructor method for CreatePackages
     * @uses CreatePackages::setAuth()
     * @uses CreatePackages::setCustomerUniqueImportId()
     * @uses CreatePackages::setIntegrId()
     * @uses CreatePackages::setPackages()
     * @uses CreatePackages::setReturnChannel()
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @param string $customerUniqueImportId
     * @param int $integrId
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageIn $packages
     * @param \PPLSDK\StructType\ReturnChannelSettings $returnChannel
     */
    public function __construct(\PPLSDK\StructType\AuthenticationData $auth = null, $customerUniqueImportId = null, $integrId = null, \PPLSDK\ArrayType\ArrayOfMyApiPackageIn $packages = null, \PPLSDK\StructType\ReturnChannelSettings $returnChannel = null)
    {
        $this
            ->setAuth($auth)
            ->setCustomerUniqueImportId($customerUniqueImportId)
            ->setIntegrId($integrId)
            ->setPackages($packages)
            ->setReturnChannel($returnChannel);
    }
    /**
     * Get Auth value
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function getAuth()
    {
        return $this->Auth;
    }
    /**
     * Set Auth value
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @return \PPLSDK\StructType\CreatePackages
     */
    public function setAuth(\PPLSDK\StructType\AuthenticationData $auth = null)
    {
        $this->Auth = $auth;
        return $this;
    }
    /**
     * Get CustomerUniqueImportId value
     * @return string
     */
    public function getCustomerUniqueImportId()
    {
        return $this->CustomerUniqueImportId;
    }
    /**
     * Set CustomerUniqueImportId value
     * @param string $customerUniqueImportId
     * @return \PPLSDK\StructType\CreatePackages
     */
    public function setCustomerUniqueImportId($customerUniqueImportId = null)
    {
        // validation for constraint: string
        if (!is_null($customerUniqueImportId) && !is_string($customerUniqueImportId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($customerUniqueImportId, true), gettype($customerUniqueImportId)), __LINE__);
        }
        $this->CustomerUniqueImportId = $customerUniqueImportId;
        return $this;
    }
    /**
     * Get IntegrId value
     * @return int
     */
    public function getIntegrId()
    {
        return $this->IntegrId;
    }
    /**
     * Set IntegrId value
     * @param int $integrId
     * @return \PPLSDK\StructType\CreatePackages
     */
    public function setIntegrId($integrId = null)
    {
        // validation for constraint: int
        if (!is_null($integrId) && !(is_int($integrId) || ctype_digit($integrId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($integrId, true), gettype($integrId)), __LINE__);
        }
        $this->IntegrId = $integrId;
        return $this;
    }
    /**
     * Get Packages value
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageIn
     */
    public function getPackages()
    {
        return $this->Packages;
    }
    /**
     * Set Packages value
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageIn $packages
     * @return \PPLSDK\StructType\CreatePackages
     */
    public function setPackages(\PPLSDK\ArrayType\ArrayOfMyApiPackageIn $packages = null)
    {
        $this->Packages = $packages;
        return $this;
    }
    /**
     * Get ReturnChannel value
     * @return \PPLSDK\StructType\ReturnChannelSettings
     */
    public function getReturnChannel()
    {
        return $this->ReturnChannel;
    }
    /**
     * Set ReturnChannel value
     * @param \PPLSDK\StructType\ReturnChannelSettings $returnChannel
     * @return \PPLSDK\StructType\CreatePackages
     */
    public function setReturnChannel(\PPLSDK\StructType\ReturnChannelSettings $returnChannel = null)
    {
        $this->ReturnChannel = $returnChannel;
        return $this;
    }
}
