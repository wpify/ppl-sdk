<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PartnerSaveStatusesResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:PartnerSaveStatusesResult
 * @subpackage Structs
 */
class PartnerSaveStatusesResult extends PartnerResultOfArrayOfPartnerItemResultpewOaM6d
{
}
