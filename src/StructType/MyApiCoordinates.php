<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiCoordinates StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiCoordinates
 * @subpackage Structs
 */
class MyApiCoordinates extends AbstractStructBase
{
    /**
     * The GPS_E_D
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $GPS_E_D;
    /**
     * The GPS_E_M
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $GPS_E_M;
    /**
     * The GPS_E_S
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $GPS_E_S;
    /**
     * The GPS_N_D
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $GPS_N_D;
    /**
     * The GPS_N_M
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $GPS_N_M;
    /**
     * The GPS_N_S
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $GPS_N_S;
    /**
     * Constructor method for MyApiCoordinates
     * @uses MyApiCoordinates::setGPS_E_D()
     * @uses MyApiCoordinates::setGPS_E_M()
     * @uses MyApiCoordinates::setGPS_E_S()
     * @uses MyApiCoordinates::setGPS_N_D()
     * @uses MyApiCoordinates::setGPS_N_M()
     * @uses MyApiCoordinates::setGPS_N_S()
     * @param int $gPS_E_D
     * @param int $gPS_E_M
     * @param float $gPS_E_S
     * @param int $gPS_N_D
     * @param int $gPS_N_M
     * @param float $gPS_N_S
     */
    public function __construct($gPS_E_D = null, $gPS_E_M = null, $gPS_E_S = null, $gPS_N_D = null, $gPS_N_M = null, $gPS_N_S = null)
    {
        $this
            ->setGPS_E_D($gPS_E_D)
            ->setGPS_E_M($gPS_E_M)
            ->setGPS_E_S($gPS_E_S)
            ->setGPS_N_D($gPS_N_D)
            ->setGPS_N_M($gPS_N_M)
            ->setGPS_N_S($gPS_N_S);
    }
    /**
     * Get GPS_E_D value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getGPS_E_D()
    {
        return isset($this->GPS_E_D) ? $this->GPS_E_D : null;
    }
    /**
     * Set GPS_E_D value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $gPS_E_D
     * @return \PPLSDK\StructType\MyApiCoordinates
     */
    public function setGPS_E_D($gPS_E_D = null)
    {
        // validation for constraint: int
        if (!is_null($gPS_E_D) && !(is_int($gPS_E_D) || ctype_digit($gPS_E_D))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($gPS_E_D, true), gettype($gPS_E_D)), __LINE__);
        }
        if (is_null($gPS_E_D) || (is_array($gPS_E_D) && empty($gPS_E_D))) {
            unset($this->GPS_E_D);
        } else {
            $this->GPS_E_D = $gPS_E_D;
        }
        return $this;
    }
    /**
     * Get GPS_E_M value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getGPS_E_M()
    {
        return isset($this->GPS_E_M) ? $this->GPS_E_M : null;
    }
    /**
     * Set GPS_E_M value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $gPS_E_M
     * @return \PPLSDK\StructType\MyApiCoordinates
     */
    public function setGPS_E_M($gPS_E_M = null)
    {
        // validation for constraint: int
        if (!is_null($gPS_E_M) && !(is_int($gPS_E_M) || ctype_digit($gPS_E_M))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($gPS_E_M, true), gettype($gPS_E_M)), __LINE__);
        }
        if (is_null($gPS_E_M) || (is_array($gPS_E_M) && empty($gPS_E_M))) {
            unset($this->GPS_E_M);
        } else {
            $this->GPS_E_M = $gPS_E_M;
        }
        return $this;
    }
    /**
     * Get GPS_E_S value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getGPS_E_S()
    {
        return isset($this->GPS_E_S) ? $this->GPS_E_S : null;
    }
    /**
     * Set GPS_E_S value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $gPS_E_S
     * @return \PPLSDK\StructType\MyApiCoordinates
     */
    public function setGPS_E_S($gPS_E_S = null)
    {
        // validation for constraint: float
        if (!is_null($gPS_E_S) && !(is_float($gPS_E_S) || is_numeric($gPS_E_S))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($gPS_E_S, true), gettype($gPS_E_S)), __LINE__);
        }
        if (is_null($gPS_E_S) || (is_array($gPS_E_S) && empty($gPS_E_S))) {
            unset($this->GPS_E_S);
        } else {
            $this->GPS_E_S = $gPS_E_S;
        }
        return $this;
    }
    /**
     * Get GPS_N_D value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getGPS_N_D()
    {
        return isset($this->GPS_N_D) ? $this->GPS_N_D : null;
    }
    /**
     * Set GPS_N_D value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $gPS_N_D
     * @return \PPLSDK\StructType\MyApiCoordinates
     */
    public function setGPS_N_D($gPS_N_D = null)
    {
        // validation for constraint: int
        if (!is_null($gPS_N_D) && !(is_int($gPS_N_D) || ctype_digit($gPS_N_D))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($gPS_N_D, true), gettype($gPS_N_D)), __LINE__);
        }
        if (is_null($gPS_N_D) || (is_array($gPS_N_D) && empty($gPS_N_D))) {
            unset($this->GPS_N_D);
        } else {
            $this->GPS_N_D = $gPS_N_D;
        }
        return $this;
    }
    /**
     * Get GPS_N_M value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getGPS_N_M()
    {
        return isset($this->GPS_N_M) ? $this->GPS_N_M : null;
    }
    /**
     * Set GPS_N_M value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $gPS_N_M
     * @return \PPLSDK\StructType\MyApiCoordinates
     */
    public function setGPS_N_M($gPS_N_M = null)
    {
        // validation for constraint: int
        if (!is_null($gPS_N_M) && !(is_int($gPS_N_M) || ctype_digit($gPS_N_M))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($gPS_N_M, true), gettype($gPS_N_M)), __LINE__);
        }
        if (is_null($gPS_N_M) || (is_array($gPS_N_M) && empty($gPS_N_M))) {
            unset($this->GPS_N_M);
        } else {
            $this->GPS_N_M = $gPS_N_M;
        }
        return $this;
    }
    /**
     * Get GPS_N_S value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getGPS_N_S()
    {
        return isset($this->GPS_N_S) ? $this->GPS_N_S : null;
    }
    /**
     * Set GPS_N_S value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $gPS_N_S
     * @return \PPLSDK\StructType\MyApiCoordinates
     */
    public function setGPS_N_S($gPS_N_S = null)
    {
        // validation for constraint: float
        if (!is_null($gPS_N_S) && !(is_float($gPS_N_S) || is_numeric($gPS_N_S))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($gPS_N_S, true), gettype($gPS_N_S)), __LINE__);
        }
        if (is_null($gPS_N_S) || (is_array($gPS_N_S) && empty($gPS_N_S))) {
            unset($this->GPS_N_S);
        } else {
            $this->GPS_N_S = $gPS_N_S;
        }
        return $this;
    }
}
