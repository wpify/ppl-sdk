<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Version StructType
 * @subpackage Structs
 */
class Version extends AbstractStructBase
{
}
