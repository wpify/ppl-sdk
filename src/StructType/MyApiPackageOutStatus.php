<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiPackageOutStatus StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiPackageOutStatus
 * @subpackage Structs
 */
class MyApiPackageOutStatus extends AbstractStructBase
{
    /**
     * The DelivPerson
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DelivPerson;
    /**
     * The Note
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Note;
    /**
     * The Note2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Note2;
    /**
     * The StaID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $StaID;
    /**
     * The StatusDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $StatusDate;
    /**
     * The StatusName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $StatusName;
    /**
     * Constructor method for MyApiPackageOutStatus
     * @uses MyApiPackageOutStatus::setDelivPerson()
     * @uses MyApiPackageOutStatus::setNote()
     * @uses MyApiPackageOutStatus::setNote2()
     * @uses MyApiPackageOutStatus::setStaID()
     * @uses MyApiPackageOutStatus::setStatusDate()
     * @uses MyApiPackageOutStatus::setStatusName()
     * @param string $delivPerson
     * @param string $note
     * @param string $note2
     * @param int $staID
     * @param string $statusDate
     * @param string $statusName
     */
    public function __construct($delivPerson = null, $note = null, $note2 = null, $staID = null, $statusDate = null, $statusName = null)
    {
        $this
            ->setDelivPerson($delivPerson)
            ->setNote($note)
            ->setNote2($note2)
            ->setStaID($staID)
            ->setStatusDate($statusDate)
            ->setStatusName($statusName);
    }
    /**
     * Get DelivPerson value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDelivPerson()
    {
        return isset($this->DelivPerson) ? $this->DelivPerson : null;
    }
    /**
     * Set DelivPerson value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $delivPerson
     * @return \PPLSDK\StructType\MyApiPackageOutStatus
     */
    public function setDelivPerson($delivPerson = null)
    {
        // validation for constraint: string
        if (!is_null($delivPerson) && !is_string($delivPerson)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($delivPerson, true), gettype($delivPerson)), __LINE__);
        }
        if (is_null($delivPerson) || (is_array($delivPerson) && empty($delivPerson))) {
            unset($this->DelivPerson);
        } else {
            $this->DelivPerson = $delivPerson;
        }
        return $this;
    }
    /**
     * Get Note value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNote()
    {
        return isset($this->Note) ? $this->Note : null;
    }
    /**
     * Set Note value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $note
     * @return \PPLSDK\StructType\MyApiPackageOutStatus
     */
    public function setNote($note = null)
    {
        // validation for constraint: string
        if (!is_null($note) && !is_string($note)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($note, true), gettype($note)), __LINE__);
        }
        if (is_null($note) || (is_array($note) && empty($note))) {
            unset($this->Note);
        } else {
            $this->Note = $note;
        }
        return $this;
    }
    /**
     * Get Note2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNote2()
    {
        return isset($this->Note2) ? $this->Note2 : null;
    }
    /**
     * Set Note2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $note2
     * @return \PPLSDK\StructType\MyApiPackageOutStatus
     */
    public function setNote2($note2 = null)
    {
        // validation for constraint: string
        if (!is_null($note2) && !is_string($note2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($note2, true), gettype($note2)), __LINE__);
        }
        if (is_null($note2) || (is_array($note2) && empty($note2))) {
            unset($this->Note2);
        } else {
            $this->Note2 = $note2;
        }
        return $this;
    }
    /**
     * Get StaID value
     * @return int|null
     */
    public function getStaID()
    {
        return $this->StaID;
    }
    /**
     * Set StaID value
     * @param int $staID
     * @return \PPLSDK\StructType\MyApiPackageOutStatus
     */
    public function setStaID($staID = null)
    {
        // validation for constraint: int
        if (!is_null($staID) && !(is_int($staID) || ctype_digit($staID))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($staID, true), gettype($staID)), __LINE__);
        }
        $this->StaID = $staID;
        return $this;
    }
    /**
     * Get StatusDate value
     * @return string|null
     */
    public function getStatusDate()
    {
        return $this->StatusDate;
    }
    /**
     * Set StatusDate value
     * @param string $statusDate
     * @return \PPLSDK\StructType\MyApiPackageOutStatus
     */
    public function setStatusDate($statusDate = null)
    {
        // validation for constraint: string
        if (!is_null($statusDate) && !is_string($statusDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($statusDate, true), gettype($statusDate)), __LINE__);
        }
        $this->StatusDate = $statusDate;
        return $this;
    }
    /**
     * Get StatusName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStatusName()
    {
        return isset($this->StatusName) ? $this->StatusName : null;
    }
    /**
     * Set StatusName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $statusName
     * @return \PPLSDK\StructType\MyApiPackageOutStatus
     */
    public function setStatusName($statusName = null)
    {
        // validation for constraint: string
        if (!is_null($statusName) && !is_string($statusName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($statusName, true), gettype($statusName)), __LINE__);
        }
        if (is_null($statusName) || (is_array($statusName) && empty($statusName))) {
            unset($this->StatusName);
        } else {
            $this->StatusName = $statusName;
        }
        return $this;
    }
}
