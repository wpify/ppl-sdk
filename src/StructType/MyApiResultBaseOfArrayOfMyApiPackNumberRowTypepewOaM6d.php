<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiResultBaseOfArrayOfMyApiPackNumberRowTypepewOaM6d
 * StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiResultBaseOfArrayOfMyApiPackNumberRowTypepewOaM6d
 * @subpackage Structs
 */
class MyApiResultBaseOfArrayOfMyApiPackNumberRowTypepewOaM6d extends MyApiResultBaseVoid
{
    /**
     * The ResultData
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiPackNumberRowType
     */
    public $ResultData;
    /**
     * Constructor method for MyApiResultBaseOfArrayOfMyApiPackNumberRowTypepewOaM6d
     * @uses MyApiResultBaseOfArrayOfMyApiPackNumberRowTypepewOaM6d::setResultData()
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackNumberRowType $resultData
     */
    public function __construct(\PPLSDK\ArrayType\ArrayOfMyApiPackNumberRowType $resultData = null)
    {
        $this
            ->setResultData($resultData);
    }
    /**
     * Get ResultData value
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackNumberRowType|null
     */
    public function getResultData()
    {
        return $this->ResultData;
    }
    /**
     * Set ResultData value
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackNumberRowType $resultData
     * @return \PPLSDK\StructType\MyApiResultBaseOfArrayOfMyApiPackNumberRowTypepewOaM6d
     */
    public function setResultData(\PPLSDK\ArrayType\ArrayOfMyApiPackNumberRowType $resultData = null)
    {
        $this->ResultData = $resultData;
        return $this;
    }
}
