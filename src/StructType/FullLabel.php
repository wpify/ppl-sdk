<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FullLabel StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:FullLabel
 * @subpackage Structs
 */
class FullLabel extends AbstractStructBase
{
    /**
     * The FullLabelUrl
     * @var bool
     */
    public $FullLabelUrl;
    /**
     * The PageSize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $PageSize;
    /**
     * The Position
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $Position;
    /**
     * Constructor method for FullLabel
     * @uses FullLabel::setFullLabelUrl()
     * @uses FullLabel::setPageSize()
     * @uses FullLabel::setPosition()
     * @param bool $fullLabelUrl
     * @param string $pageSize
     * @param int $position
     */
    public function __construct($fullLabelUrl = null, $pageSize = null, $position = null)
    {
        $this
            ->setFullLabelUrl($fullLabelUrl)
            ->setPageSize($pageSize)
            ->setPosition($position);
    }
    /**
     * Get FullLabelUrl value
     * @return bool|null
     */
    public function getFullLabelUrl()
    {
        return $this->FullLabelUrl;
    }
    /**
     * Set FullLabelUrl value
     * @param bool $fullLabelUrl
     * @return \PPLSDK\StructType\FullLabel
     */
    public function setFullLabelUrl($fullLabelUrl = null)
    {
        // validation for constraint: boolean
        if (!is_null($fullLabelUrl) && !is_bool($fullLabelUrl)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($fullLabelUrl, true), gettype($fullLabelUrl)), __LINE__);
        }
        $this->FullLabelUrl = $fullLabelUrl;
        return $this;
    }
    /**
     * Get PageSize value
     * @return string|null
     */
    public function getPageSize()
    {
        return $this->PageSize;
    }
    /**
     * Set PageSize value
     * @uses \PPLSDK\EnumType\PdfPageSize::valueIsValid()
     * @uses \PPLSDK\EnumType\PdfPageSize::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $pageSize
     * @return \PPLSDK\StructType\FullLabel
     */
    public function setPageSize($pageSize = null)
    {
        // validation for constraint: enumeration
        if (!\PPLSDK\EnumType\PdfPageSize::valueIsValid($pageSize)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \PPLSDK\EnumType\PdfPageSize', is_array($pageSize) ? implode(', ', $pageSize) : var_export($pageSize, true), implode(', ', \PPLSDK\EnumType\PdfPageSize::getValidValues())), __LINE__);
        }
        $this->PageSize = $pageSize;
        return $this;
    }
    /**
     * Get Position value
     * @return int|null
     */
    public function getPosition()
    {
        return $this->Position;
    }
    /**
     * Set Position value
     * @param int $position
     * @return \PPLSDK\StructType\FullLabel
     */
    public function setPosition($position = null)
    {
        // validation for constraint: int
        if (!is_null($position) && !(is_int($position) || ctype_digit($position))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($position, true), gettype($position)), __LINE__);
        }
        $this->Position = $position;
        return $this;
    }
}
