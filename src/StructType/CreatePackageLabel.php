<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreatePackageLabel StructType
 * @subpackage Structs
 */
class CreatePackageLabel extends AbstractStructBase
{
    /**
     * The Auth
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\AuthenticationData
     */
    public $Auth;
    /**
     * The Packages
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiPackageIn2
     */
    public $Packages;
    /**
     * The ReturnChannel
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\LabelReturnChannelSettings
     */
    public $ReturnChannel;
    /**
     * Constructor method for CreatePackageLabel
     * @uses CreatePackageLabel::setAuth()
     * @uses CreatePackageLabel::setPackages()
     * @uses CreatePackageLabel::setReturnChannel()
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageIn2 $packages
     * @param \PPLSDK\StructType\LabelReturnChannelSettings $returnChannel
     */
    public function __construct(\PPLSDK\StructType\AuthenticationData $auth = null, \PPLSDK\ArrayType\ArrayOfMyApiPackageIn2 $packages = null, \PPLSDK\StructType\LabelReturnChannelSettings $returnChannel = null)
    {
        $this
            ->setAuth($auth)
            ->setPackages($packages)
            ->setReturnChannel($returnChannel);
    }
    /**
     * Get Auth value
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function getAuth()
    {
        return $this->Auth;
    }
    /**
     * Set Auth value
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @return \PPLSDK\StructType\CreatePackageLabel
     */
    public function setAuth(\PPLSDK\StructType\AuthenticationData $auth = null)
    {
        $this->Auth = $auth;
        return $this;
    }
    /**
     * Get Packages value
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageIn2
     */
    public function getPackages()
    {
        return $this->Packages;
    }
    /**
     * Set Packages value
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageIn2 $packages
     * @return \PPLSDK\StructType\CreatePackageLabel
     */
    public function setPackages(\PPLSDK\ArrayType\ArrayOfMyApiPackageIn2 $packages = null)
    {
        $this->Packages = $packages;
        return $this;
    }
    /**
     * Get ReturnChannel value
     * @return \PPLSDK\StructType\LabelReturnChannelSettings
     */
    public function getReturnChannel()
    {
        return $this->ReturnChannel;
    }
    /**
     * Set ReturnChannel value
     * @param \PPLSDK\StructType\LabelReturnChannelSettings $returnChannel
     * @return \PPLSDK\StructType\CreatePackageLabel
     */
    public function setReturnChannel(\PPLSDK\StructType\LabelReturnChannelSettings $returnChannel = null)
    {
        $this->ReturnChannel = $returnChannel;
        return $this;
    }
}
