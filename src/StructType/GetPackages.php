<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPackages StructType
 * @subpackage Structs
 */
class GetPackages extends AbstractStructBase
{
    /**
     * The Auth
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\AuthenticationData
     */
    public $Auth;
    /**
     * The Filter
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\PackagesFilter
     */
    public $Filter;
    /**
     * Constructor method for GetPackages
     * @uses GetPackages::setAuth()
     * @uses GetPackages::setFilter()
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @param \PPLSDK\StructType\PackagesFilter $filter
     */
    public function __construct(\PPLSDK\StructType\AuthenticationData $auth = null, \PPLSDK\StructType\PackagesFilter $filter = null)
    {
        $this
            ->setAuth($auth)
            ->setFilter($filter);
    }
    /**
     * Get Auth value
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function getAuth()
    {
        return $this->Auth;
    }
    /**
     * Set Auth value
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @return \PPLSDK\StructType\GetPackages
     */
    public function setAuth(\PPLSDK\StructType\AuthenticationData $auth = null)
    {
        $this->Auth = $auth;
        return $this;
    }
    /**
     * Get Filter value
     * @return \PPLSDK\StructType\PackagesFilter
     */
    public function getFilter()
    {
        return $this->Filter;
    }
    /**
     * Set Filter value
     * @param \PPLSDK\StructType\PackagesFilter $filter
     * @return \PPLSDK\StructType\GetPackages
     */
    public function setFilter(\PPLSDK\StructType\PackagesFilter $filter = null)
    {
        $this->Filter = $filter;
        return $this;
    }
}
