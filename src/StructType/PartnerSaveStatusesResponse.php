<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PartnerSaveStatusesResponse StructType
 * @subpackage Structs
 */
class PartnerSaveStatusesResponse extends AbstractStructBase
{
    /**
     * The PartnerSaveStatusesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\PartnerSaveStatusesResult
     */
    public $PartnerSaveStatusesResult;
    /**
     * Constructor method for PartnerSaveStatusesResponse
     * @uses PartnerSaveStatusesResponse::setPartnerSaveStatusesResult()
     * @param \PPLSDK\StructType\PartnerSaveStatusesResult $partnerSaveStatusesResult
     */
    public function __construct(\PPLSDK\StructType\PartnerSaveStatusesResult $partnerSaveStatusesResult = null)
    {
        $this
            ->setPartnerSaveStatusesResult($partnerSaveStatusesResult);
    }
    /**
     * Get PartnerSaveStatusesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\PartnerSaveStatusesResult|null
     */
    public function getPartnerSaveStatusesResult()
    {
        return isset($this->PartnerSaveStatusesResult) ? $this->PartnerSaveStatusesResult : null;
    }
    /**
     * Set PartnerSaveStatusesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\PartnerSaveStatusesResult $partnerSaveStatusesResult
     * @return \PPLSDK\StructType\PartnerSaveStatusesResponse
     */
    public function setPartnerSaveStatusesResult(\PPLSDK\StructType\PartnerSaveStatusesResult $partnerSaveStatusesResult = null)
    {
        if (is_null($partnerSaveStatusesResult) || (is_array($partnerSaveStatusesResult) && empty($partnerSaveStatusesResult))) {
            unset($this->PartnerSaveStatusesResult);
        } else {
            $this->PartnerSaveStatusesResult = $partnerSaveStatusesResult;
        }
        return $this;
    }
}
