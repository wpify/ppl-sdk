<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreatePackageLabelRequestResultData StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:CreatePackageLabelRequestResultData
 * @subpackage Structs
 */
class CreatePackageLabelRequestResultData extends AbstractStructBase
{
    /**
     * The Items
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfCreatePackageLabelResultItemData
     */
    public $Items;
    /**
     * The FullLabel
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\FullLabelResultData
     */
    public $FullLabel;
    /**
     * Constructor method for CreatePackageLabelRequestResultData
     * @uses CreatePackageLabelRequestResultData::setItems()
     * @uses CreatePackageLabelRequestResultData::setFullLabel()
     * @param \PPLSDK\ArrayType\ArrayOfCreatePackageLabelResultItemData $items
     * @param \PPLSDK\StructType\FullLabelResultData $fullLabel
     */
    public function __construct(\PPLSDK\ArrayType\ArrayOfCreatePackageLabelResultItemData $items = null, \PPLSDK\StructType\FullLabelResultData $fullLabel = null)
    {
        $this
            ->setItems($items)
            ->setFullLabel($fullLabel);
    }
    /**
     * Get Items value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\ArrayType\ArrayOfCreatePackageLabelResultItemData|null
     */
    public function getItems()
    {
        return isset($this->Items) ? $this->Items : null;
    }
    /**
     * Set Items value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\ArrayType\ArrayOfCreatePackageLabelResultItemData $items
     * @return \PPLSDK\StructType\CreatePackageLabelRequestResultData
     */
    public function setItems(\PPLSDK\ArrayType\ArrayOfCreatePackageLabelResultItemData $items = null)
    {
        if (is_null($items) || (is_array($items) && empty($items))) {
            unset($this->Items);
        } else {
            $this->Items = $items;
        }
        return $this;
    }
    /**
     * Get FullLabel value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\FullLabelResultData|null
     */
    public function getFullLabel()
    {
        return isset($this->FullLabel) ? $this->FullLabel : null;
    }
    /**
     * Set FullLabel value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\FullLabelResultData $fullLabel
     * @return \PPLSDK\StructType\CreatePackageLabelRequestResultData
     */
    public function setFullLabel(\PPLSDK\StructType\FullLabelResultData $fullLabel = null)
    {
        if (is_null($fullLabel) || (is_array($fullLabel) && empty($fullLabel))) {
            unset($this->FullLabel);
        } else {
            $this->FullLabel = $fullLabel;
        }
        return $this;
    }
}
