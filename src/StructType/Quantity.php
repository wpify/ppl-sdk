<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Quantity StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:Quantity
 * @subpackage Structs
 */
class Quantity extends AbstractStructBase
{
    /**
     * The From
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $From;
    /**
     * The To
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $To;
    /**
     * The Available
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $Available;
    /**
     * Constructor method for Quantity
     * @uses Quantity::setFrom()
     * @uses Quantity::setTo()
     * @uses Quantity::setAvailable()
     * @param string $from
     * @param string $to
     * @param int $available
     */
    public function __construct($from = null, $to = null, $available = null)
    {
        $this
            ->setFrom($from)
            ->setTo($to)
            ->setAvailable($available);
    }
    /**
     * Get From value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFrom()
    {
        return isset($this->From) ? $this->From : null;
    }
    /**
     * Set From value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $from
     * @return \PPLSDK\StructType\Quantity
     */
    public function setFrom($from = null)
    {
        // validation for constraint: string
        if (!is_null($from) && !is_string($from)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($from, true), gettype($from)), __LINE__);
        }
        if (is_null($from) || (is_array($from) && empty($from))) {
            unset($this->From);
        } else {
            $this->From = $from;
        }
        return $this;
    }
    /**
     * Get To value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTo()
    {
        return isset($this->To) ? $this->To : null;
    }
    /**
     * Set To value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $to
     * @return \PPLSDK\StructType\Quantity
     */
    public function setTo($to = null)
    {
        // validation for constraint: string
        if (!is_null($to) && !is_string($to)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($to, true), gettype($to)), __LINE__);
        }
        if (is_null($to) || (is_array($to) && empty($to))) {
            unset($this->To);
        } else {
            $this->To = $to;
        }
        return $this;
    }
    /**
     * Get Available value
     * @return int|null
     */
    public function getAvailable()
    {
        return $this->Available;
    }
    /**
     * Set Available value
     * @param int $available
     * @return \PPLSDK\StructType\Quantity
     */
    public function setAvailable($available = null)
    {
        // validation for constraint: int
        if (!is_null($available) && !(is_int($available) || ctype_digit($available))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($available, true), gettype($available)), __LINE__);
        }
        $this->Available = $available;
        return $this;
    }
}
