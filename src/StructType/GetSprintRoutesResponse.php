<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSprintRoutesResponse StructType
 * @subpackage Structs
 */
class GetSprintRoutesResponse extends AbstractStructBase
{
    /**
     * The GetSprintRoutesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\GetSprintRoutesResult
     */
    public $GetSprintRoutesResult;
    /**
     * Constructor method for GetSprintRoutesResponse
     * @uses GetSprintRoutesResponse::setGetSprintRoutesResult()
     * @param \PPLSDK\StructType\GetSprintRoutesResult $getSprintRoutesResult
     */
    public function __construct(\PPLSDK\StructType\GetSprintRoutesResult $getSprintRoutesResult = null)
    {
        $this
            ->setGetSprintRoutesResult($getSprintRoutesResult);
    }
    /**
     * Get GetSprintRoutesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\GetSprintRoutesResult|null
     */
    public function getGetSprintRoutesResult()
    {
        return isset($this->GetSprintRoutesResult) ? $this->GetSprintRoutesResult : null;
    }
    /**
     * Set GetSprintRoutesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\GetSprintRoutesResult $getSprintRoutesResult
     * @return \PPLSDK\StructType\GetSprintRoutesResponse
     */
    public function setGetSprintRoutesResult(\PPLSDK\StructType\GetSprintRoutesResult $getSprintRoutesResult = null)
    {
        if (is_null($getSprintRoutesResult) || (is_array($getSprintRoutesResult) && empty($getSprintRoutesResult))) {
            unset($this->GetSprintRoutesResult);
        } else {
            $this->GetSprintRoutesResult = $getSprintRoutesResult;
        }
        return $this;
    }
}
