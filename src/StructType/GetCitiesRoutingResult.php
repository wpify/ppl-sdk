<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCitiesRoutingResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:GetCitiesRoutingResult
 * @subpackage Structs
 */
class GetCitiesRoutingResult extends MyApiResultBaseOfArrayOfMyApiCityRoutingpewOaM6d
{
}
