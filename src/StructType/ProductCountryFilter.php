<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ProductCountryFilter StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ProductCountryFilter
 * @subpackage Structs
 */
class ProductCountryFilter extends AbstractStructBase
{
    /**
     * The CustId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $CustId;
    /**
     * The CouCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CouCode;
    /**
     * The Available
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $Available;
    /**
     * The Cod
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $Cod;
    /**
     * The NoHomeCountry
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $NoHomeCountry;
    /**
     * Constructor method for ProductCountryFilter
     * @uses ProductCountryFilter::setCustId()
     * @uses ProductCountryFilter::setCouCode()
     * @uses ProductCountryFilter::setAvailable()
     * @uses ProductCountryFilter::setCod()
     * @uses ProductCountryFilter::setNoHomeCountry()
     * @param int $custId
     * @param string $couCode
     * @param bool $available
     * @param bool $cod
     * @param bool $noHomeCountry
     */
    public function __construct($custId = null, $couCode = null, $available = null, $cod = null, $noHomeCountry = null)
    {
        $this
            ->setCustId($custId)
            ->setCouCode($couCode)
            ->setAvailable($available)
            ->setCod($cod)
            ->setNoHomeCountry($noHomeCountry);
    }
    /**
     * Get CustId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getCustId()
    {
        return isset($this->CustId) ? $this->CustId : null;
    }
    /**
     * Set CustId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $custId
     * @return \PPLSDK\StructType\ProductCountryFilter
     */
    public function setCustId($custId = null)
    {
        // validation for constraint: int
        if (!is_null($custId) && !(is_int($custId) || ctype_digit($custId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($custId, true), gettype($custId)), __LINE__);
        }
        if (is_null($custId) || (is_array($custId) && empty($custId))) {
            unset($this->CustId);
        } else {
            $this->CustId = $custId;
        }
        return $this;
    }
    /**
     * Get CouCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCouCode()
    {
        return isset($this->CouCode) ? $this->CouCode : null;
    }
    /**
     * Set CouCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $couCode
     * @return \PPLSDK\StructType\ProductCountryFilter
     */
    public function setCouCode($couCode = null)
    {
        // validation for constraint: string
        if (!is_null($couCode) && !is_string($couCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($couCode, true), gettype($couCode)), __LINE__);
        }
        if (is_null($couCode) || (is_array($couCode) && empty($couCode))) {
            unset($this->CouCode);
        } else {
            $this->CouCode = $couCode;
        }
        return $this;
    }
    /**
     * Get Available value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getAvailable()
    {
        return isset($this->Available) ? $this->Available : null;
    }
    /**
     * Set Available value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $available
     * @return \PPLSDK\StructType\ProductCountryFilter
     */
    public function setAvailable($available = null)
    {
        // validation for constraint: boolean
        if (!is_null($available) && !is_bool($available)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($available, true), gettype($available)), __LINE__);
        }
        if (is_null($available) || (is_array($available) && empty($available))) {
            unset($this->Available);
        } else {
            $this->Available = $available;
        }
        return $this;
    }
    /**
     * Get Cod value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getCod()
    {
        return isset($this->Cod) ? $this->Cod : null;
    }
    /**
     * Set Cod value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $cod
     * @return \PPLSDK\StructType\ProductCountryFilter
     */
    public function setCod($cod = null)
    {
        // validation for constraint: boolean
        if (!is_null($cod) && !is_bool($cod)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($cod, true), gettype($cod)), __LINE__);
        }
        if (is_null($cod) || (is_array($cod) && empty($cod))) {
            unset($this->Cod);
        } else {
            $this->Cod = $cod;
        }
        return $this;
    }
    /**
     * Get NoHomeCountry value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getNoHomeCountry()
    {
        return isset($this->NoHomeCountry) ? $this->NoHomeCountry : null;
    }
    /**
     * Set NoHomeCountry value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $noHomeCountry
     * @return \PPLSDK\StructType\ProductCountryFilter
     */
    public function setNoHomeCountry($noHomeCountry = null)
    {
        // validation for constraint: boolean
        if (!is_null($noHomeCountry) && !is_bool($noHomeCountry)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($noHomeCountry, true), gettype($noHomeCountry)), __LINE__);
        }
        if (is_null($noHomeCountry) || (is_array($noHomeCountry) && empty($noHomeCountry))) {
            unset($this->NoHomeCountry);
        } else {
            $this->NoHomeCountry = $noHomeCountry;
        }
        return $this;
    }
}
