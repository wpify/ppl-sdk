<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiResultBaseOfArrayOfPartnerItemResultpewOaM6d
 * StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiResultBaseOfArrayOfPartnerItemResultpewOaM6d
 * @subpackage Structs
 */
class MyApiResultBaseOfArrayOfPartnerItemResultpewOaM6d extends MyApiResultBaseVoid
{
    /**
     * The ResultData
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfPartnerItemResult
     */
    public $ResultData;
    /**
     * Constructor method for MyApiResultBaseOfArrayOfPartnerItemResultpewOaM6d
     * @uses MyApiResultBaseOfArrayOfPartnerItemResultpewOaM6d::setResultData()
     * @param \PPLSDK\ArrayType\ArrayOfPartnerItemResult $resultData
     */
    public function __construct(\PPLSDK\ArrayType\ArrayOfPartnerItemResult $resultData = null)
    {
        $this
            ->setResultData($resultData);
    }
    /**
     * Get ResultData value
     * @return \PPLSDK\ArrayType\ArrayOfPartnerItemResult|null
     */
    public function getResultData()
    {
        return $this->ResultData;
    }
    /**
     * Set ResultData value
     * @param \PPLSDK\ArrayType\ArrayOfPartnerItemResult $resultData
     * @return \PPLSDK\StructType\MyApiResultBaseOfArrayOfPartnerItemResultpewOaM6d
     */
    public function setResultData(\PPLSDK\ArrayType\ArrayOfPartnerItemResult $resultData = null)
    {
        $this->ResultData = $resultData;
        return $this;
    }
}
