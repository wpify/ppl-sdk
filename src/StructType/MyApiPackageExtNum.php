<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiPackageExtNum StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiPackageExtNum
 * @subpackage Structs
 */
class MyApiPackageExtNum extends AbstractStructBase
{
    /**
     * The Code
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Code;
    /**
     * The ExtNumber
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $ExtNumber;
    /**
     * Constructor method for MyApiPackageExtNum
     * @uses MyApiPackageExtNum::setCode()
     * @uses MyApiPackageExtNum::setExtNumber()
     * @param string $code
     * @param string $extNumber
     */
    public function __construct($code = null, $extNumber = null)
    {
        $this
            ->setCode($code)
            ->setExtNumber($extNumber);
    }
    /**
     * Get Code value
     * @return string|null
     */
    public function getCode()
    {
        return $this->Code;
    }
    /**
     * Set Code value
     * @param string $code
     * @return \PPLSDK\StructType\MyApiPackageExtNum
     */
    public function setCode($code = null)
    {
        // validation for constraint: string
        if (!is_null($code) && !is_string($code)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($code, true), gettype($code)), __LINE__);
        }
        $this->Code = $code;
        return $this;
    }
    /**
     * Get ExtNumber value
     * @return string|null
     */
    public function getExtNumber()
    {
        return $this->ExtNumber;
    }
    /**
     * Set ExtNumber value
     * @param string $extNumber
     * @return \PPLSDK\StructType\MyApiPackageExtNum
     */
    public function setExtNumber($extNumber = null)
    {
        // validation for constraint: string
        if (!is_null($extNumber) && !is_string($extNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($extNumber, true), gettype($extNumber)), __LINE__);
        }
        $this->ExtNumber = $extNumber;
        return $this;
    }
}
