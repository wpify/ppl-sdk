<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiCountryProducts StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiCountryProducts
 * @subpackage Structs
 */
class MyApiCountryProducts extends AbstractStructBase
{
    /**
     * The CountryCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CountryCode;
    /**
     * The Product
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiCountryProduct
     */
    public $Product;
    /**
     * Constructor method for MyApiCountryProducts
     * @uses MyApiCountryProducts::setCountryCode()
     * @uses MyApiCountryProducts::setProduct()
     * @param string $countryCode
     * @param \PPLSDK\StructType\MyApiCountryProduct $product
     */
    public function __construct($countryCode = null, \PPLSDK\StructType\MyApiCountryProduct $product = null)
    {
        $this
            ->setCountryCode($countryCode)
            ->setProduct($product);
    }
    /**
     * Get CountryCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryCode()
    {
        return isset($this->CountryCode) ? $this->CountryCode : null;
    }
    /**
     * Set CountryCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryCode
     * @return \PPLSDK\StructType\MyApiCountryProducts
     */
    public function setCountryCode($countryCode = null)
    {
        // validation for constraint: string
        if (!is_null($countryCode) && !is_string($countryCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryCode, true), gettype($countryCode)), __LINE__);
        }
        if (is_null($countryCode) || (is_array($countryCode) && empty($countryCode))) {
            unset($this->CountryCode);
        } else {
            $this->CountryCode = $countryCode;
        }
        return $this;
    }
    /**
     * Get Product value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiCountryProduct|null
     */
    public function getProduct()
    {
        return isset($this->Product) ? $this->Product : null;
    }
    /**
     * Set Product value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\MyApiCountryProduct $product
     * @return \PPLSDK\StructType\MyApiCountryProducts
     */
    public function setProduct(\PPLSDK\StructType\MyApiCountryProduct $product = null)
    {
        if (is_null($product) || (is_array($product) && empty($product))) {
            unset($this->Product);
        } else {
            $this->Product = $product;
        }
        return $this;
    }
}
