<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AuthorizationFault StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:AuthorizationFault
 * @subpackage Structs
 */
class AuthorizationFault extends MyApiFault
{
}
