<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetNumberRangeResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:GetNumberRangeResult
 * @subpackage Structs
 */
class GetNumberRangeResult extends MyApiResultBaseOfArrayOfNumberRangepewOaM6d
{
}
