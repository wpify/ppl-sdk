<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiCityRouting StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiCityRouting
 * @subpackage Structs
 */
class MyApiCityRouting extends AbstractStructBase
{
    /**
     * The Changed
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Changed;
    /**
     * The City
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $City;
    /**
     * The CountryCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CountryCode;
    /**
     * The Created
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $Created;
    /**
     * The DepoCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DepoCode;
    /**
     * The Highlighted
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $Highlighted;
    /**
     * The PackProductName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PackProductName;
    /**
     * The PackProductType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PackProductType;
    /**
     * The Post
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Post;
    /**
     * The Region
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Region;
    /**
     * The Reject
     * @var bool
     */
    public $Reject;
    /**
     * The RouteCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $RouteCode;
    /**
     * The Services
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiCityRouteSvc
     */
    public $Services;
    /**
     * The Street
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Street;
    /**
     * The ZipCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $ZipCode;
    /**
     * Constructor method for MyApiCityRouting
     * @uses MyApiCityRouting::setChanged()
     * @uses MyApiCityRouting::setCity()
     * @uses MyApiCityRouting::setCountryCode()
     * @uses MyApiCityRouting::setCreated()
     * @uses MyApiCityRouting::setDepoCode()
     * @uses MyApiCityRouting::setHighlighted()
     * @uses MyApiCityRouting::setPackProductName()
     * @uses MyApiCityRouting::setPackProductType()
     * @uses MyApiCityRouting::setPost()
     * @uses MyApiCityRouting::setRegion()
     * @uses MyApiCityRouting::setReject()
     * @uses MyApiCityRouting::setRouteCode()
     * @uses MyApiCityRouting::setServices()
     * @uses MyApiCityRouting::setStreet()
     * @uses MyApiCityRouting::setZipCode()
     * @param string $changed
     * @param string $city
     * @param string $countryCode
     * @param string $created
     * @param string $depoCode
     * @param bool $highlighted
     * @param string $packProductName
     * @param string $packProductType
     * @param string $post
     * @param string $region
     * @param bool $reject
     * @param string $routeCode
     * @param \PPLSDK\ArrayType\ArrayOfMyApiCityRouteSvc $services
     * @param string $street
     * @param string $zipCode
     */
    public function __construct($changed = null, $city = null, $countryCode = null, $created = null, $depoCode = null, $highlighted = null, $packProductName = null, $packProductType = null, $post = null, $region = null, $reject = null, $routeCode = null, \PPLSDK\ArrayType\ArrayOfMyApiCityRouteSvc $services = null, $street = null, $zipCode = null)
    {
        $this
            ->setChanged($changed)
            ->setCity($city)
            ->setCountryCode($countryCode)
            ->setCreated($created)
            ->setDepoCode($depoCode)
            ->setHighlighted($highlighted)
            ->setPackProductName($packProductName)
            ->setPackProductType($packProductType)
            ->setPost($post)
            ->setRegion($region)
            ->setReject($reject)
            ->setRouteCode($routeCode)
            ->setServices($services)
            ->setStreet($street)
            ->setZipCode($zipCode);
    }
    /**
     * Get Changed value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getChanged()
    {
        return isset($this->Changed) ? $this->Changed : null;
    }
    /**
     * Set Changed value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $changed
     * @return \PPLSDK\StructType\MyApiCityRouting
     */
    public function setChanged($changed = null)
    {
        // validation for constraint: string
        if (!is_null($changed) && !is_string($changed)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($changed, true), gettype($changed)), __LINE__);
        }
        if (is_null($changed) || (is_array($changed) && empty($changed))) {
            unset($this->Changed);
        } else {
            $this->Changed = $changed;
        }
        return $this;
    }
    /**
     * Get City value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCity()
    {
        return isset($this->City) ? $this->City : null;
    }
    /**
     * Set City value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $city
     * @return \PPLSDK\StructType\MyApiCityRouting
     */
    public function setCity($city = null)
    {
        // validation for constraint: string
        if (!is_null($city) && !is_string($city)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($city, true), gettype($city)), __LINE__);
        }
        if (is_null($city) || (is_array($city) && empty($city))) {
            unset($this->City);
        } else {
            $this->City = $city;
        }
        return $this;
    }
    /**
     * Get CountryCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryCode()
    {
        return isset($this->CountryCode) ? $this->CountryCode : null;
    }
    /**
     * Set CountryCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryCode
     * @return \PPLSDK\StructType\MyApiCityRouting
     */
    public function setCountryCode($countryCode = null)
    {
        // validation for constraint: string
        if (!is_null($countryCode) && !is_string($countryCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryCode, true), gettype($countryCode)), __LINE__);
        }
        if (is_null($countryCode) || (is_array($countryCode) && empty($countryCode))) {
            unset($this->CountryCode);
        } else {
            $this->CountryCode = $countryCode;
        }
        return $this;
    }
    /**
     * Get Created value
     * @return string|null
     */
    public function getCreated()
    {
        return $this->Created;
    }
    /**
     * Set Created value
     * @param string $created
     * @return \PPLSDK\StructType\MyApiCityRouting
     */
    public function setCreated($created = null)
    {
        // validation for constraint: string
        if (!is_null($created) && !is_string($created)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($created, true), gettype($created)), __LINE__);
        }
        $this->Created = $created;
        return $this;
    }
    /**
     * Get DepoCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDepoCode()
    {
        return isset($this->DepoCode) ? $this->DepoCode : null;
    }
    /**
     * Set DepoCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $depoCode
     * @return \PPLSDK\StructType\MyApiCityRouting
     */
    public function setDepoCode($depoCode = null)
    {
        // validation for constraint: string
        if (!is_null($depoCode) && !is_string($depoCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($depoCode, true), gettype($depoCode)), __LINE__);
        }
        if (is_null($depoCode) || (is_array($depoCode) && empty($depoCode))) {
            unset($this->DepoCode);
        } else {
            $this->DepoCode = $depoCode;
        }
        return $this;
    }
    /**
     * Get Highlighted value
     * @return bool|null
     */
    public function getHighlighted()
    {
        return $this->Highlighted;
    }
    /**
     * Set Highlighted value
     * @param bool $highlighted
     * @return \PPLSDK\StructType\MyApiCityRouting
     */
    public function setHighlighted($highlighted = null)
    {
        // validation for constraint: boolean
        if (!is_null($highlighted) && !is_bool($highlighted)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($highlighted, true), gettype($highlighted)), __LINE__);
        }
        $this->Highlighted = $highlighted;
        return $this;
    }
    /**
     * Get PackProductName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPackProductName()
    {
        return isset($this->PackProductName) ? $this->PackProductName : null;
    }
    /**
     * Set PackProductName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $packProductName
     * @return \PPLSDK\StructType\MyApiCityRouting
     */
    public function setPackProductName($packProductName = null)
    {
        // validation for constraint: string
        if (!is_null($packProductName) && !is_string($packProductName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packProductName, true), gettype($packProductName)), __LINE__);
        }
        if (is_null($packProductName) || (is_array($packProductName) && empty($packProductName))) {
            unset($this->PackProductName);
        } else {
            $this->PackProductName = $packProductName;
        }
        return $this;
    }
    /**
     * Get PackProductType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPackProductType()
    {
        return isset($this->PackProductType) ? $this->PackProductType : null;
    }
    /**
     * Set PackProductType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $packProductType
     * @return \PPLSDK\StructType\MyApiCityRouting
     */
    public function setPackProductType($packProductType = null)
    {
        // validation for constraint: string
        if (!is_null($packProductType) && !is_string($packProductType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packProductType, true), gettype($packProductType)), __LINE__);
        }
        if (is_null($packProductType) || (is_array($packProductType) && empty($packProductType))) {
            unset($this->PackProductType);
        } else {
            $this->PackProductType = $packProductType;
        }
        return $this;
    }
    /**
     * Get Post value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPost()
    {
        return isset($this->Post) ? $this->Post : null;
    }
    /**
     * Set Post value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $post
     * @return \PPLSDK\StructType\MyApiCityRouting
     */
    public function setPost($post = null)
    {
        // validation for constraint: string
        if (!is_null($post) && !is_string($post)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($post, true), gettype($post)), __LINE__);
        }
        if (is_null($post) || (is_array($post) && empty($post))) {
            unset($this->Post);
        } else {
            $this->Post = $post;
        }
        return $this;
    }
    /**
     * Get Region value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRegion()
    {
        return isset($this->Region) ? $this->Region : null;
    }
    /**
     * Set Region value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $region
     * @return \PPLSDK\StructType\MyApiCityRouting
     */
    public function setRegion($region = null)
    {
        // validation for constraint: string
        if (!is_null($region) && !is_string($region)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($region, true), gettype($region)), __LINE__);
        }
        if (is_null($region) || (is_array($region) && empty($region))) {
            unset($this->Region);
        } else {
            $this->Region = $region;
        }
        return $this;
    }
    /**
     * Get Reject value
     * @return bool|null
     */
    public function getReject()
    {
        return $this->Reject;
    }
    /**
     * Set Reject value
     * @param bool $reject
     * @return \PPLSDK\StructType\MyApiCityRouting
     */
    public function setReject($reject = null)
    {
        // validation for constraint: boolean
        if (!is_null($reject) && !is_bool($reject)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($reject, true), gettype($reject)), __LINE__);
        }
        $this->Reject = $reject;
        return $this;
    }
    /**
     * Get RouteCode value
     * @return string|null
     */
    public function getRouteCode()
    {
        return $this->RouteCode;
    }
    /**
     * Set RouteCode value
     * @param string $routeCode
     * @return \PPLSDK\StructType\MyApiCityRouting
     */
    public function setRouteCode($routeCode = null)
    {
        // validation for constraint: string
        if (!is_null($routeCode) && !is_string($routeCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($routeCode, true), gettype($routeCode)), __LINE__);
        }
        $this->RouteCode = $routeCode;
        return $this;
    }
    /**
     * Get Services value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\ArrayType\ArrayOfMyApiCityRouteSvc|null
     */
    public function getServices()
    {
        return isset($this->Services) ? $this->Services : null;
    }
    /**
     * Set Services value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\ArrayType\ArrayOfMyApiCityRouteSvc $services
     * @return \PPLSDK\StructType\MyApiCityRouting
     */
    public function setServices(\PPLSDK\ArrayType\ArrayOfMyApiCityRouteSvc $services = null)
    {
        if (is_null($services) || (is_array($services) && empty($services))) {
            unset($this->Services);
        } else {
            $this->Services = $services;
        }
        return $this;
    }
    /**
     * Get Street value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStreet()
    {
        return isset($this->Street) ? $this->Street : null;
    }
    /**
     * Set Street value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $street
     * @return \PPLSDK\StructType\MyApiCityRouting
     */
    public function setStreet($street = null)
    {
        // validation for constraint: string
        if (!is_null($street) && !is_string($street)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($street, true), gettype($street)), __LINE__);
        }
        if (is_null($street) || (is_array($street) && empty($street))) {
            unset($this->Street);
        } else {
            $this->Street = $street;
        }
        return $this;
    }
    /**
     * Get ZipCode value
     * @return string|null
     */
    public function getZipCode()
    {
        return $this->ZipCode;
    }
    /**
     * Set ZipCode value
     * @param string $zipCode
     * @return \PPLSDK\StructType\MyApiCityRouting
     */
    public function setZipCode($zipCode = null)
    {
        // validation for constraint: string
        if (!is_null($zipCode) && !is_string($zipCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($zipCode, true), gettype($zipCode)), __LINE__);
        }
        $this->ZipCode = $zipCode;
        return $this;
    }
}
