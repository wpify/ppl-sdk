<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SaveProductTransportsResponse StructType
 * @subpackage Structs
 */
class SaveProductTransportsResponse extends AbstractStructBase
{
    /**
     * The SaveProductTransportsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $SaveProductTransportsResult;
    /**
     * Constructor method for SaveProductTransportsResponse
     * @uses SaveProductTransportsResponse::setSaveProductTransportsResult()
     * @param bool $saveProductTransportsResult
     */
    public function __construct($saveProductTransportsResult = null)
    {
        $this
            ->setSaveProductTransportsResult($saveProductTransportsResult);
    }
    /**
     * Get SaveProductTransportsResult value
     * @return bool|null
     */
    public function getSaveProductTransportsResult()
    {
        return $this->SaveProductTransportsResult;
    }
    /**
     * Set SaveProductTransportsResult value
     * @param bool $saveProductTransportsResult
     * @return \PPLSDK\StructType\SaveProductTransportsResponse
     */
    public function setSaveProductTransportsResult($saveProductTransportsResult = null)
    {
        // validation for constraint: boolean
        if (!is_null($saveProductTransportsResult) && !is_bool($saveProductTransportsResult)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($saveProductTransportsResult, true), gettype($saveProductTransportsResult)), __LINE__);
        }
        $this->SaveProductTransportsResult = $saveProductTransportsResult;
        return $this;
    }
}
