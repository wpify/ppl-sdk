<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiPackageOutColli StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiPackageOutColli
 * @subpackage Structs
 */
class MyApiPackageOutColli extends AbstractStructBase
{
    /**
     * The ColliNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ColliNumber;
    /**
     * The Height
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $Height;
    /**
     * The Length
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $Length;
    /**
     * The Weight
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $Weight;
    /**
     * The Width
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $Width;
    /**
     * The WrapCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $WrapCode;
    /**
     * Constructor method for MyApiPackageOutColli
     * @uses MyApiPackageOutColli::setColliNumber()
     * @uses MyApiPackageOutColli::setHeight()
     * @uses MyApiPackageOutColli::setLength()
     * @uses MyApiPackageOutColli::setWeight()
     * @uses MyApiPackageOutColli::setWidth()
     * @uses MyApiPackageOutColli::setWrapCode()
     * @param string $colliNumber
     * @param int $height
     * @param int $length
     * @param float $weight
     * @param int $width
     * @param string $wrapCode
     */
    public function __construct($colliNumber = null, $height = null, $length = null, $weight = null, $width = null, $wrapCode = null)
    {
        $this
            ->setColliNumber($colliNumber)
            ->setHeight($height)
            ->setLength($length)
            ->setWeight($weight)
            ->setWidth($width)
            ->setWrapCode($wrapCode);
    }
    /**
     * Get ColliNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getColliNumber()
    {
        return isset($this->ColliNumber) ? $this->ColliNumber : null;
    }
    /**
     * Set ColliNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $colliNumber
     * @return \PPLSDK\StructType\MyApiPackageOutColli
     */
    public function setColliNumber($colliNumber = null)
    {
        // validation for constraint: string
        if (!is_null($colliNumber) && !is_string($colliNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($colliNumber, true), gettype($colliNumber)), __LINE__);
        }
        if (is_null($colliNumber) || (is_array($colliNumber) && empty($colliNumber))) {
            unset($this->ColliNumber);
        } else {
            $this->ColliNumber = $colliNumber;
        }
        return $this;
    }
    /**
     * Get Height value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getHeight()
    {
        return isset($this->Height) ? $this->Height : null;
    }
    /**
     * Set Height value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $height
     * @return \PPLSDK\StructType\MyApiPackageOutColli
     */
    public function setHeight($height = null)
    {
        // validation for constraint: int
        if (!is_null($height) && !(is_int($height) || ctype_digit($height))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($height, true), gettype($height)), __LINE__);
        }
        if (is_null($height) || (is_array($height) && empty($height))) {
            unset($this->Height);
        } else {
            $this->Height = $height;
        }
        return $this;
    }
    /**
     * Get Length value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getLength()
    {
        return isset($this->Length) ? $this->Length : null;
    }
    /**
     * Set Length value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $length
     * @return \PPLSDK\StructType\MyApiPackageOutColli
     */
    public function setLength($length = null)
    {
        // validation for constraint: int
        if (!is_null($length) && !(is_int($length) || ctype_digit($length))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($length, true), gettype($length)), __LINE__);
        }
        if (is_null($length) || (is_array($length) && empty($length))) {
            unset($this->Length);
        } else {
            $this->Length = $length;
        }
        return $this;
    }
    /**
     * Get Weight value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getWeight()
    {
        return isset($this->Weight) ? $this->Weight : null;
    }
    /**
     * Set Weight value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $weight
     * @return \PPLSDK\StructType\MyApiPackageOutColli
     */
    public function setWeight($weight = null)
    {
        // validation for constraint: float
        if (!is_null($weight) && !(is_float($weight) || is_numeric($weight))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($weight, true), gettype($weight)), __LINE__);
        }
        if (is_null($weight) || (is_array($weight) && empty($weight))) {
            unset($this->Weight);
        } else {
            $this->Weight = $weight;
        }
        return $this;
    }
    /**
     * Get Width value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getWidth()
    {
        return isset($this->Width) ? $this->Width : null;
    }
    /**
     * Set Width value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $width
     * @return \PPLSDK\StructType\MyApiPackageOutColli
     */
    public function setWidth($width = null)
    {
        // validation for constraint: int
        if (!is_null($width) && !(is_int($width) || ctype_digit($width))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($width, true), gettype($width)), __LINE__);
        }
        if (is_null($width) || (is_array($width) && empty($width))) {
            unset($this->Width);
        } else {
            $this->Width = $width;
        }
        return $this;
    }
    /**
     * Get WrapCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getWrapCode()
    {
        return isset($this->WrapCode) ? $this->WrapCode : null;
    }
    /**
     * Set WrapCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $wrapCode
     * @return \PPLSDK\StructType\MyApiPackageOutColli
     */
    public function setWrapCode($wrapCode = null)
    {
        // validation for constraint: string
        if (!is_null($wrapCode) && !is_string($wrapCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($wrapCode, true), gettype($wrapCode)), __LINE__);
        }
        if (is_null($wrapCode) || (is_array($wrapCode) && empty($wrapCode))) {
            unset($this->WrapCode);
        } else {
            $this->WrapCode = $wrapCode;
        }
        return $this;
    }
}
