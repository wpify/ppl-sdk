<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProductCountry StructType
 * @subpackage Structs
 */
class GetProductCountry extends AbstractStructBase
{
    /**
     * The Filter
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\ProductCountryFilter
     */
    public $Filter;
    /**
     * Constructor method for GetProductCountry
     * @uses GetProductCountry::setFilter()
     * @param \PPLSDK\StructType\ProductCountryFilter $filter
     */
    public function __construct(\PPLSDK\StructType\ProductCountryFilter $filter = null)
    {
        $this
            ->setFilter($filter);
    }
    /**
     * Get Filter value
     * @return \PPLSDK\StructType\ProductCountryFilter
     */
    public function getFilter()
    {
        return $this->Filter;
    }
    /**
     * Set Filter value
     * @param \PPLSDK\StructType\ProductCountryFilter $filter
     * @return \PPLSDK\StructType\GetProductCountry
     */
    public function setFilter(\PPLSDK\StructType\ProductCountryFilter $filter = null)
    {
        $this->Filter = $filter;
        return $this;
    }
}
