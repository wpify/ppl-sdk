<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiProduct StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiProduct
 * @subpackage Structs
 */
class MyApiProduct extends AbstractStructBase
{
    /**
     * The Code
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $Code;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Name;
    /**
     * The Reject
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $Reject;
    /**
     * The IsCOD
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $IsCOD;
    /**
     * The IsCODOf
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $IsCODOf;
    /**
     * The Import
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $Import;
    /**
     * The Export
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $Export;
    /**
     * The Package
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $Package;
    /**
     * The Sprint
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $Sprint;
    /**
     * The Private
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $Private;
    /**
     * The Business
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $Business;
    /**
     * The Express
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $Express;
    /**
     * The PackNumberType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $PackNumberType;
    /**
     * The Deliv2PS
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $Deliv2PS;
    /**
     * Constructor method for MyApiProduct
     * @uses MyApiProduct::setCode()
     * @uses MyApiProduct::setName()
     * @uses MyApiProduct::setReject()
     * @uses MyApiProduct::setIsCOD()
     * @uses MyApiProduct::setIsCODOf()
     * @uses MyApiProduct::setImport()
     * @uses MyApiProduct::setExport()
     * @uses MyApiProduct::setPackage()
     * @uses MyApiProduct::setSprint()
     * @uses MyApiProduct::setPrivate()
     * @uses MyApiProduct::setBusiness()
     * @uses MyApiProduct::setExpress()
     * @uses MyApiProduct::setPackNumberType()
     * @uses MyApiProduct::setDeliv2PS()
     * @param int $code
     * @param string $name
     * @param bool $reject
     * @param bool $isCOD
     * @param int $isCODOf
     * @param bool $import
     * @param bool $export
     * @param bool $package
     * @param bool $sprint
     * @param bool $private
     * @param bool $business
     * @param bool $express
     * @param int $packNumberType
     * @param bool $deliv2PS
     */
    public function __construct($code = null, $name = null, $reject = null, $isCOD = null, $isCODOf = null, $import = null, $export = null, $package = null, $sprint = null, $private = null, $business = null, $express = null, $packNumberType = null, $deliv2PS = null)
    {
        $this
            ->setCode($code)
            ->setName($name)
            ->setReject($reject)
            ->setIsCOD($isCOD)
            ->setIsCODOf($isCODOf)
            ->setImport($import)
            ->setExport($export)
            ->setPackage($package)
            ->setSprint($sprint)
            ->setPrivate($private)
            ->setBusiness($business)
            ->setExpress($express)
            ->setPackNumberType($packNumberType)
            ->setDeliv2PS($deliv2PS);
    }
    /**
     * Get Code value
     * @return int|null
     */
    public function getCode()
    {
        return $this->Code;
    }
    /**
     * Set Code value
     * @param int $code
     * @return \PPLSDK\StructType\MyApiProduct
     */
    public function setCode($code = null)
    {
        // validation for constraint: int
        if (!is_null($code) && !(is_int($code) || ctype_digit($code))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($code, true), gettype($code)), __LINE__);
        }
        $this->Code = $code;
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName()
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \PPLSDK\StructType\MyApiProduct
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        return $this;
    }
    /**
     * Get Reject value
     * @return bool|null
     */
    public function getReject()
    {
        return $this->Reject;
    }
    /**
     * Set Reject value
     * @param bool $reject
     * @return \PPLSDK\StructType\MyApiProduct
     */
    public function setReject($reject = null)
    {
        // validation for constraint: boolean
        if (!is_null($reject) && !is_bool($reject)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($reject, true), gettype($reject)), __LINE__);
        }
        $this->Reject = $reject;
        return $this;
    }
    /**
     * Get IsCOD value
     * @return bool|null
     */
    public function getIsCOD()
    {
        return $this->IsCOD;
    }
    /**
     * Set IsCOD value
     * @param bool $isCOD
     * @return \PPLSDK\StructType\MyApiProduct
     */
    public function setIsCOD($isCOD = null)
    {
        // validation for constraint: boolean
        if (!is_null($isCOD) && !is_bool($isCOD)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isCOD, true), gettype($isCOD)), __LINE__);
        }
        $this->IsCOD = $isCOD;
        return $this;
    }
    /**
     * Get IsCODOf value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getIsCODOf()
    {
        return isset($this->IsCODOf) ? $this->IsCODOf : null;
    }
    /**
     * Set IsCODOf value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $isCODOf
     * @return \PPLSDK\StructType\MyApiProduct
     */
    public function setIsCODOf($isCODOf = null)
    {
        // validation for constraint: int
        if (!is_null($isCODOf) && !(is_int($isCODOf) || ctype_digit($isCODOf))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($isCODOf, true), gettype($isCODOf)), __LINE__);
        }
        if (is_null($isCODOf) || (is_array($isCODOf) && empty($isCODOf))) {
            unset($this->IsCODOf);
        } else {
            $this->IsCODOf = $isCODOf;
        }
        return $this;
    }
    /**
     * Get Import value
     * @return bool|null
     */
    public function getImport()
    {
        return $this->Import;
    }
    /**
     * Set Import value
     * @param bool $import
     * @return \PPLSDK\StructType\MyApiProduct
     */
    public function setImport($import = null)
    {
        // validation for constraint: boolean
        if (!is_null($import) && !is_bool($import)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($import, true), gettype($import)), __LINE__);
        }
        $this->Import = $import;
        return $this;
    }
    /**
     * Get Export value
     * @return bool|null
     */
    public function getExport()
    {
        return $this->Export;
    }
    /**
     * Set Export value
     * @param bool $export
     * @return \PPLSDK\StructType\MyApiProduct
     */
    public function setExport($export = null)
    {
        // validation for constraint: boolean
        if (!is_null($export) && !is_bool($export)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($export, true), gettype($export)), __LINE__);
        }
        $this->Export = $export;
        return $this;
    }
    /**
     * Get Package value
     * @return bool|null
     */
    public function getPackage()
    {
        return $this->Package;
    }
    /**
     * Set Package value
     * @param bool $package
     * @return \PPLSDK\StructType\MyApiProduct
     */
    public function setPackage($package = null)
    {
        // validation for constraint: boolean
        if (!is_null($package) && !is_bool($package)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($package, true), gettype($package)), __LINE__);
        }
        $this->Package = $package;
        return $this;
    }
    /**
     * Get Sprint value
     * @return bool|null
     */
    public function getSprint()
    {
        return $this->Sprint;
    }
    /**
     * Set Sprint value
     * @param bool $sprint
     * @return \PPLSDK\StructType\MyApiProduct
     */
    public function setSprint($sprint = null)
    {
        // validation for constraint: boolean
        if (!is_null($sprint) && !is_bool($sprint)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($sprint, true), gettype($sprint)), __LINE__);
        }
        $this->Sprint = $sprint;
        return $this;
    }
    /**
     * Get Private value
     * @return bool|null
     */
    public function getPrivate()
    {
        return $this->Private;
    }
    /**
     * Set Private value
     * @param bool $private
     * @return \PPLSDK\StructType\MyApiProduct
     */
    public function setPrivate($private = null)
    {
        // validation for constraint: boolean
        if (!is_null($private) && !is_bool($private)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($private, true), gettype($private)), __LINE__);
        }
        $this->Private = $private;
        return $this;
    }
    /**
     * Get Business value
     * @return bool|null
     */
    public function getBusiness()
    {
        return $this->Business;
    }
    /**
     * Set Business value
     * @param bool $business
     * @return \PPLSDK\StructType\MyApiProduct
     */
    public function setBusiness($business = null)
    {
        // validation for constraint: boolean
        if (!is_null($business) && !is_bool($business)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($business, true), gettype($business)), __LINE__);
        }
        $this->Business = $business;
        return $this;
    }
    /**
     * Get Express value
     * @return bool|null
     */
    public function getExpress()
    {
        return $this->Express;
    }
    /**
     * Set Express value
     * @param bool $express
     * @return \PPLSDK\StructType\MyApiProduct
     */
    public function setExpress($express = null)
    {
        // validation for constraint: boolean
        if (!is_null($express) && !is_bool($express)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($express, true), gettype($express)), __LINE__);
        }
        $this->Express = $express;
        return $this;
    }
    /**
     * Get PackNumberType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getPackNumberType()
    {
        return isset($this->PackNumberType) ? $this->PackNumberType : null;
    }
    /**
     * Set PackNumberType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $packNumberType
     * @return \PPLSDK\StructType\MyApiProduct
     */
    public function setPackNumberType($packNumberType = null)
    {
        // validation for constraint: int
        if (!is_null($packNumberType) && !(is_int($packNumberType) || ctype_digit($packNumberType))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($packNumberType, true), gettype($packNumberType)), __LINE__);
        }
        if (is_null($packNumberType) || (is_array($packNumberType) && empty($packNumberType))) {
            unset($this->PackNumberType);
        } else {
            $this->PackNumberType = $packNumberType;
        }
        return $this;
    }
    /**
     * Get Deliv2PS value
     * @return bool|null
     */
    public function getDeliv2PS()
    {
        return $this->Deliv2PS;
    }
    /**
     * Set Deliv2PS value
     * @param bool $deliv2PS
     * @return \PPLSDK\StructType\MyApiProduct
     */
    public function setDeliv2PS($deliv2PS = null)
    {
        // validation for constraint: boolean
        if (!is_null($deliv2PS) && !is_bool($deliv2PS)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($deliv2PS, true), gettype($deliv2PS)), __LINE__);
        }
        $this->Deliv2PS = $deliv2PS;
        return $this;
    }
}
