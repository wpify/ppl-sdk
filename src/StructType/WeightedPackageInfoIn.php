<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for WeightedPackageInfoIn StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:WeightedPackageInfoIn
 * @subpackage Structs
 */
class WeightedPackageInfoIn extends AbstractStructBase
{
    /**
     * The Weight
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var float
     */
    public $Weight;
    /**
     * The Routes
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfRoute
     */
    public $Routes;
    /**
     * Constructor method for WeightedPackageInfoIn
     * @uses WeightedPackageInfoIn::setWeight()
     * @uses WeightedPackageInfoIn::setRoutes()
     * @param float $weight
     * @param \PPLSDK\ArrayType\ArrayOfRoute $routes
     */
    public function __construct($weight = null, \PPLSDK\ArrayType\ArrayOfRoute $routes = null)
    {
        $this
            ->setWeight($weight)
            ->setRoutes($routes);
    }
    /**
     * Get Weight value
     * @return float|null
     */
    public function getWeight()
    {
        return $this->Weight;
    }
    /**
     * Set Weight value
     * @param float $weight
     * @return \PPLSDK\StructType\WeightedPackageInfoIn
     */
    public function setWeight($weight = null)
    {
        // validation for constraint: float
        if (!is_null($weight) && !(is_float($weight) || is_numeric($weight))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($weight, true), gettype($weight)), __LINE__);
        }
        $this->Weight = $weight;
        return $this;
    }
    /**
     * Get Routes value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\ArrayType\ArrayOfRoute|null
     */
    public function getRoutes()
    {
        return isset($this->Routes) ? $this->Routes : null;
    }
    /**
     * Set Routes value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\ArrayType\ArrayOfRoute $routes
     * @return \PPLSDK\StructType\WeightedPackageInfoIn
     */
    public function setRoutes(\PPLSDK\ArrayType\ArrayOfRoute $routes = null)
    {
        if (is_null($routes) || (is_array($routes) && empty($routes))) {
            unset($this->Routes);
        } else {
            $this->Routes = $routes;
        }
        return $this;
    }
}
