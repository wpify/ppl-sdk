<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiPickupOrderIn StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiPickupOrderIn
 * @subpackage Structs
 */
class MyApiPickupOrderIn extends AbstractStructBase
{
    /**
     * The OrdRefId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $OrdRefId;
    /**
     * The CustRef
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CustRef;
    /**
     * The CountPack
     * @var int
     */
    public $CountPack;
    /**
     * The Note
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Note;
    /**
     * The Email
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Email;
    /**
     * The SendDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $SendDate;
    /**
     * The SendTimeFrom
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SendTimeFrom;
    /**
     * The SendTimeTo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SendTimeTo;
    /**
     * The Sender
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiOrderInSender
     */
    public $Sender;
    /**
     * Constructor method for MyApiPickupOrderIn
     * @uses MyApiPickupOrderIn::setOrdRefId()
     * @uses MyApiPickupOrderIn::setCustRef()
     * @uses MyApiPickupOrderIn::setCountPack()
     * @uses MyApiPickupOrderIn::setNote()
     * @uses MyApiPickupOrderIn::setEmail()
     * @uses MyApiPickupOrderIn::setSendDate()
     * @uses MyApiPickupOrderIn::setSendTimeFrom()
     * @uses MyApiPickupOrderIn::setSendTimeTo()
     * @uses MyApiPickupOrderIn::setSender()
     * @param string $ordRefId
     * @param string $custRef
     * @param int $countPack
     * @param string $note
     * @param string $email
     * @param string $sendDate
     * @param string $sendTimeFrom
     * @param string $sendTimeTo
     * @param \PPLSDK\StructType\MyApiOrderInSender $sender
     */
    public function __construct($ordRefId = null, $custRef = null, $countPack = null, $note = null, $email = null, $sendDate = null, $sendTimeFrom = null, $sendTimeTo = null, \PPLSDK\StructType\MyApiOrderInSender $sender = null)
    {
        $this
            ->setOrdRefId($ordRefId)
            ->setCustRef($custRef)
            ->setCountPack($countPack)
            ->setNote($note)
            ->setEmail($email)
            ->setSendDate($sendDate)
            ->setSendTimeFrom($sendTimeFrom)
            ->setSendTimeTo($sendTimeTo)
            ->setSender($sender);
    }
    /**
     * Get OrdRefId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOrdRefId()
    {
        return isset($this->OrdRefId) ? $this->OrdRefId : null;
    }
    /**
     * Set OrdRefId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $ordRefId
     * @return \PPLSDK\StructType\MyApiPickupOrderIn
     */
    public function setOrdRefId($ordRefId = null)
    {
        // validation for constraint: string
        if (!is_null($ordRefId) && !is_string($ordRefId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ordRefId, true), gettype($ordRefId)), __LINE__);
        }
        if (is_null($ordRefId) || (is_array($ordRefId) && empty($ordRefId))) {
            unset($this->OrdRefId);
        } else {
            $this->OrdRefId = $ordRefId;
        }
        return $this;
    }
    /**
     * Get CustRef value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCustRef()
    {
        return isset($this->CustRef) ? $this->CustRef : null;
    }
    /**
     * Set CustRef value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $custRef
     * @return \PPLSDK\StructType\MyApiPickupOrderIn
     */
    public function setCustRef($custRef = null)
    {
        // validation for constraint: string
        if (!is_null($custRef) && !is_string($custRef)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($custRef, true), gettype($custRef)), __LINE__);
        }
        if (is_null($custRef) || (is_array($custRef) && empty($custRef))) {
            unset($this->CustRef);
        } else {
            $this->CustRef = $custRef;
        }
        return $this;
    }
    /**
     * Get CountPack value
     * @return int|null
     */
    public function getCountPack()
    {
        return $this->CountPack;
    }
    /**
     * Set CountPack value
     * @param int $countPack
     * @return \PPLSDK\StructType\MyApiPickupOrderIn
     */
    public function setCountPack($countPack = null)
    {
        // validation for constraint: int
        if (!is_null($countPack) && !(is_int($countPack) || ctype_digit($countPack))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($countPack, true), gettype($countPack)), __LINE__);
        }
        $this->CountPack = $countPack;
        return $this;
    }
    /**
     * Get Note value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNote()
    {
        return isset($this->Note) ? $this->Note : null;
    }
    /**
     * Set Note value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $note
     * @return \PPLSDK\StructType\MyApiPickupOrderIn
     */
    public function setNote($note = null)
    {
        // validation for constraint: string
        if (!is_null($note) && !is_string($note)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($note, true), gettype($note)), __LINE__);
        }
        if (is_null($note) || (is_array($note) && empty($note))) {
            unset($this->Note);
        } else {
            $this->Note = $note;
        }
        return $this;
    }
    /**
     * Get Email value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEmail()
    {
        return isset($this->Email) ? $this->Email : null;
    }
    /**
     * Set Email value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $email
     * @return \PPLSDK\StructType\MyApiPickupOrderIn
     */
    public function setEmail($email = null)
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        if (is_null($email) || (is_array($email) && empty($email))) {
            unset($this->Email);
        } else {
            $this->Email = $email;
        }
        return $this;
    }
    /**
     * Get SendDate value
     * @return string|null
     */
    public function getSendDate()
    {
        return $this->SendDate;
    }
    /**
     * Set SendDate value
     * @param string $sendDate
     * @return \PPLSDK\StructType\MyApiPickupOrderIn
     */
    public function setSendDate($sendDate = null)
    {
        // validation for constraint: string
        if (!is_null($sendDate) && !is_string($sendDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sendDate, true), gettype($sendDate)), __LINE__);
        }
        $this->SendDate = $sendDate;
        return $this;
    }
    /**
     * Get SendTimeFrom value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSendTimeFrom()
    {
        return isset($this->SendTimeFrom) ? $this->SendTimeFrom : null;
    }
    /**
     * Set SendTimeFrom value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sendTimeFrom
     * @return \PPLSDK\StructType\MyApiPickupOrderIn
     */
    public function setSendTimeFrom($sendTimeFrom = null)
    {
        // validation for constraint: string
        if (!is_null($sendTimeFrom) && !is_string($sendTimeFrom)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sendTimeFrom, true), gettype($sendTimeFrom)), __LINE__);
        }
        if (is_null($sendTimeFrom) || (is_array($sendTimeFrom) && empty($sendTimeFrom))) {
            unset($this->SendTimeFrom);
        } else {
            $this->SendTimeFrom = $sendTimeFrom;
        }
        return $this;
    }
    /**
     * Get SendTimeTo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSendTimeTo()
    {
        return isset($this->SendTimeTo) ? $this->SendTimeTo : null;
    }
    /**
     * Set SendTimeTo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sendTimeTo
     * @return \PPLSDK\StructType\MyApiPickupOrderIn
     */
    public function setSendTimeTo($sendTimeTo = null)
    {
        // validation for constraint: string
        if (!is_null($sendTimeTo) && !is_string($sendTimeTo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sendTimeTo, true), gettype($sendTimeTo)), __LINE__);
        }
        if (is_null($sendTimeTo) || (is_array($sendTimeTo) && empty($sendTimeTo))) {
            unset($this->SendTimeTo);
        } else {
            $this->SendTimeTo = $sendTimeTo;
        }
        return $this;
    }
    /**
     * Get Sender value
     * @return \PPLSDK\StructType\MyApiOrderInSender|null
     */
    public function getSender()
    {
        return $this->Sender;
    }
    /**
     * Set Sender value
     * @param \PPLSDK\StructType\MyApiOrderInSender $sender
     * @return \PPLSDK\StructType\MyApiPickupOrderIn
     */
    public function setSender(\PPLSDK\StructType\MyApiOrderInSender $sender = null)
    {
        $this->Sender = $sender;
        return $this;
    }
}
