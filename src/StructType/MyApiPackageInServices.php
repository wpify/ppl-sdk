<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiPackageInServices StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiPackageInServices
 * @subpackage Structs
 */
class MyApiPackageInServices extends AbstractStructBase
{
    /**
     * The SvcCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $SvcCode;
    /**
     * Constructor method for MyApiPackageInServices
     * @uses MyApiPackageInServices::setSvcCode()
     * @param string $svcCode
     */
    public function __construct($svcCode = null)
    {
        $this
            ->setSvcCode($svcCode);
    }
    /**
     * Get SvcCode value
     * @return string|null
     */
    public function getSvcCode()
    {
        return $this->SvcCode;
    }
    /**
     * Set SvcCode value
     * @param string $svcCode
     * @return \PPLSDK\StructType\MyApiPackageInServices
     */
    public function setSvcCode($svcCode = null)
    {
        // validation for constraint: string
        if (!is_null($svcCode) && !is_string($svcCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($svcCode, true), gettype($svcCode)), __LINE__);
        }
        $this->SvcCode = $svcCode;
        return $this;
    }
}
