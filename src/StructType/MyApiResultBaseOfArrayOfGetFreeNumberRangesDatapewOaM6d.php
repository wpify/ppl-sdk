<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiResultBaseOfArrayOfGetFreeNumberRangesDatapewOaM6d
 * StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiResultBaseOfArrayOfGetFreeNumberRangesDatapewOaM6d
 * @subpackage Structs
 */
class MyApiResultBaseOfArrayOfGetFreeNumberRangesDatapewOaM6d extends MyApiResultBaseVoid
{
    /**
     * The ResultData
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfGetFreeNumberRangesData
     */
    public $ResultData;
    /**
     * Constructor method for MyApiResultBaseOfArrayOfGetFreeNumberRangesDatapewOaM6d
     * @uses MyApiResultBaseOfArrayOfGetFreeNumberRangesDatapewOaM6d::setResultData()
     * @param \PPLSDK\ArrayType\ArrayOfGetFreeNumberRangesData $resultData
     */
    public function __construct(\PPLSDK\ArrayType\ArrayOfGetFreeNumberRangesData $resultData = null)
    {
        $this
            ->setResultData($resultData);
    }
    /**
     * Get ResultData value
     * @return \PPLSDK\ArrayType\ArrayOfGetFreeNumberRangesData|null
     */
    public function getResultData()
    {
        return $this->ResultData;
    }
    /**
     * Set ResultData value
     * @param \PPLSDK\ArrayType\ArrayOfGetFreeNumberRangesData $resultData
     * @return \PPLSDK\StructType\MyApiResultBaseOfArrayOfGetFreeNumberRangesDatapewOaM6d
     */
    public function setResultData(\PPLSDK\ArrayType\ArrayOfGetFreeNumberRangesData $resultData = null)
    {
        $this->ResultData = $resultData;
        return $this;
    }
}
