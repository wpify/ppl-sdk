<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PartnerResultOfArrayOfPartnerItemResultpewOaM6d StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:PartnerResultOfArrayOfPartnerItemResultpewOaM6d
 * @subpackage Structs
 */
class PartnerResultOfArrayOfPartnerItemResultpewOaM6d extends MyApiResultBaseOfArrayOfPartnerItemResultpewOaM6d
{
}
