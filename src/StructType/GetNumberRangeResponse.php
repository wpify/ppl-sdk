<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetNumberRangeResponse StructType
 * @subpackage Structs
 */
class GetNumberRangeResponse extends AbstractStructBase
{
    /**
     * The GetNumberRangeResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\GetNumberRangeResult
     */
    public $GetNumberRangeResult;
    /**
     * Constructor method for GetNumberRangeResponse
     * @uses GetNumberRangeResponse::setGetNumberRangeResult()
     * @param \PPLSDK\StructType\GetNumberRangeResult $getNumberRangeResult
     */
    public function __construct(\PPLSDK\StructType\GetNumberRangeResult $getNumberRangeResult = null)
    {
        $this
            ->setGetNumberRangeResult($getNumberRangeResult);
    }
    /**
     * Get GetNumberRangeResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\GetNumberRangeResult|null
     */
    public function getGetNumberRangeResult()
    {
        return isset($this->GetNumberRangeResult) ? $this->GetNumberRangeResult : null;
    }
    /**
     * Set GetNumberRangeResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\GetNumberRangeResult $getNumberRangeResult
     * @return \PPLSDK\StructType\GetNumberRangeResponse
     */
    public function setGetNumberRangeResult(\PPLSDK\StructType\GetNumberRangeResult $getNumberRangeResult = null)
    {
        if (is_null($getNumberRangeResult) || (is_array($getNumberRangeResult) && empty($getNumberRangeResult))) {
            unset($this->GetNumberRangeResult);
        } else {
            $this->GetNumberRangeResult = $getNumberRangeResult;
        }
        return $this;
    }
}
