<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for
 * MyApiResultBaseOfCreatePackageLabelRequestResultDatapewOaM6d StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiResultBaseOfCreatePackageLabelRequestResultDatapewOaM6d
 * @subpackage Structs
 */
class MyApiResultBaseOfCreatePackageLabelRequestResultDatapewOaM6d extends MyApiResultBaseVoid
{
    /**
     * The ResultData
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\StructType\CreatePackageLabelRequestResultData
     */
    public $ResultData;
    /**
     * Constructor method for
     * MyApiResultBaseOfCreatePackageLabelRequestResultDatapewOaM6d
     * @uses MyApiResultBaseOfCreatePackageLabelRequestResultDatapewOaM6d::setResultData()
     * @param \PPLSDK\StructType\CreatePackageLabelRequestResultData $resultData
     */
    public function __construct(\PPLSDK\StructType\CreatePackageLabelRequestResultData $resultData = null)
    {
        $this
            ->setResultData($resultData);
    }
    /**
     * Get ResultData value
     * @return \PPLSDK\StructType\CreatePackageLabelRequestResultData|null
     */
    public function getResultData()
    {
        return $this->ResultData;
    }
    /**
     * Set ResultData value
     * @param \PPLSDK\StructType\CreatePackageLabelRequestResultData $resultData
     * @return \PPLSDK\StructType\MyApiResultBaseOfCreatePackageLabelRequestResultDatapewOaM6d
     */
    public function setResultData(\PPLSDK\StructType\CreatePackageLabelRequestResultData $resultData = null)
    {
        $this->ResultData = $resultData;
        return $this;
    }
}
