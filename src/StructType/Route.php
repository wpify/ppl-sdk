<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Route StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:Route
 * @subpackage Structs
 */
class Route extends AbstractStructBase
{
    /**
     * The RouteType
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $RouteType;
    /**
     * The RouteCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $RouteCode;
    /**
     * Constructor method for Route
     * @uses Route::setRouteType()
     * @uses Route::setRouteCode()
     * @param string $routeType
     * @param string $routeCode
     */
    public function __construct($routeType = null, $routeCode = null)
    {
        $this
            ->setRouteType($routeType)
            ->setRouteCode($routeCode);
    }
    /**
     * Get RouteType value
     * @return string|null
     */
    public function getRouteType()
    {
        return $this->RouteType;
    }
    /**
     * Set RouteType value
     * @param string $routeType
     * @return \PPLSDK\StructType\Route
     */
    public function setRouteType($routeType = null)
    {
        // validation for constraint: string
        if (!is_null($routeType) && !is_string($routeType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($routeType, true), gettype($routeType)), __LINE__);
        }
        $this->RouteType = $routeType;
        return $this;
    }
    /**
     * Get RouteCode value
     * @return string|null
     */
    public function getRouteCode()
    {
        return $this->RouteCode;
    }
    /**
     * Set RouteCode value
     * @param string $routeCode
     * @return \PPLSDK\StructType\Route
     */
    public function setRouteCode($routeCode = null)
    {
        // validation for constraint: string
        if (!is_null($routeCode) && !is_string($routeCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($routeCode, true), gettype($routeCode)), __LINE__);
        }
        $this->RouteCode = $routeCode;
        return $this;
    }
}
