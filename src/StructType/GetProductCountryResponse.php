<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProductCountryResponse StructType
 * @subpackage Structs
 */
class GetProductCountryResponse extends AbstractStructBase
{
    /**
     * The GetProductCountryResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\GetProductCountryResult
     */
    public $GetProductCountryResult;
    /**
     * Constructor method for GetProductCountryResponse
     * @uses GetProductCountryResponse::setGetProductCountryResult()
     * @param \PPLSDK\StructType\GetProductCountryResult $getProductCountryResult
     */
    public function __construct(\PPLSDK\StructType\GetProductCountryResult $getProductCountryResult = null)
    {
        $this
            ->setGetProductCountryResult($getProductCountryResult);
    }
    /**
     * Get GetProductCountryResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\GetProductCountryResult|null
     */
    public function getGetProductCountryResult()
    {
        return isset($this->GetProductCountryResult) ? $this->GetProductCountryResult : null;
    }
    /**
     * Set GetProductCountryResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\GetProductCountryResult $getProductCountryResult
     * @return \PPLSDK\StructType\GetProductCountryResponse
     */
    public function setGetProductCountryResult(\PPLSDK\StructType\GetProductCountryResult $getProductCountryResult = null)
    {
        if (is_null($getProductCountryResult) || (is_array($getProductCountryResult) && empty($getProductCountryResult))) {
            unset($this->GetProductCountryResult);
        } else {
            $this->GetProductCountryResult = $getProductCountryResult;
        }
        return $this;
    }
}
