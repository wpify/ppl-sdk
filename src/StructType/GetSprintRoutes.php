<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSprintRoutes StructType
 * @subpackage Structs
 */
class GetSprintRoutes extends AbstractStructBase
{
}
