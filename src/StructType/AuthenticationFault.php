<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AuthenticationFault StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:AuthenticationFault
 * @subpackage Structs
 */
class AuthenticationFault extends MyApiFault
{
}
