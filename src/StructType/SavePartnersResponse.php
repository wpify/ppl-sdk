<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SavePartnersResponse StructType
 * @subpackage Structs
 */
class SavePartnersResponse extends AbstractStructBase
{
    /**
     * The SavePartnersResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\SavePartnersResult
     */
    public $SavePartnersResult;
    /**
     * Constructor method for SavePartnersResponse
     * @uses SavePartnersResponse::setSavePartnersResult()
     * @param \PPLSDK\StructType\SavePartnersResult $savePartnersResult
     */
    public function __construct(\PPLSDK\StructType\SavePartnersResult $savePartnersResult = null)
    {
        $this
            ->setSavePartnersResult($savePartnersResult);
    }
    /**
     * Get SavePartnersResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\SavePartnersResult|null
     */
    public function getSavePartnersResult()
    {
        return isset($this->SavePartnersResult) ? $this->SavePartnersResult : null;
    }
    /**
     * Set SavePartnersResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\SavePartnersResult $savePartnersResult
     * @return \PPLSDK\StructType\SavePartnersResponse
     */
    public function setSavePartnersResult(\PPLSDK\StructType\SavePartnersResult $savePartnersResult = null)
    {
        if (is_null($savePartnersResult) || (is_array($savePartnersResult) && empty($savePartnersResult))) {
            unset($this->SavePartnersResult);
        } else {
            $this->SavePartnersResult = $savePartnersResult;
        }
        return $this;
    }
}
