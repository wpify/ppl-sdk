<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProductCountryResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:GetProductCountryResult
 * @subpackage Structs
 */
class GetProductCountryResult extends MyApiResultBaseOfArrayOfMyApiCountryProductspewOaM6d
{
}
