<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCodCurrencyResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:GetCodCurrencyResult
 * @subpackage Structs
 */
class GetCodCurrencyResult extends MyApiResultBaseOfArrayOfMyApiCodCurrencypewOaM6d
{
}
