<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPackNumberRowTypesResponse StructType
 * @subpackage Structs
 */
class GetPackNumberRowTypesResponse extends AbstractStructBase
{
    /**
     * The GetPackNumberRowTypesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\GetPackNumberRowTypesResult
     */
    public $GetPackNumberRowTypesResult;
    /**
     * Constructor method for GetPackNumberRowTypesResponse
     * @uses GetPackNumberRowTypesResponse::setGetPackNumberRowTypesResult()
     * @param \PPLSDK\StructType\GetPackNumberRowTypesResult $getPackNumberRowTypesResult
     */
    public function __construct(\PPLSDK\StructType\GetPackNumberRowTypesResult $getPackNumberRowTypesResult = null)
    {
        $this
            ->setGetPackNumberRowTypesResult($getPackNumberRowTypesResult);
    }
    /**
     * Get GetPackNumberRowTypesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\GetPackNumberRowTypesResult|null
     */
    public function getGetPackNumberRowTypesResult()
    {
        return isset($this->GetPackNumberRowTypesResult) ? $this->GetPackNumberRowTypesResult : null;
    }
    /**
     * Set GetPackNumberRowTypesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\GetPackNumberRowTypesResult $getPackNumberRowTypesResult
     * @return \PPLSDK\StructType\GetPackNumberRowTypesResponse
     */
    public function setGetPackNumberRowTypesResult(\PPLSDK\StructType\GetPackNumberRowTypesResult $getPackNumberRowTypesResult = null)
    {
        if (is_null($getPackNumberRowTypesResult) || (is_array($getPackNumberRowTypesResult) && empty($getPackNumberRowTypesResult))) {
            unset($this->GetPackNumberRowTypesResult);
        } else {
            $this->GetPackNumberRowTypesResult = $getPackNumberRowTypesResult;
        }
        return $this;
    }
}
