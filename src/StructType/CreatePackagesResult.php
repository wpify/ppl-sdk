<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreatePackagesResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:CreatePackagesResult
 * @subpackage Structs
 */
class CreatePackagesResult extends MyApiResultBaseOfArrayOfItemResultpewOaM6d
{
}
