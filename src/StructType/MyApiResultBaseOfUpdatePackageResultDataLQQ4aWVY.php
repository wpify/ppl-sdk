<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiResultBaseOfUpdatePackageResultDataLQQ4aWVY
 * StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiResultBaseOfUpdatePackageResultDataLQQ4aWVY
 * @subpackage Structs
 */
class MyApiResultBaseOfUpdatePackageResultDataLQQ4aWVY extends MyApiResultBaseVoid
{
    /**
     * The ResultData
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\StructType\UpdatePackageResultData
     */
    public $ResultData;
    /**
     * Constructor method for MyApiResultBaseOfUpdatePackageResultDataLQQ4aWVY
     * @uses MyApiResultBaseOfUpdatePackageResultDataLQQ4aWVY::setResultData()
     * @param \PPLSDK\StructType\UpdatePackageResultData $resultData
     */
    public function __construct(\PPLSDK\StructType\UpdatePackageResultData $resultData = null)
    {
        $this
            ->setResultData($resultData);
    }
    /**
     * Get ResultData value
     * @return \PPLSDK\StructType\UpdatePackageResultData|null
     */
    public function getResultData()
    {
        return $this->ResultData;
    }
    /**
     * Set ResultData value
     * @param \PPLSDK\StructType\UpdatePackageResultData $resultData
     * @return \PPLSDK\StructType\MyApiResultBaseOfUpdatePackageResultDataLQQ4aWVY
     */
    public function setResultData(\PPLSDK\StructType\UpdatePackageResultData $resultData = null)
    {
        $this->ResultData = $resultData;
        return $this;
    }
}
