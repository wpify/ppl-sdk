<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DocumentsBackInfo StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:DocumentsBackInfo
 * @subpackage Structs
 */
class DocumentsBackInfo extends AbstractStructBase
{
    /**
     * The Recipient
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\DocumentsBackRecipient
     */
    public $Recipient;
    /**
     * The PackNumber
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $PackNumber;
    /**
     * Constructor method for DocumentsBackInfo
     * @uses DocumentsBackInfo::setRecipient()
     * @uses DocumentsBackInfo::setPackNumber()
     * @param \PPLSDK\StructType\DocumentsBackRecipient $recipient
     * @param string $packNumber
     */
    public function __construct(\PPLSDK\StructType\DocumentsBackRecipient $recipient = null, $packNumber = null)
    {
        $this
            ->setRecipient($recipient)
            ->setPackNumber($packNumber);
    }
    /**
     * Get Recipient value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\DocumentsBackRecipient|null
     */
    public function getRecipient()
    {
        return isset($this->Recipient) ? $this->Recipient : null;
    }
    /**
     * Set Recipient value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\DocumentsBackRecipient $recipient
     * @return \PPLSDK\StructType\DocumentsBackInfo
     */
    public function setRecipient(\PPLSDK\StructType\DocumentsBackRecipient $recipient = null)
    {
        if (is_null($recipient) || (is_array($recipient) && empty($recipient))) {
            unset($this->Recipient);
        } else {
            $this->Recipient = $recipient;
        }
        return $this;
    }
    /**
     * Get PackNumber value
     * @return string|null
     */
    public function getPackNumber()
    {
        return $this->PackNumber;
    }
    /**
     * Set PackNumber value
     * @param string $packNumber
     * @return \PPLSDK\StructType\DocumentsBackInfo
     */
    public function setPackNumber($packNumber = null)
    {
        // validation for constraint: string
        if (!is_null($packNumber) && !is_string($packNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packNumber, true), gettype($packNumber)), __LINE__);
        }
        $this->PackNumber = $packNumber;
        return $this;
    }
}
