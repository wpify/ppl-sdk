<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for VersionResponse StructType
 * @subpackage Structs
 */
class VersionResponse extends AbstractStructBase
{
    /**
     * The VersionResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $VersionResult;
    /**
     * Constructor method for VersionResponse
     * @uses VersionResponse::setVersionResult()
     * @param string $versionResult
     */
    public function __construct($versionResult = null)
    {
        $this
            ->setVersionResult($versionResult);
    }
    /**
     * Get VersionResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getVersionResult()
    {
        return isset($this->VersionResult) ? $this->VersionResult : null;
    }
    /**
     * Set VersionResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $versionResult
     * @return \PPLSDK\StructType\VersionResponse
     */
    public function setVersionResult($versionResult = null)
    {
        // validation for constraint: string
        if (!is_null($versionResult) && !is_string($versionResult)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($versionResult, true), gettype($versionResult)), __LINE__);
        }
        if (is_null($versionResult) || (is_array($versionResult) && empty($versionResult))) {
            unset($this->VersionResult);
        } else {
            $this->VersionResult = $versionResult;
        }
        return $this;
    }
}
