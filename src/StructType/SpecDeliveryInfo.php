<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SpecDeliveryInfo StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:SpecDeliveryInfo
 * @subpackage Structs
 */
class SpecDeliveryInfo extends AbstractStructBase
{
    /**
     * The ParcelShopCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ParcelShopCode;
    /**
     * The SpecDelivDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SpecDelivDate;
    /**
     * The SpecDelivTimeFrom
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SpecDelivTimeFrom;
    /**
     * The SpecDelivTimeTo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SpecDelivTimeTo;
    /**
     * The SpecTakeDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SpecTakeDate;
    /**
     * The SpecTakeTimeFrom
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SpecTakeTimeFrom;
    /**
     * The SpecTakeTimeTo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SpecTakeTimeTo;
    /**
     * Constructor method for SpecDeliveryInfo
     * @uses SpecDeliveryInfo::setParcelShopCode()
     * @uses SpecDeliveryInfo::setSpecDelivDate()
     * @uses SpecDeliveryInfo::setSpecDelivTimeFrom()
     * @uses SpecDeliveryInfo::setSpecDelivTimeTo()
     * @uses SpecDeliveryInfo::setSpecTakeDate()
     * @uses SpecDeliveryInfo::setSpecTakeTimeFrom()
     * @uses SpecDeliveryInfo::setSpecTakeTimeTo()
     * @param string $parcelShopCode
     * @param string $specDelivDate
     * @param string $specDelivTimeFrom
     * @param string $specDelivTimeTo
     * @param string $specTakeDate
     * @param string $specTakeTimeFrom
     * @param string $specTakeTimeTo
     */
    public function __construct($parcelShopCode = null, $specDelivDate = null, $specDelivTimeFrom = null, $specDelivTimeTo = null, $specTakeDate = null, $specTakeTimeFrom = null, $specTakeTimeTo = null)
    {
        $this
            ->setParcelShopCode($parcelShopCode)
            ->setSpecDelivDate($specDelivDate)
            ->setSpecDelivTimeFrom($specDelivTimeFrom)
            ->setSpecDelivTimeTo($specDelivTimeTo)
            ->setSpecTakeDate($specTakeDate)
            ->setSpecTakeTimeFrom($specTakeTimeFrom)
            ->setSpecTakeTimeTo($specTakeTimeTo);
    }
    /**
     * Get ParcelShopCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getParcelShopCode()
    {
        return isset($this->ParcelShopCode) ? $this->ParcelShopCode : null;
    }
    /**
     * Set ParcelShopCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $parcelShopCode
     * @return \PPLSDK\StructType\SpecDeliveryInfo
     */
    public function setParcelShopCode($parcelShopCode = null)
    {
        // validation for constraint: string
        if (!is_null($parcelShopCode) && !is_string($parcelShopCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelShopCode, true), gettype($parcelShopCode)), __LINE__);
        }
        if (is_null($parcelShopCode) || (is_array($parcelShopCode) && empty($parcelShopCode))) {
            unset($this->ParcelShopCode);
        } else {
            $this->ParcelShopCode = $parcelShopCode;
        }
        return $this;
    }
    /**
     * Get SpecDelivDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSpecDelivDate()
    {
        return isset($this->SpecDelivDate) ? $this->SpecDelivDate : null;
    }
    /**
     * Set SpecDelivDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $specDelivDate
     * @return \PPLSDK\StructType\SpecDeliveryInfo
     */
    public function setSpecDelivDate($specDelivDate = null)
    {
        // validation for constraint: string
        if (!is_null($specDelivDate) && !is_string($specDelivDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($specDelivDate, true), gettype($specDelivDate)), __LINE__);
        }
        if (is_null($specDelivDate) || (is_array($specDelivDate) && empty($specDelivDate))) {
            unset($this->SpecDelivDate);
        } else {
            $this->SpecDelivDate = $specDelivDate;
        }
        return $this;
    }
    /**
     * Get SpecDelivTimeFrom value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSpecDelivTimeFrom()
    {
        return isset($this->SpecDelivTimeFrom) ? $this->SpecDelivTimeFrom : null;
    }
    /**
     * Set SpecDelivTimeFrom value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $specDelivTimeFrom
     * @return \PPLSDK\StructType\SpecDeliveryInfo
     */
    public function setSpecDelivTimeFrom($specDelivTimeFrom = null)
    {
        // validation for constraint: string
        if (!is_null($specDelivTimeFrom) && !is_string($specDelivTimeFrom)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($specDelivTimeFrom, true), gettype($specDelivTimeFrom)), __LINE__);
        }
        if (is_null($specDelivTimeFrom) || (is_array($specDelivTimeFrom) && empty($specDelivTimeFrom))) {
            unset($this->SpecDelivTimeFrom);
        } else {
            $this->SpecDelivTimeFrom = $specDelivTimeFrom;
        }
        return $this;
    }
    /**
     * Get SpecDelivTimeTo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSpecDelivTimeTo()
    {
        return isset($this->SpecDelivTimeTo) ? $this->SpecDelivTimeTo : null;
    }
    /**
     * Set SpecDelivTimeTo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $specDelivTimeTo
     * @return \PPLSDK\StructType\SpecDeliveryInfo
     */
    public function setSpecDelivTimeTo($specDelivTimeTo = null)
    {
        // validation for constraint: string
        if (!is_null($specDelivTimeTo) && !is_string($specDelivTimeTo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($specDelivTimeTo, true), gettype($specDelivTimeTo)), __LINE__);
        }
        if (is_null($specDelivTimeTo) || (is_array($specDelivTimeTo) && empty($specDelivTimeTo))) {
            unset($this->SpecDelivTimeTo);
        } else {
            $this->SpecDelivTimeTo = $specDelivTimeTo;
        }
        return $this;
    }
    /**
     * Get SpecTakeDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSpecTakeDate()
    {
        return isset($this->SpecTakeDate) ? $this->SpecTakeDate : null;
    }
    /**
     * Set SpecTakeDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $specTakeDate
     * @return \PPLSDK\StructType\SpecDeliveryInfo
     */
    public function setSpecTakeDate($specTakeDate = null)
    {
        // validation for constraint: string
        if (!is_null($specTakeDate) && !is_string($specTakeDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($specTakeDate, true), gettype($specTakeDate)), __LINE__);
        }
        if (is_null($specTakeDate) || (is_array($specTakeDate) && empty($specTakeDate))) {
            unset($this->SpecTakeDate);
        } else {
            $this->SpecTakeDate = $specTakeDate;
        }
        return $this;
    }
    /**
     * Get SpecTakeTimeFrom value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSpecTakeTimeFrom()
    {
        return isset($this->SpecTakeTimeFrom) ? $this->SpecTakeTimeFrom : null;
    }
    /**
     * Set SpecTakeTimeFrom value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $specTakeTimeFrom
     * @return \PPLSDK\StructType\SpecDeliveryInfo
     */
    public function setSpecTakeTimeFrom($specTakeTimeFrom = null)
    {
        // validation for constraint: string
        if (!is_null($specTakeTimeFrom) && !is_string($specTakeTimeFrom)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($specTakeTimeFrom, true), gettype($specTakeTimeFrom)), __LINE__);
        }
        if (is_null($specTakeTimeFrom) || (is_array($specTakeTimeFrom) && empty($specTakeTimeFrom))) {
            unset($this->SpecTakeTimeFrom);
        } else {
            $this->SpecTakeTimeFrom = $specTakeTimeFrom;
        }
        return $this;
    }
    /**
     * Get SpecTakeTimeTo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSpecTakeTimeTo()
    {
        return isset($this->SpecTakeTimeTo) ? $this->SpecTakeTimeTo : null;
    }
    /**
     * Set SpecTakeTimeTo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $specTakeTimeTo
     * @return \PPLSDK\StructType\SpecDeliveryInfo
     */
    public function setSpecTakeTimeTo($specTakeTimeTo = null)
    {
        // validation for constraint: string
        if (!is_null($specTakeTimeTo) && !is_string($specTakeTimeTo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($specTakeTimeTo, true), gettype($specTakeTimeTo)), __LINE__);
        }
        if (is_null($specTakeTimeTo) || (is_array($specTakeTimeTo) && empty($specTakeTimeTo))) {
            unset($this->SpecTakeTimeTo);
        } else {
            $this->SpecTakeTimeTo = $specTakeTimeTo;
        }
        return $this;
    }
}
