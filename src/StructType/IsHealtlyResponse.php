<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for IsHealtlyResponse StructType
 * @subpackage Structs
 */
class IsHealtlyResponse extends AbstractStructBase
{
    /**
     * The IsHealtlyResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $IsHealtlyResult;
    /**
     * Constructor method for IsHealtlyResponse
     * @uses IsHealtlyResponse::setIsHealtlyResult()
     * @param string $isHealtlyResult
     */
    public function __construct($isHealtlyResult = null)
    {
        $this
            ->setIsHealtlyResult($isHealtlyResult);
    }
    /**
     * Get IsHealtlyResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getIsHealtlyResult()
    {
        return isset($this->IsHealtlyResult) ? $this->IsHealtlyResult : null;
    }
    /**
     * Set IsHealtlyResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $isHealtlyResult
     * @return \PPLSDK\StructType\IsHealtlyResponse
     */
    public function setIsHealtlyResult($isHealtlyResult = null)
    {
        // validation for constraint: string
        if (!is_null($isHealtlyResult) && !is_string($isHealtlyResult)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($isHealtlyResult, true), gettype($isHealtlyResult)), __LINE__);
        }
        if (is_null($isHealtlyResult) || (is_array($isHealtlyResult) && empty($isHealtlyResult))) {
            unset($this->IsHealtlyResult);
        } else {
            $this->IsHealtlyResult = $isHealtlyResult;
        }
        return $this;
    }
}
