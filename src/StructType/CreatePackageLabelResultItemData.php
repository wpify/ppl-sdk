<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreatePackageLabelResultItemData StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:CreatePackageLabelResultItemData
 * @subpackage Structs
 */
class CreatePackageLabelResultItemData extends AbstractStructBase
{
    /**
     * The UniqueReferenceId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $UniqueReferenceId;
    /**
     * The PackNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PackNumber;
    /**
     * The State
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $State;
    /**
     * The ErrorCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $ErrorCode;
    /**
     * The ErrorMessage
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ErrorMessage;
    /**
     * The LabelUrl
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $LabelUrl;
    /**
     * The DocumentBackLabelUrl
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DocumentBackLabelUrl;
    /**
     * The BackPackLabelUrl
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $BackPackLabelUrl;
    /**
     * Constructor method for CreatePackageLabelResultItemData
     * @uses CreatePackageLabelResultItemData::setUniqueReferenceId()
     * @uses CreatePackageLabelResultItemData::setPackNumber()
     * @uses CreatePackageLabelResultItemData::setState()
     * @uses CreatePackageLabelResultItemData::setErrorCode()
     * @uses CreatePackageLabelResultItemData::setErrorMessage()
     * @uses CreatePackageLabelResultItemData::setLabelUrl()
     * @uses CreatePackageLabelResultItemData::setDocumentBackLabelUrl()
     * @uses CreatePackageLabelResultItemData::setBackPackLabelUrl()
     * @param string $uniqueReferenceId
     * @param string $packNumber
     * @param string $state
     * @param int $errorCode
     * @param string $errorMessage
     * @param string $labelUrl
     * @param string $documentBackLabelUrl
     * @param string $backPackLabelUrl
     */
    public function __construct($uniqueReferenceId = null, $packNumber = null, $state = null, $errorCode = null, $errorMessage = null, $labelUrl = null, $documentBackLabelUrl = null, $backPackLabelUrl = null)
    {
        $this
            ->setUniqueReferenceId($uniqueReferenceId)
            ->setPackNumber($packNumber)
            ->setState($state)
            ->setErrorCode($errorCode)
            ->setErrorMessage($errorMessage)
            ->setLabelUrl($labelUrl)
            ->setDocumentBackLabelUrl($documentBackLabelUrl)
            ->setBackPackLabelUrl($backPackLabelUrl);
    }
    /**
     * Get UniqueReferenceId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUniqueReferenceId()
    {
        return isset($this->UniqueReferenceId) ? $this->UniqueReferenceId : null;
    }
    /**
     * Set UniqueReferenceId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $uniqueReferenceId
     * @return \PPLSDK\StructType\CreatePackageLabelResultItemData
     */
    public function setUniqueReferenceId($uniqueReferenceId = null)
    {
        // validation for constraint: string
        if (!is_null($uniqueReferenceId) && !is_string($uniqueReferenceId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($uniqueReferenceId, true), gettype($uniqueReferenceId)), __LINE__);
        }
        if (is_null($uniqueReferenceId) || (is_array($uniqueReferenceId) && empty($uniqueReferenceId))) {
            unset($this->UniqueReferenceId);
        } else {
            $this->UniqueReferenceId = $uniqueReferenceId;
        }
        return $this;
    }
    /**
     * Get PackNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPackNumber()
    {
        return isset($this->PackNumber) ? $this->PackNumber : null;
    }
    /**
     * Set PackNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $packNumber
     * @return \PPLSDK\StructType\CreatePackageLabelResultItemData
     */
    public function setPackNumber($packNumber = null)
    {
        // validation for constraint: string
        if (!is_null($packNumber) && !is_string($packNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packNumber, true), gettype($packNumber)), __LINE__);
        }
        if (is_null($packNumber) || (is_array($packNumber) && empty($packNumber))) {
            unset($this->PackNumber);
        } else {
            $this->PackNumber = $packNumber;
        }
        return $this;
    }
    /**
     * Get State value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getState()
    {
        return isset($this->State) ? $this->State : null;
    }
    /**
     * Set State value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $state
     * @return \PPLSDK\StructType\CreatePackageLabelResultItemData
     */
    public function setState($state = null)
    {
        // validation for constraint: string
        if (!is_null($state) && !is_string($state)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($state, true), gettype($state)), __LINE__);
        }
        if (is_null($state) || (is_array($state) && empty($state))) {
            unset($this->State);
        } else {
            $this->State = $state;
        }
        return $this;
    }
    /**
     * Get ErrorCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getErrorCode()
    {
        return isset($this->ErrorCode) ? $this->ErrorCode : null;
    }
    /**
     * Set ErrorCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $errorCode
     * @return \PPLSDK\StructType\CreatePackageLabelResultItemData
     */
    public function setErrorCode($errorCode = null)
    {
        // validation for constraint: int
        if (!is_null($errorCode) && !(is_int($errorCode) || ctype_digit($errorCode))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($errorCode, true), gettype($errorCode)), __LINE__);
        }
        if (is_null($errorCode) || (is_array($errorCode) && empty($errorCode))) {
            unset($this->ErrorCode);
        } else {
            $this->ErrorCode = $errorCode;
        }
        return $this;
    }
    /**
     * Get ErrorMessage value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getErrorMessage()
    {
        return isset($this->ErrorMessage) ? $this->ErrorMessage : null;
    }
    /**
     * Set ErrorMessage value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $errorMessage
     * @return \PPLSDK\StructType\CreatePackageLabelResultItemData
     */
    public function setErrorMessage($errorMessage = null)
    {
        // validation for constraint: string
        if (!is_null($errorMessage) && !is_string($errorMessage)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorMessage, true), gettype($errorMessage)), __LINE__);
        }
        if (is_null($errorMessage) || (is_array($errorMessage) && empty($errorMessage))) {
            unset($this->ErrorMessage);
        } else {
            $this->ErrorMessage = $errorMessage;
        }
        return $this;
    }
    /**
     * Get LabelUrl value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLabelUrl()
    {
        return isset($this->LabelUrl) ? $this->LabelUrl : null;
    }
    /**
     * Set LabelUrl value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $labelUrl
     * @return \PPLSDK\StructType\CreatePackageLabelResultItemData
     */
    public function setLabelUrl($labelUrl = null)
    {
        // validation for constraint: string
        if (!is_null($labelUrl) && !is_string($labelUrl)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($labelUrl, true), gettype($labelUrl)), __LINE__);
        }
        if (is_null($labelUrl) || (is_array($labelUrl) && empty($labelUrl))) {
            unset($this->LabelUrl);
        } else {
            $this->LabelUrl = $labelUrl;
        }
        return $this;
    }
    /**
     * Get DocumentBackLabelUrl value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDocumentBackLabelUrl()
    {
        return isset($this->DocumentBackLabelUrl) ? $this->DocumentBackLabelUrl : null;
    }
    /**
     * Set DocumentBackLabelUrl value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $documentBackLabelUrl
     * @return \PPLSDK\StructType\CreatePackageLabelResultItemData
     */
    public function setDocumentBackLabelUrl($documentBackLabelUrl = null)
    {
        // validation for constraint: string
        if (!is_null($documentBackLabelUrl) && !is_string($documentBackLabelUrl)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentBackLabelUrl, true), gettype($documentBackLabelUrl)), __LINE__);
        }
        if (is_null($documentBackLabelUrl) || (is_array($documentBackLabelUrl) && empty($documentBackLabelUrl))) {
            unset($this->DocumentBackLabelUrl);
        } else {
            $this->DocumentBackLabelUrl = $documentBackLabelUrl;
        }
        return $this;
    }
    /**
     * Get BackPackLabelUrl value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBackPackLabelUrl()
    {
        return isset($this->BackPackLabelUrl) ? $this->BackPackLabelUrl : null;
    }
    /**
     * Set BackPackLabelUrl value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $backPackLabelUrl
     * @return \PPLSDK\StructType\CreatePackageLabelResultItemData
     */
    public function setBackPackLabelUrl($backPackLabelUrl = null)
    {
        // validation for constraint: string
        if (!is_null($backPackLabelUrl) && !is_string($backPackLabelUrl)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($backPackLabelUrl, true), gettype($backPackLabelUrl)), __LINE__);
        }
        if (is_null($backPackLabelUrl) || (is_array($backPackLabelUrl) && empty($backPackLabelUrl))) {
            unset($this->BackPackLabelUrl);
        } else {
            $this->BackPackLabelUrl = $backPackLabelUrl;
        }
        return $this;
    }
}
