<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PaymentInfo StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:PaymentInfo
 * @subpackage Structs
 */
class PaymentInfo extends AbstractStructBase
{
    /**
     * The BankAccount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $BankAccount;
    /**
     * The BankCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $BankCode;
    /**
     * The CodCurrency
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CodCurrency;
    /**
     * The CodPrice
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $CodPrice;
    /**
     * The CodVarSym
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CodVarSym;
    /**
     * The IBAN
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $IBAN;
    /**
     * The InsurCurrency
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $InsurCurrency;
    /**
     * The InsurPrice
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $InsurPrice;
    /**
     * The SpecSymbol
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SpecSymbol;
    /**
     * The Swift
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Swift;
    /**
     * Constructor method for PaymentInfo
     * @uses PaymentInfo::setBankAccount()
     * @uses PaymentInfo::setBankCode()
     * @uses PaymentInfo::setCodCurrency()
     * @uses PaymentInfo::setCodPrice()
     * @uses PaymentInfo::setCodVarSym()
     * @uses PaymentInfo::setIBAN()
     * @uses PaymentInfo::setInsurCurrency()
     * @uses PaymentInfo::setInsurPrice()
     * @uses PaymentInfo::setSpecSymbol()
     * @uses PaymentInfo::setSwift()
     * @param string $bankAccount
     * @param string $bankCode
     * @param string $codCurrency
     * @param float $codPrice
     * @param string $codVarSym
     * @param string $iBAN
     * @param string $insurCurrency
     * @param float $insurPrice
     * @param string $specSymbol
     * @param string $swift
     */
    public function __construct($bankAccount = null, $bankCode = null, $codCurrency = null, $codPrice = null, $codVarSym = null, $iBAN = null, $insurCurrency = null, $insurPrice = null, $specSymbol = null, $swift = null)
    {
        $this
            ->setBankAccount($bankAccount)
            ->setBankCode($bankCode)
            ->setCodCurrency($codCurrency)
            ->setCodPrice($codPrice)
            ->setCodVarSym($codVarSym)
            ->setIBAN($iBAN)
            ->setInsurCurrency($insurCurrency)
            ->setInsurPrice($insurPrice)
            ->setSpecSymbol($specSymbol)
            ->setSwift($swift);
    }
    /**
     * Get BankAccount value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBankAccount()
    {
        return isset($this->BankAccount) ? $this->BankAccount : null;
    }
    /**
     * Set BankAccount value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $bankAccount
     * @return \PPLSDK\StructType\PaymentInfo
     */
    public function setBankAccount($bankAccount = null)
    {
        // validation for constraint: string
        if (!is_null($bankAccount) && !is_string($bankAccount)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($bankAccount, true), gettype($bankAccount)), __LINE__);
        }
        if (is_null($bankAccount) || (is_array($bankAccount) && empty($bankAccount))) {
            unset($this->BankAccount);
        } else {
            $this->BankAccount = $bankAccount;
        }
        return $this;
    }
    /**
     * Get BankCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBankCode()
    {
        return isset($this->BankCode) ? $this->BankCode : null;
    }
    /**
     * Set BankCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $bankCode
     * @return \PPLSDK\StructType\PaymentInfo
     */
    public function setBankCode($bankCode = null)
    {
        // validation for constraint: string
        if (!is_null($bankCode) && !is_string($bankCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($bankCode, true), gettype($bankCode)), __LINE__);
        }
        if (is_null($bankCode) || (is_array($bankCode) && empty($bankCode))) {
            unset($this->BankCode);
        } else {
            $this->BankCode = $bankCode;
        }
        return $this;
    }
    /**
     * Get CodCurrency value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCodCurrency()
    {
        return isset($this->CodCurrency) ? $this->CodCurrency : null;
    }
    /**
     * Set CodCurrency value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $codCurrency
     * @return \PPLSDK\StructType\PaymentInfo
     */
    public function setCodCurrency($codCurrency = null)
    {
        // validation for constraint: string
        if (!is_null($codCurrency) && !is_string($codCurrency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($codCurrency, true), gettype($codCurrency)), __LINE__);
        }
        if (is_null($codCurrency) || (is_array($codCurrency) && empty($codCurrency))) {
            unset($this->CodCurrency);
        } else {
            $this->CodCurrency = $codCurrency;
        }
        return $this;
    }
    /**
     * Get CodPrice value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getCodPrice()
    {
        return isset($this->CodPrice) ? $this->CodPrice : null;
    }
    /**
     * Set CodPrice value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $codPrice
     * @return \PPLSDK\StructType\PaymentInfo
     */
    public function setCodPrice($codPrice = null)
    {
        // validation for constraint: float
        if (!is_null($codPrice) && !(is_float($codPrice) || is_numeric($codPrice))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($codPrice, true), gettype($codPrice)), __LINE__);
        }
        if (is_null($codPrice) || (is_array($codPrice) && empty($codPrice))) {
            unset($this->CodPrice);
        } else {
            $this->CodPrice = $codPrice;
        }
        return $this;
    }
    /**
     * Get CodVarSym value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCodVarSym()
    {
        return isset($this->CodVarSym) ? $this->CodVarSym : null;
    }
    /**
     * Set CodVarSym value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $codVarSym
     * @return \PPLSDK\StructType\PaymentInfo
     */
    public function setCodVarSym($codVarSym = null)
    {
        // validation for constraint: string
        if (!is_null($codVarSym) && !is_string($codVarSym)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($codVarSym, true), gettype($codVarSym)), __LINE__);
        }
        if (is_null($codVarSym) || (is_array($codVarSym) && empty($codVarSym))) {
            unset($this->CodVarSym);
        } else {
            $this->CodVarSym = $codVarSym;
        }
        return $this;
    }
    /**
     * Get IBAN value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getIBAN()
    {
        return isset($this->IBAN) ? $this->IBAN : null;
    }
    /**
     * Set IBAN value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $iBAN
     * @return \PPLSDK\StructType\PaymentInfo
     */
    public function setIBAN($iBAN = null)
    {
        // validation for constraint: string
        if (!is_null($iBAN) && !is_string($iBAN)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($iBAN, true), gettype($iBAN)), __LINE__);
        }
        if (is_null($iBAN) || (is_array($iBAN) && empty($iBAN))) {
            unset($this->IBAN);
        } else {
            $this->IBAN = $iBAN;
        }
        return $this;
    }
    /**
     * Get InsurCurrency value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getInsurCurrency()
    {
        return isset($this->InsurCurrency) ? $this->InsurCurrency : null;
    }
    /**
     * Set InsurCurrency value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $insurCurrency
     * @return \PPLSDK\StructType\PaymentInfo
     */
    public function setInsurCurrency($insurCurrency = null)
    {
        // validation for constraint: string
        if (!is_null($insurCurrency) && !is_string($insurCurrency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($insurCurrency, true), gettype($insurCurrency)), __LINE__);
        }
        if (is_null($insurCurrency) || (is_array($insurCurrency) && empty($insurCurrency))) {
            unset($this->InsurCurrency);
        } else {
            $this->InsurCurrency = $insurCurrency;
        }
        return $this;
    }
    /**
     * Get InsurPrice value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getInsurPrice()
    {
        return isset($this->InsurPrice) ? $this->InsurPrice : null;
    }
    /**
     * Set InsurPrice value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $insurPrice
     * @return \PPLSDK\StructType\PaymentInfo
     */
    public function setInsurPrice($insurPrice = null)
    {
        // validation for constraint: float
        if (!is_null($insurPrice) && !(is_float($insurPrice) || is_numeric($insurPrice))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($insurPrice, true), gettype($insurPrice)), __LINE__);
        }
        if (is_null($insurPrice) || (is_array($insurPrice) && empty($insurPrice))) {
            unset($this->InsurPrice);
        } else {
            $this->InsurPrice = $insurPrice;
        }
        return $this;
    }
    /**
     * Get SpecSymbol value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSpecSymbol()
    {
        return isset($this->SpecSymbol) ? $this->SpecSymbol : null;
    }
    /**
     * Set SpecSymbol value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $specSymbol
     * @return \PPLSDK\StructType\PaymentInfo
     */
    public function setSpecSymbol($specSymbol = null)
    {
        // validation for constraint: string
        if (!is_null($specSymbol) && !is_string($specSymbol)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($specSymbol, true), gettype($specSymbol)), __LINE__);
        }
        if (is_null($specSymbol) || (is_array($specSymbol) && empty($specSymbol))) {
            unset($this->SpecSymbol);
        } else {
            $this->SpecSymbol = $specSymbol;
        }
        return $this;
    }
    /**
     * Get Swift value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSwift()
    {
        return isset($this->Swift) ? $this->Swift : null;
    }
    /**
     * Set Swift value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $swift
     * @return \PPLSDK\StructType\PaymentInfo
     */
    public function setSwift($swift = null)
    {
        // validation for constraint: string
        if (!is_null($swift) && !is_string($swift)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($swift, true), gettype($swift)), __LINE__);
        }
        if (is_null($swift) || (is_array($swift) && empty($swift))) {
            unset($this->Swift);
        } else {
            $this->Swift = $swift;
        }
        return $this;
    }
}
