<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiPackageOut StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiPackageOut
 * @subpackage Structs
 */
class MyApiPackageOut extends AbstractStructBase
{
    /**
     * The BackDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $BackDate;
    /**
     * The BackPackNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $BackPackNumber;
    /**
     * The BackPackNumberActive
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $BackPackNumberActive;
    /**
     * The BackedDoc
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $BackedDoc;
    /**
     * The DelivDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DelivDate;
    /**
     * The DelivPerson
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DelivPerson;
    /**
     * The DeliveryToKtm
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $DeliveryToKtm;
    /**
     * The DepInCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DepInCode;
    /**
     * The DepInName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DepInName;
    /**
     * The DepOutCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DepOutCode;
    /**
     * The DepOutName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DepOutName;
    /**
     * The DepoCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DepoCode;
    /**
     * The HubDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $HubDate;
    /**
     * The LoadDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $LoadDate;
    /**
     * The NotDelivDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $NotDelivDate;
    /**
     * The Note
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Note;
    /**
     * The OutDepDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $OutDepDate;
    /**
     * The PackNumber
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $PackNumber;
    /**
     * The PackProductType
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $PackProductType;
    /**
     * The PackageSet
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPackageSet
     */
    public $PackageSet;
    /**
     * The PackageStatuses
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiPackageOutStatus
     */
    public $PackageStatuses;
    /**
     * The PackagesExtNums
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiPackageExtNum
     */
    public $PackagesExtNums;
    /**
     * The PackagesServices
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiPackageOutService
     */
    public $PackagesServices;
    /**
     * The PalletInfo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\PalletInfoOut
     */
    public $PalletInfo;
    /**
     * The PaymentInfo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\PaymentInfoOut
     */
    public $PaymentInfo;
    /**
     * The Recipient
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPackageInRecipient
     */
    public $Recipient;
    /**
     * The Sender
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPackageInSender
     */
    public $Sender;
    /**
     * The SpecDelivery
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\SpecDeliveryInfo
     */
    public $SpecDelivery;
    /**
     * The TakeDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $TakeDate;
    /**
     * The Weight
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $Weight;
    /**
     * The WeightVol
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $WeightVol;
    /**
     * The WeightedDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $WeightedDate;
    /**
     * The Flags
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiFlag
     */
    public $Flags;
    /**
     * Constructor method for MyApiPackageOut
     * @uses MyApiPackageOut::setBackDate()
     * @uses MyApiPackageOut::setBackPackNumber()
     * @uses MyApiPackageOut::setBackPackNumberActive()
     * @uses MyApiPackageOut::setBackedDoc()
     * @uses MyApiPackageOut::setDelivDate()
     * @uses MyApiPackageOut::setDelivPerson()
     * @uses MyApiPackageOut::setDeliveryToKtm()
     * @uses MyApiPackageOut::setDepInCode()
     * @uses MyApiPackageOut::setDepInName()
     * @uses MyApiPackageOut::setDepOutCode()
     * @uses MyApiPackageOut::setDepOutName()
     * @uses MyApiPackageOut::setDepoCode()
     * @uses MyApiPackageOut::setHubDate()
     * @uses MyApiPackageOut::setLoadDate()
     * @uses MyApiPackageOut::setNotDelivDate()
     * @uses MyApiPackageOut::setNote()
     * @uses MyApiPackageOut::setOutDepDate()
     * @uses MyApiPackageOut::setPackNumber()
     * @uses MyApiPackageOut::setPackProductType()
     * @uses MyApiPackageOut::setPackageSet()
     * @uses MyApiPackageOut::setPackageStatuses()
     * @uses MyApiPackageOut::setPackagesExtNums()
     * @uses MyApiPackageOut::setPackagesServices()
     * @uses MyApiPackageOut::setPalletInfo()
     * @uses MyApiPackageOut::setPaymentInfo()
     * @uses MyApiPackageOut::setRecipient()
     * @uses MyApiPackageOut::setSender()
     * @uses MyApiPackageOut::setSpecDelivery()
     * @uses MyApiPackageOut::setTakeDate()
     * @uses MyApiPackageOut::setWeight()
     * @uses MyApiPackageOut::setWeightVol()
     * @uses MyApiPackageOut::setWeightedDate()
     * @uses MyApiPackageOut::setFlags()
     * @param string $backDate
     * @param string $backPackNumber
     * @param bool $backPackNumberActive
     * @param string $backedDoc
     * @param string $delivDate
     * @param string $delivPerson
     * @param bool $deliveryToKtm
     * @param string $depInCode
     * @param string $depInName
     * @param string $depOutCode
     * @param string $depOutName
     * @param string $depoCode
     * @param string $hubDate
     * @param string $loadDate
     * @param string $notDelivDate
     * @param string $note
     * @param string $outDepDate
     * @param string $packNumber
     * @param string $packProductType
     * @param \PPLSDK\StructType\MyApiPackageSet $packageSet
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageOutStatus $packageStatuses
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageExtNum $packagesExtNums
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageOutService $packagesServices
     * @param \PPLSDK\StructType\PalletInfoOut $palletInfo
     * @param \PPLSDK\StructType\PaymentInfoOut $paymentInfo
     * @param \PPLSDK\StructType\MyApiPackageInRecipient $recipient
     * @param \PPLSDK\StructType\MyApiPackageInSender $sender
     * @param \PPLSDK\StructType\SpecDeliveryInfo $specDelivery
     * @param string $takeDate
     * @param float $weight
     * @param float $weightVol
     * @param string $weightedDate
     * @param \PPLSDK\ArrayType\ArrayOfMyApiFlag $flags
     */
    public function __construct($backDate = null, $backPackNumber = null, $backPackNumberActive = null, $backedDoc = null, $delivDate = null, $delivPerson = null, $deliveryToKtm = null, $depInCode = null, $depInName = null, $depOutCode = null, $depOutName = null, $depoCode = null, $hubDate = null, $loadDate = null, $notDelivDate = null, $note = null, $outDepDate = null, $packNumber = null, $packProductType = null, \PPLSDK\StructType\MyApiPackageSet $packageSet = null, \PPLSDK\ArrayType\ArrayOfMyApiPackageOutStatus $packageStatuses = null, \PPLSDK\ArrayType\ArrayOfMyApiPackageExtNum $packagesExtNums = null, \PPLSDK\ArrayType\ArrayOfMyApiPackageOutService $packagesServices = null, \PPLSDK\StructType\PalletInfoOut $palletInfo = null, \PPLSDK\StructType\PaymentInfoOut $paymentInfo = null, \PPLSDK\StructType\MyApiPackageInRecipient $recipient = null, \PPLSDK\StructType\MyApiPackageInSender $sender = null, \PPLSDK\StructType\SpecDeliveryInfo $specDelivery = null, $takeDate = null, $weight = null, $weightVol = null, $weightedDate = null, \PPLSDK\ArrayType\ArrayOfMyApiFlag $flags = null)
    {
        $this
            ->setBackDate($backDate)
            ->setBackPackNumber($backPackNumber)
            ->setBackPackNumberActive($backPackNumberActive)
            ->setBackedDoc($backedDoc)
            ->setDelivDate($delivDate)
            ->setDelivPerson($delivPerson)
            ->setDeliveryToKtm($deliveryToKtm)
            ->setDepInCode($depInCode)
            ->setDepInName($depInName)
            ->setDepOutCode($depOutCode)
            ->setDepOutName($depOutName)
            ->setDepoCode($depoCode)
            ->setHubDate($hubDate)
            ->setLoadDate($loadDate)
            ->setNotDelivDate($notDelivDate)
            ->setNote($note)
            ->setOutDepDate($outDepDate)
            ->setPackNumber($packNumber)
            ->setPackProductType($packProductType)
            ->setPackageSet($packageSet)
            ->setPackageStatuses($packageStatuses)
            ->setPackagesExtNums($packagesExtNums)
            ->setPackagesServices($packagesServices)
            ->setPalletInfo($palletInfo)
            ->setPaymentInfo($paymentInfo)
            ->setRecipient($recipient)
            ->setSender($sender)
            ->setSpecDelivery($specDelivery)
            ->setTakeDate($takeDate)
            ->setWeight($weight)
            ->setWeightVol($weightVol)
            ->setWeightedDate($weightedDate)
            ->setFlags($flags);
    }
    /**
     * Get BackDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBackDate()
    {
        return isset($this->BackDate) ? $this->BackDate : null;
    }
    /**
     * Set BackDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $backDate
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setBackDate($backDate = null)
    {
        // validation for constraint: string
        if (!is_null($backDate) && !is_string($backDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($backDate, true), gettype($backDate)), __LINE__);
        }
        if (is_null($backDate) || (is_array($backDate) && empty($backDate))) {
            unset($this->BackDate);
        } else {
            $this->BackDate = $backDate;
        }
        return $this;
    }
    /**
     * Get BackPackNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBackPackNumber()
    {
        return isset($this->BackPackNumber) ? $this->BackPackNumber : null;
    }
    /**
     * Set BackPackNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $backPackNumber
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setBackPackNumber($backPackNumber = null)
    {
        // validation for constraint: string
        if (!is_null($backPackNumber) && !is_string($backPackNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($backPackNumber, true), gettype($backPackNumber)), __LINE__);
        }
        if (is_null($backPackNumber) || (is_array($backPackNumber) && empty($backPackNumber))) {
            unset($this->BackPackNumber);
        } else {
            $this->BackPackNumber = $backPackNumber;
        }
        return $this;
    }
    /**
     * Get BackPackNumberActive value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getBackPackNumberActive()
    {
        return isset($this->BackPackNumberActive) ? $this->BackPackNumberActive : null;
    }
    /**
     * Set BackPackNumberActive value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $backPackNumberActive
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setBackPackNumberActive($backPackNumberActive = null)
    {
        // validation for constraint: boolean
        if (!is_null($backPackNumberActive) && !is_bool($backPackNumberActive)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($backPackNumberActive, true), gettype($backPackNumberActive)), __LINE__);
        }
        if (is_null($backPackNumberActive) || (is_array($backPackNumberActive) && empty($backPackNumberActive))) {
            unset($this->BackPackNumberActive);
        } else {
            $this->BackPackNumberActive = $backPackNumberActive;
        }
        return $this;
    }
    /**
     * Get BackedDoc value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBackedDoc()
    {
        return isset($this->BackedDoc) ? $this->BackedDoc : null;
    }
    /**
     * Set BackedDoc value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $backedDoc
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setBackedDoc($backedDoc = null)
    {
        // validation for constraint: string
        if (!is_null($backedDoc) && !is_string($backedDoc)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($backedDoc, true), gettype($backedDoc)), __LINE__);
        }
        if (is_null($backedDoc) || (is_array($backedDoc) && empty($backedDoc))) {
            unset($this->BackedDoc);
        } else {
            $this->BackedDoc = $backedDoc;
        }
        return $this;
    }
    /**
     * Get DelivDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDelivDate()
    {
        return isset($this->DelivDate) ? $this->DelivDate : null;
    }
    /**
     * Set DelivDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $delivDate
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setDelivDate($delivDate = null)
    {
        // validation for constraint: string
        if (!is_null($delivDate) && !is_string($delivDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($delivDate, true), gettype($delivDate)), __LINE__);
        }
        if (is_null($delivDate) || (is_array($delivDate) && empty($delivDate))) {
            unset($this->DelivDate);
        } else {
            $this->DelivDate = $delivDate;
        }
        return $this;
    }
    /**
     * Get DelivPerson value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDelivPerson()
    {
        return isset($this->DelivPerson) ? $this->DelivPerson : null;
    }
    /**
     * Set DelivPerson value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $delivPerson
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setDelivPerson($delivPerson = null)
    {
        // validation for constraint: string
        if (!is_null($delivPerson) && !is_string($delivPerson)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($delivPerson, true), gettype($delivPerson)), __LINE__);
        }
        if (is_null($delivPerson) || (is_array($delivPerson) && empty($delivPerson))) {
            unset($this->DelivPerson);
        } else {
            $this->DelivPerson = $delivPerson;
        }
        return $this;
    }
    /**
     * Get DeliveryToKtm value
     * @return bool|null
     */
    public function getDeliveryToKtm()
    {
        return $this->DeliveryToKtm;
    }
    /**
     * Set DeliveryToKtm value
     * @param bool $deliveryToKtm
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setDeliveryToKtm($deliveryToKtm = null)
    {
        // validation for constraint: boolean
        if (!is_null($deliveryToKtm) && !is_bool($deliveryToKtm)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($deliveryToKtm, true), gettype($deliveryToKtm)), __LINE__);
        }
        $this->DeliveryToKtm = $deliveryToKtm;
        return $this;
    }
    /**
     * Get DepInCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDepInCode()
    {
        return isset($this->DepInCode) ? $this->DepInCode : null;
    }
    /**
     * Set DepInCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $depInCode
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setDepInCode($depInCode = null)
    {
        // validation for constraint: string
        if (!is_null($depInCode) && !is_string($depInCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($depInCode, true), gettype($depInCode)), __LINE__);
        }
        if (is_null($depInCode) || (is_array($depInCode) && empty($depInCode))) {
            unset($this->DepInCode);
        } else {
            $this->DepInCode = $depInCode;
        }
        return $this;
    }
    /**
     * Get DepInName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDepInName()
    {
        return isset($this->DepInName) ? $this->DepInName : null;
    }
    /**
     * Set DepInName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $depInName
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setDepInName($depInName = null)
    {
        // validation for constraint: string
        if (!is_null($depInName) && !is_string($depInName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($depInName, true), gettype($depInName)), __LINE__);
        }
        if (is_null($depInName) || (is_array($depInName) && empty($depInName))) {
            unset($this->DepInName);
        } else {
            $this->DepInName = $depInName;
        }
        return $this;
    }
    /**
     * Get DepOutCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDepOutCode()
    {
        return isset($this->DepOutCode) ? $this->DepOutCode : null;
    }
    /**
     * Set DepOutCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $depOutCode
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setDepOutCode($depOutCode = null)
    {
        // validation for constraint: string
        if (!is_null($depOutCode) && !is_string($depOutCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($depOutCode, true), gettype($depOutCode)), __LINE__);
        }
        if (is_null($depOutCode) || (is_array($depOutCode) && empty($depOutCode))) {
            unset($this->DepOutCode);
        } else {
            $this->DepOutCode = $depOutCode;
        }
        return $this;
    }
    /**
     * Get DepOutName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDepOutName()
    {
        return isset($this->DepOutName) ? $this->DepOutName : null;
    }
    /**
     * Set DepOutName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $depOutName
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setDepOutName($depOutName = null)
    {
        // validation for constraint: string
        if (!is_null($depOutName) && !is_string($depOutName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($depOutName, true), gettype($depOutName)), __LINE__);
        }
        if (is_null($depOutName) || (is_array($depOutName) && empty($depOutName))) {
            unset($this->DepOutName);
        } else {
            $this->DepOutName = $depOutName;
        }
        return $this;
    }
    /**
     * Get DepoCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDepoCode()
    {
        return isset($this->DepoCode) ? $this->DepoCode : null;
    }
    /**
     * Set DepoCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $depoCode
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setDepoCode($depoCode = null)
    {
        // validation for constraint: string
        if (!is_null($depoCode) && !is_string($depoCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($depoCode, true), gettype($depoCode)), __LINE__);
        }
        if (is_null($depoCode) || (is_array($depoCode) && empty($depoCode))) {
            unset($this->DepoCode);
        } else {
            $this->DepoCode = $depoCode;
        }
        return $this;
    }
    /**
     * Get HubDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getHubDate()
    {
        return isset($this->HubDate) ? $this->HubDate : null;
    }
    /**
     * Set HubDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $hubDate
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setHubDate($hubDate = null)
    {
        // validation for constraint: string
        if (!is_null($hubDate) && !is_string($hubDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($hubDate, true), gettype($hubDate)), __LINE__);
        }
        if (is_null($hubDate) || (is_array($hubDate) && empty($hubDate))) {
            unset($this->HubDate);
        } else {
            $this->HubDate = $hubDate;
        }
        return $this;
    }
    /**
     * Get LoadDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLoadDate()
    {
        return isset($this->LoadDate) ? $this->LoadDate : null;
    }
    /**
     * Set LoadDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $loadDate
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setLoadDate($loadDate = null)
    {
        // validation for constraint: string
        if (!is_null($loadDate) && !is_string($loadDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($loadDate, true), gettype($loadDate)), __LINE__);
        }
        if (is_null($loadDate) || (is_array($loadDate) && empty($loadDate))) {
            unset($this->LoadDate);
        } else {
            $this->LoadDate = $loadDate;
        }
        return $this;
    }
    /**
     * Get NotDelivDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNotDelivDate()
    {
        return isset($this->NotDelivDate) ? $this->NotDelivDate : null;
    }
    /**
     * Set NotDelivDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $notDelivDate
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setNotDelivDate($notDelivDate = null)
    {
        // validation for constraint: string
        if (!is_null($notDelivDate) && !is_string($notDelivDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($notDelivDate, true), gettype($notDelivDate)), __LINE__);
        }
        if (is_null($notDelivDate) || (is_array($notDelivDate) && empty($notDelivDate))) {
            unset($this->NotDelivDate);
        } else {
            $this->NotDelivDate = $notDelivDate;
        }
        return $this;
    }
    /**
     * Get Note value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNote()
    {
        return isset($this->Note) ? $this->Note : null;
    }
    /**
     * Set Note value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $note
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setNote($note = null)
    {
        // validation for constraint: string
        if (!is_null($note) && !is_string($note)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($note, true), gettype($note)), __LINE__);
        }
        if (is_null($note) || (is_array($note) && empty($note))) {
            unset($this->Note);
        } else {
            $this->Note = $note;
        }
        return $this;
    }
    /**
     * Get OutDepDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOutDepDate()
    {
        return isset($this->OutDepDate) ? $this->OutDepDate : null;
    }
    /**
     * Set OutDepDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $outDepDate
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setOutDepDate($outDepDate = null)
    {
        // validation for constraint: string
        if (!is_null($outDepDate) && !is_string($outDepDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($outDepDate, true), gettype($outDepDate)), __LINE__);
        }
        if (is_null($outDepDate) || (is_array($outDepDate) && empty($outDepDate))) {
            unset($this->OutDepDate);
        } else {
            $this->OutDepDate = $outDepDate;
        }
        return $this;
    }
    /**
     * Get PackNumber value
     * @return string|null
     */
    public function getPackNumber()
    {
        return $this->PackNumber;
    }
    /**
     * Set PackNumber value
     * @param string $packNumber
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setPackNumber($packNumber = null)
    {
        // validation for constraint: string
        if (!is_null($packNumber) && !is_string($packNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packNumber, true), gettype($packNumber)), __LINE__);
        }
        $this->PackNumber = $packNumber;
        return $this;
    }
    /**
     * Get PackProductType value
     * @return string|null
     */
    public function getPackProductType()
    {
        return $this->PackProductType;
    }
    /**
     * Set PackProductType value
     * @param string $packProductType
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setPackProductType($packProductType = null)
    {
        // validation for constraint: string
        if (!is_null($packProductType) && !is_string($packProductType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packProductType, true), gettype($packProductType)), __LINE__);
        }
        $this->PackProductType = $packProductType;
        return $this;
    }
    /**
     * Get PackageSet value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiPackageSet|null
     */
    public function getPackageSet()
    {
        return isset($this->PackageSet) ? $this->PackageSet : null;
    }
    /**
     * Set PackageSet value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\MyApiPackageSet $packageSet
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setPackageSet(\PPLSDK\StructType\MyApiPackageSet $packageSet = null)
    {
        if (is_null($packageSet) || (is_array($packageSet) && empty($packageSet))) {
            unset($this->PackageSet);
        } else {
            $this->PackageSet = $packageSet;
        }
        return $this;
    }
    /**
     * Get PackageStatuses value
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageOutStatus|null
     */
    public function getPackageStatuses()
    {
        return $this->PackageStatuses;
    }
    /**
     * Set PackageStatuses value
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageOutStatus $packageStatuses
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setPackageStatuses(\PPLSDK\ArrayType\ArrayOfMyApiPackageOutStatus $packageStatuses = null)
    {
        $this->PackageStatuses = $packageStatuses;
        return $this;
    }
    /**
     * Get PackagesExtNums value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageExtNum|null
     */
    public function getPackagesExtNums()
    {
        return isset($this->PackagesExtNums) ? $this->PackagesExtNums : null;
    }
    /**
     * Set PackagesExtNums value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageExtNum $packagesExtNums
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setPackagesExtNums(\PPLSDK\ArrayType\ArrayOfMyApiPackageExtNum $packagesExtNums = null)
    {
        if (is_null($packagesExtNums) || (is_array($packagesExtNums) && empty($packagesExtNums))) {
            unset($this->PackagesExtNums);
        } else {
            $this->PackagesExtNums = $packagesExtNums;
        }
        return $this;
    }
    /**
     * Get PackagesServices value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageOutService|null
     */
    public function getPackagesServices()
    {
        return isset($this->PackagesServices) ? $this->PackagesServices : null;
    }
    /**
     * Set PackagesServices value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageOutService $packagesServices
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setPackagesServices(\PPLSDK\ArrayType\ArrayOfMyApiPackageOutService $packagesServices = null)
    {
        if (is_null($packagesServices) || (is_array($packagesServices) && empty($packagesServices))) {
            unset($this->PackagesServices);
        } else {
            $this->PackagesServices = $packagesServices;
        }
        return $this;
    }
    /**
     * Get PalletInfo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\PalletInfoOut|null
     */
    public function getPalletInfo()
    {
        return isset($this->PalletInfo) ? $this->PalletInfo : null;
    }
    /**
     * Set PalletInfo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\PalletInfoOut $palletInfo
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setPalletInfo(\PPLSDK\StructType\PalletInfoOut $palletInfo = null)
    {
        if (is_null($palletInfo) || (is_array($palletInfo) && empty($palletInfo))) {
            unset($this->PalletInfo);
        } else {
            $this->PalletInfo = $palletInfo;
        }
        return $this;
    }
    /**
     * Get PaymentInfo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\PaymentInfoOut|null
     */
    public function getPaymentInfo()
    {
        return isset($this->PaymentInfo) ? $this->PaymentInfo : null;
    }
    /**
     * Set PaymentInfo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\PaymentInfoOut $paymentInfo
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setPaymentInfo(\PPLSDK\StructType\PaymentInfoOut $paymentInfo = null)
    {
        if (is_null($paymentInfo) || (is_array($paymentInfo) && empty($paymentInfo))) {
            unset($this->PaymentInfo);
        } else {
            $this->PaymentInfo = $paymentInfo;
        }
        return $this;
    }
    /**
     * Get Recipient value
     * @return \PPLSDK\StructType\MyApiPackageInRecipient|null
     */
    public function getRecipient()
    {
        return $this->Recipient;
    }
    /**
     * Set Recipient value
     * @param \PPLSDK\StructType\MyApiPackageInRecipient $recipient
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setRecipient(\PPLSDK\StructType\MyApiPackageInRecipient $recipient = null)
    {
        $this->Recipient = $recipient;
        return $this;
    }
    /**
     * Get Sender value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiPackageInSender|null
     */
    public function getSender()
    {
        return isset($this->Sender) ? $this->Sender : null;
    }
    /**
     * Set Sender value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\MyApiPackageInSender $sender
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setSender(\PPLSDK\StructType\MyApiPackageInSender $sender = null)
    {
        if (is_null($sender) || (is_array($sender) && empty($sender))) {
            unset($this->Sender);
        } else {
            $this->Sender = $sender;
        }
        return $this;
    }
    /**
     * Get SpecDelivery value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\SpecDeliveryInfo|null
     */
    public function getSpecDelivery()
    {
        return isset($this->SpecDelivery) ? $this->SpecDelivery : null;
    }
    /**
     * Set SpecDelivery value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\SpecDeliveryInfo $specDelivery
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setSpecDelivery(\PPLSDK\StructType\SpecDeliveryInfo $specDelivery = null)
    {
        if (is_null($specDelivery) || (is_array($specDelivery) && empty($specDelivery))) {
            unset($this->SpecDelivery);
        } else {
            $this->SpecDelivery = $specDelivery;
        }
        return $this;
    }
    /**
     * Get TakeDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTakeDate()
    {
        return isset($this->TakeDate) ? $this->TakeDate : null;
    }
    /**
     * Set TakeDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $takeDate
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setTakeDate($takeDate = null)
    {
        // validation for constraint: string
        if (!is_null($takeDate) && !is_string($takeDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($takeDate, true), gettype($takeDate)), __LINE__);
        }
        if (is_null($takeDate) || (is_array($takeDate) && empty($takeDate))) {
            unset($this->TakeDate);
        } else {
            $this->TakeDate = $takeDate;
        }
        return $this;
    }
    /**
     * Get Weight value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getWeight()
    {
        return isset($this->Weight) ? $this->Weight : null;
    }
    /**
     * Set Weight value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $weight
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setWeight($weight = null)
    {
        // validation for constraint: float
        if (!is_null($weight) && !(is_float($weight) || is_numeric($weight))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($weight, true), gettype($weight)), __LINE__);
        }
        if (is_null($weight) || (is_array($weight) && empty($weight))) {
            unset($this->Weight);
        } else {
            $this->Weight = $weight;
        }
        return $this;
    }
    /**
     * Get WeightVol value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getWeightVol()
    {
        return isset($this->WeightVol) ? $this->WeightVol : null;
    }
    /**
     * Set WeightVol value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $weightVol
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setWeightVol($weightVol = null)
    {
        // validation for constraint: float
        if (!is_null($weightVol) && !(is_float($weightVol) || is_numeric($weightVol))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($weightVol, true), gettype($weightVol)), __LINE__);
        }
        if (is_null($weightVol) || (is_array($weightVol) && empty($weightVol))) {
            unset($this->WeightVol);
        } else {
            $this->WeightVol = $weightVol;
        }
        return $this;
    }
    /**
     * Get WeightedDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getWeightedDate()
    {
        return isset($this->WeightedDate) ? $this->WeightedDate : null;
    }
    /**
     * Set WeightedDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $weightedDate
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setWeightedDate($weightedDate = null)
    {
        // validation for constraint: string
        if (!is_null($weightedDate) && !is_string($weightedDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($weightedDate, true), gettype($weightedDate)), __LINE__);
        }
        if (is_null($weightedDate) || (is_array($weightedDate) && empty($weightedDate))) {
            unset($this->WeightedDate);
        } else {
            $this->WeightedDate = $weightedDate;
        }
        return $this;
    }
    /**
     * Get Flags value
     * @return \PPLSDK\ArrayType\ArrayOfMyApiFlag|null
     */
    public function getFlags()
    {
        return $this->Flags;
    }
    /**
     * Set Flags value
     * @param \PPLSDK\ArrayType\ArrayOfMyApiFlag $flags
     * @return \PPLSDK\StructType\MyApiPackageOut
     */
    public function setFlags(\PPLSDK\ArrayType\ArrayOfMyApiFlag $flags = null)
    {
        $this->Flags = $flags;
        return $this;
    }
}
