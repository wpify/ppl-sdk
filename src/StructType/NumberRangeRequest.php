<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for NumberRangeRequest StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:NumberRangeRequest
 * @subpackage Structs
 */
class NumberRangeRequest extends AbstractStructBase
{
    /**
     * The PackProductType
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $PackProductType;
    /**
     * The Quantity
     * @var int
     */
    public $Quantity;
    /**
     * The DocumentBack
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $DocumentBack;
    /**
     * Constructor method for NumberRangeRequest
     * @uses NumberRangeRequest::setPackProductType()
     * @uses NumberRangeRequest::setQuantity()
     * @uses NumberRangeRequest::setDocumentBack()
     * @param string $packProductType
     * @param int $quantity
     * @param bool $documentBack
     */
    public function __construct($packProductType = null, $quantity = null, $documentBack = null)
    {
        $this
            ->setPackProductType($packProductType)
            ->setQuantity($quantity)
            ->setDocumentBack($documentBack);
    }
    /**
     * Get PackProductType value
     * @return string|null
     */
    public function getPackProductType()
    {
        return $this->PackProductType;
    }
    /**
     * Set PackProductType value
     * @param string $packProductType
     * @return \PPLSDK\StructType\NumberRangeRequest
     */
    public function setPackProductType($packProductType = null)
    {
        // validation for constraint: string
        if (!is_null($packProductType) && !is_string($packProductType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packProductType, true), gettype($packProductType)), __LINE__);
        }
        $this->PackProductType = $packProductType;
        return $this;
    }
    /**
     * Get Quantity value
     * @return int|null
     */
    public function getQuantity()
    {
        return $this->Quantity;
    }
    /**
     * Set Quantity value
     * @param int $quantity
     * @return \PPLSDK\StructType\NumberRangeRequest
     */
    public function setQuantity($quantity = null)
    {
        // validation for constraint: int
        if (!is_null($quantity) && !(is_int($quantity) || ctype_digit($quantity))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($quantity, true), gettype($quantity)), __LINE__);
        }
        $this->Quantity = $quantity;
        return $this;
    }
    /**
     * Get DocumentBack value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getDocumentBack()
    {
        return isset($this->DocumentBack) ? $this->DocumentBack : null;
    }
    /**
     * Set DocumentBack value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $documentBack
     * @return \PPLSDK\StructType\NumberRangeRequest
     */
    public function setDocumentBack($documentBack = null)
    {
        // validation for constraint: boolean
        if (!is_null($documentBack) && !is_bool($documentBack)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($documentBack, true), gettype($documentBack)), __LINE__);
        }
        if (is_null($documentBack) || (is_array($documentBack) && empty($documentBack))) {
            unset($this->DocumentBack);
        } else {
            $this->DocumentBack = $documentBack;
        }
        return $this;
    }
}
