<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiSprintRoutes StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiSprintRoutes
 * @subpackage Structs
 */
class MyApiSprintRoutes extends AbstractStructBase
{
    /**
     * The ZipFrom
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ZipFrom;
    /**
     * The ZipTo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ZipTo;
    /**
     * The DeliveryDepot
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DeliveryDepot;
    /**
     * The DeliveryRegion
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DeliveryRegion;
    /**
     * The SortCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SortCode;
    /**
     * Constructor method for MyApiSprintRoutes
     * @uses MyApiSprintRoutes::setZipFrom()
     * @uses MyApiSprintRoutes::setZipTo()
     * @uses MyApiSprintRoutes::setDeliveryDepot()
     * @uses MyApiSprintRoutes::setDeliveryRegion()
     * @uses MyApiSprintRoutes::setSortCode()
     * @param string $zipFrom
     * @param string $zipTo
     * @param string $deliveryDepot
     * @param string $deliveryRegion
     * @param string $sortCode
     */
    public function __construct($zipFrom = null, $zipTo = null, $deliveryDepot = null, $deliveryRegion = null, $sortCode = null)
    {
        $this
            ->setZipFrom($zipFrom)
            ->setZipTo($zipTo)
            ->setDeliveryDepot($deliveryDepot)
            ->setDeliveryRegion($deliveryRegion)
            ->setSortCode($sortCode);
    }
    /**
     * Get ZipFrom value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getZipFrom()
    {
        return isset($this->ZipFrom) ? $this->ZipFrom : null;
    }
    /**
     * Set ZipFrom value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $zipFrom
     * @return \PPLSDK\StructType\MyApiSprintRoutes
     */
    public function setZipFrom($zipFrom = null)
    {
        // validation for constraint: string
        if (!is_null($zipFrom) && !is_string($zipFrom)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($zipFrom, true), gettype($zipFrom)), __LINE__);
        }
        if (is_null($zipFrom) || (is_array($zipFrom) && empty($zipFrom))) {
            unset($this->ZipFrom);
        } else {
            $this->ZipFrom = $zipFrom;
        }
        return $this;
    }
    /**
     * Get ZipTo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getZipTo()
    {
        return isset($this->ZipTo) ? $this->ZipTo : null;
    }
    /**
     * Set ZipTo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $zipTo
     * @return \PPLSDK\StructType\MyApiSprintRoutes
     */
    public function setZipTo($zipTo = null)
    {
        // validation for constraint: string
        if (!is_null($zipTo) && !is_string($zipTo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($zipTo, true), gettype($zipTo)), __LINE__);
        }
        if (is_null($zipTo) || (is_array($zipTo) && empty($zipTo))) {
            unset($this->ZipTo);
        } else {
            $this->ZipTo = $zipTo;
        }
        return $this;
    }
    /**
     * Get DeliveryDepot value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDeliveryDepot()
    {
        return isset($this->DeliveryDepot) ? $this->DeliveryDepot : null;
    }
    /**
     * Set DeliveryDepot value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $deliveryDepot
     * @return \PPLSDK\StructType\MyApiSprintRoutes
     */
    public function setDeliveryDepot($deliveryDepot = null)
    {
        // validation for constraint: string
        if (!is_null($deliveryDepot) && !is_string($deliveryDepot)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deliveryDepot, true), gettype($deliveryDepot)), __LINE__);
        }
        if (is_null($deliveryDepot) || (is_array($deliveryDepot) && empty($deliveryDepot))) {
            unset($this->DeliveryDepot);
        } else {
            $this->DeliveryDepot = $deliveryDepot;
        }
        return $this;
    }
    /**
     * Get DeliveryRegion value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDeliveryRegion()
    {
        return isset($this->DeliveryRegion) ? $this->DeliveryRegion : null;
    }
    /**
     * Set DeliveryRegion value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $deliveryRegion
     * @return \PPLSDK\StructType\MyApiSprintRoutes
     */
    public function setDeliveryRegion($deliveryRegion = null)
    {
        // validation for constraint: string
        if (!is_null($deliveryRegion) && !is_string($deliveryRegion)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deliveryRegion, true), gettype($deliveryRegion)), __LINE__);
        }
        if (is_null($deliveryRegion) || (is_array($deliveryRegion) && empty($deliveryRegion))) {
            unset($this->DeliveryRegion);
        } else {
            $this->DeliveryRegion = $deliveryRegion;
        }
        return $this;
    }
    /**
     * Get SortCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSortCode()
    {
        return isset($this->SortCode) ? $this->SortCode : null;
    }
    /**
     * Set SortCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sortCode
     * @return \PPLSDK\StructType\MyApiSprintRoutes
     */
    public function setSortCode($sortCode = null)
    {
        // validation for constraint: string
        if (!is_null($sortCode) && !is_string($sortCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sortCode, true), gettype($sortCode)), __LINE__);
        }
        if (is_null($sortCode) || (is_array($sortCode) && empty($sortCode))) {
            unset($this->SortCode);
        } else {
            $this->SortCode = $sortCode;
        }
        return $this;
    }
}
