<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreatePackagesLabelsResultData StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:CreatePackagesLabelsResultData
 * @subpackage Structs
 */
class CreatePackagesLabelsResultData extends AbstractStructBase
{
    /**
     * The RequestId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $RequestId;
    /**
     * Constructor method for CreatePackagesLabelsResultData
     * @uses CreatePackagesLabelsResultData::setRequestId()
     * @param string $requestId
     */
    public function __construct($requestId = null)
    {
        $this
            ->setRequestId($requestId);
    }
    /**
     * Get RequestId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRequestId()
    {
        return isset($this->RequestId) ? $this->RequestId : null;
    }
    /**
     * Set RequestId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $requestId
     * @return \PPLSDK\StructType\CreatePackagesLabelsResultData
     */
    public function setRequestId($requestId = null)
    {
        // validation for constraint: string
        if (!is_null($requestId) && !is_string($requestId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($requestId, true), gettype($requestId)), __LINE__);
        }
        if (is_null($requestId) || (is_array($requestId) && empty($requestId))) {
            unset($this->RequestId);
        } else {
            $this->RequestId = $requestId;
        }
        return $this;
    }
}
