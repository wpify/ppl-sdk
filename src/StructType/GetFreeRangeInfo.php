<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetFreeRangeInfo StructType
 * @subpackage Structs
 */
class GetFreeRangeInfo extends AbstractStructBase
{
    /**
     * The Auth
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\AuthenticationData
     */
    public $Auth;
    /**
     * The PptId
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var string
     */
    public $PptId;
    /**
     * Constructor method for GetFreeRangeInfo
     * @uses GetFreeRangeInfo::setAuth()
     * @uses GetFreeRangeInfo::setPptId()
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @param string $pptId
     */
    public function __construct(\PPLSDK\StructType\AuthenticationData $auth = null, $pptId = null)
    {
        $this
            ->setAuth($auth)
            ->setPptId($pptId);
    }
    /**
     * Get Auth value
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function getAuth()
    {
        return $this->Auth;
    }
    /**
     * Set Auth value
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @return \PPLSDK\StructType\GetFreeRangeInfo
     */
    public function setAuth(\PPLSDK\StructType\AuthenticationData $auth = null)
    {
        $this->Auth = $auth;
        return $this;
    }
    /**
     * Get PptId value
     * @return string
     */
    public function getPptId()
    {
        return $this->PptId;
    }
    /**
     * Set PptId value
     * @param string $pptId
     * @return \PPLSDK\StructType\GetFreeRangeInfo
     */
    public function setPptId($pptId = null)
    {
        // validation for constraint: string
        if (!is_null($pptId) && !is_string($pptId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pptId, true), gettype($pptId)), __LINE__);
        }
        $this->PptId = $pptId;
        return $this;
    }
}
