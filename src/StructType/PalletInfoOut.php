<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PalletInfoOut StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:PalletInfoOut
 * @subpackage Structs
 */
class PalletInfoOut extends AbstractStructBase
{
    /**
     * The Collies
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiPackageOutColli
     */
    public $Collies;
    /**
     * The ManipulationType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $ManipulationType;
    /**
     * The PEURCount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $PEURCount;
    /**
     * The PackDesc
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PackDesc;
    /**
     * The PickupCargoTypeCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PickupCargoTypeCode;
    /**
     * The Volume
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $Volume;
    /**
     * Constructor method for PalletInfoOut
     * @uses PalletInfoOut::setCollies()
     * @uses PalletInfoOut::setManipulationType()
     * @uses PalletInfoOut::setPEURCount()
     * @uses PalletInfoOut::setPackDesc()
     * @uses PalletInfoOut::setPickupCargoTypeCode()
     * @uses PalletInfoOut::setVolume()
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageOutColli $collies
     * @param int $manipulationType
     * @param int $pEURCount
     * @param string $packDesc
     * @param string $pickupCargoTypeCode
     * @param float $volume
     */
    public function __construct(\PPLSDK\ArrayType\ArrayOfMyApiPackageOutColli $collies = null, $manipulationType = null, $pEURCount = null, $packDesc = null, $pickupCargoTypeCode = null, $volume = null)
    {
        $this
            ->setCollies($collies)
            ->setManipulationType($manipulationType)
            ->setPEURCount($pEURCount)
            ->setPackDesc($packDesc)
            ->setPickupCargoTypeCode($pickupCargoTypeCode)
            ->setVolume($volume);
    }
    /**
     * Get Collies value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageOutColli|null
     */
    public function getCollies()
    {
        return isset($this->Collies) ? $this->Collies : null;
    }
    /**
     * Set Collies value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageOutColli $collies
     * @return \PPLSDK\StructType\PalletInfoOut
     */
    public function setCollies(\PPLSDK\ArrayType\ArrayOfMyApiPackageOutColli $collies = null)
    {
        if (is_null($collies) || (is_array($collies) && empty($collies))) {
            unset($this->Collies);
        } else {
            $this->Collies = $collies;
        }
        return $this;
    }
    /**
     * Get ManipulationType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getManipulationType()
    {
        return isset($this->ManipulationType) ? $this->ManipulationType : null;
    }
    /**
     * Set ManipulationType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $manipulationType
     * @return \PPLSDK\StructType\PalletInfoOut
     */
    public function setManipulationType($manipulationType = null)
    {
        // validation for constraint: int
        if (!is_null($manipulationType) && !(is_int($manipulationType) || ctype_digit($manipulationType))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($manipulationType, true), gettype($manipulationType)), __LINE__);
        }
        if (is_null($manipulationType) || (is_array($manipulationType) && empty($manipulationType))) {
            unset($this->ManipulationType);
        } else {
            $this->ManipulationType = $manipulationType;
        }
        return $this;
    }
    /**
     * Get PEURCount value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getPEURCount()
    {
        return isset($this->PEURCount) ? $this->PEURCount : null;
    }
    /**
     * Set PEURCount value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $pEURCount
     * @return \PPLSDK\StructType\PalletInfoOut
     */
    public function setPEURCount($pEURCount = null)
    {
        // validation for constraint: int
        if (!is_null($pEURCount) && !(is_int($pEURCount) || ctype_digit($pEURCount))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pEURCount, true), gettype($pEURCount)), __LINE__);
        }
        if (is_null($pEURCount) || (is_array($pEURCount) && empty($pEURCount))) {
            unset($this->PEURCount);
        } else {
            $this->PEURCount = $pEURCount;
        }
        return $this;
    }
    /**
     * Get PackDesc value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPackDesc()
    {
        return isset($this->PackDesc) ? $this->PackDesc : null;
    }
    /**
     * Set PackDesc value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $packDesc
     * @return \PPLSDK\StructType\PalletInfoOut
     */
    public function setPackDesc($packDesc = null)
    {
        // validation for constraint: string
        if (!is_null($packDesc) && !is_string($packDesc)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packDesc, true), gettype($packDesc)), __LINE__);
        }
        if (is_null($packDesc) || (is_array($packDesc) && empty($packDesc))) {
            unset($this->PackDesc);
        } else {
            $this->PackDesc = $packDesc;
        }
        return $this;
    }
    /**
     * Get PickupCargoTypeCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPickupCargoTypeCode()
    {
        return isset($this->PickupCargoTypeCode) ? $this->PickupCargoTypeCode : null;
    }
    /**
     * Set PickupCargoTypeCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $pickupCargoTypeCode
     * @return \PPLSDK\StructType\PalletInfoOut
     */
    public function setPickupCargoTypeCode($pickupCargoTypeCode = null)
    {
        // validation for constraint: string
        if (!is_null($pickupCargoTypeCode) && !is_string($pickupCargoTypeCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pickupCargoTypeCode, true), gettype($pickupCargoTypeCode)), __LINE__);
        }
        if (is_null($pickupCargoTypeCode) || (is_array($pickupCargoTypeCode) && empty($pickupCargoTypeCode))) {
            unset($this->PickupCargoTypeCode);
        } else {
            $this->PickupCargoTypeCode = $pickupCargoTypeCode;
        }
        return $this;
    }
    /**
     * Get Volume value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getVolume()
    {
        return isset($this->Volume) ? $this->Volume : null;
    }
    /**
     * Set Volume value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $volume
     * @return \PPLSDK\StructType\PalletInfoOut
     */
    public function setVolume($volume = null)
    {
        // validation for constraint: float
        if (!is_null($volume) && !(is_float($volume) || is_numeric($volume))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($volume, true), gettype($volume)), __LINE__);
        }
        if (is_null($volume) || (is_array($volume) && empty($volume))) {
            unset($this->Volume);
        } else {
            $this->Volume = $volume;
        }
        return $this;
    }
}
