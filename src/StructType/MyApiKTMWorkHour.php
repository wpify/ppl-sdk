<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiKTMWorkHour StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiKTMWorkHour
 * @subpackage Structs
 */
class MyApiKTMWorkHour extends AbstractStructBase
{
    /**
     * The Day
     * @var string
     */
    public $Day;
    /**
     * The From
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $From;
    /**
     * The To
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $To;
    /**
     * Constructor method for MyApiKTMWorkHour
     * @uses MyApiKTMWorkHour::setDay()
     * @uses MyApiKTMWorkHour::setFrom()
     * @uses MyApiKTMWorkHour::setTo()
     * @param string $day
     * @param string $from
     * @param string $to
     */
    public function __construct($day = null, $from = null, $to = null)
    {
        $this
            ->setDay($day)
            ->setFrom($from)
            ->setTo($to);
    }
    /**
     * Get Day value
     * @return string|null
     */
    public function getDay()
    {
        return $this->Day;
    }
    /**
     * Set Day value
     * @param string $day
     * @return \PPLSDK\StructType\MyApiKTMWorkHour
     */
    public function setDay($day = null)
    {
        // validation for constraint: string
        if (!is_null($day) && !is_string($day)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($day, true), gettype($day)), __LINE__);
        }
        $this->Day = $day;
        return $this;
    }
    /**
     * Get From value
     * @return string|null
     */
    public function getFrom()
    {
        return $this->From;
    }
    /**
     * Set From value
     * @param string $from
     * @return \PPLSDK\StructType\MyApiKTMWorkHour
     */
    public function setFrom($from = null)
    {
        // validation for constraint: string
        if (!is_null($from) && !is_string($from)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($from, true), gettype($from)), __LINE__);
        }
        $this->From = $from;
        return $this;
    }
    /**
     * Get To value
     * @return string|null
     */
    public function getTo()
    {
        return $this->To;
    }
    /**
     * Set To value
     * @param string $to
     * @return \PPLSDK\StructType\MyApiKTMWorkHour
     */
    public function setTo($to = null)
    {
        // validation for constraint: string
        if (!is_null($to) && !is_string($to)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($to, true), gettype($to)), __LINE__);
        }
        $this->To = $to;
        return $this;
    }
}
