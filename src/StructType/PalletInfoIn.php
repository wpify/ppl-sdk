<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PalletInfoIn StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:PalletInfoIn
 * @subpackage Structs
 */
class PalletInfoIn extends AbstractStructBase
{
    /**
     * The Collies
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiPackageInColli
     */
    public $Collies;
    /**
     * The ManipulationType
     * @var int
     */
    public $ManipulationType;
    /**
     * The PEURCount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $PEURCount;
    /**
     * The PackDesc
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PackDesc;
    /**
     * The PickUpCargoTypeCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $PickUpCargoTypeCode;
    /**
     * The Volume
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $Volume;
    /**
     * Constructor method for PalletInfoIn
     * @uses PalletInfoIn::setCollies()
     * @uses PalletInfoIn::setManipulationType()
     * @uses PalletInfoIn::setPEURCount()
     * @uses PalletInfoIn::setPackDesc()
     * @uses PalletInfoIn::setPickUpCargoTypeCode()
     * @uses PalletInfoIn::setVolume()
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageInColli $collies
     * @param int $manipulationType
     * @param int $pEURCount
     * @param string $packDesc
     * @param string $pickUpCargoTypeCode
     * @param float $volume
     */
    public function __construct(\PPLSDK\ArrayType\ArrayOfMyApiPackageInColli $collies = null, $manipulationType = null, $pEURCount = null, $packDesc = null, $pickUpCargoTypeCode = null, $volume = null)
    {
        $this
            ->setCollies($collies)
            ->setManipulationType($manipulationType)
            ->setPEURCount($pEURCount)
            ->setPackDesc($packDesc)
            ->setPickUpCargoTypeCode($pickUpCargoTypeCode)
            ->setVolume($volume);
    }
    /**
     * Get Collies value
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageInColli|null
     */
    public function getCollies()
    {
        return $this->Collies;
    }
    /**
     * Set Collies value
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageInColli $collies
     * @return \PPLSDK\StructType\PalletInfoIn
     */
    public function setCollies(\PPLSDK\ArrayType\ArrayOfMyApiPackageInColli $collies = null)
    {
        $this->Collies = $collies;
        return $this;
    }
    /**
     * Get ManipulationType value
     * @return int|null
     */
    public function getManipulationType()
    {
        return $this->ManipulationType;
    }
    /**
     * Set ManipulationType value
     * @param int $manipulationType
     * @return \PPLSDK\StructType\PalletInfoIn
     */
    public function setManipulationType($manipulationType = null)
    {
        // validation for constraint: int
        if (!is_null($manipulationType) && !(is_int($manipulationType) || ctype_digit($manipulationType))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($manipulationType, true), gettype($manipulationType)), __LINE__);
        }
        $this->ManipulationType = $manipulationType;
        return $this;
    }
    /**
     * Get PEURCount value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getPEURCount()
    {
        return isset($this->PEURCount) ? $this->PEURCount : null;
    }
    /**
     * Set PEURCount value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $pEURCount
     * @return \PPLSDK\StructType\PalletInfoIn
     */
    public function setPEURCount($pEURCount = null)
    {
        // validation for constraint: int
        if (!is_null($pEURCount) && !(is_int($pEURCount) || ctype_digit($pEURCount))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pEURCount, true), gettype($pEURCount)), __LINE__);
        }
        if (is_null($pEURCount) || (is_array($pEURCount) && empty($pEURCount))) {
            unset($this->PEURCount);
        } else {
            $this->PEURCount = $pEURCount;
        }
        return $this;
    }
    /**
     * Get PackDesc value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPackDesc()
    {
        return isset($this->PackDesc) ? $this->PackDesc : null;
    }
    /**
     * Set PackDesc value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $packDesc
     * @return \PPLSDK\StructType\PalletInfoIn
     */
    public function setPackDesc($packDesc = null)
    {
        // validation for constraint: string
        if (!is_null($packDesc) && !is_string($packDesc)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packDesc, true), gettype($packDesc)), __LINE__);
        }
        if (is_null($packDesc) || (is_array($packDesc) && empty($packDesc))) {
            unset($this->PackDesc);
        } else {
            $this->PackDesc = $packDesc;
        }
        return $this;
    }
    /**
     * Get PickUpCargoTypeCode value
     * @return string|null
     */
    public function getPickUpCargoTypeCode()
    {
        return $this->PickUpCargoTypeCode;
    }
    /**
     * Set PickUpCargoTypeCode value
     * @param string $pickUpCargoTypeCode
     * @return \PPLSDK\StructType\PalletInfoIn
     */
    public function setPickUpCargoTypeCode($pickUpCargoTypeCode = null)
    {
        // validation for constraint: string
        if (!is_null($pickUpCargoTypeCode) && !is_string($pickUpCargoTypeCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pickUpCargoTypeCode, true), gettype($pickUpCargoTypeCode)), __LINE__);
        }
        $this->PickUpCargoTypeCode = $pickUpCargoTypeCode;
        return $this;
    }
    /**
     * Get Volume value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getVolume()
    {
        return isset($this->Volume) ? $this->Volume : null;
    }
    /**
     * Set Volume value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $volume
     * @return \PPLSDK\StructType\PalletInfoIn
     */
    public function setVolume($volume = null)
    {
        // validation for constraint: float
        if (!is_null($volume) && !(is_float($volume) || is_numeric($volume))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($volume, true), gettype($volume)), __LINE__);
        }
        if (is_null($volume) || (is_array($volume) && empty($volume))) {
            unset($this->Volume);
        } else {
            $this->Volume = $volume;
        }
        return $this;
    }
}
