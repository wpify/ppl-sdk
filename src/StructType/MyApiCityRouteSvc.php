<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiCityRouteSvc StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiCityRouteSvc
 * @subpackage Structs
 */
class MyApiCityRouteSvc extends AbstractStructBase
{
    /**
     * The Code
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Code;
    /**
     * The Value
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Value;
    /**
     * Constructor method for MyApiCityRouteSvc
     * @uses MyApiCityRouteSvc::setCode()
     * @uses MyApiCityRouteSvc::setValue()
     * @param string $code
     * @param string $value
     */
    public function __construct($code = null, $value = null)
    {
        $this
            ->setCode($code)
            ->setValue($value);
    }
    /**
     * Get Code value
     * @return string|null
     */
    public function getCode()
    {
        return $this->Code;
    }
    /**
     * Set Code value
     * @param string $code
     * @return \PPLSDK\StructType\MyApiCityRouteSvc
     */
    public function setCode($code = null)
    {
        // validation for constraint: string
        if (!is_null($code) && !is_string($code)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($code, true), gettype($code)), __LINE__);
        }
        $this->Code = $code;
        return $this;
    }
    /**
     * Get Value value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getValue()
    {
        return isset($this->Value) ? $this->Value : null;
    }
    /**
     * Set Value value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $value
     * @return \PPLSDK\StructType\MyApiCityRouteSvc
     */
    public function setValue($value = null)
    {
        // validation for constraint: string
        if (!is_null($value) && !is_string($value)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($value, true), gettype($value)), __LINE__);
        }
        if (is_null($value) || (is_array($value) && empty($value))) {
            unset($this->Value);
        } else {
            $this->Value = $value;
        }
        return $this;
    }
}
