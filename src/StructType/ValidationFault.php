<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ValidationFault StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ValidationFault
 * @subpackage Structs
 */
class ValidationFault extends MyApiFault
{
}
