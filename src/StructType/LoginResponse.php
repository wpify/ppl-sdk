<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for LoginResponse StructType
 * @subpackage Structs
 */
class LoginResponse extends AbstractStructBase
{
    /**
     * The LoginResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\LoginResult
     */
    public $LoginResult;
    /**
     * Constructor method for LoginResponse
     * @uses LoginResponse::setLoginResult()
     * @param \PPLSDK\StructType\LoginResult $loginResult
     */
    public function __construct(\PPLSDK\StructType\LoginResult $loginResult = null)
    {
        $this
            ->setLoginResult($loginResult);
    }
    /**
     * Get LoginResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\LoginResult|null
     */
    public function getLoginResult()
    {
        return isset($this->LoginResult) ? $this->LoginResult : null;
    }
    /**
     * Set LoginResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\LoginResult $loginResult
     * @return \PPLSDK\StructType\LoginResponse
     */
    public function setLoginResult(\PPLSDK\StructType\LoginResult $loginResult = null)
    {
        if (is_null($loginResult) || (is_array($loginResult) && empty($loginResult))) {
            unset($this->LoginResult);
        } else {
            $this->LoginResult = $loginResult;
        }
        return $this;
    }
}
