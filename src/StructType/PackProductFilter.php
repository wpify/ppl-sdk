<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PackProductFilter StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:PackProductFilter
 * @subpackage Structs
 */
class PackProductFilter extends AbstractStructBase
{
    /**
     * The LastUpdate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $LastUpdate;
    /**
     * Constructor method for PackProductFilter
     * @uses PackProductFilter::setLastUpdate()
     * @param string $lastUpdate
     */
    public function __construct($lastUpdate = null)
    {
        $this
            ->setLastUpdate($lastUpdate);
    }
    /**
     * Get LastUpdate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLastUpdate()
    {
        return isset($this->LastUpdate) ? $this->LastUpdate : null;
    }
    /**
     * Set LastUpdate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $lastUpdate
     * @return \PPLSDK\StructType\PackProductFilter
     */
    public function setLastUpdate($lastUpdate = null)
    {
        // validation for constraint: string
        if (!is_null($lastUpdate) && !is_string($lastUpdate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($lastUpdate, true), gettype($lastUpdate)), __LINE__);
        }
        if (is_null($lastUpdate) || (is_array($lastUpdate) && empty($lastUpdate))) {
            unset($this->LastUpdate);
        } else {
            $this->LastUpdate = $lastUpdate;
        }
        return $this;
    }
}
