<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiResultBaseOfArrayOfMyApiPackageOutpewOaM6d StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiResultBaseOfArrayOfMyApiPackageOutpewOaM6d
 * @subpackage Structs
 */
class MyApiResultBaseOfArrayOfMyApiPackageOutpewOaM6d extends MyApiResultBaseVoid
{
    /**
     * The ResultData
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiPackageOut
     */
    public $ResultData;
    /**
     * Constructor method for MyApiResultBaseOfArrayOfMyApiPackageOutpewOaM6d
     * @uses MyApiResultBaseOfArrayOfMyApiPackageOutpewOaM6d::setResultData()
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageOut $resultData
     */
    public function __construct(\PPLSDK\ArrayType\ArrayOfMyApiPackageOut $resultData = null)
    {
        $this
            ->setResultData($resultData);
    }
    /**
     * Get ResultData value
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageOut|null
     */
    public function getResultData()
    {
        return $this->ResultData;
    }
    /**
     * Set ResultData value
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageOut $resultData
     * @return \PPLSDK\StructType\MyApiResultBaseOfArrayOfMyApiPackageOutpewOaM6d
     */
    public function setResultData(\PPLSDK\ArrayType\ArrayOfMyApiPackageOut $resultData = null)
    {
        $this->ResultData = $resultData;
        return $this;
    }
}
