<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetParcelShopsResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:GetParcelShopsResult
 * @subpackage Structs
 */
class GetParcelShopsResult extends MyApiResultBaseOfArrayOfMyApiParcelShoppewOaM6d
{
}
