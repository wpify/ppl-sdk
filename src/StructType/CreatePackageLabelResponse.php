<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreatePackageLabelResponse StructType
 * @subpackage Structs
 */
class CreatePackageLabelResponse extends AbstractStructBase
{
    /**
     * The CreatePackageLabelResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\CreatePackagesLabelsResult
     */
    public $CreatePackageLabelResult;
    /**
     * Constructor method for CreatePackageLabelResponse
     * @uses CreatePackageLabelResponse::setCreatePackageLabelResult()
     * @param \PPLSDK\StructType\CreatePackagesLabelsResult $createPackageLabelResult
     */
    public function __construct(\PPLSDK\StructType\CreatePackagesLabelsResult $createPackageLabelResult = null)
    {
        $this
            ->setCreatePackageLabelResult($createPackageLabelResult);
    }
    /**
     * Get CreatePackageLabelResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\CreatePackagesLabelsResult|null
     */
    public function getCreatePackageLabelResult()
    {
        return isset($this->CreatePackageLabelResult) ? $this->CreatePackageLabelResult : null;
    }
    /**
     * Set CreatePackageLabelResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\CreatePackagesLabelsResult $createPackageLabelResult
     * @return \PPLSDK\StructType\CreatePackageLabelResponse
     */
    public function setCreatePackageLabelResult(\PPLSDK\StructType\CreatePackagesLabelsResult $createPackageLabelResult = null)
    {
        if (is_null($createPackageLabelResult) || (is_array($createPackageLabelResult) && empty($createPackageLabelResult))) {
            unset($this->CreatePackageLabelResult);
        } else {
            $this->CreatePackageLabelResult = $createPackageLabelResult;
        }
        return $this;
    }
}
