<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetFreeRangeInfoResponse StructType
 * @subpackage Structs
 */
class GetFreeRangeInfoResponse extends AbstractStructBase
{
    /**
     * The GetFreeRangeInfoResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\GetFreeRangeInfoResult
     */
    public $GetFreeRangeInfoResult;
    /**
     * Constructor method for GetFreeRangeInfoResponse
     * @uses GetFreeRangeInfoResponse::setGetFreeRangeInfoResult()
     * @param \PPLSDK\StructType\GetFreeRangeInfoResult $getFreeRangeInfoResult
     */
    public function __construct(\PPLSDK\StructType\GetFreeRangeInfoResult $getFreeRangeInfoResult = null)
    {
        $this
            ->setGetFreeRangeInfoResult($getFreeRangeInfoResult);
    }
    /**
     * Get GetFreeRangeInfoResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\GetFreeRangeInfoResult|null
     */
    public function getGetFreeRangeInfoResult()
    {
        return isset($this->GetFreeRangeInfoResult) ? $this->GetFreeRangeInfoResult : null;
    }
    /**
     * Set GetFreeRangeInfoResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\GetFreeRangeInfoResult $getFreeRangeInfoResult
     * @return \PPLSDK\StructType\GetFreeRangeInfoResponse
     */
    public function setGetFreeRangeInfoResult(\PPLSDK\StructType\GetFreeRangeInfoResult $getFreeRangeInfoResult = null)
    {
        if (is_null($getFreeRangeInfoResult) || (is_array($getFreeRangeInfoResult) && empty($getFreeRangeInfoResult))) {
            unset($this->GetFreeRangeInfoResult);
        } else {
            $this->GetFreeRangeInfoResult = $getFreeRangeInfoResult;
        }
        return $this;
    }
}
