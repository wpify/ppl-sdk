<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiFlag StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiFlag
 * @subpackage Structs
 */
class MyApiFlag extends AbstractStructBase
{
    /**
     * The Code
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Code;
    /**
     * The Value
     * @var bool
     */
    public $Value;
    /**
     * Constructor method for MyApiFlag
     * @uses MyApiFlag::setCode()
     * @uses MyApiFlag::setValue()
     * @param string $code
     * @param bool $value
     */
    public function __construct($code = null, $value = null)
    {
        $this
            ->setCode($code)
            ->setValue($value);
    }
    /**
     * Get Code value
     * @return string|null
     */
    public function getCode()
    {
        return $this->Code;
    }
    /**
     * Set Code value
     * @param string $code
     * @return \PPLSDK\StructType\MyApiFlag
     */
    public function setCode($code = null)
    {
        // validation for constraint: string
        if (!is_null($code) && !is_string($code)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($code, true), gettype($code)), __LINE__);
        }
        $this->Code = $code;
        return $this;
    }
    /**
     * Get Value value
     * @return bool|null
     */
    public function getValue()
    {
        return $this->Value;
    }
    /**
     * Set Value value
     * @param bool $value
     * @return \PPLSDK\StructType\MyApiFlag
     */
    public function setValue($value = null)
    {
        // validation for constraint: boolean
        if (!is_null($value) && !is_bool($value)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($value, true), gettype($value)), __LINE__);
        }
        $this->Value = $value;
        return $this;
    }
}
