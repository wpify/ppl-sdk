<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPackNumberRowTypesResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:GetPackNumberRowTypesResult
 * @subpackage Structs
 */
class GetPackNumberRowTypesResult extends MyApiResultBaseOfArrayOfMyApiPackNumberRowTypepewOaM6d
{
}
