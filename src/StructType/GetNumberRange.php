<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetNumberRange StructType
 * @subpackage Structs
 */
class GetNumberRange extends AbstractStructBase
{
    /**
     * The Auth
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\AuthenticationData
     */
    public $Auth;
    /**
     * The NumberRanges
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfNumberRangeRequest
     */
    public $NumberRanges;
    /**
     * Constructor method for GetNumberRange
     * @uses GetNumberRange::setAuth()
     * @uses GetNumberRange::setNumberRanges()
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @param \PPLSDK\ArrayType\ArrayOfNumberRangeRequest $numberRanges
     */
    public function __construct(\PPLSDK\StructType\AuthenticationData $auth = null, \PPLSDK\ArrayType\ArrayOfNumberRangeRequest $numberRanges = null)
    {
        $this
            ->setAuth($auth)
            ->setNumberRanges($numberRanges);
    }
    /**
     * Get Auth value
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function getAuth()
    {
        return $this->Auth;
    }
    /**
     * Set Auth value
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @return \PPLSDK\StructType\GetNumberRange
     */
    public function setAuth(\PPLSDK\StructType\AuthenticationData $auth = null)
    {
        $this->Auth = $auth;
        return $this;
    }
    /**
     * Get NumberRanges value
     * @return \PPLSDK\ArrayType\ArrayOfNumberRangeRequest
     */
    public function getNumberRanges()
    {
        return $this->NumberRanges;
    }
    /**
     * Set NumberRanges value
     * @param \PPLSDK\ArrayType\ArrayOfNumberRangeRequest $numberRanges
     * @return \PPLSDK\StructType\GetNumberRange
     */
    public function setNumberRanges(\PPLSDK\ArrayType\ArrayOfNumberRangeRequest $numberRanges = null)
    {
        $this->NumberRanges = $numberRanges;
        return $this;
    }
}
