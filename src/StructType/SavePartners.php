<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SavePartners StructType
 * @subpackage Structs
 */
class SavePartners extends AbstractStructBase
{
    /**
     * The Auth
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\AuthenticationData
     */
    public $Auth;
    /**
     * The Request
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\PartnerData
     */
    public $Request;
    /**
     * Constructor method for SavePartners
     * @uses SavePartners::setAuth()
     * @uses SavePartners::setRequest()
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @param \PPLSDK\StructType\PartnerData $request
     */
    public function __construct(\PPLSDK\StructType\AuthenticationData $auth = null, \PPLSDK\StructType\PartnerData $request = null)
    {
        $this
            ->setAuth($auth)
            ->setRequest($request);
    }
    /**
     * Get Auth value
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function getAuth()
    {
        return $this->Auth;
    }
    /**
     * Set Auth value
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @return \PPLSDK\StructType\SavePartners
     */
    public function setAuth(\PPLSDK\StructType\AuthenticationData $auth = null)
    {
        $this->Auth = $auth;
        return $this;
    }
    /**
     * Get Request value
     * @return \PPLSDK\StructType\PartnerData
     */
    public function getRequest()
    {
        return $this->Request;
    }
    /**
     * Set Request value
     * @param \PPLSDK\StructType\PartnerData $request
     * @return \PPLSDK\StructType\SavePartners
     */
    public function setRequest(\PPLSDK\StructType\PartnerData $request = null)
    {
        $this->Request = $request;
        return $this;
    }
}
