<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiPackageIn StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiPackageIn
 * @subpackage Structs
 */
class MyApiPackageIn extends AbstractStructBase
{
    /**
     * The PackNumber
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $PackNumber;
    /**
     * The PackProductType
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $PackProductType;
    /**
     * The Note
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Note;
    /**
     * The DepoCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DepoCode;
    /**
     * The Sender
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPackageInSender
     */
    public $Sender;
    /**
     * The Recipient
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPackageInRecipient
     */
    public $Recipient;
    /**
     * The SpecDelivery
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\SpecDeliveryInfo
     */
    public $SpecDelivery;
    /**
     * The PaymentInfo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\PaymentInfo
     */
    public $PaymentInfo;
    /**
     * The PackagesExtNums
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiPackageExtNum
     */
    public $PackagesExtNums;
    /**
     * The PackageServices
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiPackageInServices
     */
    public $PackageServices;
    /**
     * The Flags
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiFlag
     */
    public $Flags;
    /**
     * The PackageSet
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPackageSet
     */
    public $PackageSet;
    /**
     * The PalletInfo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\PalletInfoIn
     */
    public $PalletInfo;
    /**
     * The WeightedPackageInfo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\WeightedPackageInfoIn
     */
    public $WeightedPackageInfo;
    /**
     * The DocumentsBack
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\DocumentsBackInfo
     */
    public $DocumentsBack;
    /**
     * The AddressesForServices
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfAddressForService
     */
    public $AddressesForServices;
    /**
     * Constructor method for MyApiPackageIn
     * @uses MyApiPackageIn::setPackNumber()
     * @uses MyApiPackageIn::setPackProductType()
     * @uses MyApiPackageIn::setNote()
     * @uses MyApiPackageIn::setDepoCode()
     * @uses MyApiPackageIn::setSender()
     * @uses MyApiPackageIn::setRecipient()
     * @uses MyApiPackageIn::setSpecDelivery()
     * @uses MyApiPackageIn::setPaymentInfo()
     * @uses MyApiPackageIn::setPackagesExtNums()
     * @uses MyApiPackageIn::setPackageServices()
     * @uses MyApiPackageIn::setFlags()
     * @uses MyApiPackageIn::setPackageSet()
     * @uses MyApiPackageIn::setPalletInfo()
     * @uses MyApiPackageIn::setWeightedPackageInfo()
     * @uses MyApiPackageIn::setDocumentsBack()
     * @uses MyApiPackageIn::setAddressesForServices()
     * @param string $packNumber
     * @param string $packProductType
     * @param string $note
     * @param string $depoCode
     * @param \PPLSDK\StructType\MyApiPackageInSender $sender
     * @param \PPLSDK\StructType\MyApiPackageInRecipient $recipient
     * @param \PPLSDK\StructType\SpecDeliveryInfo $specDelivery
     * @param \PPLSDK\StructType\PaymentInfo $paymentInfo
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageExtNum $packagesExtNums
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageInServices $packageServices
     * @param \PPLSDK\ArrayType\ArrayOfMyApiFlag $flags
     * @param \PPLSDK\StructType\MyApiPackageSet $packageSet
     * @param \PPLSDK\StructType\PalletInfoIn $palletInfo
     * @param \PPLSDK\StructType\WeightedPackageInfoIn $weightedPackageInfo
     * @param \PPLSDK\StructType\DocumentsBackInfo $documentsBack
     * @param \PPLSDK\ArrayType\ArrayOfAddressForService $addressesForServices
     */
    public function __construct($packNumber = null, $packProductType = null, $note = null, $depoCode = null, \PPLSDK\StructType\MyApiPackageInSender $sender = null, \PPLSDK\StructType\MyApiPackageInRecipient $recipient = null, \PPLSDK\StructType\SpecDeliveryInfo $specDelivery = null, \PPLSDK\StructType\PaymentInfo $paymentInfo = null, \PPLSDK\ArrayType\ArrayOfMyApiPackageExtNum $packagesExtNums = null, \PPLSDK\ArrayType\ArrayOfMyApiPackageInServices $packageServices = null, \PPLSDK\ArrayType\ArrayOfMyApiFlag $flags = null, \PPLSDK\StructType\MyApiPackageSet $packageSet = null, \PPLSDK\StructType\PalletInfoIn $palletInfo = null, \PPLSDK\StructType\WeightedPackageInfoIn $weightedPackageInfo = null, \PPLSDK\StructType\DocumentsBackInfo $documentsBack = null, \PPLSDK\ArrayType\ArrayOfAddressForService $addressesForServices = null)
    {
        $this
            ->setPackNumber($packNumber)
            ->setPackProductType($packProductType)
            ->setNote($note)
            ->setDepoCode($depoCode)
            ->setSender($sender)
            ->setRecipient($recipient)
            ->setSpecDelivery($specDelivery)
            ->setPaymentInfo($paymentInfo)
            ->setPackagesExtNums($packagesExtNums)
            ->setPackageServices($packageServices)
            ->setFlags($flags)
            ->setPackageSet($packageSet)
            ->setPalletInfo($palletInfo)
            ->setWeightedPackageInfo($weightedPackageInfo)
            ->setDocumentsBack($documentsBack)
            ->setAddressesForServices($addressesForServices);
    }
    /**
     * Get PackNumber value
     * @return string|null
     */
    public function getPackNumber()
    {
        return $this->PackNumber;
    }
    /**
     * Set PackNumber value
     * @param string $packNumber
     * @return \PPLSDK\StructType\MyApiPackageIn
     */
    public function setPackNumber($packNumber = null)
    {
        // validation for constraint: string
        if (!is_null($packNumber) && !is_string($packNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packNumber, true), gettype($packNumber)), __LINE__);
        }
        $this->PackNumber = $packNumber;
        return $this;
    }
    /**
     * Get PackProductType value
     * @return string|null
     */
    public function getPackProductType()
    {
        return $this->PackProductType;
    }
    /**
     * Set PackProductType value
     * @param string $packProductType
     * @return \PPLSDK\StructType\MyApiPackageIn
     */
    public function setPackProductType($packProductType = null)
    {
        // validation for constraint: string
        if (!is_null($packProductType) && !is_string($packProductType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packProductType, true), gettype($packProductType)), __LINE__);
        }
        $this->PackProductType = $packProductType;
        return $this;
    }
    /**
     * Get Note value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNote()
    {
        return isset($this->Note) ? $this->Note : null;
    }
    /**
     * Set Note value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $note
     * @return \PPLSDK\StructType\MyApiPackageIn
     */
    public function setNote($note = null)
    {
        // validation for constraint: string
        if (!is_null($note) && !is_string($note)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($note, true), gettype($note)), __LINE__);
        }
        if (is_null($note) || (is_array($note) && empty($note))) {
            unset($this->Note);
        } else {
            $this->Note = $note;
        }
        return $this;
    }
    /**
     * Get DepoCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDepoCode()
    {
        return isset($this->DepoCode) ? $this->DepoCode : null;
    }
    /**
     * Set DepoCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $depoCode
     * @return \PPLSDK\StructType\MyApiPackageIn
     */
    public function setDepoCode($depoCode = null)
    {
        // validation for constraint: string
        if (!is_null($depoCode) && !is_string($depoCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($depoCode, true), gettype($depoCode)), __LINE__);
        }
        if (is_null($depoCode) || (is_array($depoCode) && empty($depoCode))) {
            unset($this->DepoCode);
        } else {
            $this->DepoCode = $depoCode;
        }
        return $this;
    }
    /**
     * Get Sender value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiPackageInSender|null
     */
    public function getSender()
    {
        return isset($this->Sender) ? $this->Sender : null;
    }
    /**
     * Set Sender value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\MyApiPackageInSender $sender
     * @return \PPLSDK\StructType\MyApiPackageIn
     */
    public function setSender(\PPLSDK\StructType\MyApiPackageInSender $sender = null)
    {
        if (is_null($sender) || (is_array($sender) && empty($sender))) {
            unset($this->Sender);
        } else {
            $this->Sender = $sender;
        }
        return $this;
    }
    /**
     * Get Recipient value
     * @return \PPLSDK\StructType\MyApiPackageInRecipient|null
     */
    public function getRecipient()
    {
        return $this->Recipient;
    }
    /**
     * Set Recipient value
     * @param \PPLSDK\StructType\MyApiPackageInRecipient $recipient
     * @return \PPLSDK\StructType\MyApiPackageIn
     */
    public function setRecipient(\PPLSDK\StructType\MyApiPackageInRecipient $recipient = null)
    {
        $this->Recipient = $recipient;
        return $this;
    }
    /**
     * Get SpecDelivery value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\SpecDeliveryInfo|null
     */
    public function getSpecDelivery()
    {
        return isset($this->SpecDelivery) ? $this->SpecDelivery : null;
    }
    /**
     * Set SpecDelivery value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\SpecDeliveryInfo $specDelivery
     * @return \PPLSDK\StructType\MyApiPackageIn
     */
    public function setSpecDelivery(\PPLSDK\StructType\SpecDeliveryInfo $specDelivery = null)
    {
        if (is_null($specDelivery) || (is_array($specDelivery) && empty($specDelivery))) {
            unset($this->SpecDelivery);
        } else {
            $this->SpecDelivery = $specDelivery;
        }
        return $this;
    }
    /**
     * Get PaymentInfo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\PaymentInfo|null
     */
    public function getPaymentInfo()
    {
        return isset($this->PaymentInfo) ? $this->PaymentInfo : null;
    }
    /**
     * Set PaymentInfo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\PaymentInfo $paymentInfo
     * @return \PPLSDK\StructType\MyApiPackageIn
     */
    public function setPaymentInfo(\PPLSDK\StructType\PaymentInfo $paymentInfo = null)
    {
        if (is_null($paymentInfo) || (is_array($paymentInfo) && empty($paymentInfo))) {
            unset($this->PaymentInfo);
        } else {
            $this->PaymentInfo = $paymentInfo;
        }
        return $this;
    }
    /**
     * Get PackagesExtNums value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageExtNum|null
     */
    public function getPackagesExtNums()
    {
        return isset($this->PackagesExtNums) ? $this->PackagesExtNums : null;
    }
    /**
     * Set PackagesExtNums value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageExtNum $packagesExtNums
     * @return \PPLSDK\StructType\MyApiPackageIn
     */
    public function setPackagesExtNums(\PPLSDK\ArrayType\ArrayOfMyApiPackageExtNum $packagesExtNums = null)
    {
        if (is_null($packagesExtNums) || (is_array($packagesExtNums) && empty($packagesExtNums))) {
            unset($this->PackagesExtNums);
        } else {
            $this->PackagesExtNums = $packagesExtNums;
        }
        return $this;
    }
    /**
     * Get PackageServices value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageInServices|null
     */
    public function getPackageServices()
    {
        return isset($this->PackageServices) ? $this->PackageServices : null;
    }
    /**
     * Set PackageServices value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPackageInServices $packageServices
     * @return \PPLSDK\StructType\MyApiPackageIn
     */
    public function setPackageServices(\PPLSDK\ArrayType\ArrayOfMyApiPackageInServices $packageServices = null)
    {
        if (is_null($packageServices) || (is_array($packageServices) && empty($packageServices))) {
            unset($this->PackageServices);
        } else {
            $this->PackageServices = $packageServices;
        }
        return $this;
    }
    /**
     * Get Flags value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\ArrayType\ArrayOfMyApiFlag|null
     */
    public function getFlags()
    {
        return isset($this->Flags) ? $this->Flags : null;
    }
    /**
     * Set Flags value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\ArrayType\ArrayOfMyApiFlag $flags
     * @return \PPLSDK\StructType\MyApiPackageIn
     */
    public function setFlags(\PPLSDK\ArrayType\ArrayOfMyApiFlag $flags = null)
    {
        if (is_null($flags) || (is_array($flags) && empty($flags))) {
            unset($this->Flags);
        } else {
            $this->Flags = $flags;
        }
        return $this;
    }
    /**
     * Get PackageSet value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiPackageSet|null
     */
    public function getPackageSet()
    {
        return isset($this->PackageSet) ? $this->PackageSet : null;
    }
    /**
     * Set PackageSet value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\MyApiPackageSet $packageSet
     * @return \PPLSDK\StructType\MyApiPackageIn
     */
    public function setPackageSet(\PPLSDK\StructType\MyApiPackageSet $packageSet = null)
    {
        if (is_null($packageSet) || (is_array($packageSet) && empty($packageSet))) {
            unset($this->PackageSet);
        } else {
            $this->PackageSet = $packageSet;
        }
        return $this;
    }
    /**
     * Get PalletInfo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\PalletInfoIn|null
     */
    public function getPalletInfo()
    {
        return isset($this->PalletInfo) ? $this->PalletInfo : null;
    }
    /**
     * Set PalletInfo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\PalletInfoIn $palletInfo
     * @return \PPLSDK\StructType\MyApiPackageIn
     */
    public function setPalletInfo(\PPLSDK\StructType\PalletInfoIn $palletInfo = null)
    {
        if (is_null($palletInfo) || (is_array($palletInfo) && empty($palletInfo))) {
            unset($this->PalletInfo);
        } else {
            $this->PalletInfo = $palletInfo;
        }
        return $this;
    }
    /**
     * Get WeightedPackageInfo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\WeightedPackageInfoIn|null
     */
    public function getWeightedPackageInfo()
    {
        return isset($this->WeightedPackageInfo) ? $this->WeightedPackageInfo : null;
    }
    /**
     * Set WeightedPackageInfo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\WeightedPackageInfoIn $weightedPackageInfo
     * @return \PPLSDK\StructType\MyApiPackageIn
     */
    public function setWeightedPackageInfo(\PPLSDK\StructType\WeightedPackageInfoIn $weightedPackageInfo = null)
    {
        if (is_null($weightedPackageInfo) || (is_array($weightedPackageInfo) && empty($weightedPackageInfo))) {
            unset($this->WeightedPackageInfo);
        } else {
            $this->WeightedPackageInfo = $weightedPackageInfo;
        }
        return $this;
    }
    /**
     * Get DocumentsBack value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\DocumentsBackInfo|null
     */
    public function getDocumentsBack()
    {
        return isset($this->DocumentsBack) ? $this->DocumentsBack : null;
    }
    /**
     * Set DocumentsBack value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\DocumentsBackInfo $documentsBack
     * @return \PPLSDK\StructType\MyApiPackageIn
     */
    public function setDocumentsBack(\PPLSDK\StructType\DocumentsBackInfo $documentsBack = null)
    {
        if (is_null($documentsBack) || (is_array($documentsBack) && empty($documentsBack))) {
            unset($this->DocumentsBack);
        } else {
            $this->DocumentsBack = $documentsBack;
        }
        return $this;
    }
    /**
     * Get AddressesForServices value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\ArrayType\ArrayOfAddressForService|null
     */
    public function getAddressesForServices()
    {
        return isset($this->AddressesForServices) ? $this->AddressesForServices : null;
    }
    /**
     * Set AddressesForServices value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\ArrayType\ArrayOfAddressForService $addressesForServices
     * @return \PPLSDK\StructType\MyApiPackageIn
     */
    public function setAddressesForServices(\PPLSDK\ArrayType\ArrayOfAddressForService $addressesForServices = null)
    {
        if (is_null($addressesForServices) || (is_array($addressesForServices) && empty($addressesForServices))) {
            unset($this->AddressesForServices);
        } else {
            $this->AddressesForServices = $addressesForServices;
        }
        return $this;
    }
}
