<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UpdatePackageResponse StructType
 * @subpackage Structs
 */
class UpdatePackageResponse extends AbstractStructBase
{
    /**
     * The UpdatePackageResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\UpdatePackageResult
     */
    public $UpdatePackageResult;
    /**
     * Constructor method for UpdatePackageResponse
     * @uses UpdatePackageResponse::setUpdatePackageResult()
     * @param \PPLSDK\StructType\UpdatePackageResult $updatePackageResult
     */
    public function __construct(\PPLSDK\StructType\UpdatePackageResult $updatePackageResult = null)
    {
        $this
            ->setUpdatePackageResult($updatePackageResult);
    }
    /**
     * Get UpdatePackageResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\UpdatePackageResult|null
     */
    public function getUpdatePackageResult()
    {
        return isset($this->UpdatePackageResult) ? $this->UpdatePackageResult : null;
    }
    /**
     * Set UpdatePackageResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\UpdatePackageResult $updatePackageResult
     * @return \PPLSDK\StructType\UpdatePackageResponse
     */
    public function setUpdatePackageResult(\PPLSDK\StructType\UpdatePackageResult $updatePackageResult = null)
    {
        if (is_null($updatePackageResult) || (is_array($updatePackageResult) && empty($updatePackageResult))) {
            unset($this->UpdatePackageResult);
        } else {
            $this->UpdatePackageResult = $updatePackageResult;
        }
        return $this;
    }
}
