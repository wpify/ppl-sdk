<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiPackNumberRowType StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiPackNumberRowType
 * @subpackage Structs
 */
class MyApiPackNumberRowType extends AbstractStructBase
{
    /**
     * The PckNRTID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $PckNRTID;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Name;
    /**
     * The Reject
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $Reject;
    /**
     * Constructor method for MyApiPackNumberRowType
     * @uses MyApiPackNumberRowType::setPckNRTID()
     * @uses MyApiPackNumberRowType::setName()
     * @uses MyApiPackNumberRowType::setReject()
     * @param int $pckNRTID
     * @param string $name
     * @param bool $reject
     */
    public function __construct($pckNRTID = null, $name = null, $reject = null)
    {
        $this
            ->setPckNRTID($pckNRTID)
            ->setName($name)
            ->setReject($reject);
    }
    /**
     * Get PckNRTID value
     * @return int|null
     */
    public function getPckNRTID()
    {
        return $this->PckNRTID;
    }
    /**
     * Set PckNRTID value
     * @param int $pckNRTID
     * @return \PPLSDK\StructType\MyApiPackNumberRowType
     */
    public function setPckNRTID($pckNRTID = null)
    {
        // validation for constraint: int
        if (!is_null($pckNRTID) && !(is_int($pckNRTID) || ctype_digit($pckNRTID))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pckNRTID, true), gettype($pckNRTID)), __LINE__);
        }
        $this->PckNRTID = $pckNRTID;
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName()
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \PPLSDK\StructType\MyApiPackNumberRowType
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        return $this;
    }
    /**
     * Get Reject value
     * @return bool|null
     */
    public function getReject()
    {
        return $this->Reject;
    }
    /**
     * Set Reject value
     * @param bool $reject
     * @return \PPLSDK\StructType\MyApiPackNumberRowType
     */
    public function setReject($reject = null)
    {
        // validation for constraint: boolean
        if (!is_null($reject) && !is_bool($reject)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($reject, true), gettype($reject)), __LINE__);
        }
        $this->Reject = $reject;
        return $this;
    }
}
