<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCitiesRoutingResponse StructType
 * @subpackage Structs
 */
class GetCitiesRoutingResponse extends AbstractStructBase
{
    /**
     * The GetCitiesRoutingResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\GetCitiesRoutingResult
     */
    public $GetCitiesRoutingResult;
    /**
     * Constructor method for GetCitiesRoutingResponse
     * @uses GetCitiesRoutingResponse::setGetCitiesRoutingResult()
     * @param \PPLSDK\StructType\GetCitiesRoutingResult $getCitiesRoutingResult
     */
    public function __construct(\PPLSDK\StructType\GetCitiesRoutingResult $getCitiesRoutingResult = null)
    {
        $this
            ->setGetCitiesRoutingResult($getCitiesRoutingResult);
    }
    /**
     * Get GetCitiesRoutingResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\GetCitiesRoutingResult|null
     */
    public function getGetCitiesRoutingResult()
    {
        return isset($this->GetCitiesRoutingResult) ? $this->GetCitiesRoutingResult : null;
    }
    /**
     * Set GetCitiesRoutingResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\GetCitiesRoutingResult $getCitiesRoutingResult
     * @return \PPLSDK\StructType\GetCitiesRoutingResponse
     */
    public function setGetCitiesRoutingResult(\PPLSDK\StructType\GetCitiesRoutingResult $getCitiesRoutingResult = null)
    {
        if (is_null($getCitiesRoutingResult) || (is_array($getCitiesRoutingResult) && empty($getCitiesRoutingResult))) {
            unset($this->GetCitiesRoutingResult);
        } else {
            $this->GetCitiesRoutingResult = $getCitiesRoutingResult;
        }
        return $this;
    }
}
