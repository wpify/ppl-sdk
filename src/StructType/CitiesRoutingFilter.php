<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CitiesRoutingFilter StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:CitiesRoutingFilter
 * @subpackage Structs
 */
class CitiesRoutingFilter extends AbstractStructBase
{
    /**
     * The CountryCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CountryCode;
    /**
     * The DateFrom
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DateFrom;
    /**
     * The EveningDelivery
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $EveningDelivery;
    /**
     * The MorningDelivery
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $MorningDelivery;
    /**
     * The PackProductType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PackProductType;
    /**
     * The Post
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Post;
    /**
     * The Rejected
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $Rejected;
    /**
     * The SaturdayDelivery
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $SaturdayDelivery;
    /**
     * The Street
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Street;
    /**
     * The ZipCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ZipCode;
    /**
     * Constructor method for CitiesRoutingFilter
     * @uses CitiesRoutingFilter::setCountryCode()
     * @uses CitiesRoutingFilter::setDateFrom()
     * @uses CitiesRoutingFilter::setEveningDelivery()
     * @uses CitiesRoutingFilter::setMorningDelivery()
     * @uses CitiesRoutingFilter::setPackProductType()
     * @uses CitiesRoutingFilter::setPost()
     * @uses CitiesRoutingFilter::setRejected()
     * @uses CitiesRoutingFilter::setSaturdayDelivery()
     * @uses CitiesRoutingFilter::setStreet()
     * @uses CitiesRoutingFilter::setZipCode()
     * @param string $countryCode
     * @param string $dateFrom
     * @param bool $eveningDelivery
     * @param bool $morningDelivery
     * @param string $packProductType
     * @param string $post
     * @param bool $rejected
     * @param bool $saturdayDelivery
     * @param string $street
     * @param string $zipCode
     */
    public function __construct($countryCode = null, $dateFrom = null, $eveningDelivery = null, $morningDelivery = null, $packProductType = null, $post = null, $rejected = null, $saturdayDelivery = null, $street = null, $zipCode = null)
    {
        $this
            ->setCountryCode($countryCode)
            ->setDateFrom($dateFrom)
            ->setEveningDelivery($eveningDelivery)
            ->setMorningDelivery($morningDelivery)
            ->setPackProductType($packProductType)
            ->setPost($post)
            ->setRejected($rejected)
            ->setSaturdayDelivery($saturdayDelivery)
            ->setStreet($street)
            ->setZipCode($zipCode);
    }
    /**
     * Get CountryCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryCode()
    {
        return isset($this->CountryCode) ? $this->CountryCode : null;
    }
    /**
     * Set CountryCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryCode
     * @return \PPLSDK\StructType\CitiesRoutingFilter
     */
    public function setCountryCode($countryCode = null)
    {
        // validation for constraint: string
        if (!is_null($countryCode) && !is_string($countryCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryCode, true), gettype($countryCode)), __LINE__);
        }
        if (is_null($countryCode) || (is_array($countryCode) && empty($countryCode))) {
            unset($this->CountryCode);
        } else {
            $this->CountryCode = $countryCode;
        }
        return $this;
    }
    /**
     * Get DateFrom value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDateFrom()
    {
        return isset($this->DateFrom) ? $this->DateFrom : null;
    }
    /**
     * Set DateFrom value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $dateFrom
     * @return \PPLSDK\StructType\CitiesRoutingFilter
     */
    public function setDateFrom($dateFrom = null)
    {
        // validation for constraint: string
        if (!is_null($dateFrom) && !is_string($dateFrom)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateFrom, true), gettype($dateFrom)), __LINE__);
        }
        if (is_null($dateFrom) || (is_array($dateFrom) && empty($dateFrom))) {
            unset($this->DateFrom);
        } else {
            $this->DateFrom = $dateFrom;
        }
        return $this;
    }
    /**
     * Get EveningDelivery value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getEveningDelivery()
    {
        return isset($this->EveningDelivery) ? $this->EveningDelivery : null;
    }
    /**
     * Set EveningDelivery value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $eveningDelivery
     * @return \PPLSDK\StructType\CitiesRoutingFilter
     */
    public function setEveningDelivery($eveningDelivery = null)
    {
        // validation for constraint: boolean
        if (!is_null($eveningDelivery) && !is_bool($eveningDelivery)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($eveningDelivery, true), gettype($eveningDelivery)), __LINE__);
        }
        if (is_null($eveningDelivery) || (is_array($eveningDelivery) && empty($eveningDelivery))) {
            unset($this->EveningDelivery);
        } else {
            $this->EveningDelivery = $eveningDelivery;
        }
        return $this;
    }
    /**
     * Get MorningDelivery value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getMorningDelivery()
    {
        return isset($this->MorningDelivery) ? $this->MorningDelivery : null;
    }
    /**
     * Set MorningDelivery value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $morningDelivery
     * @return \PPLSDK\StructType\CitiesRoutingFilter
     */
    public function setMorningDelivery($morningDelivery = null)
    {
        // validation for constraint: boolean
        if (!is_null($morningDelivery) && !is_bool($morningDelivery)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($morningDelivery, true), gettype($morningDelivery)), __LINE__);
        }
        if (is_null($morningDelivery) || (is_array($morningDelivery) && empty($morningDelivery))) {
            unset($this->MorningDelivery);
        } else {
            $this->MorningDelivery = $morningDelivery;
        }
        return $this;
    }
    /**
     * Get PackProductType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPackProductType()
    {
        return isset($this->PackProductType) ? $this->PackProductType : null;
    }
    /**
     * Set PackProductType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $packProductType
     * @return \PPLSDK\StructType\CitiesRoutingFilter
     */
    public function setPackProductType($packProductType = null)
    {
        // validation for constraint: string
        if (!is_null($packProductType) && !is_string($packProductType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packProductType, true), gettype($packProductType)), __LINE__);
        }
        if (is_null($packProductType) || (is_array($packProductType) && empty($packProductType))) {
            unset($this->PackProductType);
        } else {
            $this->PackProductType = $packProductType;
        }
        return $this;
    }
    /**
     * Get Post value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPost()
    {
        return isset($this->Post) ? $this->Post : null;
    }
    /**
     * Set Post value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $post
     * @return \PPLSDK\StructType\CitiesRoutingFilter
     */
    public function setPost($post = null)
    {
        // validation for constraint: string
        if (!is_null($post) && !is_string($post)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($post, true), gettype($post)), __LINE__);
        }
        if (is_null($post) || (is_array($post) && empty($post))) {
            unset($this->Post);
        } else {
            $this->Post = $post;
        }
        return $this;
    }
    /**
     * Get Rejected value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getRejected()
    {
        return isset($this->Rejected) ? $this->Rejected : null;
    }
    /**
     * Set Rejected value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $rejected
     * @return \PPLSDK\StructType\CitiesRoutingFilter
     */
    public function setRejected($rejected = null)
    {
        // validation for constraint: boolean
        if (!is_null($rejected) && !is_bool($rejected)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($rejected, true), gettype($rejected)), __LINE__);
        }
        if (is_null($rejected) || (is_array($rejected) && empty($rejected))) {
            unset($this->Rejected);
        } else {
            $this->Rejected = $rejected;
        }
        return $this;
    }
    /**
     * Get SaturdayDelivery value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getSaturdayDelivery()
    {
        return isset($this->SaturdayDelivery) ? $this->SaturdayDelivery : null;
    }
    /**
     * Set SaturdayDelivery value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $saturdayDelivery
     * @return \PPLSDK\StructType\CitiesRoutingFilter
     */
    public function setSaturdayDelivery($saturdayDelivery = null)
    {
        // validation for constraint: boolean
        if (!is_null($saturdayDelivery) && !is_bool($saturdayDelivery)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($saturdayDelivery, true), gettype($saturdayDelivery)), __LINE__);
        }
        if (is_null($saturdayDelivery) || (is_array($saturdayDelivery) && empty($saturdayDelivery))) {
            unset($this->SaturdayDelivery);
        } else {
            $this->SaturdayDelivery = $saturdayDelivery;
        }
        return $this;
    }
    /**
     * Get Street value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStreet()
    {
        return isset($this->Street) ? $this->Street : null;
    }
    /**
     * Set Street value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $street
     * @return \PPLSDK\StructType\CitiesRoutingFilter
     */
    public function setStreet($street = null)
    {
        // validation for constraint: string
        if (!is_null($street) && !is_string($street)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($street, true), gettype($street)), __LINE__);
        }
        if (is_null($street) || (is_array($street) && empty($street))) {
            unset($this->Street);
        } else {
            $this->Street = $street;
        }
        return $this;
    }
    /**
     * Get ZipCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getZipCode()
    {
        return isset($this->ZipCode) ? $this->ZipCode : null;
    }
    /**
     * Set ZipCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $zipCode
     * @return \PPLSDK\StructType\CitiesRoutingFilter
     */
    public function setZipCode($zipCode = null)
    {
        // validation for constraint: string
        if (!is_null($zipCode) && !is_string($zipCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($zipCode, true), gettype($zipCode)), __LINE__);
        }
        if (is_null($zipCode) || (is_array($zipCode) && empty($zipCode))) {
            unset($this->ZipCode);
        } else {
            $this->ZipCode = $zipCode;
        }
        return $this;
    }
}
