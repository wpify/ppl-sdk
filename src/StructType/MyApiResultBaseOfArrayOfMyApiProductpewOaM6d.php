<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiResultBaseOfArrayOfMyApiProductpewOaM6d StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiResultBaseOfArrayOfMyApiProductpewOaM6d
 * @subpackage Structs
 */
class MyApiResultBaseOfArrayOfMyApiProductpewOaM6d extends MyApiResultBaseVoid
{
    /**
     * The ResultData
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiProduct
     */
    public $ResultData;
    /**
     * Constructor method for MyApiResultBaseOfArrayOfMyApiProductpewOaM6d
     * @uses MyApiResultBaseOfArrayOfMyApiProductpewOaM6d::setResultData()
     * @param \PPLSDK\ArrayType\ArrayOfMyApiProduct $resultData
     */
    public function __construct(\PPLSDK\ArrayType\ArrayOfMyApiProduct $resultData = null)
    {
        $this
            ->setResultData($resultData);
    }
    /**
     * Get ResultData value
     * @return \PPLSDK\ArrayType\ArrayOfMyApiProduct|null
     */
    public function getResultData()
    {
        return $this->ResultData;
    }
    /**
     * Set ResultData value
     * @param \PPLSDK\ArrayType\ArrayOfMyApiProduct $resultData
     * @return \PPLSDK\StructType\MyApiResultBaseOfArrayOfMyApiProductpewOaM6d
     */
    public function setResultData(\PPLSDK\ArrayType\ArrayOfMyApiProduct $resultData = null)
    {
        $this->ResultData = $resultData;
        return $this;
    }
}
