<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPackProductsResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:GetPackProductsResult
 * @subpackage Structs
 */
class GetPackProductsResult extends MyApiResultBaseOfArrayOfMyApiProductpewOaM6d
{
}
