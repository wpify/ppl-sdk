<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PartnerData StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:PartnerData
 * @subpackage Structs
 */
class PartnerData extends AbstractStructBase
{
    /**
     * The City
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $City;
    /**
     * The Country
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Country;
    /**
     * The CustID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $CustID;
    /**
     * The Email
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Email;
    /**
     * The GpsE
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $GpsE;
    /**
     * The GpsN
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $GpsN;
    /**
     * The ID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $ID;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Name;
    /**
     * The Note
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Note;
    /**
     * The OpeningTime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $OpeningTime;
    /**
     * The PartnerID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PartnerID;
    /**
     * The PartnerType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $PartnerType;
    /**
     * The Reject
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $Reject;
    /**
     * The Street
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Street;
    /**
     * The Telephone
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Telephone;
    /**
     * The ZipCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ZipCode;
    /**
     * Constructor method for PartnerData
     * @uses PartnerData::setCity()
     * @uses PartnerData::setCountry()
     * @uses PartnerData::setCustID()
     * @uses PartnerData::setEmail()
     * @uses PartnerData::setGpsE()
     * @uses PartnerData::setGpsN()
     * @uses PartnerData::setID()
     * @uses PartnerData::setName()
     * @uses PartnerData::setNote()
     * @uses PartnerData::setOpeningTime()
     * @uses PartnerData::setPartnerID()
     * @uses PartnerData::setPartnerType()
     * @uses PartnerData::setReject()
     * @uses PartnerData::setStreet()
     * @uses PartnerData::setTelephone()
     * @uses PartnerData::setZipCode()
     * @param string $city
     * @param string $country
     * @param int $custID
     * @param string $email
     * @param float $gpsE
     * @param float $gpsN
     * @param int $iD
     * @param string $name
     * @param string $note
     * @param string $openingTime
     * @param string $partnerID
     * @param string $partnerType
     * @param int $reject
     * @param string $street
     * @param string $telephone
     * @param string $zipCode
     */
    public function __construct($city = null, $country = null, $custID = null, $email = null, $gpsE = null, $gpsN = null, $iD = null, $name = null, $note = null, $openingTime = null, $partnerID = null, $partnerType = null, $reject = null, $street = null, $telephone = null, $zipCode = null)
    {
        $this
            ->setCity($city)
            ->setCountry($country)
            ->setCustID($custID)
            ->setEmail($email)
            ->setGpsE($gpsE)
            ->setGpsN($gpsN)
            ->setID($iD)
            ->setName($name)
            ->setNote($note)
            ->setOpeningTime($openingTime)
            ->setPartnerID($partnerID)
            ->setPartnerType($partnerType)
            ->setReject($reject)
            ->setStreet($street)
            ->setTelephone($telephone)
            ->setZipCode($zipCode);
    }
    /**
     * Get City value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCity()
    {
        return isset($this->City) ? $this->City : null;
    }
    /**
     * Set City value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $city
     * @return \PPLSDK\StructType\PartnerData
     */
    public function setCity($city = null)
    {
        // validation for constraint: string
        if (!is_null($city) && !is_string($city)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($city, true), gettype($city)), __LINE__);
        }
        if (is_null($city) || (is_array($city) && empty($city))) {
            unset($this->City);
        } else {
            $this->City = $city;
        }
        return $this;
    }
    /**
     * Get Country value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountry()
    {
        return isset($this->Country) ? $this->Country : null;
    }
    /**
     * Set Country value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $country
     * @return \PPLSDK\StructType\PartnerData
     */
    public function setCountry($country = null)
    {
        // validation for constraint: string
        if (!is_null($country) && !is_string($country)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($country, true), gettype($country)), __LINE__);
        }
        if (is_null($country) || (is_array($country) && empty($country))) {
            unset($this->Country);
        } else {
            $this->Country = $country;
        }
        return $this;
    }
    /**
     * Get CustID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getCustID()
    {
        return isset($this->CustID) ? $this->CustID : null;
    }
    /**
     * Set CustID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $custID
     * @return \PPLSDK\StructType\PartnerData
     */
    public function setCustID($custID = null)
    {
        // validation for constraint: int
        if (!is_null($custID) && !(is_int($custID) || ctype_digit($custID))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($custID, true), gettype($custID)), __LINE__);
        }
        if (is_null($custID) || (is_array($custID) && empty($custID))) {
            unset($this->CustID);
        } else {
            $this->CustID = $custID;
        }
        return $this;
    }
    /**
     * Get Email value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEmail()
    {
        return isset($this->Email) ? $this->Email : null;
    }
    /**
     * Set Email value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $email
     * @return \PPLSDK\StructType\PartnerData
     */
    public function setEmail($email = null)
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        if (is_null($email) || (is_array($email) && empty($email))) {
            unset($this->Email);
        } else {
            $this->Email = $email;
        }
        return $this;
    }
    /**
     * Get GpsE value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getGpsE()
    {
        return isset($this->GpsE) ? $this->GpsE : null;
    }
    /**
     * Set GpsE value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $gpsE
     * @return \PPLSDK\StructType\PartnerData
     */
    public function setGpsE($gpsE = null)
    {
        // validation for constraint: float
        if (!is_null($gpsE) && !(is_float($gpsE) || is_numeric($gpsE))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($gpsE, true), gettype($gpsE)), __LINE__);
        }
        if (is_null($gpsE) || (is_array($gpsE) && empty($gpsE))) {
            unset($this->GpsE);
        } else {
            $this->GpsE = $gpsE;
        }
        return $this;
    }
    /**
     * Get GpsN value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getGpsN()
    {
        return isset($this->GpsN) ? $this->GpsN : null;
    }
    /**
     * Set GpsN value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $gpsN
     * @return \PPLSDK\StructType\PartnerData
     */
    public function setGpsN($gpsN = null)
    {
        // validation for constraint: float
        if (!is_null($gpsN) && !(is_float($gpsN) || is_numeric($gpsN))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($gpsN, true), gettype($gpsN)), __LINE__);
        }
        if (is_null($gpsN) || (is_array($gpsN) && empty($gpsN))) {
            unset($this->GpsN);
        } else {
            $this->GpsN = $gpsN;
        }
        return $this;
    }
    /**
     * Get ID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getID()
    {
        return isset($this->ID) ? $this->ID : null;
    }
    /**
     * Set ID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $iD
     * @return \PPLSDK\StructType\PartnerData
     */
    public function setID($iD = null)
    {
        // validation for constraint: int
        if (!is_null($iD) && !(is_int($iD) || ctype_digit($iD))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($iD, true), gettype($iD)), __LINE__);
        }
        if (is_null($iD) || (is_array($iD) && empty($iD))) {
            unset($this->ID);
        } else {
            $this->ID = $iD;
        }
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName()
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \PPLSDK\StructType\PartnerData
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        return $this;
    }
    /**
     * Get Note value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNote()
    {
        return isset($this->Note) ? $this->Note : null;
    }
    /**
     * Set Note value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $note
     * @return \PPLSDK\StructType\PartnerData
     */
    public function setNote($note = null)
    {
        // validation for constraint: string
        if (!is_null($note) && !is_string($note)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($note, true), gettype($note)), __LINE__);
        }
        if (is_null($note) || (is_array($note) && empty($note))) {
            unset($this->Note);
        } else {
            $this->Note = $note;
        }
        return $this;
    }
    /**
     * Get OpeningTime value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOpeningTime()
    {
        return isset($this->OpeningTime) ? $this->OpeningTime : null;
    }
    /**
     * Set OpeningTime value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $openingTime
     * @return \PPLSDK\StructType\PartnerData
     */
    public function setOpeningTime($openingTime = null)
    {
        // validation for constraint: string
        if (!is_null($openingTime) && !is_string($openingTime)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($openingTime, true), gettype($openingTime)), __LINE__);
        }
        if (is_null($openingTime) || (is_array($openingTime) && empty($openingTime))) {
            unset($this->OpeningTime);
        } else {
            $this->OpeningTime = $openingTime;
        }
        return $this;
    }
    /**
     * Get PartnerID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPartnerID()
    {
        return isset($this->PartnerID) ? $this->PartnerID : null;
    }
    /**
     * Set PartnerID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $partnerID
     * @return \PPLSDK\StructType\PartnerData
     */
    public function setPartnerID($partnerID = null)
    {
        // validation for constraint: string
        if (!is_null($partnerID) && !is_string($partnerID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($partnerID, true), gettype($partnerID)), __LINE__);
        }
        if (is_null($partnerID) || (is_array($partnerID) && empty($partnerID))) {
            unset($this->PartnerID);
        } else {
            $this->PartnerID = $partnerID;
        }
        return $this;
    }
    /**
     * Get PartnerType value
     * @return string|null
     */
    public function getPartnerType()
    {
        return $this->PartnerType;
    }
    /**
     * Set PartnerType value
     * @uses \PPLSDK\EnumType\PartnerType::valueIsValid()
     * @uses \PPLSDK\EnumType\PartnerType::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $partnerType
     * @return \PPLSDK\StructType\PartnerData
     */
    public function setPartnerType($partnerType = null)
    {
        // validation for constraint: enumeration
        if (!\PPLSDK\EnumType\PartnerType::valueIsValid($partnerType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \PPLSDK\EnumType\PartnerType', is_array($partnerType) ? implode(', ', $partnerType) : var_export($partnerType, true), implode(', ', \PPLSDK\EnumType\PartnerType::getValidValues())), __LINE__);
        }
        $this->PartnerType = $partnerType;
        return $this;
    }
    /**
     * Get Reject value
     * @return int|null
     */
    public function getReject()
    {
        return $this->Reject;
    }
    /**
     * Set Reject value
     * @param int $reject
     * @return \PPLSDK\StructType\PartnerData
     */
    public function setReject($reject = null)
    {
        // validation for constraint: int
        if (!is_null($reject) && !(is_int($reject) || ctype_digit($reject))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($reject, true), gettype($reject)), __LINE__);
        }
        $this->Reject = $reject;
        return $this;
    }
    /**
     * Get Street value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStreet()
    {
        return isset($this->Street) ? $this->Street : null;
    }
    /**
     * Set Street value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $street
     * @return \PPLSDK\StructType\PartnerData
     */
    public function setStreet($street = null)
    {
        // validation for constraint: string
        if (!is_null($street) && !is_string($street)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($street, true), gettype($street)), __LINE__);
        }
        if (is_null($street) || (is_array($street) && empty($street))) {
            unset($this->Street);
        } else {
            $this->Street = $street;
        }
        return $this;
    }
    /**
     * Get Telephone value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTelephone()
    {
        return isset($this->Telephone) ? $this->Telephone : null;
    }
    /**
     * Set Telephone value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $telephone
     * @return \PPLSDK\StructType\PartnerData
     */
    public function setTelephone($telephone = null)
    {
        // validation for constraint: string
        if (!is_null($telephone) && !is_string($telephone)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($telephone, true), gettype($telephone)), __LINE__);
        }
        if (is_null($telephone) || (is_array($telephone) && empty($telephone))) {
            unset($this->Telephone);
        } else {
            $this->Telephone = $telephone;
        }
        return $this;
    }
    /**
     * Get ZipCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getZipCode()
    {
        return isset($this->ZipCode) ? $this->ZipCode : null;
    }
    /**
     * Set ZipCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $zipCode
     * @return \PPLSDK\StructType\PartnerData
     */
    public function setZipCode($zipCode = null)
    {
        // validation for constraint: string
        if (!is_null($zipCode) && !is_string($zipCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($zipCode, true), gettype($zipCode)), __LINE__);
        }
        if (is_null($zipCode) || (is_array($zipCode) && empty($zipCode))) {
            unset($this->ZipCode);
        } else {
            $this->ZipCode = $zipCode;
        }
        return $this;
    }
}
