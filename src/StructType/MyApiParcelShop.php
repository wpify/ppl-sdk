<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiParcelShop StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiParcelShop
 * @subpackage Structs
 */
class MyApiParcelShop extends AbstractStructBase
{
    /**
     * The City
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $City;
    /**
     * The Country
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Country;
    /**
     * The Email
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Email;
    /**
     * The Fax
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Fax;
    /**
     * The GPSLocation
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiCoordinates
     */
    public $GPSLocation;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Name;
    /**
     * The Name2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Name2;
    /**
     * The OrgId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $OrgId;
    /**
     * The OrgVatId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $OrgVatId;
    /**
     * The ParcelShopCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ParcelShopCode;
    /**
     * The ParcelShopNote
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ParcelShopNote;
    /**
     * The Phone
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Phone;
    /**
     * The Position
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Position;
    /**
     * The QrCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $QrCode;
    /**
     * The Street
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Street;
    /**
     * The WorkHours
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiKTMWorkHour
     */
    public $WorkHours;
    /**
     * The ZipCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ZipCode;
    /**
     * Constructor method for MyApiParcelShop
     * @uses MyApiParcelShop::setCity()
     * @uses MyApiParcelShop::setCountry()
     * @uses MyApiParcelShop::setEmail()
     * @uses MyApiParcelShop::setFax()
     * @uses MyApiParcelShop::setGPSLocation()
     * @uses MyApiParcelShop::setName()
     * @uses MyApiParcelShop::setName2()
     * @uses MyApiParcelShop::setOrgId()
     * @uses MyApiParcelShop::setOrgVatId()
     * @uses MyApiParcelShop::setParcelShopCode()
     * @uses MyApiParcelShop::setParcelShopNote()
     * @uses MyApiParcelShop::setPhone()
     * @uses MyApiParcelShop::setPosition()
     * @uses MyApiParcelShop::setQrCode()
     * @uses MyApiParcelShop::setStreet()
     * @uses MyApiParcelShop::setWorkHours()
     * @uses MyApiParcelShop::setZipCode()
     * @param string $city
     * @param string $country
     * @param string $email
     * @param string $fax
     * @param \PPLSDK\StructType\MyApiCoordinates $gPSLocation
     * @param string $name
     * @param string $name2
     * @param string $orgId
     * @param string $orgVatId
     * @param string $parcelShopCode
     * @param string $parcelShopNote
     * @param string $phone
     * @param string $position
     * @param string $qrCode
     * @param string $street
     * @param \PPLSDK\ArrayType\ArrayOfMyApiKTMWorkHour $workHours
     * @param string $zipCode
     */
    public function __construct($city = null, $country = null, $email = null, $fax = null, \PPLSDK\StructType\MyApiCoordinates $gPSLocation = null, $name = null, $name2 = null, $orgId = null, $orgVatId = null, $parcelShopCode = null, $parcelShopNote = null, $phone = null, $position = null, $qrCode = null, $street = null, \PPLSDK\ArrayType\ArrayOfMyApiKTMWorkHour $workHours = null, $zipCode = null)
    {
        $this
            ->setCity($city)
            ->setCountry($country)
            ->setEmail($email)
            ->setFax($fax)
            ->setGPSLocation($gPSLocation)
            ->setName($name)
            ->setName2($name2)
            ->setOrgId($orgId)
            ->setOrgVatId($orgVatId)
            ->setParcelShopCode($parcelShopCode)
            ->setParcelShopNote($parcelShopNote)
            ->setPhone($phone)
            ->setPosition($position)
            ->setQrCode($qrCode)
            ->setStreet($street)
            ->setWorkHours($workHours)
            ->setZipCode($zipCode);
    }
    /**
     * Get City value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCity()
    {
        return isset($this->City) ? $this->City : null;
    }
    /**
     * Set City value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $city
     * @return \PPLSDK\StructType\MyApiParcelShop
     */
    public function setCity($city = null)
    {
        // validation for constraint: string
        if (!is_null($city) && !is_string($city)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($city, true), gettype($city)), __LINE__);
        }
        if (is_null($city) || (is_array($city) && empty($city))) {
            unset($this->City);
        } else {
            $this->City = $city;
        }
        return $this;
    }
    /**
     * Get Country value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountry()
    {
        return isset($this->Country) ? $this->Country : null;
    }
    /**
     * Set Country value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $country
     * @return \PPLSDK\StructType\MyApiParcelShop
     */
    public function setCountry($country = null)
    {
        // validation for constraint: string
        if (!is_null($country) && !is_string($country)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($country, true), gettype($country)), __LINE__);
        }
        if (is_null($country) || (is_array($country) && empty($country))) {
            unset($this->Country);
        } else {
            $this->Country = $country;
        }
        return $this;
    }
    /**
     * Get Email value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEmail()
    {
        return isset($this->Email) ? $this->Email : null;
    }
    /**
     * Set Email value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $email
     * @return \PPLSDK\StructType\MyApiParcelShop
     */
    public function setEmail($email = null)
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        if (is_null($email) || (is_array($email) && empty($email))) {
            unset($this->Email);
        } else {
            $this->Email = $email;
        }
        return $this;
    }
    /**
     * Get Fax value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFax()
    {
        return isset($this->Fax) ? $this->Fax : null;
    }
    /**
     * Set Fax value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $fax
     * @return \PPLSDK\StructType\MyApiParcelShop
     */
    public function setFax($fax = null)
    {
        // validation for constraint: string
        if (!is_null($fax) && !is_string($fax)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fax, true), gettype($fax)), __LINE__);
        }
        if (is_null($fax) || (is_array($fax) && empty($fax))) {
            unset($this->Fax);
        } else {
            $this->Fax = $fax;
        }
        return $this;
    }
    /**
     * Get GPSLocation value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiCoordinates|null
     */
    public function getGPSLocation()
    {
        return isset($this->GPSLocation) ? $this->GPSLocation : null;
    }
    /**
     * Set GPSLocation value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\MyApiCoordinates $gPSLocation
     * @return \PPLSDK\StructType\MyApiParcelShop
     */
    public function setGPSLocation(\PPLSDK\StructType\MyApiCoordinates $gPSLocation = null)
    {
        if (is_null($gPSLocation) || (is_array($gPSLocation) && empty($gPSLocation))) {
            unset($this->GPSLocation);
        } else {
            $this->GPSLocation = $gPSLocation;
        }
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName()
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \PPLSDK\StructType\MyApiParcelShop
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        return $this;
    }
    /**
     * Get Name2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName2()
    {
        return isset($this->Name2) ? $this->Name2 : null;
    }
    /**
     * Set Name2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name2
     * @return \PPLSDK\StructType\MyApiParcelShop
     */
    public function setName2($name2 = null)
    {
        // validation for constraint: string
        if (!is_null($name2) && !is_string($name2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name2, true), gettype($name2)), __LINE__);
        }
        if (is_null($name2) || (is_array($name2) && empty($name2))) {
            unset($this->Name2);
        } else {
            $this->Name2 = $name2;
        }
        return $this;
    }
    /**
     * Get OrgId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOrgId()
    {
        return isset($this->OrgId) ? $this->OrgId : null;
    }
    /**
     * Set OrgId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $orgId
     * @return \PPLSDK\StructType\MyApiParcelShop
     */
    public function setOrgId($orgId = null)
    {
        // validation for constraint: string
        if (!is_null($orgId) && !is_string($orgId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgId, true), gettype($orgId)), __LINE__);
        }
        if (is_null($orgId) || (is_array($orgId) && empty($orgId))) {
            unset($this->OrgId);
        } else {
            $this->OrgId = $orgId;
        }
        return $this;
    }
    /**
     * Get OrgVatId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOrgVatId()
    {
        return isset($this->OrgVatId) ? $this->OrgVatId : null;
    }
    /**
     * Set OrgVatId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $orgVatId
     * @return \PPLSDK\StructType\MyApiParcelShop
     */
    public function setOrgVatId($orgVatId = null)
    {
        // validation for constraint: string
        if (!is_null($orgVatId) && !is_string($orgVatId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgVatId, true), gettype($orgVatId)), __LINE__);
        }
        if (is_null($orgVatId) || (is_array($orgVatId) && empty($orgVatId))) {
            unset($this->OrgVatId);
        } else {
            $this->OrgVatId = $orgVatId;
        }
        return $this;
    }
    /**
     * Get ParcelShopCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getParcelShopCode()
    {
        return isset($this->ParcelShopCode) ? $this->ParcelShopCode : null;
    }
    /**
     * Set ParcelShopCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $parcelShopCode
     * @return \PPLSDK\StructType\MyApiParcelShop
     */
    public function setParcelShopCode($parcelShopCode = null)
    {
        // validation for constraint: string
        if (!is_null($parcelShopCode) && !is_string($parcelShopCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelShopCode, true), gettype($parcelShopCode)), __LINE__);
        }
        if (is_null($parcelShopCode) || (is_array($parcelShopCode) && empty($parcelShopCode))) {
            unset($this->ParcelShopCode);
        } else {
            $this->ParcelShopCode = $parcelShopCode;
        }
        return $this;
    }
    /**
     * Get ParcelShopNote value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getParcelShopNote()
    {
        return isset($this->ParcelShopNote) ? $this->ParcelShopNote : null;
    }
    /**
     * Set ParcelShopNote value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $parcelShopNote
     * @return \PPLSDK\StructType\MyApiParcelShop
     */
    public function setParcelShopNote($parcelShopNote = null)
    {
        // validation for constraint: string
        if (!is_null($parcelShopNote) && !is_string($parcelShopNote)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelShopNote, true), gettype($parcelShopNote)), __LINE__);
        }
        if (is_null($parcelShopNote) || (is_array($parcelShopNote) && empty($parcelShopNote))) {
            unset($this->ParcelShopNote);
        } else {
            $this->ParcelShopNote = $parcelShopNote;
        }
        return $this;
    }
    /**
     * Get Phone value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPhone()
    {
        return isset($this->Phone) ? $this->Phone : null;
    }
    /**
     * Set Phone value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $phone
     * @return \PPLSDK\StructType\MyApiParcelShop
     */
    public function setPhone($phone = null)
    {
        // validation for constraint: string
        if (!is_null($phone) && !is_string($phone)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($phone, true), gettype($phone)), __LINE__);
        }
        if (is_null($phone) || (is_array($phone) && empty($phone))) {
            unset($this->Phone);
        } else {
            $this->Phone = $phone;
        }
        return $this;
    }
    /**
     * Get Position value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPosition()
    {
        return isset($this->Position) ? $this->Position : null;
    }
    /**
     * Set Position value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $position
     * @return \PPLSDK\StructType\MyApiParcelShop
     */
    public function setPosition($position = null)
    {
        // validation for constraint: string
        if (!is_null($position) && !is_string($position)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($position, true), gettype($position)), __LINE__);
        }
        if (is_null($position) || (is_array($position) && empty($position))) {
            unset($this->Position);
        } else {
            $this->Position = $position;
        }
        return $this;
    }
    /**
     * Get QrCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getQrCode()
    {
        return isset($this->QrCode) ? $this->QrCode : null;
    }
    /**
     * Set QrCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $qrCode
     * @return \PPLSDK\StructType\MyApiParcelShop
     */
    public function setQrCode($qrCode = null)
    {
        // validation for constraint: string
        if (!is_null($qrCode) && !is_string($qrCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($qrCode, true), gettype($qrCode)), __LINE__);
        }
        if (is_null($qrCode) || (is_array($qrCode) && empty($qrCode))) {
            unset($this->QrCode);
        } else {
            $this->QrCode = $qrCode;
        }
        return $this;
    }
    /**
     * Get Street value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStreet()
    {
        return isset($this->Street) ? $this->Street : null;
    }
    /**
     * Set Street value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $street
     * @return \PPLSDK\StructType\MyApiParcelShop
     */
    public function setStreet($street = null)
    {
        // validation for constraint: string
        if (!is_null($street) && !is_string($street)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($street, true), gettype($street)), __LINE__);
        }
        if (is_null($street) || (is_array($street) && empty($street))) {
            unset($this->Street);
        } else {
            $this->Street = $street;
        }
        return $this;
    }
    /**
     * Get WorkHours value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\ArrayType\ArrayOfMyApiKTMWorkHour|null
     */
    public function getWorkHours()
    {
        return isset($this->WorkHours) ? $this->WorkHours : null;
    }
    /**
     * Set WorkHours value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\ArrayType\ArrayOfMyApiKTMWorkHour $workHours
     * @return \PPLSDK\StructType\MyApiParcelShop
     */
    public function setWorkHours(\PPLSDK\ArrayType\ArrayOfMyApiKTMWorkHour $workHours = null)
    {
        if (is_null($workHours) || (is_array($workHours) && empty($workHours))) {
            unset($this->WorkHours);
        } else {
            $this->WorkHours = $workHours;
        }
        return $this;
    }
    /**
     * Get ZipCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getZipCode()
    {
        return isset($this->ZipCode) ? $this->ZipCode : null;
    }
    /**
     * Set ZipCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $zipCode
     * @return \PPLSDK\StructType\MyApiParcelShop
     */
    public function setZipCode($zipCode = null)
    {
        // validation for constraint: string
        if (!is_null($zipCode) && !is_string($zipCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($zipCode, true), gettype($zipCode)), __LINE__);
        }
        if (is_null($zipCode) || (is_array($zipCode) && empty($zipCode))) {
            unset($this->ZipCode);
        } else {
            $this->ZipCode = $zipCode;
        }
        return $this;
    }
}
