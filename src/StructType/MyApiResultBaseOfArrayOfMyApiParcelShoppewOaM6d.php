<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiResultBaseOfArrayOfMyApiParcelShoppewOaM6d StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiResultBaseOfArrayOfMyApiParcelShoppewOaM6d
 * @subpackage Structs
 */
class MyApiResultBaseOfArrayOfMyApiParcelShoppewOaM6d extends MyApiResultBaseVoid
{
    /**
     * The ResultData
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiParcelShop
     */
    public $ResultData;
    /**
     * Constructor method for MyApiResultBaseOfArrayOfMyApiParcelShoppewOaM6d
     * @uses MyApiResultBaseOfArrayOfMyApiParcelShoppewOaM6d::setResultData()
     * @param \PPLSDK\ArrayType\ArrayOfMyApiParcelShop $resultData
     */
    public function __construct(\PPLSDK\ArrayType\ArrayOfMyApiParcelShop $resultData = null)
    {
        $this
            ->setResultData($resultData);
    }
    /**
     * Get ResultData value
     * @return \PPLSDK\ArrayType\ArrayOfMyApiParcelShop|null
     */
    public function getResultData()
    {
        return $this->ResultData;
    }
    /**
     * Set ResultData value
     * @param \PPLSDK\ArrayType\ArrayOfMyApiParcelShop $resultData
     * @return \PPLSDK\StructType\MyApiResultBaseOfArrayOfMyApiParcelShoppewOaM6d
     */
    public function setResultData(\PPLSDK\ArrayType\ArrayOfMyApiParcelShop $resultData = null)
    {
        $this->ResultData = $resultData;
        return $this;
    }
}
