<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPackProductsResponse StructType
 * @subpackage Structs
 */
class GetPackProductsResponse extends AbstractStructBase
{
    /**
     * The GetPackProductsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\GetPackProductsResult
     */
    public $GetPackProductsResult;
    /**
     * Constructor method for GetPackProductsResponse
     * @uses GetPackProductsResponse::setGetPackProductsResult()
     * @param \PPLSDK\StructType\GetPackProductsResult $getPackProductsResult
     */
    public function __construct(\PPLSDK\StructType\GetPackProductsResult $getPackProductsResult = null)
    {
        $this
            ->setGetPackProductsResult($getPackProductsResult);
    }
    /**
     * Get GetPackProductsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\GetPackProductsResult|null
     */
    public function getGetPackProductsResult()
    {
        return isset($this->GetPackProductsResult) ? $this->GetPackProductsResult : null;
    }
    /**
     * Set GetPackProductsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\GetPackProductsResult $getPackProductsResult
     * @return \PPLSDK\StructType\GetPackProductsResponse
     */
    public function setGetPackProductsResult(\PPLSDK\StructType\GetPackProductsResult $getPackProductsResult = null)
    {
        if (is_null($getPackProductsResult) || (is_array($getPackProductsResult) && empty($getPackProductsResult))) {
            unset($this->GetPackProductsResult);
        } else {
            $this->GetPackProductsResult = $getPackProductsResult;
        }
        return $this;
    }
}
