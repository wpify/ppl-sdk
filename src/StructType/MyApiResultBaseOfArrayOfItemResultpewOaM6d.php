<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiResultBaseOfArrayOfItemResultpewOaM6d StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiResultBaseOfArrayOfItemResultpewOaM6d
 * @subpackage Structs
 */
class MyApiResultBaseOfArrayOfItemResultpewOaM6d extends MyApiResultBaseVoid
{
    /**
     * The ResultData
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfItemResult
     */
    public $ResultData;
    /**
     * Constructor method for MyApiResultBaseOfArrayOfItemResultpewOaM6d
     * @uses MyApiResultBaseOfArrayOfItemResultpewOaM6d::setResultData()
     * @param \PPLSDK\ArrayType\ArrayOfItemResult $resultData
     */
    public function __construct(\PPLSDK\ArrayType\ArrayOfItemResult $resultData = null)
    {
        $this
            ->setResultData($resultData);
    }
    /**
     * Get ResultData value
     * @return \PPLSDK\ArrayType\ArrayOfItemResult|null
     */
    public function getResultData()
    {
        return $this->ResultData;
    }
    /**
     * Set ResultData value
     * @param \PPLSDK\ArrayType\ArrayOfItemResult $resultData
     * @return \PPLSDK\StructType\MyApiResultBaseOfArrayOfItemResultpewOaM6d
     */
    public function setResultData(\PPLSDK\ArrayType\ArrayOfItemResult $resultData = null)
    {
        $this->ResultData = $resultData;
        return $this;
    }
}
