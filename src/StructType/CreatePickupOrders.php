<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreatePickupOrders StructType
 * @subpackage Structs
 */
class CreatePickupOrders extends AbstractStructBase
{
    /**
     * The Auth
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\AuthenticationData
     */
    public $Auth;
    /**
     * The Orders
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiPickupOrderIn
     */
    public $Orders;
    /**
     * The ReturnChannel
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\ReturnChannelSettings
     */
    public $ReturnChannel;
    /**
     * Constructor method for CreatePickupOrders
     * @uses CreatePickupOrders::setAuth()
     * @uses CreatePickupOrders::setOrders()
     * @uses CreatePickupOrders::setReturnChannel()
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPickupOrderIn $orders
     * @param \PPLSDK\StructType\ReturnChannelSettings $returnChannel
     */
    public function __construct(\PPLSDK\StructType\AuthenticationData $auth = null, \PPLSDK\ArrayType\ArrayOfMyApiPickupOrderIn $orders = null, \PPLSDK\StructType\ReturnChannelSettings $returnChannel = null)
    {
        $this
            ->setAuth($auth)
            ->setOrders($orders)
            ->setReturnChannel($returnChannel);
    }
    /**
     * Get Auth value
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function getAuth()
    {
        return $this->Auth;
    }
    /**
     * Set Auth value
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @return \PPLSDK\StructType\CreatePickupOrders
     */
    public function setAuth(\PPLSDK\StructType\AuthenticationData $auth = null)
    {
        $this->Auth = $auth;
        return $this;
    }
    /**
     * Get Orders value
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPickupOrderIn
     */
    public function getOrders()
    {
        return $this->Orders;
    }
    /**
     * Set Orders value
     * @param \PPLSDK\ArrayType\ArrayOfMyApiPickupOrderIn $orders
     * @return \PPLSDK\StructType\CreatePickupOrders
     */
    public function setOrders(\PPLSDK\ArrayType\ArrayOfMyApiPickupOrderIn $orders = null)
    {
        $this->Orders = $orders;
        return $this;
    }
    /**
     * Get ReturnChannel value
     * @return \PPLSDK\StructType\ReturnChannelSettings
     */
    public function getReturnChannel()
    {
        return $this->ReturnChannel;
    }
    /**
     * Set ReturnChannel value
     * @param \PPLSDK\StructType\ReturnChannelSettings $returnChannel
     * @return \PPLSDK\StructType\CreatePickupOrders
     */
    public function setReturnChannel(\PPLSDK\StructType\ReturnChannelSettings $returnChannel = null)
    {
        $this->ReturnChannel = $returnChannel;
        return $this;
    }
}
