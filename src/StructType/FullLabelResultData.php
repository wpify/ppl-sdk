<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FullLabelResultData StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:FullLabelResultData
 * @subpackage Structs
 */
class FullLabelResultData extends AbstractStructBase
{
    /**
     * The State
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $State;
    /**
     * The LabelUrl
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $LabelUrl;
    /**
     * Constructor method for FullLabelResultData
     * @uses FullLabelResultData::setState()
     * @uses FullLabelResultData::setLabelUrl()
     * @param string $state
     * @param string $labelUrl
     */
    public function __construct($state = null, $labelUrl = null)
    {
        $this
            ->setState($state)
            ->setLabelUrl($labelUrl);
    }
    /**
     * Get State value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getState()
    {
        return isset($this->State) ? $this->State : null;
    }
    /**
     * Set State value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $state
     * @return \PPLSDK\StructType\FullLabelResultData
     */
    public function setState($state = null)
    {
        // validation for constraint: string
        if (!is_null($state) && !is_string($state)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($state, true), gettype($state)), __LINE__);
        }
        if (is_null($state) || (is_array($state) && empty($state))) {
            unset($this->State);
        } else {
            $this->State = $state;
        }
        return $this;
    }
    /**
     * Get LabelUrl value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLabelUrl()
    {
        return isset($this->LabelUrl) ? $this->LabelUrl : null;
    }
    /**
     * Set LabelUrl value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $labelUrl
     * @return \PPLSDK\StructType\FullLabelResultData
     */
    public function setLabelUrl($labelUrl = null)
    {
        // validation for constraint: string
        if (!is_null($labelUrl) && !is_string($labelUrl)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($labelUrl, true), gettype($labelUrl)), __LINE__);
        }
        if (is_null($labelUrl) || (is_array($labelUrl) && empty($labelUrl))) {
            unset($this->LabelUrl);
        } else {
            $this->LabelUrl = $labelUrl;
        }
        return $this;
    }
}
