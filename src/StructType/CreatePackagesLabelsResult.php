<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreatePackagesLabelsResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:CreatePackagesLabelsResult
 * @subpackage Structs
 */
class CreatePackagesLabelsResult extends MyApiResultBaseOfCreatePackagesLabelsResultDatapewOaM6d
{
}
