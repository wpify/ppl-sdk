<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiResultBaseOfArrayOfMyApiSprintRoutespewOaM6d
 * StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiResultBaseOfArrayOfMyApiSprintRoutespewOaM6d
 * @subpackage Structs
 */
class MyApiResultBaseOfArrayOfMyApiSprintRoutespewOaM6d extends MyApiResultBaseVoid
{
    /**
     * The ResultData
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiSprintRoutes
     */
    public $ResultData;
    /**
     * Constructor method for MyApiResultBaseOfArrayOfMyApiSprintRoutespewOaM6d
     * @uses MyApiResultBaseOfArrayOfMyApiSprintRoutespewOaM6d::setResultData()
     * @param \PPLSDK\ArrayType\ArrayOfMyApiSprintRoutes $resultData
     */
    public function __construct(\PPLSDK\ArrayType\ArrayOfMyApiSprintRoutes $resultData = null)
    {
        $this
            ->setResultData($resultData);
    }
    /**
     * Get ResultData value
     * @return \PPLSDK\ArrayType\ArrayOfMyApiSprintRoutes|null
     */
    public function getResultData()
    {
        return $this->ResultData;
    }
    /**
     * Set ResultData value
     * @param \PPLSDK\ArrayType\ArrayOfMyApiSprintRoutes $resultData
     * @return \PPLSDK\StructType\MyApiResultBaseOfArrayOfMyApiSprintRoutespewOaM6d
     */
    public function setResultData(\PPLSDK\ArrayType\ArrayOfMyApiSprintRoutes $resultData = null)
    {
        $this->ResultData = $resultData;
        return $this;
    }
}
