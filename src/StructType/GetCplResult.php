<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCplResult StructType
 * @subpackage Structs
 */
class GetCplResult extends AbstractStructBase
{
    /**
     * The Auth
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\AuthenticationData
     */
    public $Auth;
    /**
     * The RequestId
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * @var int
     */
    public $RequestId;
    /**
     * Constructor method for GetCplResult
     * @uses GetCplResult::setAuth()
     * @uses GetCplResult::setRequestId()
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @param int $requestId
     */
    public function __construct(\PPLSDK\StructType\AuthenticationData $auth = null, $requestId = null)
    {
        $this
            ->setAuth($auth)
            ->setRequestId($requestId);
    }
    /**
     * Get Auth value
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function getAuth()
    {
        return $this->Auth;
    }
    /**
     * Set Auth value
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @return \PPLSDK\StructType\GetCplResult
     */
    public function setAuth(\PPLSDK\StructType\AuthenticationData $auth = null)
    {
        $this->Auth = $auth;
        return $this;
    }
    /**
     * Get RequestId value
     * @return int
     */
    public function getRequestId()
    {
        return $this->RequestId;
    }
    /**
     * Set RequestId value
     * @param int $requestId
     * @return \PPLSDK\StructType\GetCplResult
     */
    public function setRequestId($requestId = null)
    {
        // validation for constraint: int
        if (!is_null($requestId) && !(is_int($requestId) || ctype_digit($requestId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($requestId, true), gettype($requestId)), __LINE__);
        }
        $this->RequestId = $requestId;
        return $this;
    }
}
