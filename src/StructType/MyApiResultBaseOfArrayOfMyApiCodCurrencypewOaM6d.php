<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiResultBaseOfArrayOfMyApiCodCurrencypewOaM6d
 * StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiResultBaseOfArrayOfMyApiCodCurrencypewOaM6d
 * @subpackage Structs
 */
class MyApiResultBaseOfArrayOfMyApiCodCurrencypewOaM6d extends MyApiResultBaseVoid
{
    /**
     * The ResultData
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiCodCurrency
     */
    public $ResultData;
    /**
     * Constructor method for MyApiResultBaseOfArrayOfMyApiCodCurrencypewOaM6d
     * @uses MyApiResultBaseOfArrayOfMyApiCodCurrencypewOaM6d::setResultData()
     * @param \PPLSDK\ArrayType\ArrayOfMyApiCodCurrency $resultData
     */
    public function __construct(\PPLSDK\ArrayType\ArrayOfMyApiCodCurrency $resultData = null)
    {
        $this
            ->setResultData($resultData);
    }
    /**
     * Get ResultData value
     * @return \PPLSDK\ArrayType\ArrayOfMyApiCodCurrency|null
     */
    public function getResultData()
    {
        return $this->ResultData;
    }
    /**
     * Set ResultData value
     * @param \PPLSDK\ArrayType\ArrayOfMyApiCodCurrency $resultData
     * @return \PPLSDK\StructType\MyApiResultBaseOfArrayOfMyApiCodCurrencypewOaM6d
     */
    public function setResultData(\PPLSDK\ArrayType\ArrayOfMyApiCodCurrency $resultData = null)
    {
        $this->ResultData = $resultData;
        return $this;
    }
}
