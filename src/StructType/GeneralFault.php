<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GeneralFault StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:GeneralFault
 * @subpackage Structs
 */
class GeneralFault extends MyApiFault
{
}
