<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PartnerItemResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:PartnerItemResult
 * @subpackage Structs
 */
class PartnerItemResult extends AbstractStructBase
{
    /**
     * The Code
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Code;
    /**
     * The ItemKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ItemKey;
    /**
     * The ItemSubKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $ItemSubKey;
    /**
     * The Message
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Message;
    /**
     * Constructor method for PartnerItemResult
     * @uses PartnerItemResult::setCode()
     * @uses PartnerItemResult::setItemKey()
     * @uses PartnerItemResult::setItemSubKey()
     * @uses PartnerItemResult::setMessage()
     * @param string $code
     * @param string $itemKey
     * @param int $itemSubKey
     * @param string $message
     */
    public function __construct($code = null, $itemKey = null, $itemSubKey = null, $message = null)
    {
        $this
            ->setCode($code)
            ->setItemKey($itemKey)
            ->setItemSubKey($itemSubKey)
            ->setMessage($message);
    }
    /**
     * Get Code value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCode()
    {
        return isset($this->Code) ? $this->Code : null;
    }
    /**
     * Set Code value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $code
     * @return \PPLSDK\StructType\PartnerItemResult
     */
    public function setCode($code = null)
    {
        // validation for constraint: string
        if (!is_null($code) && !is_string($code)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($code, true), gettype($code)), __LINE__);
        }
        if (is_null($code) || (is_array($code) && empty($code))) {
            unset($this->Code);
        } else {
            $this->Code = $code;
        }
        return $this;
    }
    /**
     * Get ItemKey value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getItemKey()
    {
        return isset($this->ItemKey) ? $this->ItemKey : null;
    }
    /**
     * Set ItemKey value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $itemKey
     * @return \PPLSDK\StructType\PartnerItemResult
     */
    public function setItemKey($itemKey = null)
    {
        // validation for constraint: string
        if (!is_null($itemKey) && !is_string($itemKey)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($itemKey, true), gettype($itemKey)), __LINE__);
        }
        if (is_null($itemKey) || (is_array($itemKey) && empty($itemKey))) {
            unset($this->ItemKey);
        } else {
            $this->ItemKey = $itemKey;
        }
        return $this;
    }
    /**
     * Get ItemSubKey value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getItemSubKey()
    {
        return isset($this->ItemSubKey) ? $this->ItemSubKey : null;
    }
    /**
     * Set ItemSubKey value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $itemSubKey
     * @return \PPLSDK\StructType\PartnerItemResult
     */
    public function setItemSubKey($itemSubKey = null)
    {
        // validation for constraint: int
        if (!is_null($itemSubKey) && !(is_int($itemSubKey) || ctype_digit($itemSubKey))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($itemSubKey, true), gettype($itemSubKey)), __LINE__);
        }
        if (is_null($itemSubKey) || (is_array($itemSubKey) && empty($itemSubKey))) {
            unset($this->ItemSubKey);
        } else {
            $this->ItemSubKey = $itemSubKey;
        }
        return $this;
    }
    /**
     * Get Message value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMessage()
    {
        return isset($this->Message) ? $this->Message : null;
    }
    /**
     * Set Message value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $message
     * @return \PPLSDK\StructType\PartnerItemResult
     */
    public function setMessage($message = null)
    {
        // validation for constraint: string
        if (!is_null($message) && !is_string($message)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($message, true), gettype($message)), __LINE__);
        }
        if (is_null($message) || (is_array($message) && empty($message))) {
            unset($this->Message);
        } else {
            $this->Message = $message;
        }
        return $this;
    }
}
