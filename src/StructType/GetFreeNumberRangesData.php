<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetFreeNumberRangesData StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:GetFreeNumberRangesData
 * @subpackage Structs
 */
class GetFreeNumberRangesData extends AbstractStructBase
{
    /**
     * The PackProductName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PackProductName;
    /**
     * The PackProductType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PackProductType;
    /**
     * The Quantity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\Quantity
     */
    public $Quantity;
    /**
     * The CreatedType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\CreatedType
     */
    public $CreatedType;
    /**
     * Constructor method for GetFreeNumberRangesData
     * @uses GetFreeNumberRangesData::setPackProductName()
     * @uses GetFreeNumberRangesData::setPackProductType()
     * @uses GetFreeNumberRangesData::setQuantity()
     * @uses GetFreeNumberRangesData::setCreatedType()
     * @param string $packProductName
     * @param string $packProductType
     * @param \PPLSDK\StructType\Quantity $quantity
     * @param \PPLSDK\StructType\CreatedType $createdType
     */
    public function __construct($packProductName = null, $packProductType = null, \PPLSDK\StructType\Quantity $quantity = null, \PPLSDK\StructType\CreatedType $createdType = null)
    {
        $this
            ->setPackProductName($packProductName)
            ->setPackProductType($packProductType)
            ->setQuantity($quantity)
            ->setCreatedType($createdType);
    }
    /**
     * Get PackProductName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPackProductName()
    {
        return isset($this->PackProductName) ? $this->PackProductName : null;
    }
    /**
     * Set PackProductName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $packProductName
     * @return \PPLSDK\StructType\GetFreeNumberRangesData
     */
    public function setPackProductName($packProductName = null)
    {
        // validation for constraint: string
        if (!is_null($packProductName) && !is_string($packProductName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packProductName, true), gettype($packProductName)), __LINE__);
        }
        if (is_null($packProductName) || (is_array($packProductName) && empty($packProductName))) {
            unset($this->PackProductName);
        } else {
            $this->PackProductName = $packProductName;
        }
        return $this;
    }
    /**
     * Get PackProductType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPackProductType()
    {
        return isset($this->PackProductType) ? $this->PackProductType : null;
    }
    /**
     * Set PackProductType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $packProductType
     * @return \PPLSDK\StructType\GetFreeNumberRangesData
     */
    public function setPackProductType($packProductType = null)
    {
        // validation for constraint: string
        if (!is_null($packProductType) && !is_string($packProductType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($packProductType, true), gettype($packProductType)), __LINE__);
        }
        if (is_null($packProductType) || (is_array($packProductType) && empty($packProductType))) {
            unset($this->PackProductType);
        } else {
            $this->PackProductType = $packProductType;
        }
        return $this;
    }
    /**
     * Get Quantity value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\Quantity|null
     */
    public function getQuantity()
    {
        return isset($this->Quantity) ? $this->Quantity : null;
    }
    /**
     * Set Quantity value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\Quantity $quantity
     * @return \PPLSDK\StructType\GetFreeNumberRangesData
     */
    public function setQuantity(\PPLSDK\StructType\Quantity $quantity = null)
    {
        if (is_null($quantity) || (is_array($quantity) && empty($quantity))) {
            unset($this->Quantity);
        } else {
            $this->Quantity = $quantity;
        }
        return $this;
    }
    /**
     * Get CreatedType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\CreatedType|null
     */
    public function getCreatedType()
    {
        return isset($this->CreatedType) ? $this->CreatedType : null;
    }
    /**
     * Set CreatedType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\CreatedType $createdType
     * @return \PPLSDK\StructType\GetFreeNumberRangesData
     */
    public function setCreatedType(\PPLSDK\StructType\CreatedType $createdType = null)
    {
        if (is_null($createdType) || (is_array($createdType) && empty($createdType))) {
            unset($this->CreatedType);
        } else {
            $this->CreatedType = $createdType;
        }
        return $this;
    }
}
