<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiPackageIn2 StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiPackageIn2
 * @subpackage Structs
 */
class MyApiPackageIn2 extends MyApiPackageIn
{
    /**
     * The UniqueReferenceId
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $UniqueReferenceId;
    /**
     * Constructor method for MyApiPackageIn2
     * @uses MyApiPackageIn2::setUniqueReferenceId()
     * @param string $uniqueReferenceId
     */
    public function __construct($uniqueReferenceId = null)
    {
        $this
            ->setUniqueReferenceId($uniqueReferenceId);
    }
    /**
     * Get UniqueReferenceId value
     * @return string|null
     */
    public function getUniqueReferenceId()
    {
        return $this->UniqueReferenceId;
    }
    /**
     * Set UniqueReferenceId value
     * @param string $uniqueReferenceId
     * @return \PPLSDK\StructType\MyApiPackageIn2
     */
    public function setUniqueReferenceId($uniqueReferenceId = null)
    {
        // validation for constraint: string
        if (!is_null($uniqueReferenceId) && !is_string($uniqueReferenceId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($uniqueReferenceId, true), gettype($uniqueReferenceId)), __LINE__);
        }
        $this->UniqueReferenceId = $uniqueReferenceId;
        return $this;
    }
}
