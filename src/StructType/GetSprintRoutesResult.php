<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSprintRoutesResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:GetSprintRoutesResult
 * @subpackage Structs
 */
class GetSprintRoutesResult extends MyApiResultBaseOfArrayOfMyApiSprintRoutespewOaM6d
{
}
