<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPackNumberRowTypes StructType
 * @subpackage Structs
 */
class GetPackNumberRowTypes extends AbstractStructBase
{
    /**
     * The Filter
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\PackNumberRowTypeFilter
     */
    public $Filter;
    /**
     * Constructor method for GetPackNumberRowTypes
     * @uses GetPackNumberRowTypes::setFilter()
     * @param \PPLSDK\StructType\PackNumberRowTypeFilter $filter
     */
    public function __construct(\PPLSDK\StructType\PackNumberRowTypeFilter $filter = null)
    {
        $this
            ->setFilter($filter);
    }
    /**
     * Get Filter value
     * @return \PPLSDK\StructType\PackNumberRowTypeFilter
     */
    public function getFilter()
    {
        return $this->Filter;
    }
    /**
     * Set Filter value
     * @param \PPLSDK\StructType\PackNumberRowTypeFilter $filter
     * @return \PPLSDK\StructType\GetPackNumberRowTypes
     */
    public function setFilter(\PPLSDK\StructType\PackNumberRowTypeFilter $filter = null)
    {
        $this->Filter = $filter;
        return $this;
    }
}
