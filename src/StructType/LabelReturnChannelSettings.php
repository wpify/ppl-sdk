<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for LabelReturnChannelSettings StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:LabelReturnChannelSettings
 * @subpackage Structs
 */
class LabelReturnChannelSettings extends AbstractStructBase
{
    /**
     * The Type
     * @var string
     */
    public $Type;
    /**
     * The Format
     * @var string
     */
    public $Format;
    /**
     * The FullLabel
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\FullLabel
     */
    public $FullLabel;
    /**
     * The Address
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Address;
    /**
     * The Dpi
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $Dpi;
    /**
     * Constructor method for LabelReturnChannelSettings
     * @uses LabelReturnChannelSettings::setType()
     * @uses LabelReturnChannelSettings::setFormat()
     * @uses LabelReturnChannelSettings::setFullLabel()
     * @uses LabelReturnChannelSettings::setAddress()
     * @uses LabelReturnChannelSettings::setDpi()
     * @param string $type
     * @param string $format
     * @param \PPLSDK\StructType\FullLabel $fullLabel
     * @param string $address
     * @param int $dpi
     */
    public function __construct($type = null, $format = null, \PPLSDK\StructType\FullLabel $fullLabel = null, $address = null, $dpi = null)
    {
        $this
            ->setType($type)
            ->setFormat($format)
            ->setFullLabel($fullLabel)
            ->setAddress($address)
            ->setDpi($dpi);
    }
    /**
     * Get Type value
     * @return string|null
     */
    public function getType()
    {
        return $this->Type;
    }
    /**
     * Set Type value
     * @uses \PPLSDK\EnumType\ReturnChannel::valueIsValid()
     * @uses \PPLSDK\EnumType\ReturnChannel::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $type
     * @return \PPLSDK\StructType\LabelReturnChannelSettings
     */
    public function setType($type = null)
    {
        // validation for constraint: enumeration
        if (!\PPLSDK\EnumType\ReturnChannel::valueIsValid($type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \PPLSDK\EnumType\ReturnChannel', is_array($type) ? implode(', ', $type) : var_export($type, true), implode(', ', \PPLSDK\EnumType\ReturnChannel::getValidValues())), __LINE__);
        }
        $this->Type = $type;
        return $this;
    }
    /**
     * Get Format value
     * @return string|null
     */
    public function getFormat()
    {
        return $this->Format;
    }
    /**
     * Set Format value
     * @uses \PPLSDK\EnumType\LabelFormat::valueIsValid()
     * @uses \PPLSDK\EnumType\LabelFormat::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $format
     * @return \PPLSDK\StructType\LabelReturnChannelSettings
     */
    public function setFormat($format = null)
    {
        // validation for constraint: enumeration
        if (!\PPLSDK\EnumType\LabelFormat::valueIsValid($format)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \PPLSDK\EnumType\LabelFormat', is_array($format) ? implode(', ', $format) : var_export($format, true), implode(', ', \PPLSDK\EnumType\LabelFormat::getValidValues())), __LINE__);
        }
        $this->Format = $format;
        return $this;
    }
    /**
     * Get FullLabel value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\FullLabel|null
     */
    public function getFullLabel()
    {
        return isset($this->FullLabel) ? $this->FullLabel : null;
    }
    /**
     * Set FullLabel value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\FullLabel $fullLabel
     * @return \PPLSDK\StructType\LabelReturnChannelSettings
     */
    public function setFullLabel(\PPLSDK\StructType\FullLabel $fullLabel = null)
    {
        if (is_null($fullLabel) || (is_array($fullLabel) && empty($fullLabel))) {
            unset($this->FullLabel);
        } else {
            $this->FullLabel = $fullLabel;
        }
        return $this;
    }
    /**
     * Get Address value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddress()
    {
        return isset($this->Address) ? $this->Address : null;
    }
    /**
     * Set Address value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $address
     * @return \PPLSDK\StructType\LabelReturnChannelSettings
     */
    public function setAddress($address = null)
    {
        // validation for constraint: string
        if (!is_null($address) && !is_string($address)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($address, true), gettype($address)), __LINE__);
        }
        if (is_null($address) || (is_array($address) && empty($address))) {
            unset($this->Address);
        } else {
            $this->Address = $address;
        }
        return $this;
    }
    /**
     * Get Dpi value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getDpi()
    {
        return isset($this->Dpi) ? $this->Dpi : null;
    }
    /**
     * Set Dpi value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $dpi
     * @return \PPLSDK\StructType\LabelReturnChannelSettings
     */
    public function setDpi($dpi = null)
    {
        // validation for constraint: int
        if (!is_null($dpi) && !(is_int($dpi) || ctype_digit($dpi))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($dpi, true), gettype($dpi)), __LINE__);
        }
        if (is_null($dpi) || (is_array($dpi) && empty($dpi))) {
            unset($this->Dpi);
        } else {
            $this->Dpi = $dpi;
        }
        return $this;
    }
}
