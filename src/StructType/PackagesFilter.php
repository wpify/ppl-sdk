<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PackagesFilter StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:PackagesFilter
 * @subpackage Structs
 */
class PackagesFilter extends AbstractStructBase
{
    /**
     * The CustRefs
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfstring
     */
    public $CustRefs;
    /**
     * The DateFrom
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DateFrom;
    /**
     * The DateTo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DateTo;
    /**
     * The PackNumbers
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfstring
     */
    public $PackNumbers;
    /**
     * The PackageStates
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string[]
     */
    public $PackageStates;
    /**
     * The VariableSymbolsCOD
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfstring
     */
    public $VariableSymbolsCOD;
    /**
     * Constructor method for PackagesFilter
     * @uses PackagesFilter::setCustRefs()
     * @uses PackagesFilter::setDateFrom()
     * @uses PackagesFilter::setDateTo()
     * @uses PackagesFilter::setPackNumbers()
     * @uses PackagesFilter::setPackageStates()
     * @uses PackagesFilter::setVariableSymbolsCOD()
     * @param \PPLSDK\ArrayType\ArrayOfstring $custRefs
     * @param string $dateFrom
     * @param string $dateTo
     * @param \PPLSDK\ArrayType\ArrayOfstring $packNumbers
     * @param string[] $packageStates
     * @param \PPLSDK\ArrayType\ArrayOfstring $variableSymbolsCOD
     */
    public function __construct(\PPLSDK\ArrayType\ArrayOfstring $custRefs = null, $dateFrom = null, $dateTo = null, \PPLSDK\ArrayType\ArrayOfstring $packNumbers = null, array $packageStates = array(), \PPLSDK\ArrayType\ArrayOfstring $variableSymbolsCOD = null)
    {
        $this
            ->setCustRefs($custRefs)
            ->setDateFrom($dateFrom)
            ->setDateTo($dateTo)
            ->setPackNumbers($packNumbers)
            ->setPackageStates($packageStates)
            ->setVariableSymbolsCOD($variableSymbolsCOD);
    }
    /**
     * Get CustRefs value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\ArrayType\ArrayOfstring|null
     */
    public function getCustRefs()
    {
        return isset($this->CustRefs) ? $this->CustRefs : null;
    }
    /**
     * Set CustRefs value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\ArrayType\ArrayOfstring $custRefs
     * @return \PPLSDK\StructType\PackagesFilter
     */
    public function setCustRefs(\PPLSDK\ArrayType\ArrayOfstring $custRefs = null)
    {
        if (is_null($custRefs) || (is_array($custRefs) && empty($custRefs))) {
            unset($this->CustRefs);
        } else {
            $this->CustRefs = $custRefs;
        }
        return $this;
    }
    /**
     * Get DateFrom value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDateFrom()
    {
        return isset($this->DateFrom) ? $this->DateFrom : null;
    }
    /**
     * Set DateFrom value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $dateFrom
     * @return \PPLSDK\StructType\PackagesFilter
     */
    public function setDateFrom($dateFrom = null)
    {
        // validation for constraint: string
        if (!is_null($dateFrom) && !is_string($dateFrom)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateFrom, true), gettype($dateFrom)), __LINE__);
        }
        if (is_null($dateFrom) || (is_array($dateFrom) && empty($dateFrom))) {
            unset($this->DateFrom);
        } else {
            $this->DateFrom = $dateFrom;
        }
        return $this;
    }
    /**
     * Get DateTo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDateTo()
    {
        return isset($this->DateTo) ? $this->DateTo : null;
    }
    /**
     * Set DateTo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $dateTo
     * @return \PPLSDK\StructType\PackagesFilter
     */
    public function setDateTo($dateTo = null)
    {
        // validation for constraint: string
        if (!is_null($dateTo) && !is_string($dateTo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateTo, true), gettype($dateTo)), __LINE__);
        }
        if (is_null($dateTo) || (is_array($dateTo) && empty($dateTo))) {
            unset($this->DateTo);
        } else {
            $this->DateTo = $dateTo;
        }
        return $this;
    }
    /**
     * Get PackNumbers value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\ArrayType\ArrayOfstring|null
     */
    public function getPackNumbers()
    {
        return isset($this->PackNumbers) ? $this->PackNumbers : null;
    }
    /**
     * Set PackNumbers value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\ArrayType\ArrayOfstring $packNumbers
     * @return \PPLSDK\StructType\PackagesFilter
     */
    public function setPackNumbers(\PPLSDK\ArrayType\ArrayOfstring $packNumbers = null)
    {
        if (is_null($packNumbers) || (is_array($packNumbers) && empty($packNumbers))) {
            unset($this->PackNumbers);
        } else {
            $this->PackNumbers = $packNumbers;
        }
        return $this;
    }
    /**
     * Get PackageStates value
     * @return string[]|null
     */
    public function getPackageStates()
    {
        return $this->PackageStates;
    }
    /**
     * This method is responsible for validating the values passed to the setPackageStates method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPackageStates method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePackageStatesForArrayConstraintsFromSetPackageStates(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $packagesFilterPackageStatesItem) {
            // validation for constraint: enumeration
            if (!\PPLSDK\EnumType\PackageState::valueIsValid($packagesFilterPackageStatesItem)) {
                $invalidValues[] = is_object($packagesFilterPackageStatesItem) ? get_class($packagesFilterPackageStatesItem) : sprintf('%s(%s)', gettype($packagesFilterPackageStatesItem), var_export($packagesFilterPackageStatesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \PPLSDK\EnumType\PackageState', is_array($invalidValues) ? implode(', ', $invalidValues) : var_export($invalidValues, true), implode(', ', \PPLSDK\EnumType\PackageState::getValidValues()));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set PackageStates value
     * @uses \PPLSDK\EnumType\PackageState::valueIsValid()
     * @uses \PPLSDK\EnumType\PackageState::getValidValues()
     * @throws \InvalidArgumentException
     * @param string[] $packageStates
     * @return \PPLSDK\StructType\PackagesFilter
     */
    public function setPackageStates(array $packageStates = array())
    {
        // validation for constraint: list
        if ('' !== ($packageStatesArrayErrorMessage = self::validatePackageStatesForArrayConstraintsFromSetPackageStates($packageStates))) {
            throw new \InvalidArgumentException($packageStatesArrayErrorMessage, __LINE__);
        }
        $this->PackageStates = is_array($packageStates) ? implode(' ', $packageStates) : null;
        return $this;
    }
    /**
     * Get VariableSymbolsCOD value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\ArrayType\ArrayOfstring|null
     */
    public function getVariableSymbolsCOD()
    {
        return isset($this->VariableSymbolsCOD) ? $this->VariableSymbolsCOD : null;
    }
    /**
     * Set VariableSymbolsCOD value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\ArrayType\ArrayOfstring $variableSymbolsCOD
     * @return \PPLSDK\StructType\PackagesFilter
     */
    public function setVariableSymbolsCOD(\PPLSDK\ArrayType\ArrayOfstring $variableSymbolsCOD = null)
    {
        if (is_null($variableSymbolsCOD) || (is_array($variableSymbolsCOD) && empty($variableSymbolsCOD))) {
            unset($this->VariableSymbolsCOD);
        } else {
            $this->VariableSymbolsCOD = $variableSymbolsCOD;
        }
        return $this;
    }
}
