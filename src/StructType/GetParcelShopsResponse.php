<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetParcelShopsResponse StructType
 * @subpackage Structs
 */
class GetParcelShopsResponse extends AbstractStructBase
{
    /**
     * The GetParcelShopsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\GetParcelShopsResult
     */
    public $GetParcelShopsResult;
    /**
     * Constructor method for GetParcelShopsResponse
     * @uses GetParcelShopsResponse::setGetParcelShopsResult()
     * @param \PPLSDK\StructType\GetParcelShopsResult $getParcelShopsResult
     */
    public function __construct(\PPLSDK\StructType\GetParcelShopsResult $getParcelShopsResult = null)
    {
        $this
            ->setGetParcelShopsResult($getParcelShopsResult);
    }
    /**
     * Get GetParcelShopsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\GetParcelShopsResult|null
     */
    public function getGetParcelShopsResult()
    {
        return isset($this->GetParcelShopsResult) ? $this->GetParcelShopsResult : null;
    }
    /**
     * Set GetParcelShopsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\GetParcelShopsResult $getParcelShopsResult
     * @return \PPLSDK\StructType\GetParcelShopsResponse
     */
    public function setGetParcelShopsResult(\PPLSDK\StructType\GetParcelShopsResult $getParcelShopsResult = null)
    {
        if (is_null($getParcelShopsResult) || (is_array($getParcelShopsResult) && empty($getParcelShopsResult))) {
            unset($this->GetParcelShopsResult);
        } else {
            $this->GetParcelShopsResult = $getParcelShopsResult;
        }
        return $this;
    }
}
