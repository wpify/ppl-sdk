<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateOrdersResponse StructType
 * @subpackage Structs
 */
class CreateOrdersResponse extends AbstractStructBase
{
    /**
     * The CreateOrdersResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\CreateOrdersResult
     */
    public $CreateOrdersResult;
    /**
     * Constructor method for CreateOrdersResponse
     * @uses CreateOrdersResponse::setCreateOrdersResult()
     * @param \PPLSDK\StructType\CreateOrdersResult $createOrdersResult
     */
    public function __construct(\PPLSDK\StructType\CreateOrdersResult $createOrdersResult = null)
    {
        $this
            ->setCreateOrdersResult($createOrdersResult);
    }
    /**
     * Get CreateOrdersResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\CreateOrdersResult|null
     */
    public function getCreateOrdersResult()
    {
        return isset($this->CreateOrdersResult) ? $this->CreateOrdersResult : null;
    }
    /**
     * Set CreateOrdersResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\CreateOrdersResult $createOrdersResult
     * @return \PPLSDK\StructType\CreateOrdersResponse
     */
    public function setCreateOrdersResult(\PPLSDK\StructType\CreateOrdersResult $createOrdersResult = null)
    {
        if (is_null($createOrdersResult) || (is_array($createOrdersResult) && empty($createOrdersResult))) {
            unset($this->CreateOrdersResult);
        } else {
            $this->CreateOrdersResult = $createOrdersResult;
        }
        return $this;
    }
}
