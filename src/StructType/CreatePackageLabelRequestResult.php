<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreatePackageLabelRequestResult StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:CreatePackageLabelRequestResult
 * @subpackage Structs
 */
class CreatePackageLabelRequestResult extends MyApiResultBaseOfCreatePackageLabelRequestResultDatapewOaM6d
{
}
