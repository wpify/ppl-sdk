<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreatedType StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:CreatedType
 * @subpackage Structs
 */
class CreatedType extends AbstractStructBase
{
    /**
     * The CreatedByName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CreatedByName;
    /**
     * The CreatedDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $CreatedDate;
    /**
     * Constructor method for CreatedType
     * @uses CreatedType::setCreatedByName()
     * @uses CreatedType::setCreatedDate()
     * @param string $createdByName
     * @param string $createdDate
     */
    public function __construct($createdByName = null, $createdDate = null)
    {
        $this
            ->setCreatedByName($createdByName)
            ->setCreatedDate($createdDate);
    }
    /**
     * Get CreatedByName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCreatedByName()
    {
        return isset($this->CreatedByName) ? $this->CreatedByName : null;
    }
    /**
     * Set CreatedByName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $createdByName
     * @return \PPLSDK\StructType\CreatedType
     */
    public function setCreatedByName($createdByName = null)
    {
        // validation for constraint: string
        if (!is_null($createdByName) && !is_string($createdByName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($createdByName, true), gettype($createdByName)), __LINE__);
        }
        if (is_null($createdByName) || (is_array($createdByName) && empty($createdByName))) {
            unset($this->CreatedByName);
        } else {
            $this->CreatedByName = $createdByName;
        }
        return $this;
    }
    /**
     * Get CreatedDate value
     * @return string|null
     */
    public function getCreatedDate()
    {
        return $this->CreatedDate;
    }
    /**
     * Set CreatedDate value
     * @param string $createdDate
     * @return \PPLSDK\StructType\CreatedType
     */
    public function setCreatedDate($createdDate = null)
    {
        // validation for constraint: string
        if (!is_null($createdDate) && !is_string($createdDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($createdDate, true), gettype($createdDate)), __LINE__);
        }
        $this->CreatedDate = $createdDate;
        return $this;
    }
}
