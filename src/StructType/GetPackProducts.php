<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPackProducts StructType
 * @subpackage Structs
 */
class GetPackProducts extends AbstractStructBase
{
    /**
     * The Filter
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\PackProductFilter
     */
    public $Filter;
    /**
     * Constructor method for GetPackProducts
     * @uses GetPackProducts::setFilter()
     * @param \PPLSDK\StructType\PackProductFilter $filter
     */
    public function __construct(\PPLSDK\StructType\PackProductFilter $filter = null)
    {
        $this
            ->setFilter($filter);
    }
    /**
     * Get Filter value
     * @return \PPLSDK\StructType\PackProductFilter
     */
    public function getFilter()
    {
        return $this->Filter;
    }
    /**
     * Set Filter value
     * @param \PPLSDK\StructType\PackProductFilter $filter
     * @return \PPLSDK\StructType\GetPackProducts
     */
    public function setFilter(\PPLSDK\StructType\PackProductFilter $filter = null)
    {
        $this->Filter = $filter;
        return $this;
    }
}
