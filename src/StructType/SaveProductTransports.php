<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SaveProductTransports StructType
 * @subpackage Structs
 */
class SaveProductTransports extends AbstractStructBase
{
    /**
     * The Auth
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\AuthenticationData
     */
    public $Auth;
    /**
     * The CountryProducts
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiCountryProducts
     */
    public $CountryProducts;
    /**
     * Constructor method for SaveProductTransports
     * @uses SaveProductTransports::setAuth()
     * @uses SaveProductTransports::setCountryProducts()
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @param \PPLSDK\ArrayType\ArrayOfMyApiCountryProducts $countryProducts
     */
    public function __construct(\PPLSDK\StructType\AuthenticationData $auth = null, \PPLSDK\ArrayType\ArrayOfMyApiCountryProducts $countryProducts = null)
    {
        $this
            ->setAuth($auth)
            ->setCountryProducts($countryProducts);
    }
    /**
     * Get Auth value
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function getAuth()
    {
        return $this->Auth;
    }
    /**
     * Set Auth value
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @return \PPLSDK\StructType\SaveProductTransports
     */
    public function setAuth(\PPLSDK\StructType\AuthenticationData $auth = null)
    {
        $this->Auth = $auth;
        return $this;
    }
    /**
     * Get CountryProducts value
     * @return \PPLSDK\ArrayType\ArrayOfMyApiCountryProducts
     */
    public function getCountryProducts()
    {
        return $this->CountryProducts;
    }
    /**
     * Set CountryProducts value
     * @param \PPLSDK\ArrayType\ArrayOfMyApiCountryProducts $countryProducts
     * @return \PPLSDK\StructType\SaveProductTransports
     */
    public function setCountryProducts(\PPLSDK\ArrayType\ArrayOfMyApiCountryProducts $countryProducts = null)
    {
        $this->CountryProducts = $countryProducts;
        return $this;
    }
}
