<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreatePickupOrdersResponse StructType
 * @subpackage Structs
 */
class CreatePickupOrdersResponse extends AbstractStructBase
{
    /**
     * The CreatePickupOrdersResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\CreatePickupOrdersResult
     */
    public $CreatePickupOrdersResult;
    /**
     * Constructor method for CreatePickupOrdersResponse
     * @uses CreatePickupOrdersResponse::setCreatePickupOrdersResult()
     * @param \PPLSDK\StructType\CreatePickupOrdersResult $createPickupOrdersResult
     */
    public function __construct(\PPLSDK\StructType\CreatePickupOrdersResult $createPickupOrdersResult = null)
    {
        $this
            ->setCreatePickupOrdersResult($createPickupOrdersResult);
    }
    /**
     * Get CreatePickupOrdersResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\CreatePickupOrdersResult|null
     */
    public function getCreatePickupOrdersResult()
    {
        return isset($this->CreatePickupOrdersResult) ? $this->CreatePickupOrdersResult : null;
    }
    /**
     * Set CreatePickupOrdersResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\CreatePickupOrdersResult $createPickupOrdersResult
     * @return \PPLSDK\StructType\CreatePickupOrdersResponse
     */
    public function setCreatePickupOrdersResult(\PPLSDK\StructType\CreatePickupOrdersResult $createPickupOrdersResult = null)
    {
        if (is_null($createPickupOrdersResult) || (is_array($createPickupOrdersResult) && empty($createPickupOrdersResult))) {
            unset($this->CreatePickupOrdersResult);
        } else {
            $this->CreatePickupOrdersResult = $createPickupOrdersResult;
        }
        return $this;
    }
}
