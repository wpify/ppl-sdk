<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCodCurrencyResponse StructType
 * @subpackage Structs
 */
class GetCodCurrencyResponse extends AbstractStructBase
{
    /**
     * The GetCodCurrencyResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\GetCodCurrencyResult
     */
    public $GetCodCurrencyResult;
    /**
     * Constructor method for GetCodCurrencyResponse
     * @uses GetCodCurrencyResponse::setGetCodCurrencyResult()
     * @param \PPLSDK\StructType\GetCodCurrencyResult $getCodCurrencyResult
     */
    public function __construct(\PPLSDK\StructType\GetCodCurrencyResult $getCodCurrencyResult = null)
    {
        $this
            ->setGetCodCurrencyResult($getCodCurrencyResult);
    }
    /**
     * Get GetCodCurrencyResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\GetCodCurrencyResult|null
     */
    public function getGetCodCurrencyResult()
    {
        return isset($this->GetCodCurrencyResult) ? $this->GetCodCurrencyResult : null;
    }
    /**
     * Set GetCodCurrencyResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\GetCodCurrencyResult $getCodCurrencyResult
     * @return \PPLSDK\StructType\GetCodCurrencyResponse
     */
    public function setGetCodCurrencyResult(\PPLSDK\StructType\GetCodCurrencyResult $getCodCurrencyResult = null)
    {
        if (is_null($getCodCurrencyResult) || (is_array($getCodCurrencyResult) && empty($getCodCurrencyResult))) {
            unset($this->GetCodCurrencyResult);
        } else {
            $this->GetCodCurrencyResult = $getCodCurrencyResult;
        }
        return $this;
    }
}
