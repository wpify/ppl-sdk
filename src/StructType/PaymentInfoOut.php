<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PaymentInfoOut StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:PaymentInfoOut
 * @subpackage Structs
 */
class PaymentInfoOut extends AbstractStructBase
{
    /**
     * The BankAccount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $BankAccount;
    /**
     * The BankCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $BankCode;
    /**
     * The CanPayByCard
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $CanPayByCard;
    /**
     * The CodBankStatementDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CodBankStatementDate;
    /**
     * The CodCurrency
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CodCurrency;
    /**
     * The CodPaidDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CodPaidDate;
    /**
     * The CodPaymentAccDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CodPaymentAccDate;
    /**
     * The CodPrice
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $CodPrice;
    /**
     * The CodVarSym
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CodVarSym;
    /**
     * The IBAN
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $IBAN;
    /**
     * The InvDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $InvDate;
    /**
     * The InvNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $InvNumber;
    /**
     * The PaidByCard
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $PaidByCard;
    /**
     * The SpecSymbol
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SpecSymbol;
    /**
     * The Swift
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Swift;
    /**
     * Constructor method for PaymentInfoOut
     * @uses PaymentInfoOut::setBankAccount()
     * @uses PaymentInfoOut::setBankCode()
     * @uses PaymentInfoOut::setCanPayByCard()
     * @uses PaymentInfoOut::setCodBankStatementDate()
     * @uses PaymentInfoOut::setCodCurrency()
     * @uses PaymentInfoOut::setCodPaidDate()
     * @uses PaymentInfoOut::setCodPaymentAccDate()
     * @uses PaymentInfoOut::setCodPrice()
     * @uses PaymentInfoOut::setCodVarSym()
     * @uses PaymentInfoOut::setIBAN()
     * @uses PaymentInfoOut::setInvDate()
     * @uses PaymentInfoOut::setInvNumber()
     * @uses PaymentInfoOut::setPaidByCard()
     * @uses PaymentInfoOut::setSpecSymbol()
     * @uses PaymentInfoOut::setSwift()
     * @param string $bankAccount
     * @param string $bankCode
     * @param bool $canPayByCard
     * @param string $codBankStatementDate
     * @param string $codCurrency
     * @param string $codPaidDate
     * @param string $codPaymentAccDate
     * @param float $codPrice
     * @param string $codVarSym
     * @param string $iBAN
     * @param string $invDate
     * @param string $invNumber
     * @param bool $paidByCard
     * @param string $specSymbol
     * @param string $swift
     */
    public function __construct($bankAccount = null, $bankCode = null, $canPayByCard = null, $codBankStatementDate = null, $codCurrency = null, $codPaidDate = null, $codPaymentAccDate = null, $codPrice = null, $codVarSym = null, $iBAN = null, $invDate = null, $invNumber = null, $paidByCard = null, $specSymbol = null, $swift = null)
    {
        $this
            ->setBankAccount($bankAccount)
            ->setBankCode($bankCode)
            ->setCanPayByCard($canPayByCard)
            ->setCodBankStatementDate($codBankStatementDate)
            ->setCodCurrency($codCurrency)
            ->setCodPaidDate($codPaidDate)
            ->setCodPaymentAccDate($codPaymentAccDate)
            ->setCodPrice($codPrice)
            ->setCodVarSym($codVarSym)
            ->setIBAN($iBAN)
            ->setInvDate($invDate)
            ->setInvNumber($invNumber)
            ->setPaidByCard($paidByCard)
            ->setSpecSymbol($specSymbol)
            ->setSwift($swift);
    }
    /**
     * Get BankAccount value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBankAccount()
    {
        return isset($this->BankAccount) ? $this->BankAccount : null;
    }
    /**
     * Set BankAccount value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $bankAccount
     * @return \PPLSDK\StructType\PaymentInfoOut
     */
    public function setBankAccount($bankAccount = null)
    {
        // validation for constraint: string
        if (!is_null($bankAccount) && !is_string($bankAccount)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($bankAccount, true), gettype($bankAccount)), __LINE__);
        }
        if (is_null($bankAccount) || (is_array($bankAccount) && empty($bankAccount))) {
            unset($this->BankAccount);
        } else {
            $this->BankAccount = $bankAccount;
        }
        return $this;
    }
    /**
     * Get BankCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBankCode()
    {
        return isset($this->BankCode) ? $this->BankCode : null;
    }
    /**
     * Set BankCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $bankCode
     * @return \PPLSDK\StructType\PaymentInfoOut
     */
    public function setBankCode($bankCode = null)
    {
        // validation for constraint: string
        if (!is_null($bankCode) && !is_string($bankCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($bankCode, true), gettype($bankCode)), __LINE__);
        }
        if (is_null($bankCode) || (is_array($bankCode) && empty($bankCode))) {
            unset($this->BankCode);
        } else {
            $this->BankCode = $bankCode;
        }
        return $this;
    }
    /**
     * Get CanPayByCard value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getCanPayByCard()
    {
        return isset($this->CanPayByCard) ? $this->CanPayByCard : null;
    }
    /**
     * Set CanPayByCard value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $canPayByCard
     * @return \PPLSDK\StructType\PaymentInfoOut
     */
    public function setCanPayByCard($canPayByCard = null)
    {
        // validation for constraint: boolean
        if (!is_null($canPayByCard) && !is_bool($canPayByCard)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($canPayByCard, true), gettype($canPayByCard)), __LINE__);
        }
        if (is_null($canPayByCard) || (is_array($canPayByCard) && empty($canPayByCard))) {
            unset($this->CanPayByCard);
        } else {
            $this->CanPayByCard = $canPayByCard;
        }
        return $this;
    }
    /**
     * Get CodBankStatementDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCodBankStatementDate()
    {
        return isset($this->CodBankStatementDate) ? $this->CodBankStatementDate : null;
    }
    /**
     * Set CodBankStatementDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $codBankStatementDate
     * @return \PPLSDK\StructType\PaymentInfoOut
     */
    public function setCodBankStatementDate($codBankStatementDate = null)
    {
        // validation for constraint: string
        if (!is_null($codBankStatementDate) && !is_string($codBankStatementDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($codBankStatementDate, true), gettype($codBankStatementDate)), __LINE__);
        }
        if (is_null($codBankStatementDate) || (is_array($codBankStatementDate) && empty($codBankStatementDate))) {
            unset($this->CodBankStatementDate);
        } else {
            $this->CodBankStatementDate = $codBankStatementDate;
        }
        return $this;
    }
    /**
     * Get CodCurrency value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCodCurrency()
    {
        return isset($this->CodCurrency) ? $this->CodCurrency : null;
    }
    /**
     * Set CodCurrency value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $codCurrency
     * @return \PPLSDK\StructType\PaymentInfoOut
     */
    public function setCodCurrency($codCurrency = null)
    {
        // validation for constraint: string
        if (!is_null($codCurrency) && !is_string($codCurrency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($codCurrency, true), gettype($codCurrency)), __LINE__);
        }
        if (is_null($codCurrency) || (is_array($codCurrency) && empty($codCurrency))) {
            unset($this->CodCurrency);
        } else {
            $this->CodCurrency = $codCurrency;
        }
        return $this;
    }
    /**
     * Get CodPaidDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCodPaidDate()
    {
        return isset($this->CodPaidDate) ? $this->CodPaidDate : null;
    }
    /**
     * Set CodPaidDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $codPaidDate
     * @return \PPLSDK\StructType\PaymentInfoOut
     */
    public function setCodPaidDate($codPaidDate = null)
    {
        // validation for constraint: string
        if (!is_null($codPaidDate) && !is_string($codPaidDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($codPaidDate, true), gettype($codPaidDate)), __LINE__);
        }
        if (is_null($codPaidDate) || (is_array($codPaidDate) && empty($codPaidDate))) {
            unset($this->CodPaidDate);
        } else {
            $this->CodPaidDate = $codPaidDate;
        }
        return $this;
    }
    /**
     * Get CodPaymentAccDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCodPaymentAccDate()
    {
        return isset($this->CodPaymentAccDate) ? $this->CodPaymentAccDate : null;
    }
    /**
     * Set CodPaymentAccDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $codPaymentAccDate
     * @return \PPLSDK\StructType\PaymentInfoOut
     */
    public function setCodPaymentAccDate($codPaymentAccDate = null)
    {
        // validation for constraint: string
        if (!is_null($codPaymentAccDate) && !is_string($codPaymentAccDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($codPaymentAccDate, true), gettype($codPaymentAccDate)), __LINE__);
        }
        if (is_null($codPaymentAccDate) || (is_array($codPaymentAccDate) && empty($codPaymentAccDate))) {
            unset($this->CodPaymentAccDate);
        } else {
            $this->CodPaymentAccDate = $codPaymentAccDate;
        }
        return $this;
    }
    /**
     * Get CodPrice value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getCodPrice()
    {
        return isset($this->CodPrice) ? $this->CodPrice : null;
    }
    /**
     * Set CodPrice value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $codPrice
     * @return \PPLSDK\StructType\PaymentInfoOut
     */
    public function setCodPrice($codPrice = null)
    {
        // validation for constraint: float
        if (!is_null($codPrice) && !(is_float($codPrice) || is_numeric($codPrice))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($codPrice, true), gettype($codPrice)), __LINE__);
        }
        if (is_null($codPrice) || (is_array($codPrice) && empty($codPrice))) {
            unset($this->CodPrice);
        } else {
            $this->CodPrice = $codPrice;
        }
        return $this;
    }
    /**
     * Get CodVarSym value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCodVarSym()
    {
        return isset($this->CodVarSym) ? $this->CodVarSym : null;
    }
    /**
     * Set CodVarSym value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $codVarSym
     * @return \PPLSDK\StructType\PaymentInfoOut
     */
    public function setCodVarSym($codVarSym = null)
    {
        // validation for constraint: string
        if (!is_null($codVarSym) && !is_string($codVarSym)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($codVarSym, true), gettype($codVarSym)), __LINE__);
        }
        if (is_null($codVarSym) || (is_array($codVarSym) && empty($codVarSym))) {
            unset($this->CodVarSym);
        } else {
            $this->CodVarSym = $codVarSym;
        }
        return $this;
    }
    /**
     * Get IBAN value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getIBAN()
    {
        return isset($this->IBAN) ? $this->IBAN : null;
    }
    /**
     * Set IBAN value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $iBAN
     * @return \PPLSDK\StructType\PaymentInfoOut
     */
    public function setIBAN($iBAN = null)
    {
        // validation for constraint: string
        if (!is_null($iBAN) && !is_string($iBAN)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($iBAN, true), gettype($iBAN)), __LINE__);
        }
        if (is_null($iBAN) || (is_array($iBAN) && empty($iBAN))) {
            unset($this->IBAN);
        } else {
            $this->IBAN = $iBAN;
        }
        return $this;
    }
    /**
     * Get InvDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getInvDate()
    {
        return isset($this->InvDate) ? $this->InvDate : null;
    }
    /**
     * Set InvDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $invDate
     * @return \PPLSDK\StructType\PaymentInfoOut
     */
    public function setInvDate($invDate = null)
    {
        // validation for constraint: string
        if (!is_null($invDate) && !is_string($invDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($invDate, true), gettype($invDate)), __LINE__);
        }
        if (is_null($invDate) || (is_array($invDate) && empty($invDate))) {
            unset($this->InvDate);
        } else {
            $this->InvDate = $invDate;
        }
        return $this;
    }
    /**
     * Get InvNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getInvNumber()
    {
        return isset($this->InvNumber) ? $this->InvNumber : null;
    }
    /**
     * Set InvNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $invNumber
     * @return \PPLSDK\StructType\PaymentInfoOut
     */
    public function setInvNumber($invNumber = null)
    {
        // validation for constraint: string
        if (!is_null($invNumber) && !is_string($invNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($invNumber, true), gettype($invNumber)), __LINE__);
        }
        if (is_null($invNumber) || (is_array($invNumber) && empty($invNumber))) {
            unset($this->InvNumber);
        } else {
            $this->InvNumber = $invNumber;
        }
        return $this;
    }
    /**
     * Get PaidByCard value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getPaidByCard()
    {
        return isset($this->PaidByCard) ? $this->PaidByCard : null;
    }
    /**
     * Set PaidByCard value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $paidByCard
     * @return \PPLSDK\StructType\PaymentInfoOut
     */
    public function setPaidByCard($paidByCard = null)
    {
        // validation for constraint: boolean
        if (!is_null($paidByCard) && !is_bool($paidByCard)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($paidByCard, true), gettype($paidByCard)), __LINE__);
        }
        if (is_null($paidByCard) || (is_array($paidByCard) && empty($paidByCard))) {
            unset($this->PaidByCard);
        } else {
            $this->PaidByCard = $paidByCard;
        }
        return $this;
    }
    /**
     * Get SpecSymbol value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSpecSymbol()
    {
        return isset($this->SpecSymbol) ? $this->SpecSymbol : null;
    }
    /**
     * Set SpecSymbol value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $specSymbol
     * @return \PPLSDK\StructType\PaymentInfoOut
     */
    public function setSpecSymbol($specSymbol = null)
    {
        // validation for constraint: string
        if (!is_null($specSymbol) && !is_string($specSymbol)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($specSymbol, true), gettype($specSymbol)), __LINE__);
        }
        if (is_null($specSymbol) || (is_array($specSymbol) && empty($specSymbol))) {
            unset($this->SpecSymbol);
        } else {
            $this->SpecSymbol = $specSymbol;
        }
        return $this;
    }
    /**
     * Get Swift value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSwift()
    {
        return isset($this->Swift) ? $this->Swift : null;
    }
    /**
     * Set Swift value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $swift
     * @return \PPLSDK\StructType\PaymentInfoOut
     */
    public function setSwift($swift = null)
    {
        // validation for constraint: string
        if (!is_null($swift) && !is_string($swift)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($swift, true), gettype($swift)), __LINE__);
        }
        if (is_null($swift) || (is_array($swift) && empty($swift))) {
            unset($this->Swift);
        } else {
            $this->Swift = $swift;
        }
        return $this;
    }
}
