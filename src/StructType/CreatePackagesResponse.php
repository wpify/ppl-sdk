<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreatePackagesResponse StructType
 * @subpackage Structs
 */
class CreatePackagesResponse extends AbstractStructBase
{
    /**
     * The CreatePackagesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\CreatePackagesResult
     */
    public $CreatePackagesResult;
    /**
     * Constructor method for CreatePackagesResponse
     * @uses CreatePackagesResponse::setCreatePackagesResult()
     * @param \PPLSDK\StructType\CreatePackagesResult $createPackagesResult
     */
    public function __construct(\PPLSDK\StructType\CreatePackagesResult $createPackagesResult = null)
    {
        $this
            ->setCreatePackagesResult($createPackagesResult);
    }
    /**
     * Get CreatePackagesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\CreatePackagesResult|null
     */
    public function getCreatePackagesResult()
    {
        return isset($this->CreatePackagesResult) ? $this->CreatePackagesResult : null;
    }
    /**
     * Set CreatePackagesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \PPLSDK\StructType\CreatePackagesResult $createPackagesResult
     * @return \PPLSDK\StructType\CreatePackagesResponse
     */
    public function setCreatePackagesResult(\PPLSDK\StructType\CreatePackagesResult $createPackagesResult = null)
    {
        if (is_null($createPackagesResult) || (is_array($createPackagesResult) && empty($createPackagesResult))) {
            unset($this->CreatePackagesResult);
        } else {
            $this->CreatePackagesResult = $createPackagesResult;
        }
        return $this;
    }
}
