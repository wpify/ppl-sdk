<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PartnerResultOfPartnerDatapewOaM6d StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:PartnerResultOfPartnerDatapewOaM6d
 * @subpackage Structs
 */
class PartnerResultOfPartnerDatapewOaM6d extends MyApiResultBaseOfArrayOfPartnerItemResultpewOaM6d
{
}
