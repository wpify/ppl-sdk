<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCitiesRouting StructType
 * @subpackage Structs
 */
class GetCitiesRouting extends AbstractStructBase
{
    /**
     * The Auth
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\AuthenticationData
     */
    public $Auth;
    /**
     * The Filter
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\CitiesRoutingFilter
     */
    public $Filter;
    /**
     * Constructor method for GetCitiesRouting
     * @uses GetCitiesRouting::setAuth()
     * @uses GetCitiesRouting::setFilter()
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @param \PPLSDK\StructType\CitiesRoutingFilter $filter
     */
    public function __construct(\PPLSDK\StructType\AuthenticationData $auth = null, \PPLSDK\StructType\CitiesRoutingFilter $filter = null)
    {
        $this
            ->setAuth($auth)
            ->setFilter($filter);
    }
    /**
     * Get Auth value
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function getAuth()
    {
        return $this->Auth;
    }
    /**
     * Set Auth value
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @return \PPLSDK\StructType\GetCitiesRouting
     */
    public function setAuth(\PPLSDK\StructType\AuthenticationData $auth = null)
    {
        $this->Auth = $auth;
        return $this;
    }
    /**
     * Get Filter value
     * @return \PPLSDK\StructType\CitiesRoutingFilter
     */
    public function getFilter()
    {
        return $this->Filter;
    }
    /**
     * Set Filter value
     * @param \PPLSDK\StructType\CitiesRoutingFilter $filter
     * @return \PPLSDK\StructType\GetCitiesRouting
     */
    public function setFilter(\PPLSDK\StructType\CitiesRoutingFilter $filter = null)
    {
        $this->Filter = $filter;
        return $this;
    }
}
