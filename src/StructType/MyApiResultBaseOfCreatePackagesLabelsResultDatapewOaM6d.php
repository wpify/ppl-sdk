<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiResultBaseOfCreatePackagesLabelsResultDatapewOaM6d
 * StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiResultBaseOfCreatePackagesLabelsResultDatapewOaM6d
 * @subpackage Structs
 */
class MyApiResultBaseOfCreatePackagesLabelsResultDatapewOaM6d extends MyApiResultBaseVoid
{
    /**
     * The ResultData
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\StructType\CreatePackagesLabelsResultData
     */
    public $ResultData;
    /**
     * Constructor method for MyApiResultBaseOfCreatePackagesLabelsResultDatapewOaM6d
     * @uses MyApiResultBaseOfCreatePackagesLabelsResultDatapewOaM6d::setResultData()
     * @param \PPLSDK\StructType\CreatePackagesLabelsResultData $resultData
     */
    public function __construct(\PPLSDK\StructType\CreatePackagesLabelsResultData $resultData = null)
    {
        $this
            ->setResultData($resultData);
    }
    /**
     * Get ResultData value
     * @return \PPLSDK\StructType\CreatePackagesLabelsResultData|null
     */
    public function getResultData()
    {
        return $this->ResultData;
    }
    /**
     * Set ResultData value
     * @param \PPLSDK\StructType\CreatePackagesLabelsResultData $resultData
     * @return \PPLSDK\StructType\MyApiResultBaseOfCreatePackagesLabelsResultDatapewOaM6d
     */
    public function setResultData(\PPLSDK\StructType\CreatePackagesLabelsResultData $resultData = null)
    {
        $this->ResultData = $resultData;
        return $this;
    }
}
