<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiResultBaseVoid StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiResultBaseVoid
 * @subpackage Structs
 */
class MyApiResultBaseVoid extends AbstractStructBase
{
    /**
     * The AuthToken
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $AuthToken;
    /**
     * Constructor method for MyApiResultBaseVoid
     * @uses MyApiResultBaseVoid::setAuthToken()
     * @param string $authToken
     */
    public function __construct($authToken = null)
    {
        $this
            ->setAuthToken($authToken);
    }
    /**
     * Get AuthToken value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAuthToken()
    {
        return isset($this->AuthToken) ? $this->AuthToken : null;
    }
    /**
     * Set AuthToken value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $authToken
     * @return \PPLSDK\StructType\MyApiResultBaseVoid
     */
    public function setAuthToken($authToken = null)
    {
        // validation for constraint: string
        if (!is_null($authToken) && !is_string($authToken)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($authToken, true), gettype($authToken)), __LINE__);
        }
        if (is_null($authToken) || (is_array($authToken) && empty($authToken))) {
            unset($this->AuthToken);
        } else {
            $this->AuthToken = $authToken;
        }
        return $this;
    }
}
