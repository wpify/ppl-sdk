<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MyApiResultBaseOfArrayOfMyApiCountryProductspewOaM6d
 * StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:MyApiResultBaseOfArrayOfMyApiCountryProductspewOaM6d
 * @subpackage Structs
 */
class MyApiResultBaseOfArrayOfMyApiCountryProductspewOaM6d extends MyApiResultBaseVoid
{
    /**
     * The ResultData
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfMyApiCountryProducts
     */
    public $ResultData;
    /**
     * Constructor method for MyApiResultBaseOfArrayOfMyApiCountryProductspewOaM6d
     * @uses MyApiResultBaseOfArrayOfMyApiCountryProductspewOaM6d::setResultData()
     * @param \PPLSDK\ArrayType\ArrayOfMyApiCountryProducts $resultData
     */
    public function __construct(\PPLSDK\ArrayType\ArrayOfMyApiCountryProducts $resultData = null)
    {
        $this
            ->setResultData($resultData);
    }
    /**
     * Get ResultData value
     * @return \PPLSDK\ArrayType\ArrayOfMyApiCountryProducts|null
     */
    public function getResultData()
    {
        return $this->ResultData;
    }
    /**
     * Set ResultData value
     * @param \PPLSDK\ArrayType\ArrayOfMyApiCountryProducts $resultData
     * @return \PPLSDK\StructType\MyApiResultBaseOfArrayOfMyApiCountryProductspewOaM6d
     */
    public function setResultData(\PPLSDK\ArrayType\ArrayOfMyApiCountryProducts $resultData = null)
    {
        $this->ResultData = $resultData;
        return $this;
    }
}
