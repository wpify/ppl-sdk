<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PartnerSaveStatuses StructType
 * @subpackage Structs
 */
class PartnerSaveStatuses extends AbstractStructBase
{
    /**
     * The Auth
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\AuthenticationData
     */
    public $Auth;
    /**
     * The Request
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\ArrayType\ArrayOfStatusData
     */
    public $Request;
    /**
     * Constructor method for PartnerSaveStatuses
     * @uses PartnerSaveStatuses::setAuth()
     * @uses PartnerSaveStatuses::setRequest()
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @param \PPLSDK\ArrayType\ArrayOfStatusData $request
     */
    public function __construct(\PPLSDK\StructType\AuthenticationData $auth = null, \PPLSDK\ArrayType\ArrayOfStatusData $request = null)
    {
        $this
            ->setAuth($auth)
            ->setRequest($request);
    }
    /**
     * Get Auth value
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function getAuth()
    {
        return $this->Auth;
    }
    /**
     * Set Auth value
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @return \PPLSDK\StructType\PartnerSaveStatuses
     */
    public function setAuth(\PPLSDK\StructType\AuthenticationData $auth = null)
    {
        $this->Auth = $auth;
        return $this;
    }
    /**
     * Get Request value
     * @return \PPLSDK\ArrayType\ArrayOfStatusData
     */
    public function getRequest()
    {
        return $this->Request;
    }
    /**
     * Set Request value
     * @param \PPLSDK\ArrayType\ArrayOfStatusData $request
     * @return \PPLSDK\StructType\PartnerSaveStatuses
     */
    public function setRequest(\PPLSDK\ArrayType\ArrayOfStatusData $request = null)
    {
        $this->Request = $request;
        return $this;
    }
}
