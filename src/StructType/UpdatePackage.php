<?php

namespace PPLSDK\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UpdatePackage StructType
 * @subpackage Structs
 */
class UpdatePackage extends AbstractStructBase
{
    /**
     * The Auth
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\AuthenticationData
     */
    public $Auth;
    /**
     * The Update
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPackageUpdate
     */
    public $Update;
    /**
     * Constructor method for UpdatePackage
     * @uses UpdatePackage::setAuth()
     * @uses UpdatePackage::setUpdate()
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @param \PPLSDK\StructType\MyApiPackageUpdate $update
     */
    public function __construct(\PPLSDK\StructType\AuthenticationData $auth = null, \PPLSDK\StructType\MyApiPackageUpdate $update = null)
    {
        $this
            ->setAuth($auth)
            ->setUpdate($update);
    }
    /**
     * Get Auth value
     * @return \PPLSDK\StructType\AuthenticationData
     */
    public function getAuth()
    {
        return $this->Auth;
    }
    /**
     * Set Auth value
     * @param \PPLSDK\StructType\AuthenticationData $auth
     * @return \PPLSDK\StructType\UpdatePackage
     */
    public function setAuth(\PPLSDK\StructType\AuthenticationData $auth = null)
    {
        $this->Auth = $auth;
        return $this;
    }
    /**
     * Get Update value
     * @return \PPLSDK\StructType\MyApiPackageUpdate
     */
    public function getUpdate()
    {
        return $this->Update;
    }
    /**
     * Set Update value
     * @param \PPLSDK\StructType\MyApiPackageUpdate $update
     * @return \PPLSDK\StructType\UpdatePackage
     */
    public function setUpdate(\PPLSDK\StructType\MyApiPackageUpdate $update = null)
    {
        $this->Update = $update;
        return $this;
    }
}
