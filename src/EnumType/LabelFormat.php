<?php

namespace PPLSDK\EnumType;

use \WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for LabelFormat EnumType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:LabelFormat
 * @subpackage Enumerations
 */
class LabelFormat extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Jpeg'
     * @return string 'Jpeg'
     */
    const VALUE_JPEG = 'Jpeg';
    /**
     * Constant for value 'JP'
     * @return string 'JP'
     */
    const VALUE_JP = 'JP';
    /**
     * Constant for value 'Pdf'
     * @return string 'Pdf'
     */
    const VALUE_PDF = 'Pdf';
    /**
     * Constant for value 'PD'
     * @return string 'PD'
     */
    const VALUE_PD = 'PD';
    /**
     * Constant for value 'Zpl'
     * @return string 'Zpl'
     */
    const VALUE_ZPL = 'Zpl';
    /**
     * Constant for value 'ZP'
     * @return string 'ZP'
     */
    const VALUE_ZP = 'ZP';
    /**
     * Constant for value 'Epl'
     * @return string 'Epl'
     */
    const VALUE_EPL = 'Epl';
    /**
     * Constant for value 'EP'
     * @return string 'EP'
     */
    const VALUE_EP = 'EP';
    /**
     * Return allowed values
     * @uses self::VALUE_JPEG
     * @uses self::VALUE_JP
     * @uses self::VALUE_PDF
     * @uses self::VALUE_PD
     * @uses self::VALUE_ZPL
     * @uses self::VALUE_ZP
     * @uses self::VALUE_EPL
     * @uses self::VALUE_EP
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_JPEG,
            self::VALUE_JP,
            self::VALUE_PDF,
            self::VALUE_PD,
            self::VALUE_ZPL,
            self::VALUE_ZP,
            self::VALUE_EPL,
            self::VALUE_EP,
        );
    }
}
