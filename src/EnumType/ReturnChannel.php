<?php

namespace PPLSDK\EnumType;

use \WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for ReturnChannel EnumType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ReturnChannel
 * @subpackage Enumerations
 */
class ReturnChannel extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Email'
     * @return string 'Email'
     */
    const VALUE_EMAIL = 'Email';
    /**
     * Constant for value 'Ftp'
     * @return string 'Ftp'
     */
    const VALUE_FTP = 'Ftp';
    /**
     * Constant for value 'DirectResponse'
     * @return string 'DirectResponse'
     */
    const VALUE_DIRECT_RESPONSE = 'DirectResponse';
    /**
     * Constant for value 'EM'
     * @return string 'EM'
     */
    const VALUE_EM = 'EM';
    /**
     * Constant for value 'FT'
     * @return string 'FT'
     */
    const VALUE_FT = 'FT';
    /**
     * Constant for value 'XM'
     * @return string 'XM'
     */
    const VALUE_XM = 'XM';
    /**
     * Return allowed values
     * @uses self::VALUE_EMAIL
     * @uses self::VALUE_FTP
     * @uses self::VALUE_DIRECT_RESPONSE
     * @uses self::VALUE_EM
     * @uses self::VALUE_FT
     * @uses self::VALUE_XM
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_EMAIL,
            self::VALUE_FTP,
            self::VALUE_DIRECT_RESPONSE,
            self::VALUE_EM,
            self::VALUE_FT,
            self::VALUE_XM,
        );
    }
}
