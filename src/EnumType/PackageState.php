<?php

namespace PPLSDK\EnumType;

use \WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for PackageState EnumType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:PackageState
 * @subpackage Enumerations
 */
class PackageState extends AbstractStructEnumBase
{
    /**
     * Constant for value 'None'
     * @return string 'None'
     */
    const VALUE_NONE = 'None';
    /**
     * Constant for value 'PickedUpFromSender'
     * @return string 'PickedUpFromSender'
     */
    const VALUE_PICKED_UP_FROM_SENDER = 'PickedUpFromSender';
    /**
     * Constant for value 'DeliveredToParcelShop'
     * @return string 'DeliveredToParcelShop'
     */
    const VALUE_DELIVERED_TO_PARCEL_SHOP = 'DeliveredToParcelShop';
    /**
     * Constant for value 'OutForDelivery'
     * @return string 'OutForDelivery'
     */
    const VALUE_OUT_FOR_DELIVERY = 'OutForDelivery';
    /**
     * Constant for value 'Delivered'
     * @return string 'Delivered'
     */
    const VALUE_DELIVERED = 'Delivered';
    /**
     * Constant for value 'NotDelivered'
     * @return string 'NotDelivered'
     */
    const VALUE_NOT_DELIVERED = 'NotDelivered';
    /**
     * Constant for value 'CodPaidDate'
     * @return string 'CodPaidDate'
     */
    const VALUE_COD_PAID_DATE = 'CodPaidDate';
    /**
     * Constant for value 'BackToSender'
     * @return string 'BackToSender'
     */
    const VALUE_BACK_TO_SENDER = 'BackToSender';
    /**
     * Constant for value 'Rejected'
     * @return string 'Rejected'
     */
    const VALUE_REJECTED = 'Rejected';
    /**
     * Constant for value 'DataShipment'
     * @return string 'DataShipment'
     */
    const VALUE_DATA_SHIPMENT = 'DataShipment';
    /**
     * Return allowed values
     * @uses self::VALUE_NONE
     * @uses self::VALUE_PICKED_UP_FROM_SENDER
     * @uses self::VALUE_DELIVERED_TO_PARCEL_SHOP
     * @uses self::VALUE_OUT_FOR_DELIVERY
     * @uses self::VALUE_DELIVERED
     * @uses self::VALUE_NOT_DELIVERED
     * @uses self::VALUE_COD_PAID_DATE
     * @uses self::VALUE_BACK_TO_SENDER
     * @uses self::VALUE_REJECTED
     * @uses self::VALUE_DATA_SHIPMENT
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_NONE,
            self::VALUE_PICKED_UP_FROM_SENDER,
            self::VALUE_DELIVERED_TO_PARCEL_SHOP,
            self::VALUE_OUT_FOR_DELIVERY,
            self::VALUE_DELIVERED,
            self::VALUE_NOT_DELIVERED,
            self::VALUE_COD_PAID_DATE,
            self::VALUE_BACK_TO_SENDER,
            self::VALUE_REJECTED,
            self::VALUE_DATA_SHIPMENT,
        );
    }
}
