<?php

namespace PPLSDK\EnumType;

use \WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for PartnerType EnumType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:PartnerType
 * @subpackage Enumerations
 */
class PartnerType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'ParcelShop'
     * @return string 'ParcelShop'
     */
    const VALUE_PARCEL_SHOP = 'ParcelShop';
    /**
     * Constant for value 'ZabkaPoint'
     * @return string 'ZabkaPoint'
     */
    const VALUE_ZABKA_POINT = 'ZabkaPoint';
    /**
     * Return allowed values
     * @uses self::VALUE_PARCEL_SHOP
     * @uses self::VALUE_ZABKA_POINT
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_PARCEL_SHOP,
            self::VALUE_ZABKA_POINT,
        );
    }
}
