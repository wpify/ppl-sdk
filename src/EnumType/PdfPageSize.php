<?php

namespace PPLSDK\EnumType;

use \WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for PdfPageSize EnumType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:PdfPageSize
 * @subpackage Enumerations
 */
class PdfPageSize extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Default'
     * @return string 'Default'
     */
    const VALUE_DEFAULT = 'Default';
    /**
     * Constant for value 'A4'
     * @return string 'A4'
     */
    const VALUE_A_4 = 'A4';
    /**
     * Return allowed values
     * @uses self::VALUE_DEFAULT
     * @uses self::VALUE_A_4
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_DEFAULT,
            self::VALUE_A_4,
        );
    }
}
