<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiPickupOrderIn ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiPickupOrderIn
 * @subpackage Arrays
 */
class ArrayOfMyApiPickupOrderIn extends AbstractStructArrayBase
{
    /**
     * The MyApiPickupOrderIn
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPickupOrderIn[]
     */
    public $MyApiPickupOrderIn;
    /**
     * Constructor method for ArrayOfMyApiPickupOrderIn
     * @uses ArrayOfMyApiPickupOrderIn::setMyApiPickupOrderIn()
     * @param \PPLSDK\StructType\MyApiPickupOrderIn[] $myApiPickupOrderIn
     */
    public function __construct(array $myApiPickupOrderIn = array())
    {
        $this
            ->setMyApiPickupOrderIn($myApiPickupOrderIn);
    }
    /**
     * Get MyApiPickupOrderIn value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiPickupOrderIn[]|null
     */
    public function getMyApiPickupOrderIn()
    {
        return isset($this->MyApiPickupOrderIn) ? $this->MyApiPickupOrderIn : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiPickupOrderIn method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiPickupOrderIn method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiPickupOrderInForArrayConstraintsFromSetMyApiPickupOrderIn(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiPickupOrderInMyApiPickupOrderInItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiPickupOrderInMyApiPickupOrderInItem instanceof \PPLSDK\StructType\MyApiPickupOrderIn) {
                $invalidValues[] = is_object($arrayOfMyApiPickupOrderInMyApiPickupOrderInItem) ? get_class($arrayOfMyApiPickupOrderInMyApiPickupOrderInItem) : sprintf('%s(%s)', gettype($arrayOfMyApiPickupOrderInMyApiPickupOrderInItem), var_export($arrayOfMyApiPickupOrderInMyApiPickupOrderInItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiPickupOrderIn property can only contain items of type \PPLSDK\StructType\MyApiPickupOrderIn, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiPickupOrderIn value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPickupOrderIn[] $myApiPickupOrderIn
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPickupOrderIn
     */
    public function setMyApiPickupOrderIn(array $myApiPickupOrderIn = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiPickupOrderInArrayErrorMessage = self::validateMyApiPickupOrderInForArrayConstraintsFromSetMyApiPickupOrderIn($myApiPickupOrderIn))) {
            throw new \InvalidArgumentException($myApiPickupOrderInArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiPickupOrderIn) || (is_array($myApiPickupOrderIn) && empty($myApiPickupOrderIn))) {
            unset($this->MyApiPickupOrderIn);
        } else {
            $this->MyApiPickupOrderIn = $myApiPickupOrderIn;
        }
        return $this;
    }
    /**
     * Add item to MyApiPickupOrderIn value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPickupOrderIn $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPickupOrderIn
     */
    public function addToMyApiPickupOrderIn(\PPLSDK\StructType\MyApiPickupOrderIn $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiPickupOrderIn) {
            throw new \InvalidArgumentException(sprintf('The MyApiPickupOrderIn property can only contain items of type \PPLSDK\StructType\MyApiPickupOrderIn, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiPickupOrderIn[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiPickupOrderIn|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiPickupOrderIn|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiPickupOrderIn|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiPickupOrderIn|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiPickupOrderIn|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiPickupOrderIn
     */
    public function getAttributeName()
    {
        return 'MyApiPickupOrderIn';
    }
}
