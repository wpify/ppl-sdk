<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiCountryProducts ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiCountryProducts
 * @subpackage Arrays
 */
class ArrayOfMyApiCountryProducts extends AbstractStructArrayBase
{
    /**
     * The MyApiCountryProducts
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiCountryProducts[]
     */
    public $MyApiCountryProducts;
    /**
     * Constructor method for ArrayOfMyApiCountryProducts
     * @uses ArrayOfMyApiCountryProducts::setMyApiCountryProducts()
     * @param \PPLSDK\StructType\MyApiCountryProducts[] $myApiCountryProducts
     */
    public function __construct(array $myApiCountryProducts = array())
    {
        $this
            ->setMyApiCountryProducts($myApiCountryProducts);
    }
    /**
     * Get MyApiCountryProducts value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiCountryProducts[]|null
     */
    public function getMyApiCountryProducts()
    {
        return isset($this->MyApiCountryProducts) ? $this->MyApiCountryProducts : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiCountryProducts method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiCountryProducts method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiCountryProductsForArrayConstraintsFromSetMyApiCountryProducts(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiCountryProductsMyApiCountryProductsItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiCountryProductsMyApiCountryProductsItem instanceof \PPLSDK\StructType\MyApiCountryProducts) {
                $invalidValues[] = is_object($arrayOfMyApiCountryProductsMyApiCountryProductsItem) ? get_class($arrayOfMyApiCountryProductsMyApiCountryProductsItem) : sprintf('%s(%s)', gettype($arrayOfMyApiCountryProductsMyApiCountryProductsItem), var_export($arrayOfMyApiCountryProductsMyApiCountryProductsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiCountryProducts property can only contain items of type \PPLSDK\StructType\MyApiCountryProducts, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiCountryProducts value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiCountryProducts[] $myApiCountryProducts
     * @return \PPLSDK\ArrayType\ArrayOfMyApiCountryProducts
     */
    public function setMyApiCountryProducts(array $myApiCountryProducts = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiCountryProductsArrayErrorMessage = self::validateMyApiCountryProductsForArrayConstraintsFromSetMyApiCountryProducts($myApiCountryProducts))) {
            throw new \InvalidArgumentException($myApiCountryProductsArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiCountryProducts) || (is_array($myApiCountryProducts) && empty($myApiCountryProducts))) {
            unset($this->MyApiCountryProducts);
        } else {
            $this->MyApiCountryProducts = $myApiCountryProducts;
        }
        return $this;
    }
    /**
     * Add item to MyApiCountryProducts value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiCountryProducts $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiCountryProducts
     */
    public function addToMyApiCountryProducts(\PPLSDK\StructType\MyApiCountryProducts $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiCountryProducts) {
            throw new \InvalidArgumentException(sprintf('The MyApiCountryProducts property can only contain items of type \PPLSDK\StructType\MyApiCountryProducts, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiCountryProducts[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiCountryProducts|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiCountryProducts|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiCountryProducts|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiCountryProducts|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiCountryProducts|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiCountryProducts
     */
    public function getAttributeName()
    {
        return 'MyApiCountryProducts';
    }
}
