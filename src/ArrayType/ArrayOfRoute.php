<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfRoute ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfRoute
 * @subpackage Arrays
 */
class ArrayOfRoute extends AbstractStructArrayBase
{
    /**
     * The Route
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\Route[]
     */
    public $Route;
    /**
     * Constructor method for ArrayOfRoute
     * @uses ArrayOfRoute::setRoute()
     * @param \PPLSDK\StructType\Route[] $route
     */
    public function __construct(array $route = array())
    {
        $this
            ->setRoute($route);
    }
    /**
     * Get Route value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\Route[]|null
     */
    public function getRoute()
    {
        return isset($this->Route) ? $this->Route : null;
    }
    /**
     * This method is responsible for validating the values passed to the setRoute method
     * This method is willingly generated in order to preserve the one-line inline validation within the setRoute method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateRouteForArrayConstraintsFromSetRoute(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfRouteRouteItem) {
            // validation for constraint: itemType
            if (!$arrayOfRouteRouteItem instanceof \PPLSDK\StructType\Route) {
                $invalidValues[] = is_object($arrayOfRouteRouteItem) ? get_class($arrayOfRouteRouteItem) : sprintf('%s(%s)', gettype($arrayOfRouteRouteItem), var_export($arrayOfRouteRouteItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The Route property can only contain items of type \PPLSDK\StructType\Route, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set Route value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\Route[] $route
     * @return \PPLSDK\ArrayType\ArrayOfRoute
     */
    public function setRoute(array $route = array())
    {
        // validation for constraint: array
        if ('' !== ($routeArrayErrorMessage = self::validateRouteForArrayConstraintsFromSetRoute($route))) {
            throw new \InvalidArgumentException($routeArrayErrorMessage, __LINE__);
        }
        if (is_null($route) || (is_array($route) && empty($route))) {
            unset($this->Route);
        } else {
            $this->Route = $route;
        }
        return $this;
    }
    /**
     * Add item to Route value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\Route $item
     * @return \PPLSDK\ArrayType\ArrayOfRoute
     */
    public function addToRoute(\PPLSDK\StructType\Route $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\Route) {
            throw new \InvalidArgumentException(sprintf('The Route property can only contain items of type \PPLSDK\StructType\Route, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->Route[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\Route|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\Route|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\Route|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\Route|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\Route|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string Route
     */
    public function getAttributeName()
    {
        return 'Route';
    }
}
