<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiPackageOutService ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiPackageOutService
 * @subpackage Arrays
 */
class ArrayOfMyApiPackageOutService extends AbstractStructArrayBase
{
    /**
     * The MyApiPackageOutService
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPackageOutService[]
     */
    public $MyApiPackageOutService;
    /**
     * Constructor method for ArrayOfMyApiPackageOutService
     * @uses ArrayOfMyApiPackageOutService::setMyApiPackageOutService()
     * @param \PPLSDK\StructType\MyApiPackageOutService[] $myApiPackageOutService
     */
    public function __construct(array $myApiPackageOutService = array())
    {
        $this
            ->setMyApiPackageOutService($myApiPackageOutService);
    }
    /**
     * Get MyApiPackageOutService value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiPackageOutService[]|null
     */
    public function getMyApiPackageOutService()
    {
        return isset($this->MyApiPackageOutService) ? $this->MyApiPackageOutService : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiPackageOutService method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiPackageOutService method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiPackageOutServiceForArrayConstraintsFromSetMyApiPackageOutService(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiPackageOutServiceMyApiPackageOutServiceItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiPackageOutServiceMyApiPackageOutServiceItem instanceof \PPLSDK\StructType\MyApiPackageOutService) {
                $invalidValues[] = is_object($arrayOfMyApiPackageOutServiceMyApiPackageOutServiceItem) ? get_class($arrayOfMyApiPackageOutServiceMyApiPackageOutServiceItem) : sprintf('%s(%s)', gettype($arrayOfMyApiPackageOutServiceMyApiPackageOutServiceItem), var_export($arrayOfMyApiPackageOutServiceMyApiPackageOutServiceItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiPackageOutService property can only contain items of type \PPLSDK\StructType\MyApiPackageOutService, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiPackageOutService value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageOutService[] $myApiPackageOutService
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageOutService
     */
    public function setMyApiPackageOutService(array $myApiPackageOutService = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiPackageOutServiceArrayErrorMessage = self::validateMyApiPackageOutServiceForArrayConstraintsFromSetMyApiPackageOutService($myApiPackageOutService))) {
            throw new \InvalidArgumentException($myApiPackageOutServiceArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiPackageOutService) || (is_array($myApiPackageOutService) && empty($myApiPackageOutService))) {
            unset($this->MyApiPackageOutService);
        } else {
            $this->MyApiPackageOutService = $myApiPackageOutService;
        }
        return $this;
    }
    /**
     * Add item to MyApiPackageOutService value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageOutService $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageOutService
     */
    public function addToMyApiPackageOutService(\PPLSDK\StructType\MyApiPackageOutService $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiPackageOutService) {
            throw new \InvalidArgumentException(sprintf('The MyApiPackageOutService property can only contain items of type \PPLSDK\StructType\MyApiPackageOutService, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiPackageOutService[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiPackageOutService|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiPackageOutService|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiPackageOutService|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiPackageOutService|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiPackageOutService|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiPackageOutService
     */
    public function getAttributeName()
    {
        return 'MyApiPackageOutService';
    }
}
