<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiPackageOut ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiPackageOut
 * @subpackage Arrays
 */
class ArrayOfMyApiPackageOut extends AbstractStructArrayBase
{
    /**
     * The MyApiPackageOut
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPackageOut[]
     */
    public $MyApiPackageOut;
    /**
     * Constructor method for ArrayOfMyApiPackageOut
     * @uses ArrayOfMyApiPackageOut::setMyApiPackageOut()
     * @param \PPLSDK\StructType\MyApiPackageOut[] $myApiPackageOut
     */
    public function __construct(array $myApiPackageOut = array())
    {
        $this
            ->setMyApiPackageOut($myApiPackageOut);
    }
    /**
     * Get MyApiPackageOut value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiPackageOut[]|null
     */
    public function getMyApiPackageOut()
    {
        return isset($this->MyApiPackageOut) ? $this->MyApiPackageOut : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiPackageOut method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiPackageOut method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiPackageOutForArrayConstraintsFromSetMyApiPackageOut(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiPackageOutMyApiPackageOutItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiPackageOutMyApiPackageOutItem instanceof \PPLSDK\StructType\MyApiPackageOut) {
                $invalidValues[] = is_object($arrayOfMyApiPackageOutMyApiPackageOutItem) ? get_class($arrayOfMyApiPackageOutMyApiPackageOutItem) : sprintf('%s(%s)', gettype($arrayOfMyApiPackageOutMyApiPackageOutItem), var_export($arrayOfMyApiPackageOutMyApiPackageOutItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiPackageOut property can only contain items of type \PPLSDK\StructType\MyApiPackageOut, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiPackageOut value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageOut[] $myApiPackageOut
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageOut
     */
    public function setMyApiPackageOut(array $myApiPackageOut = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiPackageOutArrayErrorMessage = self::validateMyApiPackageOutForArrayConstraintsFromSetMyApiPackageOut($myApiPackageOut))) {
            throw new \InvalidArgumentException($myApiPackageOutArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiPackageOut) || (is_array($myApiPackageOut) && empty($myApiPackageOut))) {
            unset($this->MyApiPackageOut);
        } else {
            $this->MyApiPackageOut = $myApiPackageOut;
        }
        return $this;
    }
    /**
     * Add item to MyApiPackageOut value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageOut $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageOut
     */
    public function addToMyApiPackageOut(\PPLSDK\StructType\MyApiPackageOut $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiPackageOut) {
            throw new \InvalidArgumentException(sprintf('The MyApiPackageOut property can only contain items of type \PPLSDK\StructType\MyApiPackageOut, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiPackageOut[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiPackageOut|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiPackageOut|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiPackageOut|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiPackageOut|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiPackageOut|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiPackageOut
     */
    public function getAttributeName()
    {
        return 'MyApiPackageOut';
    }
}
