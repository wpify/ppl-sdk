<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiCityRouteSvc ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiCityRouteSvc
 * @subpackage Arrays
 */
class ArrayOfMyApiCityRouteSvc extends AbstractStructArrayBase
{
    /**
     * The MyApiCityRouteSvc
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiCityRouteSvc[]
     */
    public $MyApiCityRouteSvc;
    /**
     * Constructor method for ArrayOfMyApiCityRouteSvc
     * @uses ArrayOfMyApiCityRouteSvc::setMyApiCityRouteSvc()
     * @param \PPLSDK\StructType\MyApiCityRouteSvc[] $myApiCityRouteSvc
     */
    public function __construct(array $myApiCityRouteSvc = array())
    {
        $this
            ->setMyApiCityRouteSvc($myApiCityRouteSvc);
    }
    /**
     * Get MyApiCityRouteSvc value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiCityRouteSvc[]|null
     */
    public function getMyApiCityRouteSvc()
    {
        return isset($this->MyApiCityRouteSvc) ? $this->MyApiCityRouteSvc : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiCityRouteSvc method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiCityRouteSvc method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiCityRouteSvcForArrayConstraintsFromSetMyApiCityRouteSvc(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiCityRouteSvcMyApiCityRouteSvcItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiCityRouteSvcMyApiCityRouteSvcItem instanceof \PPLSDK\StructType\MyApiCityRouteSvc) {
                $invalidValues[] = is_object($arrayOfMyApiCityRouteSvcMyApiCityRouteSvcItem) ? get_class($arrayOfMyApiCityRouteSvcMyApiCityRouteSvcItem) : sprintf('%s(%s)', gettype($arrayOfMyApiCityRouteSvcMyApiCityRouteSvcItem), var_export($arrayOfMyApiCityRouteSvcMyApiCityRouteSvcItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiCityRouteSvc property can only contain items of type \PPLSDK\StructType\MyApiCityRouteSvc, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiCityRouteSvc value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiCityRouteSvc[] $myApiCityRouteSvc
     * @return \PPLSDK\ArrayType\ArrayOfMyApiCityRouteSvc
     */
    public function setMyApiCityRouteSvc(array $myApiCityRouteSvc = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiCityRouteSvcArrayErrorMessage = self::validateMyApiCityRouteSvcForArrayConstraintsFromSetMyApiCityRouteSvc($myApiCityRouteSvc))) {
            throw new \InvalidArgumentException($myApiCityRouteSvcArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiCityRouteSvc) || (is_array($myApiCityRouteSvc) && empty($myApiCityRouteSvc))) {
            unset($this->MyApiCityRouteSvc);
        } else {
            $this->MyApiCityRouteSvc = $myApiCityRouteSvc;
        }
        return $this;
    }
    /**
     * Add item to MyApiCityRouteSvc value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiCityRouteSvc $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiCityRouteSvc
     */
    public function addToMyApiCityRouteSvc(\PPLSDK\StructType\MyApiCityRouteSvc $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiCityRouteSvc) {
            throw new \InvalidArgumentException(sprintf('The MyApiCityRouteSvc property can only contain items of type \PPLSDK\StructType\MyApiCityRouteSvc, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiCityRouteSvc[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiCityRouteSvc|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiCityRouteSvc|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiCityRouteSvc|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiCityRouteSvc|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiCityRouteSvc|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiCityRouteSvc
     */
    public function getAttributeName()
    {
        return 'MyApiCityRouteSvc';
    }
}
