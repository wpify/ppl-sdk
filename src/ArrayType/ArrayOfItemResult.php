<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfItemResult ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfItemResult
 * @subpackage Arrays
 */
class ArrayOfItemResult extends AbstractStructArrayBase
{
    /**
     * The ItemResult
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\ItemResult[]
     */
    public $ItemResult;
    /**
     * Constructor method for ArrayOfItemResult
     * @uses ArrayOfItemResult::setItemResult()
     * @param \PPLSDK\StructType\ItemResult[] $itemResult
     */
    public function __construct(array $itemResult = array())
    {
        $this
            ->setItemResult($itemResult);
    }
    /**
     * Get ItemResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\ItemResult[]|null
     */
    public function getItemResult()
    {
        return isset($this->ItemResult) ? $this->ItemResult : null;
    }
    /**
     * This method is responsible for validating the values passed to the setItemResult method
     * This method is willingly generated in order to preserve the one-line inline validation within the setItemResult method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateItemResultForArrayConstraintsFromSetItemResult(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfItemResultItemResultItem) {
            // validation for constraint: itemType
            if (!$arrayOfItemResultItemResultItem instanceof \PPLSDK\StructType\ItemResult) {
                $invalidValues[] = is_object($arrayOfItemResultItemResultItem) ? get_class($arrayOfItemResultItemResultItem) : sprintf('%s(%s)', gettype($arrayOfItemResultItemResultItem), var_export($arrayOfItemResultItemResultItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The ItemResult property can only contain items of type \PPLSDK\StructType\ItemResult, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set ItemResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\ItemResult[] $itemResult
     * @return \PPLSDK\ArrayType\ArrayOfItemResult
     */
    public function setItemResult(array $itemResult = array())
    {
        // validation for constraint: array
        if ('' !== ($itemResultArrayErrorMessage = self::validateItemResultForArrayConstraintsFromSetItemResult($itemResult))) {
            throw new \InvalidArgumentException($itemResultArrayErrorMessage, __LINE__);
        }
        if (is_null($itemResult) || (is_array($itemResult) && empty($itemResult))) {
            unset($this->ItemResult);
        } else {
            $this->ItemResult = $itemResult;
        }
        return $this;
    }
    /**
     * Add item to ItemResult value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\ItemResult $item
     * @return \PPLSDK\ArrayType\ArrayOfItemResult
     */
    public function addToItemResult(\PPLSDK\StructType\ItemResult $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\ItemResult) {
            throw new \InvalidArgumentException(sprintf('The ItemResult property can only contain items of type \PPLSDK\StructType\ItemResult, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->ItemResult[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\ItemResult|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\ItemResult|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\ItemResult|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\ItemResult|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\ItemResult|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string ItemResult
     */
    public function getAttributeName()
    {
        return 'ItemResult';
    }
}
