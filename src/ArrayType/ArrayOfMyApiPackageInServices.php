<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiPackageInServices ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiPackageInServices
 * @subpackage Arrays
 */
class ArrayOfMyApiPackageInServices extends AbstractStructArrayBase
{
    /**
     * The MyApiPackageInServices
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPackageInServices[]
     */
    public $MyApiPackageInServices;
    /**
     * Constructor method for ArrayOfMyApiPackageInServices
     * @uses ArrayOfMyApiPackageInServices::setMyApiPackageInServices()
     * @param \PPLSDK\StructType\MyApiPackageInServices[] $myApiPackageInServices
     */
    public function __construct(array $myApiPackageInServices = array())
    {
        $this
            ->setMyApiPackageInServices($myApiPackageInServices);
    }
    /**
     * Get MyApiPackageInServices value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiPackageInServices[]|null
     */
    public function getMyApiPackageInServices()
    {
        return isset($this->MyApiPackageInServices) ? $this->MyApiPackageInServices : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiPackageInServices method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiPackageInServices method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiPackageInServicesForArrayConstraintsFromSetMyApiPackageInServices(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiPackageInServicesMyApiPackageInServicesItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiPackageInServicesMyApiPackageInServicesItem instanceof \PPLSDK\StructType\MyApiPackageInServices) {
                $invalidValues[] = is_object($arrayOfMyApiPackageInServicesMyApiPackageInServicesItem) ? get_class($arrayOfMyApiPackageInServicesMyApiPackageInServicesItem) : sprintf('%s(%s)', gettype($arrayOfMyApiPackageInServicesMyApiPackageInServicesItem), var_export($arrayOfMyApiPackageInServicesMyApiPackageInServicesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiPackageInServices property can only contain items of type \PPLSDK\StructType\MyApiPackageInServices, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiPackageInServices value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageInServices[] $myApiPackageInServices
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageInServices
     */
    public function setMyApiPackageInServices(array $myApiPackageInServices = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiPackageInServicesArrayErrorMessage = self::validateMyApiPackageInServicesForArrayConstraintsFromSetMyApiPackageInServices($myApiPackageInServices))) {
            throw new \InvalidArgumentException($myApiPackageInServicesArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiPackageInServices) || (is_array($myApiPackageInServices) && empty($myApiPackageInServices))) {
            unset($this->MyApiPackageInServices);
        } else {
            $this->MyApiPackageInServices = $myApiPackageInServices;
        }
        return $this;
    }
    /**
     * Add item to MyApiPackageInServices value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageInServices $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageInServices
     */
    public function addToMyApiPackageInServices(\PPLSDK\StructType\MyApiPackageInServices $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiPackageInServices) {
            throw new \InvalidArgumentException(sprintf('The MyApiPackageInServices property can only contain items of type \PPLSDK\StructType\MyApiPackageInServices, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiPackageInServices[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiPackageInServices|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiPackageInServices|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiPackageInServices|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiPackageInServices|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiPackageInServices|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiPackageInServices
     */
    public function getAttributeName()
    {
        return 'MyApiPackageInServices';
    }
}
