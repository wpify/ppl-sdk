<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiPackageOutColli ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiPackageOutColli
 * @subpackage Arrays
 */
class ArrayOfMyApiPackageOutColli extends AbstractStructArrayBase
{
    /**
     * The MyApiPackageOutColli
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPackageOutColli[]
     */
    public $MyApiPackageOutColli;
    /**
     * Constructor method for ArrayOfMyApiPackageOutColli
     * @uses ArrayOfMyApiPackageOutColli::setMyApiPackageOutColli()
     * @param \PPLSDK\StructType\MyApiPackageOutColli[] $myApiPackageOutColli
     */
    public function __construct(array $myApiPackageOutColli = array())
    {
        $this
            ->setMyApiPackageOutColli($myApiPackageOutColli);
    }
    /**
     * Get MyApiPackageOutColli value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiPackageOutColli[]|null
     */
    public function getMyApiPackageOutColli()
    {
        return isset($this->MyApiPackageOutColli) ? $this->MyApiPackageOutColli : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiPackageOutColli method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiPackageOutColli method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiPackageOutColliForArrayConstraintsFromSetMyApiPackageOutColli(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiPackageOutColliMyApiPackageOutColliItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiPackageOutColliMyApiPackageOutColliItem instanceof \PPLSDK\StructType\MyApiPackageOutColli) {
                $invalidValues[] = is_object($arrayOfMyApiPackageOutColliMyApiPackageOutColliItem) ? get_class($arrayOfMyApiPackageOutColliMyApiPackageOutColliItem) : sprintf('%s(%s)', gettype($arrayOfMyApiPackageOutColliMyApiPackageOutColliItem), var_export($arrayOfMyApiPackageOutColliMyApiPackageOutColliItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiPackageOutColli property can only contain items of type \PPLSDK\StructType\MyApiPackageOutColli, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiPackageOutColli value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageOutColli[] $myApiPackageOutColli
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageOutColli
     */
    public function setMyApiPackageOutColli(array $myApiPackageOutColli = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiPackageOutColliArrayErrorMessage = self::validateMyApiPackageOutColliForArrayConstraintsFromSetMyApiPackageOutColli($myApiPackageOutColli))) {
            throw new \InvalidArgumentException($myApiPackageOutColliArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiPackageOutColli) || (is_array($myApiPackageOutColli) && empty($myApiPackageOutColli))) {
            unset($this->MyApiPackageOutColli);
        } else {
            $this->MyApiPackageOutColli = $myApiPackageOutColli;
        }
        return $this;
    }
    /**
     * Add item to MyApiPackageOutColli value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageOutColli $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageOutColli
     */
    public function addToMyApiPackageOutColli(\PPLSDK\StructType\MyApiPackageOutColli $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiPackageOutColli) {
            throw new \InvalidArgumentException(sprintf('The MyApiPackageOutColli property can only contain items of type \PPLSDK\StructType\MyApiPackageOutColli, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiPackageOutColli[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiPackageOutColli|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiPackageOutColli|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiPackageOutColli|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiPackageOutColli|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiPackageOutColli|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiPackageOutColli
     */
    public function getAttributeName()
    {
        return 'MyApiPackageOutColli';
    }
}
