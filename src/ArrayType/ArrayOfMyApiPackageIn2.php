<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiPackageIn2 ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiPackageIn2
 * @subpackage Arrays
 */
class ArrayOfMyApiPackageIn2 extends AbstractStructArrayBase
{
    /**
     * The MyApiPackageIn2
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPackageIn2[]
     */
    public $MyApiPackageIn2;
    /**
     * Constructor method for ArrayOfMyApiPackageIn2
     * @uses ArrayOfMyApiPackageIn2::setMyApiPackageIn2()
     * @param \PPLSDK\StructType\MyApiPackageIn2[] $myApiPackageIn2
     */
    public function __construct(array $myApiPackageIn2 = array())
    {
        $this
            ->setMyApiPackageIn2($myApiPackageIn2);
    }
    /**
     * Get MyApiPackageIn2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiPackageIn2[]|null
     */
    public function getMyApiPackageIn2()
    {
        return isset($this->MyApiPackageIn2) ? $this->MyApiPackageIn2 : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiPackageIn2 method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiPackageIn2 method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiPackageIn2ForArrayConstraintsFromSetMyApiPackageIn2(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiPackageIn2MyApiPackageIn2Item) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiPackageIn2MyApiPackageIn2Item instanceof \PPLSDK\StructType\MyApiPackageIn2) {
                $invalidValues[] = is_object($arrayOfMyApiPackageIn2MyApiPackageIn2Item) ? get_class($arrayOfMyApiPackageIn2MyApiPackageIn2Item) : sprintf('%s(%s)', gettype($arrayOfMyApiPackageIn2MyApiPackageIn2Item), var_export($arrayOfMyApiPackageIn2MyApiPackageIn2Item, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiPackageIn2 property can only contain items of type \PPLSDK\StructType\MyApiPackageIn2, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiPackageIn2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageIn2[] $myApiPackageIn2
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageIn2
     */
    public function setMyApiPackageIn2(array $myApiPackageIn2 = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiPackageIn2ArrayErrorMessage = self::validateMyApiPackageIn2ForArrayConstraintsFromSetMyApiPackageIn2($myApiPackageIn2))) {
            throw new \InvalidArgumentException($myApiPackageIn2ArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiPackageIn2) || (is_array($myApiPackageIn2) && empty($myApiPackageIn2))) {
            unset($this->MyApiPackageIn2);
        } else {
            $this->MyApiPackageIn2 = $myApiPackageIn2;
        }
        return $this;
    }
    /**
     * Add item to MyApiPackageIn2 value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageIn2 $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageIn2
     */
    public function addToMyApiPackageIn2(\PPLSDK\StructType\MyApiPackageIn2 $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiPackageIn2) {
            throw new \InvalidArgumentException(sprintf('The MyApiPackageIn2 property can only contain items of type \PPLSDK\StructType\MyApiPackageIn2, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiPackageIn2[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiPackageIn2|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiPackageIn2|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiPackageIn2|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiPackageIn2|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiPackageIn2|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiPackageIn2
     */
    public function getAttributeName()
    {
        return 'MyApiPackageIn2';
    }
}
