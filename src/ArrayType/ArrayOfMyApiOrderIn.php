<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiOrderIn ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiOrderIn
 * @subpackage Arrays
 */
class ArrayOfMyApiOrderIn extends AbstractStructArrayBase
{
    /**
     * The MyApiOrderIn
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiOrderIn[]
     */
    public $MyApiOrderIn;
    /**
     * Constructor method for ArrayOfMyApiOrderIn
     * @uses ArrayOfMyApiOrderIn::setMyApiOrderIn()
     * @param \PPLSDK\StructType\MyApiOrderIn[] $myApiOrderIn
     */
    public function __construct(array $myApiOrderIn = array())
    {
        $this
            ->setMyApiOrderIn($myApiOrderIn);
    }
    /**
     * Get MyApiOrderIn value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiOrderIn[]|null
     */
    public function getMyApiOrderIn()
    {
        return isset($this->MyApiOrderIn) ? $this->MyApiOrderIn : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiOrderIn method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiOrderIn method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiOrderInForArrayConstraintsFromSetMyApiOrderIn(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiOrderInMyApiOrderInItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiOrderInMyApiOrderInItem instanceof \PPLSDK\StructType\MyApiOrderIn) {
                $invalidValues[] = is_object($arrayOfMyApiOrderInMyApiOrderInItem) ? get_class($arrayOfMyApiOrderInMyApiOrderInItem) : sprintf('%s(%s)', gettype($arrayOfMyApiOrderInMyApiOrderInItem), var_export($arrayOfMyApiOrderInMyApiOrderInItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiOrderIn property can only contain items of type \PPLSDK\StructType\MyApiOrderIn, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiOrderIn value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiOrderIn[] $myApiOrderIn
     * @return \PPLSDK\ArrayType\ArrayOfMyApiOrderIn
     */
    public function setMyApiOrderIn(array $myApiOrderIn = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiOrderInArrayErrorMessage = self::validateMyApiOrderInForArrayConstraintsFromSetMyApiOrderIn($myApiOrderIn))) {
            throw new \InvalidArgumentException($myApiOrderInArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiOrderIn) || (is_array($myApiOrderIn) && empty($myApiOrderIn))) {
            unset($this->MyApiOrderIn);
        } else {
            $this->MyApiOrderIn = $myApiOrderIn;
        }
        return $this;
    }
    /**
     * Add item to MyApiOrderIn value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiOrderIn $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiOrderIn
     */
    public function addToMyApiOrderIn(\PPLSDK\StructType\MyApiOrderIn $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiOrderIn) {
            throw new \InvalidArgumentException(sprintf('The MyApiOrderIn property can only contain items of type \PPLSDK\StructType\MyApiOrderIn, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiOrderIn[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiOrderIn|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiOrderIn|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiOrderIn|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiOrderIn|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiOrderIn|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiOrderIn
     */
    public function getAttributeName()
    {
        return 'MyApiOrderIn';
    }
}
