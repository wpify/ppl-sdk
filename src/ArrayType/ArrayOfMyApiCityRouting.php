<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiCityRouting ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiCityRouting
 * @subpackage Arrays
 */
class ArrayOfMyApiCityRouting extends AbstractStructArrayBase
{
    /**
     * The MyApiCityRouting
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiCityRouting[]
     */
    public $MyApiCityRouting;
    /**
     * Constructor method for ArrayOfMyApiCityRouting
     * @uses ArrayOfMyApiCityRouting::setMyApiCityRouting()
     * @param \PPLSDK\StructType\MyApiCityRouting[] $myApiCityRouting
     */
    public function __construct(array $myApiCityRouting = array())
    {
        $this
            ->setMyApiCityRouting($myApiCityRouting);
    }
    /**
     * Get MyApiCityRouting value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiCityRouting[]|null
     */
    public function getMyApiCityRouting()
    {
        return isset($this->MyApiCityRouting) ? $this->MyApiCityRouting : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiCityRouting method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiCityRouting method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiCityRoutingForArrayConstraintsFromSetMyApiCityRouting(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiCityRoutingMyApiCityRoutingItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiCityRoutingMyApiCityRoutingItem instanceof \PPLSDK\StructType\MyApiCityRouting) {
                $invalidValues[] = is_object($arrayOfMyApiCityRoutingMyApiCityRoutingItem) ? get_class($arrayOfMyApiCityRoutingMyApiCityRoutingItem) : sprintf('%s(%s)', gettype($arrayOfMyApiCityRoutingMyApiCityRoutingItem), var_export($arrayOfMyApiCityRoutingMyApiCityRoutingItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiCityRouting property can only contain items of type \PPLSDK\StructType\MyApiCityRouting, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiCityRouting value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiCityRouting[] $myApiCityRouting
     * @return \PPLSDK\ArrayType\ArrayOfMyApiCityRouting
     */
    public function setMyApiCityRouting(array $myApiCityRouting = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiCityRoutingArrayErrorMessage = self::validateMyApiCityRoutingForArrayConstraintsFromSetMyApiCityRouting($myApiCityRouting))) {
            throw new \InvalidArgumentException($myApiCityRoutingArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiCityRouting) || (is_array($myApiCityRouting) && empty($myApiCityRouting))) {
            unset($this->MyApiCityRouting);
        } else {
            $this->MyApiCityRouting = $myApiCityRouting;
        }
        return $this;
    }
    /**
     * Add item to MyApiCityRouting value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiCityRouting $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiCityRouting
     */
    public function addToMyApiCityRouting(\PPLSDK\StructType\MyApiCityRouting $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiCityRouting) {
            throw new \InvalidArgumentException(sprintf('The MyApiCityRouting property can only contain items of type \PPLSDK\StructType\MyApiCityRouting, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiCityRouting[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiCityRouting|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiCityRouting|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiCityRouting|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiCityRouting|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiCityRouting|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiCityRouting
     */
    public function getAttributeName()
    {
        return 'MyApiCityRouting';
    }
}
