<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfAddressForService ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfAddressForService
 * @subpackage Arrays
 */
class ArrayOfAddressForService extends AbstractStructArrayBase
{
    /**
     * The AddressForService
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\AddressForService[]
     */
    public $AddressForService;
    /**
     * Constructor method for ArrayOfAddressForService
     * @uses ArrayOfAddressForService::setAddressForService()
     * @param \PPLSDK\StructType\AddressForService[] $addressForService
     */
    public function __construct(array $addressForService = array())
    {
        $this
            ->setAddressForService($addressForService);
    }
    /**
     * Get AddressForService value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\AddressForService[]|null
     */
    public function getAddressForService()
    {
        return isset($this->AddressForService) ? $this->AddressForService : null;
    }
    /**
     * This method is responsible for validating the values passed to the setAddressForService method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAddressForService method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAddressForServiceForArrayConstraintsFromSetAddressForService(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfAddressForServiceAddressForServiceItem) {
            // validation for constraint: itemType
            if (!$arrayOfAddressForServiceAddressForServiceItem instanceof \PPLSDK\StructType\AddressForService) {
                $invalidValues[] = is_object($arrayOfAddressForServiceAddressForServiceItem) ? get_class($arrayOfAddressForServiceAddressForServiceItem) : sprintf('%s(%s)', gettype($arrayOfAddressForServiceAddressForServiceItem), var_export($arrayOfAddressForServiceAddressForServiceItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The AddressForService property can only contain items of type \PPLSDK\StructType\AddressForService, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set AddressForService value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\AddressForService[] $addressForService
     * @return \PPLSDK\ArrayType\ArrayOfAddressForService
     */
    public function setAddressForService(array $addressForService = array())
    {
        // validation for constraint: array
        if ('' !== ($addressForServiceArrayErrorMessage = self::validateAddressForServiceForArrayConstraintsFromSetAddressForService($addressForService))) {
            throw new \InvalidArgumentException($addressForServiceArrayErrorMessage, __LINE__);
        }
        if (is_null($addressForService) || (is_array($addressForService) && empty($addressForService))) {
            unset($this->AddressForService);
        } else {
            $this->AddressForService = $addressForService;
        }
        return $this;
    }
    /**
     * Add item to AddressForService value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\AddressForService $item
     * @return \PPLSDK\ArrayType\ArrayOfAddressForService
     */
    public function addToAddressForService(\PPLSDK\StructType\AddressForService $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\AddressForService) {
            throw new \InvalidArgumentException(sprintf('The AddressForService property can only contain items of type \PPLSDK\StructType\AddressForService, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->AddressForService[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\AddressForService|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\AddressForService|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\AddressForService|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\AddressForService|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\AddressForService|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string AddressForService
     */
    public function getAttributeName()
    {
        return 'AddressForService';
    }
}
