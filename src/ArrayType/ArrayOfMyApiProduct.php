<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiProduct ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiProduct
 * @subpackage Arrays
 */
class ArrayOfMyApiProduct extends AbstractStructArrayBase
{
    /**
     * The MyApiProduct
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiProduct[]
     */
    public $MyApiProduct;
    /**
     * Constructor method for ArrayOfMyApiProduct
     * @uses ArrayOfMyApiProduct::setMyApiProduct()
     * @param \PPLSDK\StructType\MyApiProduct[] $myApiProduct
     */
    public function __construct(array $myApiProduct = array())
    {
        $this
            ->setMyApiProduct($myApiProduct);
    }
    /**
     * Get MyApiProduct value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiProduct[]|null
     */
    public function getMyApiProduct()
    {
        return isset($this->MyApiProduct) ? $this->MyApiProduct : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiProduct method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiProduct method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiProductForArrayConstraintsFromSetMyApiProduct(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiProductMyApiProductItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiProductMyApiProductItem instanceof \PPLSDK\StructType\MyApiProduct) {
                $invalidValues[] = is_object($arrayOfMyApiProductMyApiProductItem) ? get_class($arrayOfMyApiProductMyApiProductItem) : sprintf('%s(%s)', gettype($arrayOfMyApiProductMyApiProductItem), var_export($arrayOfMyApiProductMyApiProductItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiProduct property can only contain items of type \PPLSDK\StructType\MyApiProduct, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiProduct value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiProduct[] $myApiProduct
     * @return \PPLSDK\ArrayType\ArrayOfMyApiProduct
     */
    public function setMyApiProduct(array $myApiProduct = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiProductArrayErrorMessage = self::validateMyApiProductForArrayConstraintsFromSetMyApiProduct($myApiProduct))) {
            throw new \InvalidArgumentException($myApiProductArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiProduct) || (is_array($myApiProduct) && empty($myApiProduct))) {
            unset($this->MyApiProduct);
        } else {
            $this->MyApiProduct = $myApiProduct;
        }
        return $this;
    }
    /**
     * Add item to MyApiProduct value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiProduct $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiProduct
     */
    public function addToMyApiProduct(\PPLSDK\StructType\MyApiProduct $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiProduct) {
            throw new \InvalidArgumentException(sprintf('The MyApiProduct property can only contain items of type \PPLSDK\StructType\MyApiProduct, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiProduct[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiProduct|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiProduct|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiProduct|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiProduct|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiProduct|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiProduct
     */
    public function getAttributeName()
    {
        return 'MyApiProduct';
    }
}
