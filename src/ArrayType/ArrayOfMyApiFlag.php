<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiFlag ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiFlag
 * @subpackage Arrays
 */
class ArrayOfMyApiFlag extends AbstractStructArrayBase
{
    /**
     * The MyApiFlag
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiFlag[]
     */
    public $MyApiFlag;
    /**
     * Constructor method for ArrayOfMyApiFlag
     * @uses ArrayOfMyApiFlag::setMyApiFlag()
     * @param \PPLSDK\StructType\MyApiFlag[] $myApiFlag
     */
    public function __construct(array $myApiFlag = array())
    {
        $this
            ->setMyApiFlag($myApiFlag);
    }
    /**
     * Get MyApiFlag value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiFlag[]|null
     */
    public function getMyApiFlag()
    {
        return isset($this->MyApiFlag) ? $this->MyApiFlag : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiFlag method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiFlag method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiFlagForArrayConstraintsFromSetMyApiFlag(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiFlagMyApiFlagItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiFlagMyApiFlagItem instanceof \PPLSDK\StructType\MyApiFlag) {
                $invalidValues[] = is_object($arrayOfMyApiFlagMyApiFlagItem) ? get_class($arrayOfMyApiFlagMyApiFlagItem) : sprintf('%s(%s)', gettype($arrayOfMyApiFlagMyApiFlagItem), var_export($arrayOfMyApiFlagMyApiFlagItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiFlag property can only contain items of type \PPLSDK\StructType\MyApiFlag, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiFlag value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiFlag[] $myApiFlag
     * @return \PPLSDK\ArrayType\ArrayOfMyApiFlag
     */
    public function setMyApiFlag(array $myApiFlag = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiFlagArrayErrorMessage = self::validateMyApiFlagForArrayConstraintsFromSetMyApiFlag($myApiFlag))) {
            throw new \InvalidArgumentException($myApiFlagArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiFlag) || (is_array($myApiFlag) && empty($myApiFlag))) {
            unset($this->MyApiFlag);
        } else {
            $this->MyApiFlag = $myApiFlag;
        }
        return $this;
    }
    /**
     * Add item to MyApiFlag value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiFlag $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiFlag
     */
    public function addToMyApiFlag(\PPLSDK\StructType\MyApiFlag $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiFlag) {
            throw new \InvalidArgumentException(sprintf('The MyApiFlag property can only contain items of type \PPLSDK\StructType\MyApiFlag, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiFlag[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiFlag|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiFlag|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiFlag|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiFlag|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiFlag|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiFlag
     */
    public function getAttributeName()
    {
        return 'MyApiFlag';
    }
}
