<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfCreatePackageLabelResultItemData ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfCreatePackageLabelResultItemData
 * @subpackage Arrays
 */
class ArrayOfCreatePackageLabelResultItemData extends AbstractStructArrayBase
{
    /**
     * The CreatePackageLabelResultItemData
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\CreatePackageLabelResultItemData[]
     */
    public $CreatePackageLabelResultItemData;
    /**
     * Constructor method for ArrayOfCreatePackageLabelResultItemData
     * @uses ArrayOfCreatePackageLabelResultItemData::setCreatePackageLabelResultItemData()
     * @param \PPLSDK\StructType\CreatePackageLabelResultItemData[] $createPackageLabelResultItemData
     */
    public function __construct(array $createPackageLabelResultItemData = array())
    {
        $this
            ->setCreatePackageLabelResultItemData($createPackageLabelResultItemData);
    }
    /**
     * Get CreatePackageLabelResultItemData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\CreatePackageLabelResultItemData[]|null
     */
    public function getCreatePackageLabelResultItemData()
    {
        return isset($this->CreatePackageLabelResultItemData) ? $this->CreatePackageLabelResultItemData : null;
    }
    /**
     * This method is responsible for validating the values passed to the setCreatePackageLabelResultItemData method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCreatePackageLabelResultItemData method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCreatePackageLabelResultItemDataForArrayConstraintsFromSetCreatePackageLabelResultItemData(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfCreatePackageLabelResultItemDataCreatePackageLabelResultItemDataItem) {
            // validation for constraint: itemType
            if (!$arrayOfCreatePackageLabelResultItemDataCreatePackageLabelResultItemDataItem instanceof \PPLSDK\StructType\CreatePackageLabelResultItemData) {
                $invalidValues[] = is_object($arrayOfCreatePackageLabelResultItemDataCreatePackageLabelResultItemDataItem) ? get_class($arrayOfCreatePackageLabelResultItemDataCreatePackageLabelResultItemDataItem) : sprintf('%s(%s)', gettype($arrayOfCreatePackageLabelResultItemDataCreatePackageLabelResultItemDataItem), var_export($arrayOfCreatePackageLabelResultItemDataCreatePackageLabelResultItemDataItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The CreatePackageLabelResultItemData property can only contain items of type \PPLSDK\StructType\CreatePackageLabelResultItemData, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set CreatePackageLabelResultItemData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\CreatePackageLabelResultItemData[] $createPackageLabelResultItemData
     * @return \PPLSDK\ArrayType\ArrayOfCreatePackageLabelResultItemData
     */
    public function setCreatePackageLabelResultItemData(array $createPackageLabelResultItemData = array())
    {
        // validation for constraint: array
        if ('' !== ($createPackageLabelResultItemDataArrayErrorMessage = self::validateCreatePackageLabelResultItemDataForArrayConstraintsFromSetCreatePackageLabelResultItemData($createPackageLabelResultItemData))) {
            throw new \InvalidArgumentException($createPackageLabelResultItemDataArrayErrorMessage, __LINE__);
        }
        if (is_null($createPackageLabelResultItemData) || (is_array($createPackageLabelResultItemData) && empty($createPackageLabelResultItemData))) {
            unset($this->CreatePackageLabelResultItemData);
        } else {
            $this->CreatePackageLabelResultItemData = $createPackageLabelResultItemData;
        }
        return $this;
    }
    /**
     * Add item to CreatePackageLabelResultItemData value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\CreatePackageLabelResultItemData $item
     * @return \PPLSDK\ArrayType\ArrayOfCreatePackageLabelResultItemData
     */
    public function addToCreatePackageLabelResultItemData(\PPLSDK\StructType\CreatePackageLabelResultItemData $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\CreatePackageLabelResultItemData) {
            throw new \InvalidArgumentException(sprintf('The CreatePackageLabelResultItemData property can only contain items of type \PPLSDK\StructType\CreatePackageLabelResultItemData, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->CreatePackageLabelResultItemData[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\CreatePackageLabelResultItemData|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\CreatePackageLabelResultItemData|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\CreatePackageLabelResultItemData|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\CreatePackageLabelResultItemData|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\CreatePackageLabelResultItemData|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string CreatePackageLabelResultItemData
     */
    public function getAttributeName()
    {
        return 'CreatePackageLabelResultItemData';
    }
}
