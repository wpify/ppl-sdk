<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfStatusData ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfStatusData
 * @subpackage Arrays
 */
class ArrayOfStatusData extends AbstractStructArrayBase
{
    /**
     * The StatusData
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\StatusData[]
     */
    public $StatusData;
    /**
     * Constructor method for ArrayOfStatusData
     * @uses ArrayOfStatusData::setStatusData()
     * @param \PPLSDK\StructType\StatusData[] $statusData
     */
    public function __construct(array $statusData = array())
    {
        $this
            ->setStatusData($statusData);
    }
    /**
     * Get StatusData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\StatusData[]|null
     */
    public function getStatusData()
    {
        return isset($this->StatusData) ? $this->StatusData : null;
    }
    /**
     * This method is responsible for validating the values passed to the setStatusData method
     * This method is willingly generated in order to preserve the one-line inline validation within the setStatusData method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateStatusDataForArrayConstraintsFromSetStatusData(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfStatusDataStatusDataItem) {
            // validation for constraint: itemType
            if (!$arrayOfStatusDataStatusDataItem instanceof \PPLSDK\StructType\StatusData) {
                $invalidValues[] = is_object($arrayOfStatusDataStatusDataItem) ? get_class($arrayOfStatusDataStatusDataItem) : sprintf('%s(%s)', gettype($arrayOfStatusDataStatusDataItem), var_export($arrayOfStatusDataStatusDataItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The StatusData property can only contain items of type \PPLSDK\StructType\StatusData, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set StatusData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\StatusData[] $statusData
     * @return \PPLSDK\ArrayType\ArrayOfStatusData
     */
    public function setStatusData(array $statusData = array())
    {
        // validation for constraint: array
        if ('' !== ($statusDataArrayErrorMessage = self::validateStatusDataForArrayConstraintsFromSetStatusData($statusData))) {
            throw new \InvalidArgumentException($statusDataArrayErrorMessage, __LINE__);
        }
        if (is_null($statusData) || (is_array($statusData) && empty($statusData))) {
            unset($this->StatusData);
        } else {
            $this->StatusData = $statusData;
        }
        return $this;
    }
    /**
     * Add item to StatusData value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\StatusData $item
     * @return \PPLSDK\ArrayType\ArrayOfStatusData
     */
    public function addToStatusData(\PPLSDK\StructType\StatusData $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\StatusData) {
            throw new \InvalidArgumentException(sprintf('The StatusData property can only contain items of type \PPLSDK\StructType\StatusData, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->StatusData[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\StatusData|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\StatusData|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\StatusData|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\StatusData|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\StatusData|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string StatusData
     */
    public function getAttributeName()
    {
        return 'StatusData';
    }
}
