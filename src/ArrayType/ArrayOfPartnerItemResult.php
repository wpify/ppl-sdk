<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfPartnerItemResult ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfPartnerItemResult
 * @subpackage Arrays
 */
class ArrayOfPartnerItemResult extends AbstractStructArrayBase
{
    /**
     * The PartnerItemResult
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\PartnerItemResult[]
     */
    public $PartnerItemResult;
    /**
     * Constructor method for ArrayOfPartnerItemResult
     * @uses ArrayOfPartnerItemResult::setPartnerItemResult()
     * @param \PPLSDK\StructType\PartnerItemResult[] $partnerItemResult
     */
    public function __construct(array $partnerItemResult = array())
    {
        $this
            ->setPartnerItemResult($partnerItemResult);
    }
    /**
     * Get PartnerItemResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\PartnerItemResult[]|null
     */
    public function getPartnerItemResult()
    {
        return isset($this->PartnerItemResult) ? $this->PartnerItemResult : null;
    }
    /**
     * This method is responsible for validating the values passed to the setPartnerItemResult method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPartnerItemResult method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePartnerItemResultForArrayConstraintsFromSetPartnerItemResult(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfPartnerItemResultPartnerItemResultItem) {
            // validation for constraint: itemType
            if (!$arrayOfPartnerItemResultPartnerItemResultItem instanceof \PPLSDK\StructType\PartnerItemResult) {
                $invalidValues[] = is_object($arrayOfPartnerItemResultPartnerItemResultItem) ? get_class($arrayOfPartnerItemResultPartnerItemResultItem) : sprintf('%s(%s)', gettype($arrayOfPartnerItemResultPartnerItemResultItem), var_export($arrayOfPartnerItemResultPartnerItemResultItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The PartnerItemResult property can only contain items of type \PPLSDK\StructType\PartnerItemResult, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set PartnerItemResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\PartnerItemResult[] $partnerItemResult
     * @return \PPLSDK\ArrayType\ArrayOfPartnerItemResult
     */
    public function setPartnerItemResult(array $partnerItemResult = array())
    {
        // validation for constraint: array
        if ('' !== ($partnerItemResultArrayErrorMessage = self::validatePartnerItemResultForArrayConstraintsFromSetPartnerItemResult($partnerItemResult))) {
            throw new \InvalidArgumentException($partnerItemResultArrayErrorMessage, __LINE__);
        }
        if (is_null($partnerItemResult) || (is_array($partnerItemResult) && empty($partnerItemResult))) {
            unset($this->PartnerItemResult);
        } else {
            $this->PartnerItemResult = $partnerItemResult;
        }
        return $this;
    }
    /**
     * Add item to PartnerItemResult value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\PartnerItemResult $item
     * @return \PPLSDK\ArrayType\ArrayOfPartnerItemResult
     */
    public function addToPartnerItemResult(\PPLSDK\StructType\PartnerItemResult $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\PartnerItemResult) {
            throw new \InvalidArgumentException(sprintf('The PartnerItemResult property can only contain items of type \PPLSDK\StructType\PartnerItemResult, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->PartnerItemResult[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\PartnerItemResult|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\PartnerItemResult|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\PartnerItemResult|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\PartnerItemResult|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\PartnerItemResult|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string PartnerItemResult
     */
    public function getAttributeName()
    {
        return 'PartnerItemResult';
    }
}
