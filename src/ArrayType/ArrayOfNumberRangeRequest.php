<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfNumberRangeRequest ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfNumberRangeRequest
 * @subpackage Arrays
 */
class ArrayOfNumberRangeRequest extends AbstractStructArrayBase
{
    /**
     * The NumberRangeRequest
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\NumberRangeRequest[]
     */
    public $NumberRangeRequest;
    /**
     * Constructor method for ArrayOfNumberRangeRequest
     * @uses ArrayOfNumberRangeRequest::setNumberRangeRequest()
     * @param \PPLSDK\StructType\NumberRangeRequest[] $numberRangeRequest
     */
    public function __construct(array $numberRangeRequest = array())
    {
        $this
            ->setNumberRangeRequest($numberRangeRequest);
    }
    /**
     * Get NumberRangeRequest value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\NumberRangeRequest[]|null
     */
    public function getNumberRangeRequest()
    {
        return isset($this->NumberRangeRequest) ? $this->NumberRangeRequest : null;
    }
    /**
     * This method is responsible for validating the values passed to the setNumberRangeRequest method
     * This method is willingly generated in order to preserve the one-line inline validation within the setNumberRangeRequest method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateNumberRangeRequestForArrayConstraintsFromSetNumberRangeRequest(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfNumberRangeRequestNumberRangeRequestItem) {
            // validation for constraint: itemType
            if (!$arrayOfNumberRangeRequestNumberRangeRequestItem instanceof \PPLSDK\StructType\NumberRangeRequest) {
                $invalidValues[] = is_object($arrayOfNumberRangeRequestNumberRangeRequestItem) ? get_class($arrayOfNumberRangeRequestNumberRangeRequestItem) : sprintf('%s(%s)', gettype($arrayOfNumberRangeRequestNumberRangeRequestItem), var_export($arrayOfNumberRangeRequestNumberRangeRequestItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The NumberRangeRequest property can only contain items of type \PPLSDK\StructType\NumberRangeRequest, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set NumberRangeRequest value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\NumberRangeRequest[] $numberRangeRequest
     * @return \PPLSDK\ArrayType\ArrayOfNumberRangeRequest
     */
    public function setNumberRangeRequest(array $numberRangeRequest = array())
    {
        // validation for constraint: array
        if ('' !== ($numberRangeRequestArrayErrorMessage = self::validateNumberRangeRequestForArrayConstraintsFromSetNumberRangeRequest($numberRangeRequest))) {
            throw new \InvalidArgumentException($numberRangeRequestArrayErrorMessage, __LINE__);
        }
        if (is_null($numberRangeRequest) || (is_array($numberRangeRequest) && empty($numberRangeRequest))) {
            unset($this->NumberRangeRequest);
        } else {
            $this->NumberRangeRequest = $numberRangeRequest;
        }
        return $this;
    }
    /**
     * Add item to NumberRangeRequest value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\NumberRangeRequest $item
     * @return \PPLSDK\ArrayType\ArrayOfNumberRangeRequest
     */
    public function addToNumberRangeRequest(\PPLSDK\StructType\NumberRangeRequest $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\NumberRangeRequest) {
            throw new \InvalidArgumentException(sprintf('The NumberRangeRequest property can only contain items of type \PPLSDK\StructType\NumberRangeRequest, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->NumberRangeRequest[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\NumberRangeRequest|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\NumberRangeRequest|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\NumberRangeRequest|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\NumberRangeRequest|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\NumberRangeRequest|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string NumberRangeRequest
     */
    public function getAttributeName()
    {
        return 'NumberRangeRequest';
    }
}
