<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiCodCurrency ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiCodCurrency
 * @subpackage Arrays
 */
class ArrayOfMyApiCodCurrency extends AbstractStructArrayBase
{
    /**
     * The MyApiCodCurrency
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiCodCurrency[]
     */
    public $MyApiCodCurrency;
    /**
     * Constructor method for ArrayOfMyApiCodCurrency
     * @uses ArrayOfMyApiCodCurrency::setMyApiCodCurrency()
     * @param \PPLSDK\StructType\MyApiCodCurrency[] $myApiCodCurrency
     */
    public function __construct(array $myApiCodCurrency = array())
    {
        $this
            ->setMyApiCodCurrency($myApiCodCurrency);
    }
    /**
     * Get MyApiCodCurrency value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiCodCurrency[]|null
     */
    public function getMyApiCodCurrency()
    {
        return isset($this->MyApiCodCurrency) ? $this->MyApiCodCurrency : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiCodCurrency method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiCodCurrency method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiCodCurrencyForArrayConstraintsFromSetMyApiCodCurrency(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiCodCurrencyMyApiCodCurrencyItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiCodCurrencyMyApiCodCurrencyItem instanceof \PPLSDK\StructType\MyApiCodCurrency) {
                $invalidValues[] = is_object($arrayOfMyApiCodCurrencyMyApiCodCurrencyItem) ? get_class($arrayOfMyApiCodCurrencyMyApiCodCurrencyItem) : sprintf('%s(%s)', gettype($arrayOfMyApiCodCurrencyMyApiCodCurrencyItem), var_export($arrayOfMyApiCodCurrencyMyApiCodCurrencyItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiCodCurrency property can only contain items of type \PPLSDK\StructType\MyApiCodCurrency, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiCodCurrency value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiCodCurrency[] $myApiCodCurrency
     * @return \PPLSDK\ArrayType\ArrayOfMyApiCodCurrency
     */
    public function setMyApiCodCurrency(array $myApiCodCurrency = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiCodCurrencyArrayErrorMessage = self::validateMyApiCodCurrencyForArrayConstraintsFromSetMyApiCodCurrency($myApiCodCurrency))) {
            throw new \InvalidArgumentException($myApiCodCurrencyArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiCodCurrency) || (is_array($myApiCodCurrency) && empty($myApiCodCurrency))) {
            unset($this->MyApiCodCurrency);
        } else {
            $this->MyApiCodCurrency = $myApiCodCurrency;
        }
        return $this;
    }
    /**
     * Add item to MyApiCodCurrency value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiCodCurrency $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiCodCurrency
     */
    public function addToMyApiCodCurrency(\PPLSDK\StructType\MyApiCodCurrency $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiCodCurrency) {
            throw new \InvalidArgumentException(sprintf('The MyApiCodCurrency property can only contain items of type \PPLSDK\StructType\MyApiCodCurrency, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiCodCurrency[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiCodCurrency|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiCodCurrency|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiCodCurrency|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiCodCurrency|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiCodCurrency|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiCodCurrency
     */
    public function getAttributeName()
    {
        return 'MyApiCodCurrency';
    }
}
