<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiParcelShop ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiParcelShop
 * @subpackage Arrays
 */
class ArrayOfMyApiParcelShop extends AbstractStructArrayBase
{
    /**
     * The MyApiParcelShop
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiParcelShop[]
     */
    public $MyApiParcelShop;
    /**
     * Constructor method for ArrayOfMyApiParcelShop
     * @uses ArrayOfMyApiParcelShop::setMyApiParcelShop()
     * @param \PPLSDK\StructType\MyApiParcelShop[] $myApiParcelShop
     */
    public function __construct(array $myApiParcelShop = array())
    {
        $this
            ->setMyApiParcelShop($myApiParcelShop);
    }
    /**
     * Get MyApiParcelShop value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiParcelShop[]|null
     */
    public function getMyApiParcelShop()
    {
        return isset($this->MyApiParcelShop) ? $this->MyApiParcelShop : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiParcelShop method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiParcelShop method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiParcelShopForArrayConstraintsFromSetMyApiParcelShop(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiParcelShopMyApiParcelShopItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiParcelShopMyApiParcelShopItem instanceof \PPLSDK\StructType\MyApiParcelShop) {
                $invalidValues[] = is_object($arrayOfMyApiParcelShopMyApiParcelShopItem) ? get_class($arrayOfMyApiParcelShopMyApiParcelShopItem) : sprintf('%s(%s)', gettype($arrayOfMyApiParcelShopMyApiParcelShopItem), var_export($arrayOfMyApiParcelShopMyApiParcelShopItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiParcelShop property can only contain items of type \PPLSDK\StructType\MyApiParcelShop, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiParcelShop value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiParcelShop[] $myApiParcelShop
     * @return \PPLSDK\ArrayType\ArrayOfMyApiParcelShop
     */
    public function setMyApiParcelShop(array $myApiParcelShop = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiParcelShopArrayErrorMessage = self::validateMyApiParcelShopForArrayConstraintsFromSetMyApiParcelShop($myApiParcelShop))) {
            throw new \InvalidArgumentException($myApiParcelShopArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiParcelShop) || (is_array($myApiParcelShop) && empty($myApiParcelShop))) {
            unset($this->MyApiParcelShop);
        } else {
            $this->MyApiParcelShop = $myApiParcelShop;
        }
        return $this;
    }
    /**
     * Add item to MyApiParcelShop value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiParcelShop $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiParcelShop
     */
    public function addToMyApiParcelShop(\PPLSDK\StructType\MyApiParcelShop $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiParcelShop) {
            throw new \InvalidArgumentException(sprintf('The MyApiParcelShop property can only contain items of type \PPLSDK\StructType\MyApiParcelShop, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiParcelShop[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiParcelShop|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiParcelShop|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiParcelShop|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiParcelShop|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiParcelShop|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiParcelShop
     */
    public function getAttributeName()
    {
        return 'MyApiParcelShop';
    }
}
