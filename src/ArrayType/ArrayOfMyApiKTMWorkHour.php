<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiKTMWorkHour ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiKTMWorkHour
 * @subpackage Arrays
 */
class ArrayOfMyApiKTMWorkHour extends AbstractStructArrayBase
{
    /**
     * The MyApiKTMWorkHour
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiKTMWorkHour[]
     */
    public $MyApiKTMWorkHour;
    /**
     * Constructor method for ArrayOfMyApiKTMWorkHour
     * @uses ArrayOfMyApiKTMWorkHour::setMyApiKTMWorkHour()
     * @param \PPLSDK\StructType\MyApiKTMWorkHour[] $myApiKTMWorkHour
     */
    public function __construct(array $myApiKTMWorkHour = array())
    {
        $this
            ->setMyApiKTMWorkHour($myApiKTMWorkHour);
    }
    /**
     * Get MyApiKTMWorkHour value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiKTMWorkHour[]|null
     */
    public function getMyApiKTMWorkHour()
    {
        return isset($this->MyApiKTMWorkHour) ? $this->MyApiKTMWorkHour : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiKTMWorkHour method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiKTMWorkHour method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiKTMWorkHourForArrayConstraintsFromSetMyApiKTMWorkHour(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiKTMWorkHourMyApiKTMWorkHourItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiKTMWorkHourMyApiKTMWorkHourItem instanceof \PPLSDK\StructType\MyApiKTMWorkHour) {
                $invalidValues[] = is_object($arrayOfMyApiKTMWorkHourMyApiKTMWorkHourItem) ? get_class($arrayOfMyApiKTMWorkHourMyApiKTMWorkHourItem) : sprintf('%s(%s)', gettype($arrayOfMyApiKTMWorkHourMyApiKTMWorkHourItem), var_export($arrayOfMyApiKTMWorkHourMyApiKTMWorkHourItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiKTMWorkHour property can only contain items of type \PPLSDK\StructType\MyApiKTMWorkHour, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiKTMWorkHour value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiKTMWorkHour[] $myApiKTMWorkHour
     * @return \PPLSDK\ArrayType\ArrayOfMyApiKTMWorkHour
     */
    public function setMyApiKTMWorkHour(array $myApiKTMWorkHour = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiKTMWorkHourArrayErrorMessage = self::validateMyApiKTMWorkHourForArrayConstraintsFromSetMyApiKTMWorkHour($myApiKTMWorkHour))) {
            throw new \InvalidArgumentException($myApiKTMWorkHourArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiKTMWorkHour) || (is_array($myApiKTMWorkHour) && empty($myApiKTMWorkHour))) {
            unset($this->MyApiKTMWorkHour);
        } else {
            $this->MyApiKTMWorkHour = $myApiKTMWorkHour;
        }
        return $this;
    }
    /**
     * Add item to MyApiKTMWorkHour value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiKTMWorkHour $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiKTMWorkHour
     */
    public function addToMyApiKTMWorkHour(\PPLSDK\StructType\MyApiKTMWorkHour $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiKTMWorkHour) {
            throw new \InvalidArgumentException(sprintf('The MyApiKTMWorkHour property can only contain items of type \PPLSDK\StructType\MyApiKTMWorkHour, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiKTMWorkHour[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiKTMWorkHour|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiKTMWorkHour|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiKTMWorkHour|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiKTMWorkHour|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiKTMWorkHour|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiKTMWorkHour
     */
    public function getAttributeName()
    {
        return 'MyApiKTMWorkHour';
    }
}
