<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfNumberRange ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfNumberRange
 * @subpackage Arrays
 */
class ArrayOfNumberRange extends AbstractStructArrayBase
{
    /**
     * The NumberRange
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\NumberRange[]
     */
    public $NumberRange;
    /**
     * Constructor method for ArrayOfNumberRange
     * @uses ArrayOfNumberRange::setNumberRange()
     * @param \PPLSDK\StructType\NumberRange[] $numberRange
     */
    public function __construct(array $numberRange = array())
    {
        $this
            ->setNumberRange($numberRange);
    }
    /**
     * Get NumberRange value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\NumberRange[]|null
     */
    public function getNumberRange()
    {
        return isset($this->NumberRange) ? $this->NumberRange : null;
    }
    /**
     * This method is responsible for validating the values passed to the setNumberRange method
     * This method is willingly generated in order to preserve the one-line inline validation within the setNumberRange method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateNumberRangeForArrayConstraintsFromSetNumberRange(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfNumberRangeNumberRangeItem) {
            // validation for constraint: itemType
            if (!$arrayOfNumberRangeNumberRangeItem instanceof \PPLSDK\StructType\NumberRange) {
                $invalidValues[] = is_object($arrayOfNumberRangeNumberRangeItem) ? get_class($arrayOfNumberRangeNumberRangeItem) : sprintf('%s(%s)', gettype($arrayOfNumberRangeNumberRangeItem), var_export($arrayOfNumberRangeNumberRangeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The NumberRange property can only contain items of type \PPLSDK\StructType\NumberRange, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set NumberRange value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\NumberRange[] $numberRange
     * @return \PPLSDK\ArrayType\ArrayOfNumberRange
     */
    public function setNumberRange(array $numberRange = array())
    {
        // validation for constraint: array
        if ('' !== ($numberRangeArrayErrorMessage = self::validateNumberRangeForArrayConstraintsFromSetNumberRange($numberRange))) {
            throw new \InvalidArgumentException($numberRangeArrayErrorMessage, __LINE__);
        }
        if (is_null($numberRange) || (is_array($numberRange) && empty($numberRange))) {
            unset($this->NumberRange);
        } else {
            $this->NumberRange = $numberRange;
        }
        return $this;
    }
    /**
     * Add item to NumberRange value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\NumberRange $item
     * @return \PPLSDK\ArrayType\ArrayOfNumberRange
     */
    public function addToNumberRange(\PPLSDK\StructType\NumberRange $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\NumberRange) {
            throw new \InvalidArgumentException(sprintf('The NumberRange property can only contain items of type \PPLSDK\StructType\NumberRange, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->NumberRange[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\NumberRange|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\NumberRange|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\NumberRange|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\NumberRange|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\NumberRange|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string NumberRange
     */
    public function getAttributeName()
    {
        return 'NumberRange';
    }
}
