<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiPackageOutStatus ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiPackageOutStatus
 * @subpackage Arrays
 */
class ArrayOfMyApiPackageOutStatus extends AbstractStructArrayBase
{
    /**
     * The MyApiPackageOutStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPackageOutStatus[]
     */
    public $MyApiPackageOutStatus;
    /**
     * Constructor method for ArrayOfMyApiPackageOutStatus
     * @uses ArrayOfMyApiPackageOutStatus::setMyApiPackageOutStatus()
     * @param \PPLSDK\StructType\MyApiPackageOutStatus[] $myApiPackageOutStatus
     */
    public function __construct(array $myApiPackageOutStatus = array())
    {
        $this
            ->setMyApiPackageOutStatus($myApiPackageOutStatus);
    }
    /**
     * Get MyApiPackageOutStatus value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiPackageOutStatus[]|null
     */
    public function getMyApiPackageOutStatus()
    {
        return isset($this->MyApiPackageOutStatus) ? $this->MyApiPackageOutStatus : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiPackageOutStatus method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiPackageOutStatus method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiPackageOutStatusForArrayConstraintsFromSetMyApiPackageOutStatus(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiPackageOutStatusMyApiPackageOutStatusItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiPackageOutStatusMyApiPackageOutStatusItem instanceof \PPLSDK\StructType\MyApiPackageOutStatus) {
                $invalidValues[] = is_object($arrayOfMyApiPackageOutStatusMyApiPackageOutStatusItem) ? get_class($arrayOfMyApiPackageOutStatusMyApiPackageOutStatusItem) : sprintf('%s(%s)', gettype($arrayOfMyApiPackageOutStatusMyApiPackageOutStatusItem), var_export($arrayOfMyApiPackageOutStatusMyApiPackageOutStatusItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiPackageOutStatus property can only contain items of type \PPLSDK\StructType\MyApiPackageOutStatus, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiPackageOutStatus value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageOutStatus[] $myApiPackageOutStatus
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageOutStatus
     */
    public function setMyApiPackageOutStatus(array $myApiPackageOutStatus = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiPackageOutStatusArrayErrorMessage = self::validateMyApiPackageOutStatusForArrayConstraintsFromSetMyApiPackageOutStatus($myApiPackageOutStatus))) {
            throw new \InvalidArgumentException($myApiPackageOutStatusArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiPackageOutStatus) || (is_array($myApiPackageOutStatus) && empty($myApiPackageOutStatus))) {
            unset($this->MyApiPackageOutStatus);
        } else {
            $this->MyApiPackageOutStatus = $myApiPackageOutStatus;
        }
        return $this;
    }
    /**
     * Add item to MyApiPackageOutStatus value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageOutStatus $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageOutStatus
     */
    public function addToMyApiPackageOutStatus(\PPLSDK\StructType\MyApiPackageOutStatus $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiPackageOutStatus) {
            throw new \InvalidArgumentException(sprintf('The MyApiPackageOutStatus property can only contain items of type \PPLSDK\StructType\MyApiPackageOutStatus, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiPackageOutStatus[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiPackageOutStatus|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiPackageOutStatus|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiPackageOutStatus|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiPackageOutStatus|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiPackageOutStatus|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiPackageOutStatus
     */
    public function getAttributeName()
    {
        return 'MyApiPackageOutStatus';
    }
}
