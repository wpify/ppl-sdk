<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiPackNumberRowType ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiPackNumberRowType
 * @subpackage Arrays
 */
class ArrayOfMyApiPackNumberRowType extends AbstractStructArrayBase
{
    /**
     * The MyApiPackNumberRowType
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPackNumberRowType[]
     */
    public $MyApiPackNumberRowType;
    /**
     * Constructor method for ArrayOfMyApiPackNumberRowType
     * @uses ArrayOfMyApiPackNumberRowType::setMyApiPackNumberRowType()
     * @param \PPLSDK\StructType\MyApiPackNumberRowType[] $myApiPackNumberRowType
     */
    public function __construct(array $myApiPackNumberRowType = array())
    {
        $this
            ->setMyApiPackNumberRowType($myApiPackNumberRowType);
    }
    /**
     * Get MyApiPackNumberRowType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiPackNumberRowType[]|null
     */
    public function getMyApiPackNumberRowType()
    {
        return isset($this->MyApiPackNumberRowType) ? $this->MyApiPackNumberRowType : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiPackNumberRowType method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiPackNumberRowType method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiPackNumberRowTypeForArrayConstraintsFromSetMyApiPackNumberRowType(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiPackNumberRowTypeMyApiPackNumberRowTypeItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiPackNumberRowTypeMyApiPackNumberRowTypeItem instanceof \PPLSDK\StructType\MyApiPackNumberRowType) {
                $invalidValues[] = is_object($arrayOfMyApiPackNumberRowTypeMyApiPackNumberRowTypeItem) ? get_class($arrayOfMyApiPackNumberRowTypeMyApiPackNumberRowTypeItem) : sprintf('%s(%s)', gettype($arrayOfMyApiPackNumberRowTypeMyApiPackNumberRowTypeItem), var_export($arrayOfMyApiPackNumberRowTypeMyApiPackNumberRowTypeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiPackNumberRowType property can only contain items of type \PPLSDK\StructType\MyApiPackNumberRowType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiPackNumberRowType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackNumberRowType[] $myApiPackNumberRowType
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackNumberRowType
     */
    public function setMyApiPackNumberRowType(array $myApiPackNumberRowType = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiPackNumberRowTypeArrayErrorMessage = self::validateMyApiPackNumberRowTypeForArrayConstraintsFromSetMyApiPackNumberRowType($myApiPackNumberRowType))) {
            throw new \InvalidArgumentException($myApiPackNumberRowTypeArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiPackNumberRowType) || (is_array($myApiPackNumberRowType) && empty($myApiPackNumberRowType))) {
            unset($this->MyApiPackNumberRowType);
        } else {
            $this->MyApiPackNumberRowType = $myApiPackNumberRowType;
        }
        return $this;
    }
    /**
     * Add item to MyApiPackNumberRowType value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackNumberRowType $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackNumberRowType
     */
    public function addToMyApiPackNumberRowType(\PPLSDK\StructType\MyApiPackNumberRowType $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiPackNumberRowType) {
            throw new \InvalidArgumentException(sprintf('The MyApiPackNumberRowType property can only contain items of type \PPLSDK\StructType\MyApiPackNumberRowType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiPackNumberRowType[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiPackNumberRowType|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiPackNumberRowType|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiPackNumberRowType|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiPackNumberRowType|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiPackNumberRowType|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiPackNumberRowType
     */
    public function getAttributeName()
    {
        return 'MyApiPackNumberRowType';
    }
}
