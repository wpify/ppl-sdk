<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiPackageExtNum ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiPackageExtNum
 * @subpackage Arrays
 */
class ArrayOfMyApiPackageExtNum extends AbstractStructArrayBase
{
    /**
     * The MyApiPackageExtNum
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPackageExtNum[]
     */
    public $MyApiPackageExtNum;
    /**
     * Constructor method for ArrayOfMyApiPackageExtNum
     * @uses ArrayOfMyApiPackageExtNum::setMyApiPackageExtNum()
     * @param \PPLSDK\StructType\MyApiPackageExtNum[] $myApiPackageExtNum
     */
    public function __construct(array $myApiPackageExtNum = array())
    {
        $this
            ->setMyApiPackageExtNum($myApiPackageExtNum);
    }
    /**
     * Get MyApiPackageExtNum value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiPackageExtNum[]|null
     */
    public function getMyApiPackageExtNum()
    {
        return isset($this->MyApiPackageExtNum) ? $this->MyApiPackageExtNum : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiPackageExtNum method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiPackageExtNum method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiPackageExtNumForArrayConstraintsFromSetMyApiPackageExtNum(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiPackageExtNumMyApiPackageExtNumItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiPackageExtNumMyApiPackageExtNumItem instanceof \PPLSDK\StructType\MyApiPackageExtNum) {
                $invalidValues[] = is_object($arrayOfMyApiPackageExtNumMyApiPackageExtNumItem) ? get_class($arrayOfMyApiPackageExtNumMyApiPackageExtNumItem) : sprintf('%s(%s)', gettype($arrayOfMyApiPackageExtNumMyApiPackageExtNumItem), var_export($arrayOfMyApiPackageExtNumMyApiPackageExtNumItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiPackageExtNum property can only contain items of type \PPLSDK\StructType\MyApiPackageExtNum, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiPackageExtNum value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageExtNum[] $myApiPackageExtNum
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageExtNum
     */
    public function setMyApiPackageExtNum(array $myApiPackageExtNum = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiPackageExtNumArrayErrorMessage = self::validateMyApiPackageExtNumForArrayConstraintsFromSetMyApiPackageExtNum($myApiPackageExtNum))) {
            throw new \InvalidArgumentException($myApiPackageExtNumArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiPackageExtNum) || (is_array($myApiPackageExtNum) && empty($myApiPackageExtNum))) {
            unset($this->MyApiPackageExtNum);
        } else {
            $this->MyApiPackageExtNum = $myApiPackageExtNum;
        }
        return $this;
    }
    /**
     * Add item to MyApiPackageExtNum value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageExtNum $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageExtNum
     */
    public function addToMyApiPackageExtNum(\PPLSDK\StructType\MyApiPackageExtNum $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiPackageExtNum) {
            throw new \InvalidArgumentException(sprintf('The MyApiPackageExtNum property can only contain items of type \PPLSDK\StructType\MyApiPackageExtNum, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiPackageExtNum[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiPackageExtNum|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiPackageExtNum|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiPackageExtNum|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiPackageExtNum|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiPackageExtNum|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiPackageExtNum
     */
    public function getAttributeName()
    {
        return 'MyApiPackageExtNum';
    }
}
