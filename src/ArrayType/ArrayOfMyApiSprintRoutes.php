<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiSprintRoutes ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiSprintRoutes
 * @subpackage Arrays
 */
class ArrayOfMyApiSprintRoutes extends AbstractStructArrayBase
{
    /**
     * The MyApiSprintRoutes
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiSprintRoutes[]
     */
    public $MyApiSprintRoutes;
    /**
     * Constructor method for ArrayOfMyApiSprintRoutes
     * @uses ArrayOfMyApiSprintRoutes::setMyApiSprintRoutes()
     * @param \PPLSDK\StructType\MyApiSprintRoutes[] $myApiSprintRoutes
     */
    public function __construct(array $myApiSprintRoutes = array())
    {
        $this
            ->setMyApiSprintRoutes($myApiSprintRoutes);
    }
    /**
     * Get MyApiSprintRoutes value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiSprintRoutes[]|null
     */
    public function getMyApiSprintRoutes()
    {
        return isset($this->MyApiSprintRoutes) ? $this->MyApiSprintRoutes : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiSprintRoutes method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiSprintRoutes method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiSprintRoutesForArrayConstraintsFromSetMyApiSprintRoutes(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiSprintRoutesMyApiSprintRoutesItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiSprintRoutesMyApiSprintRoutesItem instanceof \PPLSDK\StructType\MyApiSprintRoutes) {
                $invalidValues[] = is_object($arrayOfMyApiSprintRoutesMyApiSprintRoutesItem) ? get_class($arrayOfMyApiSprintRoutesMyApiSprintRoutesItem) : sprintf('%s(%s)', gettype($arrayOfMyApiSprintRoutesMyApiSprintRoutesItem), var_export($arrayOfMyApiSprintRoutesMyApiSprintRoutesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiSprintRoutes property can only contain items of type \PPLSDK\StructType\MyApiSprintRoutes, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiSprintRoutes value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiSprintRoutes[] $myApiSprintRoutes
     * @return \PPLSDK\ArrayType\ArrayOfMyApiSprintRoutes
     */
    public function setMyApiSprintRoutes(array $myApiSprintRoutes = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiSprintRoutesArrayErrorMessage = self::validateMyApiSprintRoutesForArrayConstraintsFromSetMyApiSprintRoutes($myApiSprintRoutes))) {
            throw new \InvalidArgumentException($myApiSprintRoutesArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiSprintRoutes) || (is_array($myApiSprintRoutes) && empty($myApiSprintRoutes))) {
            unset($this->MyApiSprintRoutes);
        } else {
            $this->MyApiSprintRoutes = $myApiSprintRoutes;
        }
        return $this;
    }
    /**
     * Add item to MyApiSprintRoutes value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiSprintRoutes $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiSprintRoutes
     */
    public function addToMyApiSprintRoutes(\PPLSDK\StructType\MyApiSprintRoutes $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiSprintRoutes) {
            throw new \InvalidArgumentException(sprintf('The MyApiSprintRoutes property can only contain items of type \PPLSDK\StructType\MyApiSprintRoutes, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiSprintRoutes[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiSprintRoutes|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiSprintRoutes|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiSprintRoutes|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiSprintRoutes|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiSprintRoutes|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiSprintRoutes
     */
    public function getAttributeName()
    {
        return 'MyApiSprintRoutes';
    }
}
