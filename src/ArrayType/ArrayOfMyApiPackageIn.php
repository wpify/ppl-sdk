<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiPackageIn ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiPackageIn
 * @subpackage Arrays
 */
class ArrayOfMyApiPackageIn extends AbstractStructArrayBase
{
    /**
     * The MyApiPackageIn
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPackageIn[]
     */
    public $MyApiPackageIn;
    /**
     * Constructor method for ArrayOfMyApiPackageIn
     * @uses ArrayOfMyApiPackageIn::setMyApiPackageIn()
     * @param \PPLSDK\StructType\MyApiPackageIn[] $myApiPackageIn
     */
    public function __construct(array $myApiPackageIn = array())
    {
        $this
            ->setMyApiPackageIn($myApiPackageIn);
    }
    /**
     * Get MyApiPackageIn value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiPackageIn[]|null
     */
    public function getMyApiPackageIn()
    {
        return isset($this->MyApiPackageIn) ? $this->MyApiPackageIn : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiPackageIn method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiPackageIn method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiPackageInForArrayConstraintsFromSetMyApiPackageIn(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiPackageInMyApiPackageInItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiPackageInMyApiPackageInItem instanceof \PPLSDK\StructType\MyApiPackageIn) {
                $invalidValues[] = is_object($arrayOfMyApiPackageInMyApiPackageInItem) ? get_class($arrayOfMyApiPackageInMyApiPackageInItem) : sprintf('%s(%s)', gettype($arrayOfMyApiPackageInMyApiPackageInItem), var_export($arrayOfMyApiPackageInMyApiPackageInItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiPackageIn property can only contain items of type \PPLSDK\StructType\MyApiPackageIn, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiPackageIn value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageIn[] $myApiPackageIn
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageIn
     */
    public function setMyApiPackageIn(array $myApiPackageIn = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiPackageInArrayErrorMessage = self::validateMyApiPackageInForArrayConstraintsFromSetMyApiPackageIn($myApiPackageIn))) {
            throw new \InvalidArgumentException($myApiPackageInArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiPackageIn) || (is_array($myApiPackageIn) && empty($myApiPackageIn))) {
            unset($this->MyApiPackageIn);
        } else {
            $this->MyApiPackageIn = $myApiPackageIn;
        }
        return $this;
    }
    /**
     * Add item to MyApiPackageIn value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageIn $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageIn
     */
    public function addToMyApiPackageIn(\PPLSDK\StructType\MyApiPackageIn $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiPackageIn) {
            throw new \InvalidArgumentException(sprintf('The MyApiPackageIn property can only contain items of type \PPLSDK\StructType\MyApiPackageIn, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiPackageIn[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiPackageIn|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiPackageIn|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiPackageIn|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiPackageIn|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiPackageIn|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiPackageIn
     */
    public function getAttributeName()
    {
        return 'MyApiPackageIn';
    }
}
