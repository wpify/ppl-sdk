<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGetFreeNumberRangesData ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfGetFreeNumberRangesData
 * @subpackage Arrays
 */
class ArrayOfGetFreeNumberRangesData extends AbstractStructArrayBase
{
    /**
     * The GetFreeNumberRangesData
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\GetFreeNumberRangesData[]
     */
    public $GetFreeNumberRangesData;
    /**
     * Constructor method for ArrayOfGetFreeNumberRangesData
     * @uses ArrayOfGetFreeNumberRangesData::setGetFreeNumberRangesData()
     * @param \PPLSDK\StructType\GetFreeNumberRangesData[] $getFreeNumberRangesData
     */
    public function __construct(array $getFreeNumberRangesData = array())
    {
        $this
            ->setGetFreeNumberRangesData($getFreeNumberRangesData);
    }
    /**
     * Get GetFreeNumberRangesData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\GetFreeNumberRangesData[]|null
     */
    public function getGetFreeNumberRangesData()
    {
        return isset($this->GetFreeNumberRangesData) ? $this->GetFreeNumberRangesData : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGetFreeNumberRangesData method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGetFreeNumberRangesData method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGetFreeNumberRangesDataForArrayConstraintsFromSetGetFreeNumberRangesData(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGetFreeNumberRangesDataGetFreeNumberRangesDataItem) {
            // validation for constraint: itemType
            if (!$arrayOfGetFreeNumberRangesDataGetFreeNumberRangesDataItem instanceof \PPLSDK\StructType\GetFreeNumberRangesData) {
                $invalidValues[] = is_object($arrayOfGetFreeNumberRangesDataGetFreeNumberRangesDataItem) ? get_class($arrayOfGetFreeNumberRangesDataGetFreeNumberRangesDataItem) : sprintf('%s(%s)', gettype($arrayOfGetFreeNumberRangesDataGetFreeNumberRangesDataItem), var_export($arrayOfGetFreeNumberRangesDataGetFreeNumberRangesDataItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GetFreeNumberRangesData property can only contain items of type \PPLSDK\StructType\GetFreeNumberRangesData, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set GetFreeNumberRangesData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\GetFreeNumberRangesData[] $getFreeNumberRangesData
     * @return \PPLSDK\ArrayType\ArrayOfGetFreeNumberRangesData
     */
    public function setGetFreeNumberRangesData(array $getFreeNumberRangesData = array())
    {
        // validation for constraint: array
        if ('' !== ($getFreeNumberRangesDataArrayErrorMessage = self::validateGetFreeNumberRangesDataForArrayConstraintsFromSetGetFreeNumberRangesData($getFreeNumberRangesData))) {
            throw new \InvalidArgumentException($getFreeNumberRangesDataArrayErrorMessage, __LINE__);
        }
        if (is_null($getFreeNumberRangesData) || (is_array($getFreeNumberRangesData) && empty($getFreeNumberRangesData))) {
            unset($this->GetFreeNumberRangesData);
        } else {
            $this->GetFreeNumberRangesData = $getFreeNumberRangesData;
        }
        return $this;
    }
    /**
     * Add item to GetFreeNumberRangesData value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\GetFreeNumberRangesData $item
     * @return \PPLSDK\ArrayType\ArrayOfGetFreeNumberRangesData
     */
    public function addToGetFreeNumberRangesData(\PPLSDK\StructType\GetFreeNumberRangesData $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\GetFreeNumberRangesData) {
            throw new \InvalidArgumentException(sprintf('The GetFreeNumberRangesData property can only contain items of type \PPLSDK\StructType\GetFreeNumberRangesData, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->GetFreeNumberRangesData[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\GetFreeNumberRangesData|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\GetFreeNumberRangesData|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\GetFreeNumberRangesData|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\GetFreeNumberRangesData|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\GetFreeNumberRangesData|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GetFreeNumberRangesData
     */
    public function getAttributeName()
    {
        return 'GetFreeNumberRangesData';
    }
}
