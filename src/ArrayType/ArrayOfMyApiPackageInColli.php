<?php

namespace PPLSDK\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfMyApiPackageInColli ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfMyApiPackageInColli
 * @subpackage Arrays
 */
class ArrayOfMyApiPackageInColli extends AbstractStructArrayBase
{
    /**
     * The MyApiPackageInColli
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \PPLSDK\StructType\MyApiPackageInColli[]
     */
    public $MyApiPackageInColli;
    /**
     * Constructor method for ArrayOfMyApiPackageInColli
     * @uses ArrayOfMyApiPackageInColli::setMyApiPackageInColli()
     * @param \PPLSDK\StructType\MyApiPackageInColli[] $myApiPackageInColli
     */
    public function __construct(array $myApiPackageInColli = array())
    {
        $this
            ->setMyApiPackageInColli($myApiPackageInColli);
    }
    /**
     * Get MyApiPackageInColli value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \PPLSDK\StructType\MyApiPackageInColli[]|null
     */
    public function getMyApiPackageInColli()
    {
        return isset($this->MyApiPackageInColli) ? $this->MyApiPackageInColli : null;
    }
    /**
     * This method is responsible for validating the values passed to the setMyApiPackageInColli method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMyApiPackageInColli method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMyApiPackageInColliForArrayConstraintsFromSetMyApiPackageInColli(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfMyApiPackageInColliMyApiPackageInColliItem) {
            // validation for constraint: itemType
            if (!$arrayOfMyApiPackageInColliMyApiPackageInColliItem instanceof \PPLSDK\StructType\MyApiPackageInColli) {
                $invalidValues[] = is_object($arrayOfMyApiPackageInColliMyApiPackageInColliItem) ? get_class($arrayOfMyApiPackageInColliMyApiPackageInColliItem) : sprintf('%s(%s)', gettype($arrayOfMyApiPackageInColliMyApiPackageInColliItem), var_export($arrayOfMyApiPackageInColliMyApiPackageInColliItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The MyApiPackageInColli property can only contain items of type \PPLSDK\StructType\MyApiPackageInColli, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set MyApiPackageInColli value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageInColli[] $myApiPackageInColli
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageInColli
     */
    public function setMyApiPackageInColli(array $myApiPackageInColli = array())
    {
        // validation for constraint: array
        if ('' !== ($myApiPackageInColliArrayErrorMessage = self::validateMyApiPackageInColliForArrayConstraintsFromSetMyApiPackageInColli($myApiPackageInColli))) {
            throw new \InvalidArgumentException($myApiPackageInColliArrayErrorMessage, __LINE__);
        }
        if (is_null($myApiPackageInColli) || (is_array($myApiPackageInColli) && empty($myApiPackageInColli))) {
            unset($this->MyApiPackageInColli);
        } else {
            $this->MyApiPackageInColli = $myApiPackageInColli;
        }
        return $this;
    }
    /**
     * Add item to MyApiPackageInColli value
     * @throws \InvalidArgumentException
     * @param \PPLSDK\StructType\MyApiPackageInColli $item
     * @return \PPLSDK\ArrayType\ArrayOfMyApiPackageInColli
     */
    public function addToMyApiPackageInColli(\PPLSDK\StructType\MyApiPackageInColli $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \PPLSDK\StructType\MyApiPackageInColli) {
            throw new \InvalidArgumentException(sprintf('The MyApiPackageInColli property can only contain items of type \PPLSDK\StructType\MyApiPackageInColli, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->MyApiPackageInColli[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \PPLSDK\StructType\MyApiPackageInColli|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \PPLSDK\StructType\MyApiPackageInColli|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \PPLSDK\StructType\MyApiPackageInColli|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \PPLSDK\StructType\MyApiPackageInColli|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \PPLSDK\StructType\MyApiPackageInColli|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string MyApiPackageInColli
     */
    public function getAttributeName()
    {
        return 'MyApiPackageInColli';
    }
}
