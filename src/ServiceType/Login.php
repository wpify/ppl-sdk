<?php

namespace PPLSDK\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Login ServiceType
 * @subpackage Services
 */
class Login extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Login
     * Meta information extracted from the WSDL
     * - documentation: Vrátí autentikační ticket
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\Login $parameters
     * @return \PPLSDK\StructType\LoginResponse|bool
     */
    public function Login(\PPLSDK\StructType\Login $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Login', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \PPLSDK\StructType\LoginResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
