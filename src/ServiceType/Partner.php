<?php

namespace PPLSDK\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Partner ServiceType
 * @subpackage Services
 */
class Partner extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named PartnerSaveStatuses
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\PartnerSaveStatuses $parameters
     * @return \PPLSDK\StructType\PartnerSaveStatusesResponse|bool
     */
    public function PartnerSaveStatuses(\PPLSDK\StructType\PartnerSaveStatuses $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('PartnerSaveStatuses', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \PPLSDK\StructType\PartnerSaveStatusesResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
