<?php

namespace PPLSDK\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Create ServiceType
 * @subpackage Services
 */
class Create extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named CreatePackages
     * Meta information extracted from the WSDL
     * - documentation: Vloží nové zásilky pro import do systému.
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\CreatePackages $parameters
     * @return \PPLSDK\StructType\CreatePackagesResponse|bool
     */
    public function CreatePackages(\PPLSDK\StructType\CreatePackages $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('CreatePackages', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named CreatePackageLabel
     * Meta information extracted from the WSDL
     * - documentation: Vloží nové zásilky pro import do systému
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\CreatePackageLabel $parameters
     * @return \PPLSDK\StructType\CreatePackageLabelResponse|bool
     */
    public function CreatePackageLabel(\PPLSDK\StructType\CreatePackageLabel $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('CreatePackageLabel', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named CreateOrders
     * Meta information extracted from the WSDL
     * - documentation: Vytvoří objednávky přepravy
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\CreateOrders $parameters
     * @return \PPLSDK\StructType\CreateOrdersResponse|bool
     */
    public function CreateOrders(\PPLSDK\StructType\CreateOrders $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('CreateOrders', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named CreatePickupOrders
     * Meta information extracted from the WSDL
     * - documentation: Vytvoří objednávky svozu
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\CreatePickupOrders $parameters
     * @return \PPLSDK\StructType\CreatePickupOrdersResponse|bool
     */
    public function CreatePickupOrders(\PPLSDK\StructType\CreatePickupOrders $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('CreatePickupOrders', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \PPLSDK\StructType\CreateOrdersResponse|\PPLSDK\StructType\CreatePackageLabelResponse|\PPLSDK\StructType\CreatePackagesResponse|\PPLSDK\StructType\CreatePickupOrdersResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
