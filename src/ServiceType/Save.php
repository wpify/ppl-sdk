<?php

namespace PPLSDK\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Save ServiceType
 * @subpackage Services
 */
class Save extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SavePartners
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\SavePartners $parameters
     * @return \PPLSDK\StructType\SavePartnersResponse|bool
     */
    public function SavePartners(\PPLSDK\StructType\SavePartners $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('SavePartners', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named SaveProductTransports
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\SaveProductTransports $parameters
     * @return \PPLSDK\StructType\SaveProductTransportsResponse|bool
     */
    public function SaveProductTransports(\PPLSDK\StructType\SaveProductTransports $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('SaveProductTransports', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \PPLSDK\StructType\SavePartnersResponse|\PPLSDK\StructType\SaveProductTransportsResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
