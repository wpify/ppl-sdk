<?php

namespace PPLSDK\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Is ServiceType
 * @subpackage Services
 */
class Is extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named IsHealtly
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\IsHealtly $parameters
     * @return \PPLSDK\StructType\IsHealtlyResponse|bool
     */
    public function IsHealtly(\PPLSDK\StructType\IsHealtly $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('IsHealtly', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \PPLSDK\StructType\IsHealtlyResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
