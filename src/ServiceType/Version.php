<?php

namespace PPLSDK\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Version ServiceType
 * @subpackage Services
 */
class Version extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Version
     * Meta information extracted from the WSDL
     * - documentation: Verze služby
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\Version $parameters
     * @return \PPLSDK\StructType\VersionResponse|bool
     */
    public function Version(\PPLSDK\StructType\Version $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Version', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \PPLSDK\StructType\VersionResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
