<?php

namespace PPLSDK\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Get ServiceType
 * @subpackage Services
 */
class Get extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named GetCplResult
     * Meta information extracted from the WSDL
     * - documentation: Stav zpracovani requestu
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\GetCplResult $parameters
     * @return \PPLSDK\StructType\GetCplResultResponse|bool
     */
    public function GetCplResult(\PPLSDK\StructType\GetCplResult $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('GetCplResult', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetPackages
     * Meta information extracted from the WSDL
     * - documentation: Vrátí seznam zásilek dle zadaného filtru
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\GetPackages $parameters
     * @return \PPLSDK\StructType\GetPackagesResponse|bool
     */
    public function GetPackages(\PPLSDK\StructType\GetPackages $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('GetPackages', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCitiesRouting
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\GetCitiesRouting $parameters
     * @return \PPLSDK\StructType\GetCitiesRoutingResponse|bool
     */
    public function GetCitiesRouting(\PPLSDK\StructType\GetCitiesRouting $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('GetCitiesRouting', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetParcelShops
     * Meta information extracted from the WSDL
     * - documentation: Vrátí seznam ParcelShopů
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\GetParcelShops $parameters
     * @return \PPLSDK\StructType\GetParcelShopsResponse|bool
     */
    public function GetParcelShops(\PPLSDK\StructType\GetParcelShops $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('GetParcelShops', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCodCurrency
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\GetCodCurrency $parameters
     * @return \PPLSDK\StructType\GetCodCurrencyResponse|bool
     */
    public function GetCodCurrency(\PPLSDK\StructType\GetCodCurrency $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('GetCodCurrency', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetSprintRoutes
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\GetSprintRoutes $parameters
     * @return \PPLSDK\StructType\GetSprintRoutesResponse|bool
     */
    public function GetSprintRoutes(\PPLSDK\StructType\GetSprintRoutes $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('GetSprintRoutes', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetProductCountry
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\GetProductCountry $parameters
     * @return \PPLSDK\StructType\GetProductCountryResponse|bool
     */
    public function GetProductCountry(\PPLSDK\StructType\GetProductCountry $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('GetProductCountry', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetPackProducts
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\GetPackProducts $parameters
     * @return \PPLSDK\StructType\GetPackProductsResponse|bool
     */
    public function GetPackProducts(\PPLSDK\StructType\GetPackProducts $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('GetPackProducts', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetPackNumberRowTypes
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\GetPackNumberRowTypes $parameters
     * @return \PPLSDK\StructType\GetPackNumberRowTypesResponse|bool
     */
    public function GetPackNumberRowTypes(\PPLSDK\StructType\GetPackNumberRowTypes $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('GetPackNumberRowTypes', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetNumberRange
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\GetNumberRange $parameters
     * @return \PPLSDK\StructType\GetNumberRangeResponse|bool
     */
    public function GetNumberRange(\PPLSDK\StructType\GetNumberRange $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('GetNumberRange', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetFreeRangeInfo
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\GetFreeRangeInfo $parameters
     * @return \PPLSDK\StructType\GetFreeRangeInfoResponse|bool
     */
    public function GetFreeRangeInfo(\PPLSDK\StructType\GetFreeRangeInfo $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('GetFreeRangeInfo', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \PPLSDK\StructType\GetCitiesRoutingResponse|\PPLSDK\StructType\GetCodCurrencyResponse|\PPLSDK\StructType\GetCplResultResponse|\PPLSDK\StructType\GetFreeRangeInfoResponse|\PPLSDK\StructType\GetNumberRangeResponse|\PPLSDK\StructType\GetPackagesResponse|\PPLSDK\StructType\GetPackNumberRowTypesResponse|\PPLSDK\StructType\GetPackProductsResponse|\PPLSDK\StructType\GetParcelShopsResponse|\PPLSDK\StructType\GetProductCountryResponse|\PPLSDK\StructType\GetSprintRoutesResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
