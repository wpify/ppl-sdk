<?php

namespace PPLSDK\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Update ServiceType
 * @subpackage Services
 */
class Update extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named UpdatePackage
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PPLSDK\StructType\UpdatePackage $parameters
     * @return \PPLSDK\StructType\UpdatePackageResponse|bool
     */
    public function UpdatePackage(\PPLSDK\StructType\UpdatePackage $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('UpdatePackage', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \PPLSDK\StructType\UpdatePackageResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
